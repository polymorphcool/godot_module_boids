# BOIDS

module to create boids in [godot](http://godotengine.org) engine

this module provides an accelerated computation of the boids (c++) and uses particles to render the meshes

this is a core module, and therefore requires engine recompilation

## demo

![](https://polymorph.cool/wp-content/uploads/2021/03/editor_screenshot_2021-03-20T0250400100.png)

you can download a demo for linux & windows on [itch.io](https://frankiezafe.itch.io/boids)

code of the project on [gitlab](https://gitlab.com/polymorphcool/boids)

## concepts

There are 3 main concepts in this module:

- domain;
- boid;
- modifier.

### domain

A domain is a 3d grid defining the space where boids will be processed. It has a number of cells in XYZ axis and 
a cell size, witch together defines the total volume of the domain.

The domain can also deal with what's happening at its frontiers. When outside or close to the edge, you can define
these behaviour:

- none: boids ignore the fact its outside the domain;
- bounce: boids instantly change direction;
- push: when closer then 'domain_border_radius', boids are pushed away;
- attract: boids are pulled towards the domain as soon as they leave it (ideal for flying boids);
- respawn: boids are reset to a new position in the domain;
- teleport: boids are teleported on the other side of the domain as soon as they leave it;
- deactivate: boids are deactivated as soon they leave the domain.

You can define how many planes (~axis) of freedom boids are allowed to use in the domain:

- all: boids are free to move in all directions;
- floor: boids are bounded to XZ plane.

The domain has a gravity vector, representing a force applied constantly on every boids.

### boid

In itself, a boid is an autonoumous agent that moves, stears and is aware of its close environment.
Boids can only see at a distance of 1 cell.

By default, boids reacts to their environment using 3 variables:

- separation: avoiding neighbours;
- alignment: align its direction on the median neighbours' direction;
- cohesion: going towards the barycentre of its neighbours.

Separation and cohesion are 2 opposite forces.

When an area becomes crowdy, boids performs collision tests with their neighbours to avoid overlapping.
To simplify computation, boid's collider is a sphere of wich you can set radius. Depending on their 
weight, they will react or not to gravity.

### modifier

Modifier is a abstract class of objects intended to interact with boids.
They have to be registrated in the domain to have ann effect.

They are of 4 types:

- source: create new boids;
- target: attracts boids;
- collider: defines a volume forbidden to boids;
- attractor: you can see them as wind or balckholes.

Only 3 shapes are allowed for modifiers:

- none: the modifier is a point in space;
- box: a parallelipiped;
- sphere: you guessed it, a sphere.

#### source

a source creates (recycles) boids randomly in its volume

**variables of sources**

*source_dir*: direction of boids at creation

- random: random direction;
- radial: opposite to distance to source center;
- right (+X): along the local X axis;
- left (-X): opposite to local X axis;
- up (+Y): along the local Y axis;
- down (-X): opposite to local Y axis;
- forward (+Z): along the local Z axis;
- backward (-Z): opposite to local Z axis.

*rate*: amount of boids per second is called 

*automatic_target*: boid will receive a target automatically, depending 
on the number of targets registered in boids class

#### target

a target attracts boids towards a random position in its volume

**variables of targets**

*strength*: at each pass, strength multiplies the normalized vector 
towards the target position

*on_enter*: defines what is happening when the boid enter the target
space

- nothing: boid will keep aiming for the position;
- free: boid stops aiming for the position;
- stop: boid stop moving;
- deactivate: boid desappears and is available for recycling.

## installation

clone [godot repository](https://github.com/godotengine/godot/)

```
$ git clone https://github.com/godotengine/godot.git godot_3x
$ cd godot_3x
```

switch to branch 3.2

`$ git checkout 3.2 origin/3.2`

clone this repository in modules

```
$ cd modules
$ git clone git@gitlab.com:polymorphcool/godot_module_boids.git boids
```

and recompile the engine

for linux:
`$ scons platform=x11`

