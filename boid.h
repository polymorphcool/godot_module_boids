/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid.h
 *
 *  Created on: Apr 7, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_BOIDS_BOID_H_
#define MODULES_BOIDS_BOID_H_

#include <string>
#include <algorithm>
#include "core/color.h"
#include "core/math/transform.h"
#include "scene/main/node.h"
#include "boid_common.h"

class Boid : public Object {

private:
	GDCLASS(Boid, Object); // @suppress("Symbol is not resolved")

protected:
	static void _bind_methods();

public:

	// all types of boid control
	// - NONE: object is a boid_control and has no action on boids
	// - SOURCE: spawn boids in domain
	// - TARGET: attracts boids towards them, always visible by boids
	// - COLLIDER: 'rigid bodies' in domain
	// - ATTRACTOR: attracts towards them or pushes boid away from them, only visible when in close neighborhood
	enum ControlType {
		BCTRL_TYPE_NONE,
		BCTRL_TYPE_SOURCE,
		BCTRL_TYPE_TARGET,
		BCTRL_TYPE_COLLIDER,
		BCTRL_TYPE_ATTRACTOR
	};

	// valid types of boid control shapes, used for collisions and influence
	enum ControlShape {
		BCTRL_SHAPE_POINT,
		BCTRL_SHAPE_BOX,
		BCTRL_SHAPE_SPHERE
	};

	enum ControlDirection {
		BCTRL_DIRECTION_RANDOM,
		BCTRL_DIRECTION_RADIAL,
		BCTRL_DIRECTION_X,
		BCTRL_DIRECTION_Xi,
		BCTRL_DIRECTION_Y,
		BCTRL_DIRECTION_Yi,
		BCTRL_DIRECTION_Z,
		BCTRL_DIRECTION_Zi
	};

	// how a boid control influences boids far from its center
	// - NONE: constant strength as soon boid enters control space
	// - LINEAR: strength is multiplied by 1 - distance/radius
	// - SQRT: square root, (1 - mult)²
	// - SQRTi: square root inverted, 1 - mult²
	enum ControlFalloff {
		BCTRL_FALLOFF_NONE,
		BCTRL_FALLOFF_LINEAR,
		BCTRL_FALLOFF_SQRT,
		BCTRL_FALLOFF_SQRTi
	};

	//// how the source select the target of boid
	//// will be replaced by behaviours
	//enum BoidControlSourceTarget {
	//	BCTRL_SOURCETARGET_NONE,
	//	BCTRL_SOURCETARGET_RANDOM_CHILDREN,
	//	BCTRL_SOURCETARGET_CLOSEST_CHILD,
	//	BCTRL_SOURCETARGET_RANDOM_ALL,
	//	BCTRL_SOURCETARGET_CLOSEST_ALL
	//};

	// what the boid control do when a boid enters its space
	// - NOTHING: nothing happens
	// - FREE: set boid target to NULL
	// - STOP: speed of boid decrease to 0
	// - DEACTIVATE: boid is flagged as inactive and ready to be recycled
	// will be replaced by behaviours
	enum ControlOnEnter {
		BCTRL_ONENTER_NOTHING,
		BCTRL_ONENTER_FREE,
		BCTRL_ONENTER_STOP,
		BCTRL_ONENTER_DEACTIVATE
	};

	// reaction of domain when boid reach the borders:
	// - NONE: do nothing
	// - BOUNCE: aggressive relocation and retargeting of boid
	// - PUSH: aggressive relocation but soft influence on direction
	// - ATTRACT: boid pulled once they leave domain (ideal for flying boids)
	// - RESPAWN: random relocation of boid
	// - TELEPORT: boid is teleport to the opposite side of domain
	// - DEACTIVATE: boid is flagged as inactive and ready to be recycled
	enum DomainBorder {
		BORDER_NONE,
		BORDER_BOUNCE,
		BORDER_PUSH,
		BORDER_ATTRACT,
		BORDER_RESPAWN,
		BORDER_TELEPORT,
		BORDER_DEACTIVATE
	};

	// degrees of liberty in domain, XYZ (ALL) or XZ (FLOOR)
	enum DomainPlanes {
		DOMAIN_PLANES_ALL,
		DOMAIN_PLANES_FLOOR
	};

	// type of boids, influence the management in boid system
	enum Type {
		BTYPE_NONE,
		BTYPE_STATIC,
		BTYPE_DYNAMIC,
		BTYPE_BOID
	};

	// type of boid actions, action parameters depends on this
	enum ActionType {
		BACTION_TYPE_NONE = 				0,
		BACTION_TYPE_SET_ATTRIBUTE = 		1,
		BACTION_TYPE_INCREMENT_ATTRIBUTE = 	2,
		BACTION_TYPE_RANDOMISE_ATTRIBUTE = 	3,
		BACTION_TYPE_ASSIGN_TARGET = 		4,
		BACTION_TYPE_AIM_AT = 				5,
		BACTION_TYPE_REACH = 				6,
		BACTION_TYPE_ESCAPE = 				7,
		BACTION_TYPE_COUPLE = 				8,
		BACTION_TYPE_LEAVE = 				9,
		BACTION_TYPE_SIGNAL = 				10
	};

	// list of boid attributes usable in action typed "BACTION_TYPE_SET_ATTRIBUTE"
	enum ActionAttribute {
		BACTION_ATTR_NONE = 				0,
		BACTION_ATTR_ACTIVE = 				1,		// boolean
		BACTION_ATTR_COLLISION = 			2,		// boolean
		BACTION_ATTR_GROUP = 				3,		// bits
		BACTION_ATTR_ANIMATION = 			4,		// bits
		BACTION_ATTR_SEPARATION = 			5,		// float
		BACTION_ATTR_ALIGNMENT = 			6,		// float
		BACTION_ATTR_COHESION = 			7,		// float
		BACTION_ATTR_STEER = 				8,		// float
		BACTION_ATTR_SPEED = 				9,		// float
		BACTION_ATTR_INERTIA = 				10,		// float
		BACTION_ATTR_WEIGHT = 				11,		// float
		BACTION_ATTR_RADIUS = 				12,		// float
		BACTION_ATTR_FRICTION = 			13,		// float
		BACTION_ATTR_COLOR = 				14,		// vec4
		BACTION_ATTR_AIM = 					15,		// vec3
		BACTION_ATTR_COUNTER = 				16,		// int
		BACTION_ATTR_TIMER = 				17,		// float
		BACTION_ATTR_LIFETIME = 			18,		// float
		BACTION_ATTR_POSITION = 			19,		// vec3
		BACTION_ATTR_DIRECTION = 			20,		// vec3
		BACTION_ATTR_FRONT = 				21,		// vec3
		BACTION_ATTR_PUSH = 				22,		// vec3
		BACTION_ATTR_PUSH_SPEED = 			23		// float
	};

	// type of boid test, action parameters depends on this
	enum TestType {
		BTEST_TYPE_NONE = 				0,
		BTEST_TYPE_ALWAYS_TRUE = 		1,
		BTEST_TYPE_ATTRIBUTE = 			2,
		BTEST_TYPE_RANDOM = 			3,
		BTEST_TYPE_NEIGHBORHOUD = 		4,
		BTEST_TYPE_INSIDE = 			5,
		BTEST_TYPE_OUTSIDE = 			6,
		BTEST_TYPE_EVENT = 				7,
		BTEST_TYPE_NEIGHBOR_COUNT = 	8
	};

	// list of boid attributes usable in test typed "BTEST_TYPE_ATTRIBUTE"
	enum TestAttribute {
		BTEST_ATTR_NONE = 				0,
		BTEST_ATTR_ACTIVE = 			1,
		BTEST_ATTR_COLLISION = 			2,
		BTEST_ATTR_MOVING = 			3,
		BTEST_ATTR_GROUP = 				4,
		BTEST_ATTR_ANIMATION = 			5,
		BTEST_ATTR_SPEED = 				6,
		BTEST_ATTR_WEIGHT = 			7,
		BTEST_ATTR_RADIUS = 			8,
		BTEST_ATTR_PUSH = 				9,
		BTEST_ATTR_COUNTER = 			10,
		BTEST_ATTR_TIMER = 				11,
		BTEST_ATTR_LIFETIME = 			12,
		BTEST_ATTR_PUSH_SPEED = 		13
	};

	// list of boid events usable in test typed "BTEST_TYPE_EVENT"
	enum TestEvent {
		BTEST_EVENT_NONE = 				0,
		BTEST_EVENT_BORN = 				1,
		BTEST_EVENT_TIMER_END = 		2,
		BTEST_EVENT_MOVING_START = 		3,
		BTEST_EVENT_MOVING_STOP = 		4
	};

	// defines what the test will do after completing its action
	enum TestAfter {
		BTEST_AFTER_NONE = 				0,
		BTEST_AFTER_EXIT = 				1,
		BTEST_AFTER_AND = 				2,
		BTEST_AFTER_OR = 				3,
		BTEST_AFTER_GOTO = 				4
	};

	// defines what how many times the test will be done
	enum TestRepeat {
		BTEST_REPEAT_CONTINUOUS = 		0,
		BTEST_REPEAT_ONCE = 			1
	};

	// different possibilities to say yes or no, must be linked to the right parameters
	enum TestEval {
		BTEST_EVAL_NONE = 				0,
		BTEST_EVAL_EQUAL = 				1,
		BTEST_EVAL_NOT_EQUAL = 			2,
		BTEST_EVAL_GREATER = 			3,
		BTEST_EVAL_GREATER_EQUAL = 		4,
		BTEST_EVAL_LESSER = 			5,
		BTEST_EVAL_LESSER_EQUAL = 		6,
		BTEST_EVAL_MODULO = 			7,
		BTEST_EVAL_BITWISE_AND = 		8,
		BTEST_EVAL_BITWISE_XOR = 		9
	};

	// parameters required by tests & actions for all uint params of boids (group)
	typedef struct boid_param_int {
		bool initialised;
		int value;
		bool clamped;
		int min;
		int max;
	} boid_param_uint;

	// parameters required by tests & actions for all float params of boids
	typedef struct boid_param_float {
		bool initialised;
		float value;
		bool clamped;
		float min;
		float max;
	} boid_param_float;

	// parameters required by tests & actions for all vector3 params of boids
	typedef struct boid_param_vec3 {

		bool initialised;
		Vector3 value;
		bool clamped;
		Vector3 min;
		Vector3 max;

	} boid_param_vec3;

	// parameters required by tests & actions for all vector4 params of boids (color)
	typedef struct boid_param_vec4 {
		bool initialised;
		Color value;
		bool clamped;
		Color min;
		Color max;
	} boid_param_vec4;

	// parameters required by tests & actions to work with controls
	struct boid_control_data;
	typedef struct boid_param_control {
		bool initialised;
		boid_control_data* value;	// set by boid system
		int control_UID; 			// @suppress("Type cannot be resolved")
	} boid_param_control;

	// describes a boid action and points to next action if defined
	typedef struct boid_behaviour_action {

		ActionType type;
		ActionAttribute attr;
		bool active;
		bool valid;

		// params
		bool param_bool;
		boid_param_int param_int;
		boid_param_float param_float;
		boid_param_vec3 param_vec3;
		boid_param_vec4 param_vec4;
		boid_param_control param_ctrl;

		// links
		boid_behaviour_action* next;
		std::string msg;

	} boid_behaviour_action;

	// describes a boid test and points to next test if defined
	typedef struct boid_behaviour_test {

		TestType type;
		TestAttribute attr;
		TestEvent event;
		TestAfter after;
		TestRepeat repeat;
		TestEval eval;
		bool active;
		bool valid;
		int jumpto;

		// params
		bool param_bool;
		boid_param_int param_int;
		boid_param_float param_float;
		boid_param_vec3 param_vec3;
		boid_param_vec4 param_vec4;
		boid_param_control param_ctrl;

		// links
		boid_behaviour_action* action;
		boid_behaviour_test* next;
		std::string msg;

	} boid_behaviour_test;

	// provides an easy communication struct to edit behaviour
	typedef struct boid_behaviour_item {

		boid_behaviour_test* test;
		boid_behaviour_action* action;

	} boid_behaviour_item;

	// stores all tests and actions of a boid, used by boid system during solve pass
	typedef struct boid_behaviour {

		bool active;
		boid_behaviour_test* test;				// pointer to first test
		void* owner;
		int test_count;
		boid_behaviour_test** tests;			// stores sequentially all tests
		int action_count;
		boid_behaviour_action** actions;		// stores sequentially all actions

	} boid_behaviour;

	// used to pass data from modifiers to boids
	typedef struct boid_control_data {

		// crucial for behaviours
		int UID;								// unique ID of a control, managed by boid system
		String name;							// used in behaviour editor
		// common to all controls
		ControlType type;						// none, source, target, attractor || collider
		void* owner;							// pointer to control managing this boid_control_data
		bool active;							// active or not
		uint32_t group;							// group flags // @suppress("Type cannot be resolved")
		ControlShape shape;						// point, box || sphere
		int subdivision;						// only used for box shapes
		bool is_static;							// if true, modifier is registered in domain and must not move
		Transform global;						// global transform
		Transform local;						// global > boids space, generated by boids
		Transform local_inv;					// boids space > modifier space, generated by boids
		Vector3 dimension;						// size of modifier in XYZ
		float volume;							// total volume
		void* mesh;								// pointer to a boid_mesh
		bool regenerate_domain;					// important to set to true only when necessary: avoid unusefull reprocessing of the domain
		// source & attractor
		ControlDirection direction;				// how to orient boids emitted
		// attractor only
		ControlFalloff falloff;					// describe how attraction power decrease with distance
		// source only
		float emission_rate;					// boid/second
		// source only
		float elapsed_time;						// will be used by parent boids
		// source only
		boid_behaviour behaviour;				// will be set by system
		// target only
		ControlOnEnter on_enter;

		// collider, target & attractor
		float force;

		// attractor only
		float pull;

		// collider only
		bool influence_direction;
		bool influence_front;
		float weight;
		bool ground;
		bool margin;

	} boid_control_data;

	// controls grouped by BoidControlType, can be used for other purposes
	typedef struct boid_control_group {
		boid_control_data** list;
		int count;
	} boid_control_group;

	// used by boid behaviour editor to generate dropdown lists of controllers
	typedef struct boid_control_info {
		int UID;
		String name;
		ControlType type;
	} boid_control_info;

	// contains all required fields of each boid
	typedef struct boid {

		int UID;								// value set by boid system, to interact with boid from script

		Type type;								// normal boid, static || dynamic (used to store fake boids in the grid)
		void* owner;							// pointer to object that requested the creation of this boid
		bool active;							// flag to activate or desactivate a boid
		bool lock;								// flag prevent boid to be updated by system
		bool collision;							// flag to activate or desactivate collisions
		uint32_t group;							// group flags // @suppress("Type cannot be resolved")

		uint32_t reach_group;					// move forward to any boid from group flagged // @suppress("Type cannot be resolved")
		float reach_strength;					// influence of reach in boid computation
		bool reach_group_center;				// use group center if no boid o reach_group is visible
		uint32_t escape_group;					// move away from any boid from group flagged // @suppress("Type cannot be resolved")
		float escape_strength;					// influence of escape in boid computation
		bool escape_group_center;				// use group center if no boid o reach_group is visible
		uint32_t aim_at_group;					// @suppress("Type cannot be resolved")
		float aim_at_strength;
		float aim_at_group_center;

		uint32_t animation;						// animation flags // @suppress("Type cannot be resolved")
		DomainPlanes plane;						// freedom of the boid
		Vector3 domain_pos;						// position in domain space (used for computation)
		Vector3 local_pos;						// position in local space (used for display)
		Vector3 previous_pos;					// stores previous local_pos
		Vector3 motion;							// difference of previous and current pos
		Vector3 dir;							// direction boid must go normalized
		Vector3 front;							// direction boid is actually aiming at, default @1
		Vector3 aim;							// only used if aim_at_group is not 0 or aiming is at true
		Vector3 prev_front;						// previous front vector or aim, if aiming
		Vector3 push;							// translation to apply on position to avoid collision
		Basis mat3x3;							// 3x3 matrix representing the local orientation of the boid
		float inertia;							// affect speed change, default is 1
		float speed;							// speed requested, default is 1
		float push_speed;						// push speed, default is 1
		float internal_speed;					// speed used when moving, default is 0
		float internal_push_speed;				// speed used when pushing, default is 0
		float weight;							// reaction to gravity, default is 1
		float radius;							// used to compute collisions, default @1
		float steer;							// reactivity to direction change (higher=faster)
		float friction;							// removes push force (higher=less force), default 0.05
		int index[3];							// index of cell (row,column,depth)
		uint32_t cellid;						// index of cell in the grid // @suppress("Type cannot be resolved")
		boid* next;								// link to next boid in the same cell
		boid_control_data* control;				// pointer to control data, for BTYPE_BOID it represent the target to reach, for static & dynamic the source modifier
		Vector3 target_offset;					// target offset in global space
		Vector3 sac_weights;					// separation, alignement & cohesion weights
		const boid_behaviour* behaviour;		// will be set by system
		float travel_distance;					// total travel distance
		float lifetime;							// boid local time
		float timer;							// used in behaviour
		float counter;							// used in behaviour
		float prev_counter;						// used in behaviour, contains last counter value
		Color color;
		bool touch_ground;						// true if boid is touching a collider with flag ground
		bool aiming;							// indicate if aim vector is updated
		bool timer_end;							// event time_end
		bool moving;							// motion flag
		bool moving_start;						// event start moving
		bool moving_stop;						// event end moving
		float ground_rotation;					// radians/sec if boidsystem'plane is DOMAIN_PLANES_FLOOR

		// behaviour specific
		int test_count;
		bool * tests;
		int neighbor_count;
		bool event_enabled;
		int event;

	} boid;

	// interactions between boidmesh and boid system
	typedef struct boid_set {

		void* owner;							// pointer to BoidMesh
		uint32_t group;							// group flags // @suppress("Type cannot be resolved")
		int count;								// current boids total in set (adjusted by boid mesh)
		int active;								// current active boids count in set (computed by boid system)
		boid** boids;							// array of boid pointers
		Vector3 min;							// AABB minimum
		Vector3 max;							// AABB maximum

	} boid_set;

	// info about boid groups, max 20
	typedef struct boid_group {

		int count;
		Vector3 center;

	} boid_group;


	Boid();
	virtual ~Boid();

};

VARIANT_ENUM_CAST(Boid::DomainBorder)
VARIANT_ENUM_CAST(Boid::DomainPlanes)

VARIANT_ENUM_CAST(Boid::ControlShape)
VARIANT_ENUM_CAST(Boid::ControlDirection)
VARIANT_ENUM_CAST(Boid::ControlFalloff)
VARIANT_ENUM_CAST(Boid::ControlOnEnter)

VARIANT_ENUM_CAST(Boid::ActionType)
VARIANT_ENUM_CAST(Boid::ActionAttribute)
VARIANT_ENUM_CAST(Boid::TestType)
VARIANT_ENUM_CAST(Boid::TestAttribute)
VARIANT_ENUM_CAST(Boid::TestEvent)
VARIANT_ENUM_CAST(Boid::TestAfter)
VARIANT_ENUM_CAST(Boid::TestRepeat)
VARIANT_ENUM_CAST(Boid::TestEval)

#endif /* MODULES_BOIDS_BOID_H_ */
