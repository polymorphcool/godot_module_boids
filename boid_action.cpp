/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_action.cpp
 *
 *  Created on: Apr 6, 2021
 *      Author: frankiezafe
 */

#include "boid_action.h"

const Boid::boid_behaviour_action* BoidAction::get_data() {
	return &data;
}

void BoidAction::set_type( Boid::ActionType p_type ) {
	data.type = p_type;
	behaviour.validate_action(&data);
}

Boid::ActionType BoidAction::get_type() const {
	return data.type;
}

void BoidAction::set_attribute( Boid::ActionAttribute p_attr ) {
	data.attr = p_attr;
	behaviour.validate_action(&data);
}

Boid::ActionAttribute BoidAction::get_attribute() const {
	return data.attr;
}

void BoidAction::set_active( const bool& p_active ) {
	data.active = p_active;
}

const bool& BoidAction::is_active() const {
	return data.active;
}

void BoidAction::set_bool( const bool& p_bool ) {
	data.param_bool = p_bool;
}

const bool& BoidAction::get_bool() const {
	return data.param_bool;
}

void BoidAction::set_int( const int& p_int ) {
	data.param_int.value = p_int;
	behaviour.validate_action(&data);
}

int BoidAction::get_int() const {
	if ( !data.param_int.initialised ) {
		WARN_PRINT( "boid action parameter int is not initialised correctly!" );
		return 0;
	}
	return data.param_int.value;
}

void BoidAction::set_float( const float& p_float ) {
	data.param_float.value = p_float;
	behaviour.validate_action(&data);
}

float BoidAction::get_float() const {
	if ( !data.param_float.initialised ) {
		WARN_PRINT( "boid action parameter float is not initialised correctly!" );
		return 0;
	}
	return data.param_float.value;
}

void BoidAction::set_vec3( const Vector3& p_vec3 ) {
	data.param_vec3.value = p_vec3;
	behaviour.validate_action(&data);
}

Vector3 BoidAction::get_vec3() const {
	if ( !data.param_vec3.initialised ) {
		WARN_PRINT( "boid action parameter vec3 is not initialised correctly!" );
		return Vector3(0,0,0);
	}
	return data.param_vec3.value;
}

void BoidAction::set_vec4( const Color& p_vec4 ) {
	data.param_vec4.value = p_vec4;
	behaviour.validate_action(&data);
}

Color BoidAction::get_vec4() const {
	if ( !data.param_vec3.initialised ) {
		WARN_PRINT( "boid action parameter vec4 is not initialised correctly!" );
		return Color(0,0,0,0);
	}
	return data.param_vec4.value;
}

void BoidAction::_bind_methods() {

	ClassDB::bind_method(D_METHOD("set_type", "type"), &BoidAction::set_type);
	ClassDB::bind_method(D_METHOD("get_type"), &BoidAction::get_type);

	ClassDB::bind_method(D_METHOD("set_attribute", "attribute"), &BoidAction::set_attribute);
	ClassDB::bind_method(D_METHOD("get_attribute"), &BoidAction::get_attribute);

	ClassDB::bind_method(D_METHOD("set_active", "active"), &BoidAction::set_active);
	ClassDB::bind_method(D_METHOD("is_active"), &BoidAction::is_active);

	ClassDB::bind_method(D_METHOD("set_bool", "p_bool"), &BoidAction::set_bool);
	ClassDB::bind_method(D_METHOD("get_bool"), &BoidAction::get_bool);

	ClassDB::bind_method(D_METHOD("set_int", "p_int"), &BoidAction::set_int);
	ClassDB::bind_method(D_METHOD("get_int"), &BoidAction::get_int);

	ClassDB::bind_method(D_METHOD("set_float", "p_float"), &BoidAction::set_float);
	ClassDB::bind_method(D_METHOD("get_float"), &BoidAction::get_int);

	ClassDB::bind_method(D_METHOD("set_vec3", "p_vec3"), &BoidAction::set_vec3);
	ClassDB::bind_method(D_METHOD("get_vec3"), &BoidAction::get_int);

	ClassDB::bind_method(D_METHOD("set_vec4", "p_vec4"), &BoidAction::set_vec4);
	ClassDB::bind_method(D_METHOD("get_vec4"), &BoidAction::get_vec4);

}

BoidAction::BoidAction() {
	BoidBehaviour::init_action(&data);
}

BoidAction::~BoidAction() {
	BoidBehaviour::purge_action(&data);
}

