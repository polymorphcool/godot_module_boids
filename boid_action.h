/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_action.h
 *
 *  Created on: Apr 6, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_BOIDS_BOID_ACTION_H_
#define MODULES_BOIDS_BOID_ACTION_H_

#include "scene/main/node.h"
#include "boid_common.h"
#include "boid_behaviour.h"

class BoidAction : public Object {

private:
	GDCLASS(BoidAction, Object); // @suppress("Symbol is not resolved")

	BoidBehaviour behaviour;
	Boid::boid_behaviour_action data;

protected:
	static void _bind_methods();

public:

	// accessible in cpp only
	const Boid::boid_behaviour_action* get_data();

	// setters & getters
	void set_type( Boid::ActionType p_type );
	Boid::ActionType get_type() const;

	void set_attribute( Boid::ActionAttribute p_attr );
	Boid::ActionAttribute get_attribute() const;

	void set_active( const bool& p_active );
	const bool& is_active() const;

	void set_bool( const bool& p_bool );
	const bool& get_bool() const;

	void set_int( const int& p_int );
	int get_int() const;

	void set_float( const float& p_float );
	float get_float() const;

	void set_vec3( const Vector3& p_vec3 );
	Vector3 get_vec3() const;

	void set_vec4( const Color& p_vec4 );
	Color get_vec4() const;

	BoidAction();
	virtual ~BoidAction();

};

#endif /* MODULES_BOIDS_BOID_ACTION_H_ */
