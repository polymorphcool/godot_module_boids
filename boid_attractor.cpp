/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_attractor.cpp
 *
 *  Created on: Feb 9, 2021
 *      Author: frankiezafe
 */

#include "boid_attractor.h"

void BoidAttractor::set_falloff( Boid::ControlFalloff p_falloff ) {
	data.falloff = p_falloff;
}

Boid::ControlFalloff BoidAttractor::get_falloff() const {
	return data.falloff;
}

void BoidAttractor::set_direction( Boid::ControlDirection p_dir ) {
	dir = p_dir;
	data.direction = dir;
}

Boid::ControlDirection BoidAttractor::get_direction() const {
	return dir;
}

void BoidAttractor::set_strength_dir( float p_strength ) {
	data.force = p_strength;
}

const float& BoidAttractor::get_strength_dir() const {
	return data.force;
}

void BoidAttractor::set_strength_pull( float p_pull ) {
	data.pull = p_pull;
}

const float& BoidAttractor::get_strength_pull() const {
	return data.pull;
}

void BoidAttractor::_bind_methods() {

	ClassDB::bind_method(D_METHOD("set_direction", "direction"), &BoidAttractor::set_direction);
	ClassDB::bind_method(D_METHOD("get_direction"), &BoidAttractor::get_direction);

	ClassDB::bind_method(D_METHOD("set_strength_dir", "strength_direction"), &BoidAttractor::set_strength_dir);
	ClassDB::bind_method(D_METHOD("get_strength_dir"), &BoidAttractor::get_strength_dir);

	ClassDB::bind_method(D_METHOD("set_strength_pull", "strength_pull"), &BoidAttractor::set_strength_pull);
	ClassDB::bind_method(D_METHOD("get_strength_pull"), &BoidAttractor::get_strength_pull);

	ClassDB::bind_method(D_METHOD("set_falloff", "falloff"), &BoidAttractor::set_falloff);
	ClassDB::bind_method(D_METHOD("get_falloff"), &BoidAttractor::get_falloff);

	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "direction", PROPERTY_HINT_ENUM,
					"random, radial, right (X), left (-X), up (Y), down (-Y), forward (Z), backward (-Z)"), "set_direction",
			"get_direction");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "falloff", PROPERTY_HINT_ENUM,
					"none, linear, square root, 1 - square root"), "set_falloff",
			"get_falloff");
	ADD_PROPERTY(
			PropertyInfo(Variant::REAL, "strength_direction", PROPERTY_HINT_RANGE,
					"-10,10,0.001,or_greater,or_lesser"), "set_strength_dir", "get_strength_dir");
	ADD_PROPERTY(
			PropertyInfo(Variant::REAL, "strength_pull", PROPERTY_HINT_RANGE,
					"-10,10,0.001,or_greater,or_lesser"), "set_strength_pull", "get_strength_pull");

}

BoidAttractor::BoidAttractor() {
	data.owner = this;
	data.type = Boid::ControlType::BCTRL_TYPE_ATTRACTOR;
	data.group = 31775; // [0,4] + [10,14]
	data.force = 1;
}

BoidAttractor::~BoidAttractor() {
}
