/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_behaviour.cpp
 *
 *  Created on: Mar 16, 2021
 *      Author: frankiezafe
 */
 
#include "boid_behaviour.h"

void BoidBehaviour::get_behaviour_enums( HashMap<String,String>& map ) {

	map["test_type"] =
			"none,"
			"always true,"
			"attribute,"
			"random,"
			"neighborhoud,"
			"inside,"
			"outside,"
			"event,"
			"neighbor_count";

	map["test_attr"] =
			"none,"
			"active [bool],"
			"collision [bool],"
			"moving [bool],"
			"group [flag],"
			"animation [flag],"
			"speed [float],"
			"weight [float],"
			"radius [float],"
			"push [float],"
			"counter [float],"
			"timer [float],"
			"lifetime [float],"
			"push_speed [float]";

	map["test_after"] =
			"continue,"
			"exit,"
			"and,"
			"or,"
			"goto";

	map["test_repeat"] =
			"always,"
			"once";

	map["test_eval"] =
			"auto,"
			"==,"
			"!=,"
			">,"
			">=,"
			"<,"
			"<=,"
			"%,"
			"& [bitwise],"
			"^ [bitwise]";

	map["test_event"] =
			"none,"
			"born,"
			"timer_end,"
			"moving_start,"
			"moving_stop";

	map["action_type"] =
			"none,"
			"set attribute,"
			"increment attribute,"
			"randomise attribute,"
			"assign target,"
			"aim at,"
			"reach,"
			"escape,"
			"couple,"
			"leave domain,"
			"signal";

	map["action_attr"] =
			"none,"
			"active [bool],"
			"collision [bool],"
			"group [flag],"
			"animation [flag],"
			"separation [float],"
			"alignment [float],"
			"cohesion [float],"
			"steer [float],"
			"speed [float],"
			"inertia [float],"
			"weight [float],"
			"radius [float],"
			"friction [float],"
			"color [v4],"
			"aim [v3],"
			"counter [float],"
			"timer [float],"
			"lifetime [float],"
			"position [v3],"
			"direction [v3],"
			"front [v3],"
			"push [v3],"
			"push_speed [float]";

}

void BoidBehaviour::dump( Boid::boid_behaviour* bb ) {

	if ( bb == NULL ) {
		std::cout << "BoidBehaviour::dump, empty pointer" << std::endl;
		return;
	}

	std::cout << "BoidBehaviour::dump, " << bb << " active:" << bb->active << std::endl;

	Boid::boid_behaviour_test* current_test = bb->test;
	int test_i = 0;
	int action_i = 0;
	while( current_test != NULL ) {

		std::cout << "\t" << "test [" << (test_i++) << "] = ";
		switch( current_test->type ) {
			case Boid::TestType::BTEST_TYPE_NONE:
				std::cout << "type:NONE ";
				break;
			case Boid::TestType::BTEST_TYPE_ALWAYS_TRUE:
				std::cout << "type:TRUE ";
				break;
			case Boid::TestType::BTEST_TYPE_ATTRIBUTE:
				std::cout << "type:ATTR ";
				switch( current_test->attr ) {
					case Boid::TestAttribute::BTEST_ATTR_NONE:
						std::cout << "attr:NONE ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_ACTIVE:
						std::cout << "attr:ACTIVE ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_COLLISION:
						std::cout << "attr:COLLISION ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_MOVING:
						std::cout << "attr:MOVING ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_GROUP:
						std::cout << "attr:GROUP ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_ANIMATION:
						std::cout << "attr:ANIMATION ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_SPEED:
						std::cout << "attr:SPEED ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_PUSH_SPEED:
						std::cout << "attr:PUSH_SPEED ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_WEIGHT:
						std::cout << "attr:WEIGHT ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_RADIUS:
						std::cout << "attr:RADIUS ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_PUSH:
						std::cout << "attr:PUSH ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_COUNTER:
						std::cout << "attr:COUNTER ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_TIMER:
						std::cout << "attr:TIMER ";
						break;
					case Boid::TestAttribute::BTEST_ATTR_LIFETIME:
						std::cout << "attr:TIMER ";
						break;
				}
				break;
			case Boid::TestType::BTEST_TYPE_RANDOM:
				std::cout << "type:RAND ";
				break;
			case Boid::TestType::BTEST_TYPE_NEIGHBORHOUD:
				std::cout << "type:NEIGHBORHOUD ";
				break;
			case Boid::TestType::BTEST_TYPE_INSIDE:
				std::cout << "type:INSIDE ";
				break;
			case Boid::TestType::BTEST_TYPE_OUTSIDE:
				std::cout << "type:OUTSIDE ";
				break;
			case Boid::TestType::BTEST_TYPE_EVENT:
				std::cout << "type:EVENT ";
				switch( current_test->event ) {
					case Boid::TestEvent::BTEST_EVENT_NONE:
						std::cout << "event:NONE ";
						break;
					case Boid::TestEvent::BTEST_EVENT_BORN:
						std::cout << "event:BORN ";
						break;
					case Boid::TestEvent::BTEST_EVENT_TIMER_END:
						std::cout << "event:TIMER ";
						break;
					case Boid::TestEvent::BTEST_EVENT_MOVING_START:
						std::cout << "event:MOVING_START ";
						break;
					case Boid::TestEvent::BTEST_EVENT_MOVING_STOP:
						std::cout << "event:MOVING_STOP ";
						break;
				}
				break;
			case Boid::TestType::BTEST_TYPE_NEIGHBOR_COUNT:
				std::cout << "type:NEIGHBOR_COUNT ";
				break;
			default:
				std::cout << "type:? ";
				break;
		}
		std::cout << std::endl;

		Boid::boid_behaviour_action* current_action = current_test->action;
		while( current_action != NULL ) {
			std::cout << "\t\t" << "action [" << (action_i++) << "] = ";
			switch( current_action->type ) {
				case Boid::ActionType::BACTION_TYPE_NONE:
					std::cout << "type:NONE ";
					break;
				case Boid::ActionType::BACTION_TYPE_SET_ATTRIBUTE:
					std::cout << "type:ATTR ";
					break;
				case Boid::ActionType::BACTION_TYPE_INCREMENT_ATTRIBUTE:
					std::cout << "type:++ ";
					break;
				case Boid::ActionType::BACTION_TYPE_RANDOMISE_ATTRIBUTE:
					std::cout << "type:RANDOM ";
					break;
				case Boid::ActionType::BACTION_TYPE_ASSIGN_TARGET:
					std::cout << "type:TARGET ";
					break;
				case Boid::ActionType::BACTION_TYPE_AIM_AT:
					std::cout << "type:AIM ";
					break;
				case Boid::ActionType::BACTION_TYPE_REACH:
					std::cout << "type:REACH ";
					break;
				case Boid::ActionType::BACTION_TYPE_ESCAPE:
					std::cout << "type:ESC ";
					break;
				case Boid::ActionType::BACTION_TYPE_COUPLE:
					std::cout << "type:CPL ";
					break;
				case Boid::ActionType::BACTION_TYPE_LEAVE:
					std::cout << "type:BYE ";
					break;
				case Boid::ActionType::BACTION_TYPE_SIGNAL:
					std::cout << "type:SIG ";
					break;
			}
			std::cout << std::endl;
			current_action = current_action->next;
		}

		current_test = current_test->next;

	}

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\\\ MANAGEMENT //////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

Boid::boid_behaviour* BoidBehaviour::new_behaviour() {

	Boid::boid_behaviour* newb = new Boid::boid_behaviour();
	newb->active = false;
	newb->test = NULL;
	newb->owner = NULL;
	newb->test_count = 0;
	newb->tests = NULL;
	newb->action_count = 0;
	newb->actions = NULL;
	return newb;

}

void BoidBehaviour::init( Boid::boid_behaviour* b ) {
	b->active = false;
	b->test = NULL;
	b->owner = NULL;
	b->test_count = 0;
	b->tests = NULL;
	b->action_count = 0;
	b->actions = NULL;
}

//\\\\\\\\\\\\\ TEST

void BoidBehaviour::init_test( Boid::boid_behaviour_test* test ) {
	test->type = Boid::TestType::BTEST_TYPE_NONE;
	test->attr = Boid::TestAttribute::BTEST_ATTR_NONE;
	test->event = Boid::TestEvent::BTEST_EVENT_NONE;
	test->after = Boid::TestAfter::BTEST_AFTER_NONE;
	test->repeat = Boid::TestRepeat::BTEST_REPEAT_CONTINUOUS;
	test->eval = Boid::TestEval::BTEST_EVAL_NONE;
	test->active = true;
	test->valid = false;
	test->jumpto = -1;
	test->param_bool = false;
	init_param_int(test, 0);
	init_param_float(test, 0);
	init_param_vec3(test, Vector3());
	init_param_vec4(test, Color());
	init_param_ctrl(test);
	// reseting initialised flags
	test->param_int.initialised = false;
	test->param_float.initialised = false;
	test->param_vec3.initialised = false;
	test->param_vec4.initialised = false;
	test->param_ctrl.initialised = false;
	test->action = NULL;
	test->next = NULL;
}

void BoidBehaviour::clone_test( const Boid::boid_behaviour_test* src, Boid::boid_behaviour_test* dst ) {

	dst->type = src->type;
	dst->attr = src->attr;
	dst->event = src->event;
	dst->after = src->after;
	dst->repeat = src->repeat;
	dst->eval = src->eval;
	dst->active = src->active;
	dst->valid = src->valid;
	dst->jumpto = src->jumpto;
	dst->param_bool = src->param_bool;
	// param_int
	dst->param_int.initialised = src->param_int.initialised;
	dst->param_int.value = src->param_int.value;
	dst->param_int.clamped = src->param_int.clamped;
	dst->param_int.min = src->param_int.min;
	dst->param_int.max = src->param_int.max;
	// param_float
	dst->param_float.initialised = src->param_float.initialised;
	dst->param_float.value = src->param_float.value;
	dst->param_float.clamped = src->param_float.clamped;
	dst->param_float.min = src->param_float.min;
	dst->param_float.max = src->param_float.max;
	// param_vec3
	dst->param_vec3.initialised = src->param_vec3.initialised;
	dst->param_vec3.value = src->param_vec3.value;
	dst->param_vec3.clamped = src->param_vec3.clamped;
	dst->param_vec3.min = src->param_vec3.min;
	dst->param_vec3.max = src->param_vec3.max;
	// param_vec4
	dst->param_vec4.initialised = src->param_vec4.initialised;
	dst->param_vec4.value = src->param_vec4.value;
	dst->param_vec4.clamped = src->param_vec4.clamped;
	dst->param_vec4.min = src->param_vec4.min;
	dst->param_vec4.max = src->param_vec4.max;
	// param_ctrl
	init_param_ctrl(dst);
	dst->param_ctrl.initialised = src->param_ctrl.initialised;
	dst->param_ctrl.control_UID = src->param_ctrl.control_UID;

	dst->action = NULL;
	dst->next = NULL;
	dst->msg = src->msg;

}

void BoidBehaviour::purge_test( Boid::boid_behaviour_test* test ) {
	test->action = NULL;
	test->next = NULL;
}

void BoidBehaviour::init_param_int( Boid::boid_behaviour_test* test, int p_default ) {
	if ( !test->param_int.initialised ) {
		test->param_int.value = p_default;
		test->param_int.clamped = false;
		test->param_int.min = p_default;
		test->param_int.max = p_default;
		test->param_int.initialised = true;
	}
}

void BoidBehaviour::init_param_float( Boid::boid_behaviour_test* test, float p_default ) {
	if ( !test->param_float.initialised ) {
		test->param_float.value = p_default;
		test->param_float.clamped = false;
		test->param_float.min = p_default;
		test->param_float.max = p_default;
		test->param_float.initialised = true;
	}
}

void BoidBehaviour::init_param_vec3( Boid::boid_behaviour_test* test, Vector3 p_default ) {
	if ( !test->param_vec3.initialised ) {
		test->param_vec3.value = p_default;
		test->param_vec3.clamped = false;
		test->param_vec3.min = p_default;
		test->param_vec3.max = p_default;
		test->param_vec3.initialised = true;
	}
}

void BoidBehaviour::init_param_vec4( Boid::boid_behaviour_test* test, Color p_default ) {
	if ( !test->param_vec4.initialised ) {
		test->param_vec4.value = p_default;
		test->param_vec4.clamped = false;
		test->param_vec4.min = p_default;
		test->param_vec4.max = p_default;
		test->param_vec4.initialised = true;
	}
}

void BoidBehaviour::init_param_ctrl( Boid::boid_behaviour_test* test ) {
	if ( !test->param_ctrl.initialised ) {
		test->param_ctrl.value = NULL;
		test->param_ctrl.control_UID = -1;
		test->param_ctrl.initialised = true;
	}
}

void BoidBehaviour::validate_params( Boid::boid_behaviour_test* test ) {
	if ( test->param_int.initialised && test->param_int.clamped ) {
		Boid::boid_param_int& p = test->param_int;
		if ( p.value < p.min ) {
			p.value = p.min;
		} else if ( p.value > p.max ) {
			p.value = p.max;
		}
	}
	if ( test->param_float.initialised && test->param_float.clamped ) {
		Boid::boid_param_float& p = test->param_float;
		if ( p.value < p.min ) {
			p.value = p.min;
		} else if ( p.value > p.max ) {
			p.value = p.max;
		}
	}
	if ( test->param_vec3.initialised && test->param_vec3.clamped ) {
		Boid::boid_param_vec3& p = test->param_vec3;
		if ( p.value < p.min ) {
			p.value = p.min;
		} else if ( p.value > p.max ) {
			p.value = p.max;
		}
	}
	if ( test->param_vec4.initialised && test->param_vec4.clamped ) {
		Boid::boid_param_vec4& p = test->param_vec4;
		for ( int i = 0; i < 4; ++i ) {
			if ( p.value[i] < p.min[i] ) {
				p.value[i] = p.min[i];
			} else if ( p.value[i] > p.max[i] ) {
				p.value[i] = p.max[i];
			}
		}
	}
}

void BoidBehaviour::push_message( Boid::boid_behaviour_test* test, std::string msg ) { // @suppress("Member declaration not found")

	if ( test->msg != "" ) {
		test->msg += "\n";
	}
	test->msg += msg;

}

void BoidBehaviour::validate_test( Boid::boid_behaviour_test* test, Boid::boid_behaviour_test* next ) {

	if ( test == NULL ) {
		return;
	}

	test->msg = "";
	bool type_invalid = false;

	// checking type and params are correctly setup
	switch( test->type ) {

		case Boid::TestType::BTEST_TYPE_NONE:
			push_message( test, "select a test's type" );
			test->valid = false;
			break;

		case Boid::TestType::BTEST_TYPE_ALWAYS_TRUE:
			test->valid = true;
			type_invalid = true;
			break;

		case Boid::TestType::BTEST_TYPE_ATTRIBUTE:
			switch( test->attr ) {
				case Boid::TestAttribute::BTEST_ATTR_NONE:
					push_message( test, "select an attribute" );
					test->valid = false;
					break;
				case Boid::TestAttribute::BTEST_ATTR_ACTIVE:
				case Boid::TestAttribute::BTEST_ATTR_COLLISION:
				case Boid::TestAttribute::BTEST_ATTR_MOVING:
					test->valid = true;
					break;
				case Boid::TestAttribute::BTEST_ATTR_GROUP:
				case Boid::TestAttribute::BTEST_ATTR_ANIMATION:
					init_param_int( test, 1 );
					test->param_int.clamped = true;
					test->param_int.min = 0;
					test->param_int.max = 1048575;
					test->valid = true;
					break;
				case Boid::TestAttribute::BTEST_ATTR_COUNTER:
					init_param_float( test, 0 );
					test->param_float.clamped = false;
					test->valid = true;
					break;
				case Boid::TestAttribute::BTEST_ATTR_SPEED:
				case Boid::TestAttribute::BTEST_ATTR_PUSH_SPEED:
				case Boid::TestAttribute::BTEST_ATTR_WEIGHT:
				case Boid::TestAttribute::BTEST_ATTR_RADIUS:
				case Boid::TestAttribute::BTEST_ATTR_PUSH:
				case Boid::TestAttribute::BTEST_ATTR_TIMER:
				case Boid::TestAttribute::BTEST_ATTR_LIFETIME:
					init_param_float( test, 1 );
					test->param_float.clamped = false;
					test->valid = true;
					break;
				default:
					break;
			}
			break;

		case Boid::TestType::BTEST_TYPE_RANDOM:
			init_param_float( test, .5 );
			test->param_float.clamped = true;
			test->param_float.min = 0;
			test->param_float.max = 1;
			test->valid = true;
			break;

		case Boid::TestType::BTEST_TYPE_NEIGHBORHOUD:
			init_param_int( test, 1 );
			test->param_int.clamped = true;
			test->param_int.min = 0;
			test->param_int.max = 1048575;
			test->valid = true;
			break;

		case Boid::TestType::BTEST_TYPE_INSIDE:
		case Boid::TestType::BTEST_TYPE_OUTSIDE:
			init_param_ctrl( test );
			if ( control_info.empty() ) {
				push_message( test, "no target or collider available" );
				test->valid = false;
				break;
			} else {
				bool found = false;
				for ( int i = 0, imax = control_info.size(); i < imax; ++i ) {
					switch ( control_info[i].type ) {
						case Boid::ControlType::BCTRL_TYPE_TARGET:
						case Boid::ControlType::BCTRL_TYPE_COLLIDER:
							found = true;
							break;
						case Boid::ControlType::BCTRL_TYPE_NONE:
						case Boid::ControlType::BCTRL_TYPE_SOURCE:
						case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
						default:
							break;
					}
				}
				if ( !found ) {
					push_message( test, "no target or collider available" );
					test->valid = false;
					break;
				}
			}
			if ( test->param_ctrl.control_UID == -1 ) {
				push_message( test, "select target or collider" );
				test->valid = false;
			} else {
				bool found = false;
				bool go_on = true;
				for ( int i = 0, imax = control_info.size(); i < imax; ++i ) {
					go_on = true;
					switch ( control_info[i].type ) {
						case Boid::ControlType::BCTRL_TYPE_TARGET:
						case Boid::ControlType::BCTRL_TYPE_COLLIDER:
							break;
						case Boid::ControlType::BCTRL_TYPE_NONE:
						case Boid::ControlType::BCTRL_TYPE_SOURCE:
						case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
						default:
							go_on = false;
							break;
					}
					if ( !go_on ) {
						continue;
					}
					if ( control_info[i].UID == test->param_ctrl.control_UID ) {
						found = true;
						break;
					}
				}
				if (!found) {
					push_message( test, "invalid target / collider" );
				}
				test->valid = found;
			}
			break;

		case Boid::TestType::BTEST_TYPE_EVENT:
			switch( test->event ) {
				case Boid::TestEvent::BTEST_EVENT_NONE:
					push_message( test, "select an event" );
					test->valid = false;
					break;
				case Boid::TestEvent::BTEST_EVENT_BORN:
				case Boid::TestEvent::BTEST_EVENT_TIMER_END:
				case Boid::TestEvent::BTEST_EVENT_MOVING_START:
				case Boid::TestEvent::BTEST_EVENT_MOVING_STOP:
				default:
					test->valid = true;
					break;
			}
			break;

		case Boid::TestType::BTEST_TYPE_NEIGHBOR_COUNT:
			init_param_int( test, 1 );
			test->param_int.clamped = false;
			test->valid = true;
			break;


		default:
			break;
	}

	// checking evaluation sign is test is correctly configured
	if ( test->valid && !type_invalid ) {

		switch( test->eval ) {

			case Boid::TestEval::BTEST_EVAL_NONE:

				// auto select eval type

				switch( test->type ) {
					case Boid::TestType::BTEST_TYPE_NONE:
					case Boid::TestType::BTEST_TYPE_ALWAYS_TRUE:
					case Boid::TestType::BTEST_TYPE_INSIDE:
					case Boid::TestType::BTEST_TYPE_OUTSIDE:
					case Boid::TestType::BTEST_TYPE_EVENT:
					default:
						break;

					case Boid::TestType::BTEST_TYPE_RANDOM:
						test->eval = Boid::TestEval::BTEST_EVAL_GREATER;
						break;

					case Boid::TestType::BTEST_TYPE_ATTRIBUTE:
						switch( test->attr ) {
							case Boid::TestAttribute::BTEST_ATTR_ACTIVE:
							case Boid::TestAttribute::BTEST_ATTR_COLLISION:
							case Boid::TestAttribute::BTEST_ATTR_MOVING:
							case Boid::TestAttribute::BTEST_ATTR_COUNTER:
								test->eval= Boid::TestEval::BTEST_EVAL_EQUAL;
								break;
							case Boid::TestAttribute::BTEST_ATTR_GROUP:
							case Boid::TestAttribute::BTEST_ATTR_ANIMATION:
								test->eval= Boid::TestEval::BTEST_EVAL_BITWISE_AND;
								break;
							case Boid::TestAttribute::BTEST_ATTR_SPEED:
							case Boid::TestAttribute::BTEST_ATTR_PUSH_SPEED:
							case Boid::TestAttribute::BTEST_ATTR_WEIGHT:
							case Boid::TestAttribute::BTEST_ATTR_RADIUS:
							case Boid::TestAttribute::BTEST_ATTR_PUSH:
							case Boid::TestAttribute::BTEST_ATTR_TIMER:
							case Boid::TestAttribute::BTEST_ATTR_LIFETIME:
								test->eval= Boid::TestEval::BTEST_EVAL_GREATER;
								break;
							case Boid::TestAttribute::BTEST_ATTR_NONE:
							default:
								test->valid = false;
								break;
						}
						break;

					case Boid::TestType::BTEST_TYPE_NEIGHBORHOUD:
						test->eval= Boid::TestEval::BTEST_EVAL_BITWISE_AND;
						break;

					case Boid::TestType::BTEST_TYPE_NEIGHBOR_COUNT:
						test->eval= Boid::TestEval::BTEST_EVAL_GREATER;
						break;

				}
				break;

			// can be done on any test
			case Boid::TestEval::BTEST_EVAL_EQUAL:
			case Boid::TestEval::BTEST_EVAL_NOT_EQUAL:
				switch( test->type ) {

					case Boid::TestType::BTEST_TYPE_ALWAYS_TRUE:
					case Boid::TestType::BTEST_TYPE_RANDOM:
					case Boid::TestType::BTEST_TYPE_INSIDE:
					case Boid::TestType::BTEST_TYPE_OUTSIDE:
					case Boid::TestType::BTEST_TYPE_EVENT:
					case Boid::TestType::BTEST_TYPE_NEIGHBOR_COUNT:
						// always ok
						break;

					case Boid::TestType::BTEST_TYPE_ATTRIBUTE:
						switch( test->attr ) {

							case Boid::TestAttribute::BTEST_ATTR_ACTIVE:
							case Boid::TestAttribute::BTEST_ATTR_COLLISION:
							case Boid::TestAttribute::BTEST_ATTR_MOVING:
							case Boid::TestAttribute::BTEST_ATTR_COUNTER:
							case Boid::TestAttribute::BTEST_ATTR_SPEED:
							case Boid::TestAttribute::BTEST_ATTR_PUSH_SPEED:
							case Boid::TestAttribute::BTEST_ATTR_WEIGHT:
							case Boid::TestAttribute::BTEST_ATTR_RADIUS:
								// always ok
								break;

							case Boid::TestAttribute::BTEST_ATTR_PUSH:
							case Boid::TestAttribute::BTEST_ATTR_TIMER:
							case Boid::TestAttribute::BTEST_ATTR_LIFETIME:
								push_message( test, "use greater or lesser" );
								test->valid = false;
								break;

							case Boid::TestAttribute::BTEST_ATTR_GROUP:
							case Boid::TestAttribute::BTEST_ATTR_ANIMATION:
								push_message( test, "use bitwise operators" );
								test->valid = false;
								break;

							case Boid::TestAttribute::BTEST_ATTR_NONE:
							default:
								push_message( test, "select an attribute" );
								test->valid = false;
								break;
						}
						break;

					case Boid::TestType::BTEST_TYPE_NEIGHBORHOUD:
						push_message( test, "use bitwise operators" );
						test->valid = false;
						break;

					case Boid::TestType::BTEST_TYPE_NONE:
					default:
						test->valid = false;
						break;

				}
				break;

			// here it's much more tricky! only int, float, vec3 and vec4 are accepted
			case Boid::TestEval::BTEST_EVAL_GREATER:
			case Boid::TestEval::BTEST_EVAL_GREATER_EQUAL:
			case Boid::TestEval::BTEST_EVAL_LESSER:
			case Boid::TestEval::BTEST_EVAL_LESSER_EQUAL:
				switch( test->type ) {

					case Boid::TestType::BTEST_TYPE_ALWAYS_TRUE:
					case Boid::TestType::BTEST_TYPE_RANDOM:
					case Boid::TestType::BTEST_TYPE_INSIDE:
					case Boid::TestType::BTEST_TYPE_OUTSIDE:
					case Boid::TestType::BTEST_TYPE_EVENT:
					case Boid::TestType::BTEST_TYPE_NEIGHBOR_COUNT:
						// always ok
						break;

					case Boid::TestType::BTEST_TYPE_ATTRIBUTE:
						switch( test->attr ) {

							case Boid::TestAttribute::BTEST_ATTR_ACTIVE:
							case Boid::TestAttribute::BTEST_ATTR_COLLISION:
							case Boid::TestAttribute::BTEST_ATTR_MOVING:
								push_message( test, "active can only be true or false" );
								test->valid = false;
								break;

							case Boid::TestAttribute::BTEST_ATTR_GROUP:
							case Boid::TestAttribute::BTEST_ATTR_ANIMATION:
								push_message( test, "use bitwise operators" );
								test->valid = false;
								break;

							case Boid::TestAttribute::BTEST_ATTR_SPEED:
							case Boid::TestAttribute::BTEST_ATTR_PUSH_SPEED:
							case Boid::TestAttribute::BTEST_ATTR_WEIGHT:
							case Boid::TestAttribute::BTEST_ATTR_RADIUS:
							case Boid::TestAttribute::BTEST_ATTR_PUSH:
							case Boid::TestAttribute::BTEST_ATTR_COUNTER:
							case Boid::TestAttribute::BTEST_ATTR_TIMER:
							case Boid::TestAttribute::BTEST_ATTR_LIFETIME:
								// always ok
								break;

							case Boid::TestAttribute::BTEST_ATTR_NONE:
							default:
								push_message( test, "select an attribute" );
								test->valid = false;
								break;
						}
						break;

					case Boid::TestType::BTEST_TYPE_NEIGHBORHOUD:
						push_message( test, "use bitwise operators" );
						test->valid = false;
						break;

					case Boid::TestType::BTEST_TYPE_NONE:
					default:
						push_message( test, "select a test's type" );
						test->valid = false;
						break;
				}
				break;

			case Boid::TestEval::BTEST_EVAL_MODULO:
				switch( test->type ) {

					case Boid::TestType::BTEST_TYPE_ALWAYS_TRUE:
					case Boid::TestType::BTEST_TYPE_INSIDE:
					case Boid::TestType::BTEST_TYPE_OUTSIDE:
					case Boid::TestType::BTEST_TYPE_EVENT:
					case Boid::TestType::BTEST_TYPE_NEIGHBOR_COUNT:
						// ok
						break;
					default:
						push_message( test, "modulo not supported" );
						test->valid = false;
						break;
				}
				break;

			case Boid::TestEval::BTEST_EVAL_BITWISE_AND:
			case Boid::TestEval::BTEST_EVAL_BITWISE_XOR:
				switch( test->type ) {

					case Boid::TestType::BTEST_TYPE_ALWAYS_TRUE:
					case Boid::TestType::BTEST_TYPE_NEIGHBORHOUD:
					case Boid::TestType::BTEST_TYPE_INSIDE:
					case Boid::TestType::BTEST_TYPE_OUTSIDE:
					case Boid::TestType::BTEST_TYPE_EVENT:
						// always ok
						break;

					// tricky
					case Boid::TestType::BTEST_TYPE_ATTRIBUTE:
						switch( test->attr ) {
							case Boid::TestAttribute::BTEST_ATTR_NONE:
								push_message( test, "select an attribute" );
								test->valid = false;
								break;
							case Boid::TestAttribute::BTEST_ATTR_GROUP:
							case Boid::TestAttribute::BTEST_ATTR_ANIMATION:
								// ok
								break;
							default:
								push_message( test, "bitwise operator not supported" );
								test->valid = false;
								break;
						}
						break;

					case Boid::TestType::BTEST_TYPE_RANDOM:
						push_message( test, "bitwise operator not supported" );
						test->valid = false;
						break;

					case Boid::TestType::BTEST_TYPE_NONE:
					default:
						push_message( test, "select a test's type" );
						test->valid = false;
						break;
				}
				break;
		}

	}

	// checking what to do after, in link to *next
	if ( test->valid ) {

		switch( test->after ) {

			case Boid::TestAfter::BTEST_AFTER_AND:
			case Boid::TestAfter::BTEST_AFTER_OR:
				test->valid = next != NULL;
				if ( !test->valid ) {
					push_message( test, "add a test after this one" );
				}
				break;

			case Boid::TestAfter::BTEST_AFTER_NONE:
			case Boid::TestAfter::BTEST_AFTER_EXIT:
			case Boid::TestAfter::BTEST_AFTER_GOTO:
				// always ok
				break;

		}

	}

	validate_params( test );

}

//\\\\\\\\\\\\\ ACTION

void BoidBehaviour::init_action( Boid::boid_behaviour_action* action ) {
	action->type = Boid::ActionType::BACTION_TYPE_NONE;
	action->attr = Boid::ActionAttribute::BACTION_ATTR_NONE;
	action->active = true;
	action->valid = false;
	action->param_bool = false;
	init_param_int(action, 0);
	init_param_float(action, 0);
	init_param_vec3(action, Vector3());
	init_param_vec4(action, Color());
	init_param_ctrl(action);
	// reseting initialised flags
	action->param_int.initialised = false;
	action->param_float.initialised = false;
	action->param_vec3.initialised = false;
	action->param_vec4.initialised = false;
	action->param_ctrl.initialised = false;
	action->param_ctrl.initialised = false;
	action->next = NULL;
}

void BoidBehaviour::clone_action( const Boid::boid_behaviour_action* src, Boid::boid_behaviour_action* dst ) {

	dst->type = src->type;
	dst->attr = src->attr;
	dst->active = src->active;
	dst->valid = src->valid;
	dst->param_bool = src->param_bool;

	// param_int
	dst->param_int.initialised = src->param_int.initialised;
	dst->param_int.value = src->param_int.value;
	dst->param_int.clamped = src->param_int.clamped;
	dst->param_int.min = src->param_int.min;
	dst->param_int.max = src->param_int.max;

	// param_float
	dst->param_float.initialised = src->param_float.initialised;
	dst->param_float.value = src->param_float.value;
	dst->param_float.clamped = src->param_float.clamped;
	dst->param_float.min = src->param_float.min;
	dst->param_float.max = src->param_float.max;

	// param_vec3
	dst->param_vec3.initialised = src->param_vec3.initialised;
	dst->param_vec3.value = src->param_vec3.value;
	dst->param_vec3.clamped = src->param_vec3.clamped;
	dst->param_vec3.min = src->param_vec3.min;
	dst->param_vec3.max = src->param_vec3.max;

	// param_vec4
	dst->param_vec4.initialised = src->param_vec4.initialised;
	dst->param_vec4.value = src->param_vec4.value;
	dst->param_vec4.clamped = src->param_vec4.clamped;
	dst->param_vec4.min = src->param_vec4.min;
	dst->param_vec4.max = src->param_vec4.max;

	// param_ctrl
	init_param_ctrl(dst);
	dst->param_ctrl.initialised = src->param_ctrl.initialised;
	dst->param_ctrl.control_UID = src->param_ctrl.control_UID;

	dst->next = NULL;
	dst->msg = src->msg;

}

void BoidBehaviour::purge_action( Boid::boid_behaviour_action* action ) {
	action->next = NULL;
}

void BoidBehaviour::init_param_int( Boid::boid_behaviour_action* action, int p_default ) {
	if ( !action->param_int.initialised ) {
		action->param_int.value = p_default;
		action->param_int.clamped = false;
		action->param_int.min = p_default;
		action->param_int.max = p_default;
		action->param_int.initialised = true;
	}
}

void BoidBehaviour::init_param_float( Boid::boid_behaviour_action* action, float p_default ) {
	if ( !action->param_float.initialised ) {
		action->param_float.value = p_default;
		action->param_float.clamped = false;
		action->param_float.min = p_default;
		action->param_float.max = p_default;
		action->param_float.initialised = true;
	}
}

void BoidBehaviour::init_param_vec3( Boid::boid_behaviour_action* action, Vector3 p_default ) {
	if ( !action->param_vec3.initialised ) {
		action->param_vec3.value = p_default;
		action->param_vec3.clamped = false;
		action->param_vec3.min = p_default;
		action->param_vec3.max = p_default;
		action->param_vec3.initialised = true;
	}
}

void BoidBehaviour::init_param_vec4( Boid::boid_behaviour_action* action, Color p_default ) {
	if ( !action->param_vec4.initialised ) {
		action->param_vec4.value = p_default;
		action->param_vec4.clamped = false;
		action->param_vec4.min = p_default;
		action->param_vec4.max = p_default;
		action->param_vec4.initialised = true;
	}
}

void BoidBehaviour::init_param_ctrl( Boid::boid_behaviour_action* action ) {
	if ( !action->param_ctrl.initialised ) {
		action->param_ctrl.value = NULL;
		action->param_ctrl.control_UID = -1;
		action->param_ctrl.initialised = true;
	}
}

void BoidBehaviour::validate_params( Boid::boid_behaviour_action* action ) {
	if ( action->param_int.initialised && action->param_int.clamped ) {
		Boid::boid_param_int& p = action->param_int;
		if ( p.value < p.min ) {
			p.value = p.min;
		} else if ( p.value > p.max ) {
			p.value = p.max;
		}
	}
	if ( action->param_float.initialised && action->param_float.clamped ) {
		Boid::boid_param_float& p = action->param_float;
		if ( p.value < p.min ) {
			p.value = p.min;
		} else if ( p.value > p.max ) {
			p.value = p.max;
		}
	}
	if ( action->param_vec3.initialised && action->param_vec3.clamped ) {
		Boid::boid_param_vec3& p = action->param_vec3;
		if ( p.value < p.min ) {
			p.value = p.min;
		} else if ( p.value > p.max ) {
			p.value = p.max;
		}
	}
	if ( action->param_vec4.initialised && action->param_vec4.clamped ) {
		Boid::boid_param_vec4& p = action->param_vec4;
		for ( int i = 0; i < 4; ++i ) {
			if ( p.value[i] < p.min[i] ) {
				p.value[i] = p.min[i];
			} else if ( p.value[i] > p.max[i] ) {
				p.value[i] = p.max[i];
			}
		}
	}
}

void BoidBehaviour::push_message(Boid::boid_behaviour_action* action, std::string msg ) { // @suppress("Member declaration not found")

	if ( action->msg != "" ) {
		action->msg += "\n";
	}
	action->msg += msg;

}

void BoidBehaviour::validate_action( Boid::boid_behaviour_action* action ) {

	if ( action == NULL ) {
		return;
	}

	action->msg = "";

	// checking type and params are correctly setup
	switch( action->type ) {

		case Boid::ActionType::BACTION_TYPE_NONE:
			push_message( action, "select action" );
			action->valid = false;
			break;

		case Boid::ActionType::BACTION_TYPE_SET_ATTRIBUTE:
			switch ( action->attr ) {
				case Boid::ActionAttribute::BACTION_ATTR_NONE:
					push_message( action, "select an attribute" );
					action->valid = false;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_ACTIVE:
				case Boid::ActionAttribute::BACTION_ATTR_COLLISION:
					action->valid = true;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_GROUP:
				case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
					init_param_int( action, 1 );
					action->param_int.clamped = true;
					action->param_int.min = 0;
					action->param_int.max = 1048575;
					action->valid = true;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
				case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
				case Boid::ActionAttribute::BACTION_ATTR_COHESION:
				case Boid::ActionAttribute::BACTION_ATTR_STEER:
				case Boid::ActionAttribute::BACTION_ATTR_SPEED:
				case Boid::ActionAttribute::BACTION_ATTR_PUSH_SPEED:
				case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
				case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
				case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
				case Boid::ActionAttribute::BACTION_ATTR_FRICTION:
				case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
				case Boid::ActionAttribute::BACTION_ATTR_TIMER:
				case Boid::ActionAttribute::BACTION_ATTR_LIFETIME:
					init_param_float( action, 1 );
					action->param_float.clamped = false;
					action->valid = true;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_AIM:
				case Boid::ActionAttribute::BACTION_ATTR_POSITION:
				case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
				case Boid::ActionAttribute::BACTION_ATTR_FRONT:
				case Boid::ActionAttribute::BACTION_ATTR_PUSH:
					init_param_vec3(action, Vector3(0,0,1));
					action->param_vec3.clamped = false;
					action->valid = true;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_COLOR:
					init_param_vec4(action, Color(1,1,1,1));
					action->param_vec4.clamped = false;
					action->valid = true;
					break;
				default:
					action->valid = false;
					break;
			}
			break;

		case Boid::ActionType::BACTION_TYPE_INCREMENT_ATTRIBUTE:
			switch ( action->attr ) {
				case Boid::ActionAttribute::BACTION_ATTR_NONE:
					push_message( action, "select a int of float attribute" );
					action->valid = false;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
				case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
				case Boid::ActionAttribute::BACTION_ATTR_COHESION:
				case Boid::ActionAttribute::BACTION_ATTR_STEER:
				case Boid::ActionAttribute::BACTION_ATTR_SPEED:
				case Boid::ActionAttribute::BACTION_ATTR_PUSH_SPEED:
				case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
				case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
				case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
				case Boid::ActionAttribute::BACTION_ATTR_FRICTION:
				case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
				case Boid::ActionAttribute::BACTION_ATTR_TIMER:
					init_param_float( action, 1 );
					action->param_float.clamped = false;
					action->valid = true;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_ACTIVE:
				case Boid::ActionAttribute::BACTION_ATTR_COLLISION:
				case Boid::ActionAttribute::BACTION_ATTR_GROUP:
				case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
				case Boid::ActionAttribute::BACTION_ATTR_AIM:
				case Boid::ActionAttribute::BACTION_ATTR_COLOR:
				case Boid::ActionAttribute::BACTION_ATTR_POSITION:
				case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
				case Boid::ActionAttribute::BACTION_ATTR_FRONT:
				case Boid::ActionAttribute::BACTION_ATTR_PUSH:
				default:
					push_message( action, "only int and float attributes can be incremented or decremented" );
					action->valid = false;
					break;

			}
			break;

		case Boid::ActionType::BACTION_TYPE_RANDOMISE_ATTRIBUTE:
			switch ( action->attr ) {
				case Boid::ActionAttribute::BACTION_ATTR_ACTIVE:
				case Boid::ActionAttribute::BACTION_ATTR_COLLISION:
				case Boid::ActionAttribute::BACTION_ATTR_GROUP:
				case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
					push_message( action, "only valid for numeric & vector attributes" );
					action->valid = false;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
				case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
				case Boid::ActionAttribute::BACTION_ATTR_COHESION:
				case Boid::ActionAttribute::BACTION_ATTR_STEER:
				case Boid::ActionAttribute::BACTION_ATTR_SPEED:
				case Boid::ActionAttribute::BACTION_ATTR_PUSH_SPEED:
				case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
				case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
				case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
				case Boid::ActionAttribute::BACTION_ATTR_FRICTION:
				case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
				case Boid::ActionAttribute::BACTION_ATTR_TIMER:
					init_param_float( action, 1 );
					action->param_float.clamped = false;
					action->valid = true;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_AIM:
				case Boid::ActionAttribute::BACTION_ATTR_POSITION:
				case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
				case Boid::ActionAttribute::BACTION_ATTR_FRONT:
				case Boid::ActionAttribute::BACTION_ATTR_PUSH:
					init_param_vec3(action, Vector3(0,0,0));
					action->param_vec3.clamped = false;
					action->valid = true;
					break;

				case Boid::ActionAttribute::BACTION_ATTR_COLOR:
					init_param_vec4(action, Color(1,1,1,1));
					action->param_vec4.clamped = false;
					action->valid = true;
					break;

				case Boid::ActionAttribute::BACTION_ATTR_NONE:
				default:
					push_message( action, "select a numeric of vector attribute" );
					action->valid = false;
					break;

			}
			break;

		case Boid::ActionType::BACTION_TYPE_ASSIGN_TARGET:
			init_param_ctrl(action);
			if ( control_info.empty() ) {
				push_message( action, "no target or collider available" );
				action->valid = false;
				break;
			} else {
				bool found = false;
				for ( int i = 0, imax = control_info.size(); i < imax; ++i ) {
					switch ( control_info[i].type ) {
						case Boid::ControlType::BCTRL_TYPE_TARGET:
						case Boid::ControlType::BCTRL_TYPE_COLLIDER:
							found = true;
							break;
						case Boid::ControlType::BCTRL_TYPE_NONE:
						case Boid::ControlType::BCTRL_TYPE_SOURCE:
						case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
						default:
							break;
					}
				}
				if ( !found ) {
					push_message( action, "no target or collider available" );
					action->valid = false;
					break;
				}
			}
			if ( action->param_ctrl.control_UID == -1 ) {
				push_message( action, "select target or collider" );
				action->valid = false;
			} else {
				bool found = false;
				bool go_on = true;
				for ( int i = 0, imax = control_info.size(); i < imax; ++i ) {
					go_on = true;
					switch ( control_info[i].type ) {
						case Boid::ControlType::BCTRL_TYPE_TARGET:
						case Boid::ControlType::BCTRL_TYPE_COLLIDER:
							break;
						case Boid::ControlType::BCTRL_TYPE_NONE:
						case Boid::ControlType::BCTRL_TYPE_SOURCE:
						case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
						default:
							go_on = false;
							break;
					}
					if ( !go_on ) {
						continue;
					}
					if ( control_info[i].UID == action->param_ctrl.control_UID ) {
						found = true;
						break;
					}
				}
				if (!found) {
					push_message( action, "invalid target / collider" );
					action->param_ctrl.control_UID = -1;
				}
				action->valid = found;
			}
			break;

		case Boid::ActionType::BACTION_TYPE_AIM_AT:
		case Boid::ActionType::BACTION_TYPE_REACH:
		case Boid::ActionType::BACTION_TYPE_ESCAPE:
			init_param_int(action,1);
			action->param_int.clamped = true;
			action->param_int.min = 0;
			action->param_int.max = 1048575;
			init_param_float(action,1);
			action->param_float.clamped = false;
			action->valid = true;
			break;

		case Boid::ActionType::BACTION_TYPE_COUPLE:
			init_param_int(action,1);
			action->param_int.clamped = true;
			action->param_int.min = 0;
			action->param_int.max = 1048575;
			action->valid = true;
			break;

		case Boid::ActionType::BACTION_TYPE_LEAVE:
			action->valid = true;
			break;

		case Boid::ActionType::BACTION_TYPE_SIGNAL:
			init_param_int(action,1);
			action->param_int.clamped = false;
			action->valid = true;
			break;

	}

	validate_params( action );


}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\ SERIALISATION /////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

Dictionary BoidBehaviour::serialise_param_int( Boid::boid_param_int& p ) {
	Dictionary a;
	a["value"] = p.value;
	a["clamped"] = p.clamped;
	a["min"] = p.min;
	a["max"] = p.max;
	return a;
}

Dictionary BoidBehaviour::serialise_param_float( Boid::boid_param_float& p ) {
	Dictionary a;
	a["value"] = p.value;
	a["clamped"] = p.clamped;
	a["min"] = p.min;
	a["max"] = p.max;
	return a;
}

Dictionary BoidBehaviour::serialise_param_vec3( Boid::boid_param_vec3& p ) {
	Dictionary a;
	Array vs;
	Array min;
	Array max;
	for ( int i = 0; i < 3; ++i ) {
		vs.push_back( p.value[i] );
		min.push_back( p.min[i] );
		max.push_back( p.max[i] );
	}
	a["value"] = vs;
	a["clamped"] = p.clamped;
	a["min"] = min;
	a["max"] = max;
	return a;
}

Dictionary BoidBehaviour::serialise_param_vec4( Boid::boid_param_vec4& p ) {
	Dictionary a;
	Array vs;
	Array min;
	Array max;
	for ( int i = 0; i < 4; ++i ) {
		vs.push_back( p.value[i] );
		min.push_back( p.min[i] );
		max.push_back( p.max[i] );
	}
	a["value"] = vs;
	a["clamped"] = p.clamped;
	a["min"] = min;
	a["max"] = max;
	return a;
}

Dictionary BoidBehaviour::serialise_param_ctrl( Boid::boid_param_control& p ) {
	Dictionary a;
	a["control_UID"] = p.control_UID;
	return a;
}

Dictionary BoidBehaviour::serialise( Boid::boid_behaviour_test* test ) {
	Dictionary d;
	d["class"] = "test";
	d["type"] = int(test->type);
	d["attr"] = int(test->attr);
	d["event"] = int(test->event);
	d["after"] = int(test->after);
	d["repeat"] = int(test->repeat);
	d["eval"] = int(test->eval);
	d["active"] = int(test->active);
	d["valid"] = int(test->valid);
	d["jumpto"] = int(test->jumpto);
	d["param_bool"] = int(test->param_bool);
	if ( test->param_int.initialised ) {
		d["param_int"] = serialise_param_int( test->param_int );
	}
	if ( test->param_float.initialised ) {
		d["param_float"] = serialise_param_float( test->param_float );
	}
	if ( test->param_vec3.initialised ) {
		d["param_vec3"] = serialise_param_vec3( test->param_vec3 );
	}
	if ( test->param_vec4.initialised ) {
		d["param_vec4"] = serialise_param_vec4( test->param_vec4 );
	}
	if ( test->param_ctrl.initialised ) {
		d["param_ctrl"] = serialise_param_ctrl( test->param_ctrl );
	}
	return d;
}

Dictionary BoidBehaviour::serialise( Boid::boid_behaviour_action* action ) {
	Dictionary d;
	d["class"] = "action";
	d["type"] = int(action->type);
	d["attr"] = int(action->attr);
	d["active"] = int(action->active);
	d["valid"] = int(action->valid);
	d["param_bool"] = int(action->param_bool);
	if ( action->param_int.initialised ) {
		d["param_int"] = serialise_param_int( action->param_int );
	}
	if ( action->param_float.initialised ) {
		d["param_float"] = serialise_param_float( action->param_float );
	}
	if ( action->param_vec3.initialised ) {
		d["param_vec3"] = serialise_param_vec3( action->param_vec3 );
	}
	if ( action->param_vec4.initialised ) {
		d["param_vec4"] = serialise_param_vec4( action->param_vec4 );
	}
	if ( action->param_ctrl.initialised ) {
		d["param_ctrl"] = serialise_param_ctrl( action->param_ctrl );
	}
	return d;
}

void BoidBehaviour::serialise( Vector<Boid::boid_behaviour_item>& list, bool do_emit ) {

	Array all;
	for ( int i = 0, imax = list.size(); i < imax; ++i ) {
		if ( list[i].test != NULL ) {
			all.push_back( serialise( list[i].test ) );
		} else if ( list[i].action != NULL ) {
			all.push_back( serialise( list[i].action ) );
		}
	}
	// if not true, no need to update the boid system
	emit_behaviour_updated = do_emit;
	JSON js;
	set_json( js.print( all ) );
	emit_behaviour_updated = true;

}

bool BoidBehaviour::deserialise_param_int( Boid::boid_param_int& p, const Dictionary& d ) {
	if (
			!d.has( "value" ) ||
			!d.has( "clamped" ) ||
			!d.has( "min" ) ||
			!d.has( "max" )
			) {
		return false;
	}
	p.value = d["value"];
	p.clamped = d["clamped"];
	p.min = d["min"];
	p.max = d["max"];
	return true;
}

bool BoidBehaviour::deserialise_param_float( Boid::boid_param_float& p, const Dictionary& d ) {
	if (
			!d.has( "value" ) ||
			!d.has( "clamped" ) ||
			!d.has( "min" ) ||
			!d.has( "max" )
			) {
		return false;
	}
	p.value = d["value"];
	p.clamped = d["clamped"];
	p.min = d["min"];
	p.max = d["max"];
	return true;
}

bool BoidBehaviour::deserialise_param_vec3( Boid::boid_param_vec3& p, const Dictionary& d ) {
	if (
			!d.has( "value" ) ||
			!d.has( "clamped" ) ||
			!d.has( "min" ) ||
			!d.has( "max" )
			) {
		return false;
	}
	Array vs = d["value"];
	Array min = d["min"];
	Array max = d["max"];
	if ( vs.size() != 3 || min.size() != 3 || max.size() != 3 ) {
		return false;
	}
	p.clamped = d["clamped"];
	for ( int i = 0; i < 3; ++i ) {
		p.value[i] = vs[i];
		p.min[i] = min[i];
		p.max[i] = max[i];
	}
	return true;
}

bool BoidBehaviour::deserialise_param_vec4( Boid::boid_param_vec4& p, const Dictionary& d ) {
	if (
			!d.has( "value" ) ||
			!d.has( "clamped" ) ||
			!d.has( "min" ) ||
			!d.has( "max" )
			) {
		return false;
	}
	Array vs = d["value"];
	Array min = d["min"];
	Array max = d["max"];
	if ( vs.size() != 4 || min.size() != 4 || max.size() != 4 ) {
		return false;
	}
	p.clamped = d["clamped"];
	for ( int i = 0; i < 4; ++i ) {
		p.value[i] = vs[i];
		p.min[i] = min[i];
		p.max[i] = max[i];
	}
	return true;
}

bool BoidBehaviour::deserialise_param_ctrl( Boid::boid_param_control& p, const Dictionary& d ) {
	if ( !d.has( "control_UID" ) ) {
		return false;
	}
	p.control_UID = d["control_UID"];
	return true;
}

bool BoidBehaviour::deserialise( Boid::boid_behaviour_test* test, const Dictionary& d ) {

	if (
			!d.has( "type" ) ||
			!d.has( "attr" ) ||
			!d.has( "event" ) ||
			!d.has( "after" ) ||
			!d.has( "repeat" ) ||
			!d.has( "eval" ) ||
			!d.has( "active" ) ||
			!d.has( "valid" ) ||
			!d.has( "jumpto" ) ||
			!d.has( "param_bool" )
			) {
		return false;
	}

	init_test(test);
	int type = d["type"];
	int attr = d["attr"];
	int event = d["event"];
	int after = d["after"];
	int repeat = d["repeat"];
	int eval = d["eval"];
	test->type = Boid::TestType( type );
	test->attr = Boid::TestAttribute( attr );
	test->event = Boid::TestEvent( event );
	test->after = Boid::TestAfter( after );
	test->repeat = Boid::TestRepeat( repeat );
	test->eval= Boid::TestEval( eval );
	test->active = int( d["active"] );
	test->valid = int( d["valid"] );
	test->jumpto = d["jumpto"];
	test->param_bool = int( d["param_bool"] );
	if ( d.has( "param_int" ) ) {
		init_param_int(test);
		if ( !deserialise_param_int( test->param_int, d["param_int"]  ) ) {
			return false;
		}
 	}
	if ( d.has( "param_float" ) ) {
		init_param_float(test);
		if ( !deserialise_param_float( test->param_float, d["param_float"] ) ) {
			return false;
		}
	}
	if ( d.has( "param_vec3" ) ) {
		init_param_vec3(test,Vector3(0,0,0));
		if ( !deserialise_param_vec3( test->param_vec3, d["param_vec3"] ) ) {
			return false;
		}
	}
	if ( d.has( "param_vec4" ) ) {
		init_param_vec4(test,Color(1,1,1,1));
		if ( !deserialise_param_vec4( test->param_vec4, d["param_vec4"] ) ) {
			return false;
		}
	}
	if ( d.has( "param_ctrl" ) ) {
		init_param_ctrl(test);
		if ( !deserialise_param_ctrl( test->param_ctrl, d["param_ctrl"] ) ) {
			return false;
		}
	}

	if ( test->param_ctrl.initialised ) {
		test->valid = false;
		for ( int i = 0; i < control_info.size(); ++i ) {
			if ( control_info[i].UID == test->param_ctrl.control_UID ) {
				test->valid = true;
				break;
			}
		}
	}

	return true;

}

bool BoidBehaviour::deserialise( Boid::boid_behaviour_action* action, const Dictionary& d ) {
	if (
			!d.has( "type" ) ||
			!d.has( "attr" ) ||
			!d.has( "active" ) ||
			!d.has( "valid" ) ||
			!d.has( "param_bool" )
			) {
		return false;
	}

	init_action(action);
	int type = d["type"];
	int attr = d["attr"];
	action->type = Boid::ActionType( type );
	action->attr = Boid::ActionAttribute( attr );
	action->active = int( d["active"] );
	action->valid = int( d["valid"] );
	action->param_bool = int( d["param_bool"] );
	if ( d.has( "param_int" ) ) {
		init_param_int(action);
		if ( !deserialise_param_int( action->param_int, d["param_int"]  ) ) {
			return false;
		}
	}
	if ( d.has( "param_float" ) ) {
		init_param_float(action);
		if ( !deserialise_param_float( action->param_float, d["param_float"] ) ) {
			return false;
		}
	}
	if ( d.has( "param_vec3" ) ) {
		init_param_vec3(action,Vector3(0,0,0));
		if ( !deserialise_param_vec3( action->param_vec3, d["param_vec3"] ) ) {
			return false;
		}
	}
	if ( d.has( "param_vec4" ) ) {
		init_param_vec4(action,Color(1,1,1,1));
		if ( !deserialise_param_vec4( action->param_vec4, d["param_vec4"] ) ) {
			return false;
		}
	}
	if ( d.has( "param_ctrl" ) ) {
		init_param_ctrl(action);
		if ( !deserialise_param_ctrl( action->param_ctrl, d["param_ctrl"] ) ) {
			return false;
		}
	}

	if ( action->param_ctrl.initialised ) {
		action->valid = false;
		for ( int i = 0; i < control_info.size(); ++i ) {
			if ( control_info[i].UID == action->param_ctrl.control_UID ) {
				action->valid = true;
				break;
			}
		}
	}

	return true;
}

void BoidBehaviour::deserialise( Vector<Boid::boid_behaviour_item>& list ) {

	JSON js;
	String r_err_str;
	int r_err_line;
	Variant v;
	Error err = js.parse( json, v, r_err_str, r_err_line );
	if ( err ) {
		std::wcout << "ERROR " << r_err_line << " = " << r_err_str.c_str() << std::endl;
		return;
	}

	Array all = v;
	for ( int i = 0, imax = all.size(); i < imax; ++i ) {
		Dictionary d = all[i];
		if ( d.has("class") ) {
			if ( d["class"] == "test" ) {
				Boid::boid_behaviour_test* tmp = new (Boid::boid_behaviour_test);
				if ( deserialise( tmp, d ) ) {
					Boid::boid_behaviour_item bi;
					bi.test = tmp;
					bi.action = NULL;
					list.push_back(bi);
				} else {
					purge_test(tmp);
					delete(tmp);
				}
			} else if ( d["class"] == "action" ) {
				Boid::boid_behaviour_action* tmp = new (Boid::boid_behaviour_action);
				if ( deserialise( tmp, d ) ) {
					Boid::boid_behaviour_item bi;
					bi.test = NULL;
					bi.action = tmp;
					list.push_back(bi);
				} else {
					purge_action(tmp);
					delete(tmp);
				}
			}
		}
	}

	// validation of tests & action, in case somebody did something stupid
	for ( int i = 0, imax = list.size(); i < imax; ++i ) {
		Boid::boid_behaviour_test* t = list[i].test;
		Boid::boid_behaviour_action* a = list[i].action;
		Boid::boid_behaviour_test* next_t = NULL;
		if ( i < imax-1 ) {
			next_t = list[i+1].test;
		}
		validate_test(t, next_t);
		validate_action( a );
	}

	emit_signal("behaviour_edited");

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\ SETTERS & GETTERS ///////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidBehaviour::purge( Boid::boid_behaviour* bb ) {

	if ( bb == NULL ) {
		return;
	}

	// purge previous
	if (bb->test_count > 0) {
		for ( int i = 0; i < bb->test_count; ++i ) {
			purge_test( bb->tests[i] );
			delete( bb->tests[i] );
			bb->tests[i] = NULL;
		}
		delete [] bb->tests;
	}

	if (bb->action_count > 0) {
		for ( int i = 0; i < bb->action_count; ++i ) {
			purge_action( bb->actions[i] );
			delete( bb->actions[i] );
			bb->actions[i] = NULL;
		}
		delete [] bb->actions;
	}

	bb->active = false;
	bb->test = NULL;
	bb->test_count = 0;
	bb->tests = NULL;
	bb->action_count = 0;
	bb->actions = NULL;

}

bool BoidBehaviour::load( Boid::boid_behaviour* bb ) {

	purge(bb);

	if (json.empty()) {
		return false;
	}

	JSON js;
	String r_err_str;
	int r_err_line;
	Variant v;
	Error err = js.parse(json, v, r_err_str, r_err_line);
	if ( err ) {
		std::wcout << "ERROR " << r_err_line << " = " << r_err_str.c_str() << std::endl;
		return false;
	}

	Boid::boid_behaviour_test* curr_test = NULL;
	Boid::boid_behaviour_action* curr_action = NULL;

	Array all = v;
	bool previous_test_valid = false;

	for ( int i = 0, imax = all.size(); i < imax; ++i ) {

		Dictionary d = all[i];

		if ( d.has("class") ) {

			if ( d["class"] == "test" ) {

				Boid::boid_behaviour_test* tmp = new (Boid::boid_behaviour_test);
				bool success = deserialise( tmp, d );
				if ( success && tmp->valid ) {
					if ( curr_test == NULL ) {
						bb->test = tmp;
					} else {
						curr_test->next = tmp;
					}
					bb->test_count++;
					curr_test = tmp;
					curr_action = NULL;
					previous_test_valid = true;
				} else {
					purge_test(tmp);
					delete(tmp);
					previous_test_valid = false;
				}

			} else if ( previous_test_valid && d["class"] == "action" ) {

				// to load action, previous test MUST be valid!
				// this prevents the action to be loaded in the wrong test

				Boid::boid_behaviour_action* tmp = new (Boid::boid_behaviour_action);
				bool success = deserialise( tmp, d );
				if ( success && tmp->valid && curr_test != NULL ) {
					if ( curr_action == NULL ) {
						curr_test->action = tmp;
					} else {
						curr_action->next = tmp;
					}
					bb->action_count++;
					curr_action = tmp;
				} else {
					purge_action(tmp);
					delete(tmp);
				}

			}

		}

	}

	if ( bb->test_count > 0 ) {
		bb->tests = new Boid::boid_behaviour_test*[bb->test_count];
	}
	if ( bb->action_count > 0 ) {
		bb->actions = new Boid::boid_behaviour_action*[bb->action_count];
	}

	int ti = 0;
	int ai = 0;
	// validation of tests & action, in case somebody did something stupid
	// + storage of tests and action in array
	curr_test = bb->test;
	while( curr_test != NULL ) {
		bb->tests[ti++] = curr_test;
		validate_test(curr_test, curr_test->next);
		curr_action = curr_test->action;
		while( curr_action != NULL ) {
			bb->actions[ai++] = curr_action;
			validate_action(curr_action);
			curr_action = curr_action->next;
		}
		curr_test = curr_test->next;
	}

	bb->active = bb->test != NULL;

	return true;

}

void BoidBehaviour::set_json( const String& p_json ) {

	json = p_json;
	if ( emit_behaviour_updated ) {
		emit_signal("behaviour_updated");
	}

}

const String& BoidBehaviour::get_json() const {
	return json;
}

void BoidBehaviour::set_control_info( const Vector<Boid::boid_control_info> &list ) {
	control_info = list;
	emit_signal("behaviour_info_updated");
}

const Vector<Boid::boid_control_info>& BoidBehaviour::get_control_info() const {
	return control_info;
}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\ ENGINE LINKS //////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidBehaviour::_bind_methods() {

	ADD_SIGNAL(MethodInfo("behaviour_updated"));
	ADD_SIGNAL(MethodInfo("behaviour_edited"));
	ADD_SIGNAL(MethodInfo("behaviour_info_updated"));

	ClassDB::bind_method(D_METHOD("set_json", "json"), &BoidBehaviour::set_json);
	ClassDB::bind_method(D_METHOD("get_json"), &BoidBehaviour::get_json);

	ADD_PROPERTY(PropertyInfo(Variant::STRING, "json"), "set_json", "get_json");

}

void BoidBehaviour::_notification(int p_what) {
	switch (p_what) {}
}
 
BoidBehaviour::BoidBehaviour() : json(""), emit_behaviour_updated(true) {}

BoidBehaviour::~BoidBehaviour() {}
