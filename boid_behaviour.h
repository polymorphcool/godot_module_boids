/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_behaviour.h
 *
 *  Created on: Mar 16, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_BOIDS_BOID_BEAHVIOUR_H_
#define MODULES_BOIDS_BOID_BEAHVIOUR_H_

#include <iostream>
#include "core/array.h"
#include "core/hash_map.h"
#include "core/io/json.h"
#include "scene/main/node.h"

#include "boid_common.h"
#include "boid.h"

class BoidBehaviour : public Resource {

private:
	GDCLASS(BoidBehaviour, Resource);
	OBJ_CATEGORY("Boid");
	RES_BASE_EXTENSION("behaviour");

private:

	Vector<Boid::boid_behaviour_test* > tests;
	Vector<Boid::boid_control_info> control_info;

	static void init_param_int( Boid::boid_behaviour_test* test, int p_default = 0 );
	static void init_param_float( Boid::boid_behaviour_test* test, float p_default = 0 );
	static void init_param_vec3( Boid::boid_behaviour_test* test, Vector3 p_default );
	static void init_param_vec4( Boid::boid_behaviour_test* test, Color p_default );
	static void init_param_ctrl( Boid::boid_behaviour_test* test );
	static void validate_params( Boid::boid_behaviour_test* test );
	static void push_message( Boid::boid_behaviour_test* test, std::string msg );

	static void init_param_int( Boid::boid_behaviour_action* action, int p_default = 0 );
	static void init_param_float( Boid::boid_behaviour_action* action, float p_default = 0 );
	static void init_param_vec3( Boid::boid_behaviour_action* action, Vector3 p_default );
	static void init_param_vec4( Boid::boid_behaviour_action* action, Color p_default );
	static void init_param_ctrl( Boid::boid_behaviour_action* action );
	static void validate_params( Boid::boid_behaviour_action* action );
	static void push_message( Boid::boid_behaviour_action* action, std::string msg );

	Dictionary serialise_param_int( Boid::boid_param_int& p );
	Dictionary serialise_param_float( Boid::boid_param_float& p );
	Dictionary serialise_param_vec3( Boid::boid_param_vec3& p );
	Dictionary serialise_param_vec4( Boid::boid_param_vec4& p );
	Dictionary serialise_param_ctrl( Boid::boid_param_control& p );
	Dictionary serialise( Boid::boid_behaviour_test* test );
	Dictionary serialise( Boid::boid_behaviour_action* action );

	bool deserialise_param_int( Boid::boid_param_int& p, const Dictionary& d );
	bool deserialise_param_float( Boid::boid_param_float& p, const Dictionary& d );
	bool deserialise_param_vec3( Boid::boid_param_vec3& p, const Dictionary& d );
	bool deserialise_param_vec4( Boid::boid_param_vec4& p, const Dictionary& d );
	bool deserialise_param_ctrl( Boid::boid_param_control& p, const Dictionary& d );
	bool deserialise( Boid::boid_behaviour_test*, const Dictionary& d );
	bool deserialise( Boid::boid_behaviour_action*, const Dictionary& d );

	String json;

	bool emit_behaviour_updated;

protected:

	static void _bind_methods();
	void _notification(int p_what);

public:

	static void get_behaviour_enums( HashMap<String,String>& map );
	static void dump( Boid::boid_behaviour* bb );

	static Boid::boid_behaviour* new_behaviour();

	static void init( Boid::boid_behaviour* b );
	static void init_test( Boid::boid_behaviour_test* test );
	static void clone_test( const Boid::boid_behaviour_test* src, Boid::boid_behaviour_test* dst );
	static void purge_test( Boid::boid_behaviour_test* test );
	void validate_test( Boid::boid_behaviour_test* test, Boid::boid_behaviour_test* next_test );

	static void init_action( Boid::boid_behaviour_action* action );
	static void clone_action( const Boid::boid_behaviour_action* src, Boid::boid_behaviour_action* dst );
	static void purge_action( Boid::boid_behaviour_action* action );
	void validate_action( Boid::boid_behaviour_action* action );

	void serialise( Vector<Boid::boid_behaviour_item>& list, bool do_emit );
	void deserialise( Vector<Boid::boid_behaviour_item>& list );

	static void purge( Boid::boid_behaviour* bb );
	bool load( Boid::boid_behaviour* bb );

	// called by boid system
	void set_control_info( const Vector<Boid::boid_control_info> &list );
	// called by boid behaviour editor
	const Vector<Boid::boid_control_info>& get_control_info() const;

	void set_active( const bool& p_active );
	const bool& is_active() const;

	void set_json( const String& p_json );
	const String& get_json() const;

	BoidBehaviour();
	~BoidBehaviour();

};

#endif /* MODULES_BOIDS_BOID_BEAHVIOUR_H_ */
