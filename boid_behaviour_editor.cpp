/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 * boid_behaviour_editor.cpp
 *
 *  Created on: Mar 16, 2021
 *      Author: frankiezafe
 */

#include "boid_behaviour_editor.h"

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\\\\ STYLES /////////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

static Color panel_bg_color = Color(0,0,0,1);
static Color highlight_color = Color(1,1./255,131./255,1);
static Color active_bg_color = Color(.09,.09,.09,1);
static Color preview_border_color = Color(.23,.23,.23,1);
static Color preview_text_color = Color(.53,.53,.53,1);
static Color fieldname_text_color = Color(.53,.53,.53,1);
static Color inactive_bg_color = Color(.38,0,.2,1);
static Color section_color = Color(.23,.23,.23,1);
static Color error_bg_color = Color(.23,0,0,1);
static Color error_border_color = Color(1,0,0,1);
static Color vector_attr_bg_color = Color(0,0,0,1);

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_main_panel() {
	if ( main_panel.is_null() ) {
		main_panel.instance();
		main_panel->set_draw_center(true);
		main_panel->set_bg_color(panel_bg_color);
		main_panel->set_border_width_all(1);
		main_panel->set_border_color(highlight_color);
		main_panel->set_default_margin(Margin::MARGIN_LEFT, 2);
		main_panel->set_default_margin(Margin::MARGIN_TOP, 2);
		main_panel->set_default_margin(Margin::MARGIN_RIGHT, 2);
		main_panel->set_default_margin(Margin::MARGIN_BOTTOM, 8);
	}
	return main_panel;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_button_normal() {
	if ( button_normal.is_null() ) {
		button_normal.instance();
		button_normal->set_draw_center(false);
		button_normal->set_border_width_all(1);
		button_normal->set_border_color(Color(0,0,0,0));
		button_normal->set_default_margin(Margin::MARGIN_LEFT, 2);
	}
	return button_normal;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_button_hover() {
	if ( button_hover.is_null() ) {
		button_hover.instance();
		button_hover->set_draw_center(false);
		button_hover->set_border_width_all(1);
		button_hover->set_border_color(highlight_color);
		button_hover->set_default_margin(Margin::MARGIN_LEFT, 2);
	}
	return button_hover;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_test_active_panel() {
	if ( test_active_panel.is_null() ) {
		test_active_panel.instance();
		test_active_panel->set_draw_center(false);
		test_active_panel->set_default_margin(Margin::MARGIN_LEFT, 2);
	}
	return test_active_panel;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_test_inactive_panel() {
	if ( test_inactive_panel.is_null() ) {
		test_inactive_panel.instance();
		test_active_panel->set_draw_center(false);
		test_inactive_panel->set_default_margin(Margin::MARGIN_LEFT, 2);
	}
	return test_inactive_panel;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_action_active_panel() {
	if ( action_active_panel.is_null() ) {
		action_active_panel.instance();
		action_active_panel->set_draw_center(false);
		action_active_panel->set_default_margin(Margin::MARGIN_LEFT, 16);
	}
	return action_active_panel;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_action_inactive_panel() {
	if ( action_inactive_panel.is_null() ) {
		action_inactive_panel.instance();
		action_inactive_panel->set_draw_center(true);
		action_inactive_panel->set_bg_color(inactive_bg_color);
		action_inactive_panel->set_default_margin(Margin::MARGIN_LEFT, 16);
	}
	return action_inactive_panel;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_section_panel() {
	if ( section_panel.is_null() ) {
		section_panel.instance();
		section_panel->set_draw_center(true);
		section_panel->set_bg_color(section_color);
		section_panel->set_default_margin(Margin::MARGIN_LEFT, 2);
		section_panel->set_default_margin(Margin::MARGIN_RIGHT, 2);
		section_panel->set_default_margin(Margin::MARGIN_TOP, 5);
		section_panel->set_default_margin(Margin::MARGIN_BOTTOM, 5);
	}
	return section_panel;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_editor_menu() {
	if ( editor_menu.is_null() ) {
		editor_menu.instance();
		editor_menu->set_draw_center(true);
		editor_menu->set_bg_color(section_color);
		editor_menu->set_default_margin(Margin::MARGIN_LEFT, 2);
		editor_menu->set_default_margin(Margin::MARGIN_RIGHT, 2);
		editor_menu->set_default_margin(Margin::MARGIN_TOP, 2);
		editor_menu->set_default_margin(Margin::MARGIN_BOTTOM, 2);
	}
	return editor_menu;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_test_preview() {
	if ( test_preview.is_null() ) {
		test_preview.instance();
		test_preview->set_draw_center(true);
		test_preview->set_bg_color(active_bg_color);
		test_preview->set_default_margin(Margin::MARGIN_LEFT, 5);
		test_preview->set_default_margin(Margin::MARGIN_RIGHT, 5);
		test_preview->set_default_margin(Margin::MARGIN_TOP, 5);
		test_preview->set_default_margin(Margin::MARGIN_BOTTOM, 5);
		test_preview->set_border_color(preview_border_color);
		test_preview->set_border_width_all(1);
	}
	return test_preview;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_error() {
	if ( panel_error.is_null() ) {
		panel_error.instance();
		panel_error->set_draw_center(true);
		panel_error->set_bg_color(error_bg_color);
		panel_error->set_default_margin(Margin::MARGIN_LEFT, 5);
		panel_error->set_default_margin(Margin::MARGIN_RIGHT, 5);
		panel_error->set_default_margin(Margin::MARGIN_TOP, 5);
		panel_error->set_default_margin(Margin::MARGIN_BOTTOM, 5);
		panel_error->set_border_color(error_border_color);
		panel_error->set_border_width_all(1);
	}
	return panel_error;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_test_attribute() {
	if ( test_attribute.is_null() ) {
		test_attribute.instance();
		test_attribute->set_draw_center(false);
		test_attribute->set_bg_color(error_bg_color);
		test_attribute->set_default_margin(Margin::MARGIN_LEFT, 8);
		test_attribute->set_default_margin(Margin::MARGIN_TOP, 4);
		test_attribute->set_border_color(fieldname_text_color);
		test_attribute->set_border_width(Margin::MARGIN_LEFT, 4);
	}
	return test_attribute;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_no_margin() {
	if ( no_margin.is_null() ) {
		no_margin.instance();
		no_margin->set_draw_center(false);
		no_margin->set_default_margin(Margin::MARGIN_LEFT, -1);
		no_margin->set_default_margin(Margin::MARGIN_TOP, -1);
		no_margin->set_default_margin(Margin::MARGIN_RIGHT, -1);
		no_margin->set_default_margin(Margin::MARGIN_BOTTOM, -1);
	}
	return no_margin;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_vector_attr() {
	if ( vector_attr.is_null() ) {
		vector_attr.instance();
		vector_attr->set_draw_center(true);
		vector_attr->set_bg_color(vector_attr_bg_color);
		vector_attr->set_default_margin(Margin::MARGIN_LEFT, 4);
		vector_attr->set_default_margin(Margin::MARGIN_TOP, 4);
		vector_attr->set_default_margin(Margin::MARGIN_RIGHT, 4);
		vector_attr->set_default_margin(Margin::MARGIN_BOTTOM, 4);
	}
	return vector_attr;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_set_active() {
	if ( set_active.is_null() ) {
		set_active.instance();
		set_active->set_draw_center(false);
		set_active->set_border_width(Margin::MARGIN_LEFT, 1);
		set_active->set_border_width(Margin::MARGIN_TOP, 1);
		set_active->set_border_width(Margin::MARGIN_RIGHT, 1);
		set_active->set_border_width(Margin::MARGIN_BOTTOM, 1);
		set_active->set_default_margin(Margin::MARGIN_LEFT, 2);
		set_active->set_default_margin(Margin::MARGIN_TOP, 2);
		set_active->set_default_margin(Margin::MARGIN_RIGHT, 2);
		set_active->set_default_margin(Margin::MARGIN_BOTTOM, 2);
		set_active->set_border_color(section_color);
	}
	return set_active;
}

const Ref<StyleBoxFlat>& BoidBehaviourEditor::style_set_inactive() {
	if ( set_inactive.is_null() ) {
		set_inactive.instance();
		set_inactive->set_draw_center(true);
		set_inactive->set_bg_color(inactive_bg_color);
		set_inactive->set_border_width(Margin::MARGIN_LEFT, 1);
		set_inactive->set_border_width(Margin::MARGIN_TOP, 1);
		set_inactive->set_border_width(Margin::MARGIN_RIGHT, 1);
		set_inactive->set_border_width(Margin::MARGIN_BOTTOM, 1);
		set_inactive->set_default_margin(Margin::MARGIN_LEFT, 2);
		set_inactive->set_default_margin(Margin::MARGIN_TOP, 2);
		set_inactive->set_default_margin(Margin::MARGIN_RIGHT, 2);
		set_inactive->set_default_margin(Margin::MARGIN_BOTTOM, 2);
		set_inactive->set_border_color(inactive_bg_color);
	}
	return set_inactive;
}

void BoidBehaviourEditor::prepare_button( Button* b ) {
	b->set_focus_mode( FocusMode::FOCUS_NONE );
	b->set_pressed(false);
}

void BoidBehaviourEditor::prepare_error( Label* l ) {
	l->set_h_size_flags(SIZE_EXPAND_FILL);
	l->set_autowrap(true);
	l->add_color_override("font_color", error_border_color);
	l->add_style_override("normal", style_error());
}

void BoidBehaviourEditor::prepare_preview( Label* l ) {
	l->set_h_size_flags(SIZE_EXPAND_FILL);
	l->add_color_override("font_color", preview_text_color);
	l->add_style_override("normal", style_test_preview());
}

void BoidBehaviourEditor::prepare_group_checkbox( CheckBox* cb, const int& i ) {

	cb->set_toggle_mode(true);
	cb->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	Color cg;
	if ( get_group_color( i, cg ) ) {
		Ref<StyleBoxFlat> group_style;
		group_style.instance();
		group_style->set_draw_center(true);
		group_style->set_bg_color(cg);
		group_style->set_default_margin(Margin::MARGIN_LEFT, -1);
		group_style->set_default_margin(Margin::MARGIN_TOP, -1);
		group_style->set_default_margin(Margin::MARGIN_RIGHT, -1);
		group_style->set_default_margin(Margin::MARGIN_BOTTOM, -1);
		cb->add_style_override("normal", group_style);
		cb->add_style_override("hover_pressed", group_style);
		cb->add_style_override("disabled", group_style);
		cb->add_style_override("pressed", group_style);
		cb->add_style_override("hover", group_style);
	} else {
		cb->add_style_override("normal", style_no_margin());
		cb->add_style_override("hover_pressed", style_no_margin());
		cb->add_style_override("disabled", style_no_margin());
		cb->add_style_override("pressed", style_no_margin());
		cb->add_style_override("hover", style_no_margin());
	}

}

void BoidBehaviourEditor::prepare_anim_checkbox( CheckBox* cb, const int& i ) {

	cb->set_toggle_mode(true);
	cb->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	Color cg;
	if ( get_anim_color( i, cg ) ) {
		Ref<StyleBoxFlat> group_style;
		group_style.instance();
		group_style->set_draw_center(true);
		group_style->set_bg_color(cg);
		group_style->set_default_margin(Margin::MARGIN_LEFT, -1);
		group_style->set_default_margin(Margin::MARGIN_TOP, -1);
		group_style->set_default_margin(Margin::MARGIN_RIGHT, -1);
		group_style->set_default_margin(Margin::MARGIN_BOTTOM, -1);
		cb->add_style_override("normal", group_style);
		cb->add_style_override("hover_pressed", group_style);
		cb->add_style_override("disabled", group_style);
		cb->add_style_override("pressed", group_style);
		cb->add_style_override("hover", group_style);
	} else {
		cb->add_style_override("normal", style_no_margin());
		cb->add_style_override("hover_pressed", style_no_margin());
		cb->add_style_override("disabled", style_no_margin());
		cb->add_style_override("pressed", style_no_margin());
		cb->add_style_override("hover", style_no_margin());
	}

}

void BoidBehaviourEditor::adjust_item_display( list_item* li ) {

	if ( li->display == NULL ) {
		return;
	}

	if ( li->is_test ) {
		if ( li->test.valid ) {
			li->warning->set_visible(false);
			if ( li == current_item ) {
				test_fields.error_lbl->set_visible(false);
				test_fields.error->set_visible(false);
			}
		} else {
			li->warning->set_visible(true);
			li->warning->set_tooltip( String( li->test.msg.c_str() ) );
			if ( li == current_item  ) {
				test_fields.error_lbl->set_visible(true);
				test_fields.error->set_visible(true);
				test_fields.error->set_text( String( li->test.msg.c_str() ) );
			}
		}
		if ( li == current_item ) {
			regenerate_test_preview();
		}
	}
	if ( li->is_action ) {
		if ( li->action.valid ) {
			li->display->add_style_override("panel", style_action_active_panel());
			li->warning->set_visible(false);
			if ( li == current_item ) {
				action_fields.error_lbl->set_visible(false);
				action_fields.error->set_visible(false);
			}
		} else {
			li->display->add_style_override("panel", style_action_inactive_panel());
			li->warning->set_visible(true);
			li->warning->set_tooltip( String( li->action.msg.c_str() ) );
			if ( li == current_item ) {
				action_fields.error_lbl->set_visible(true);
				action_fields.error->set_visible(true);
				action_fields.error->set_text( String( li->action.msg.c_str() ) );
			}
		}
	}

	regenerate_item_name(li);

}

void BoidBehaviourEditor::regenerate_item_name( list_item* li ) {

	if ( li == NULL ) {
		return;
	}

	String name ="#" + String::num( li->uid ) + " pending...";
	String msg = "";

	if ( li->is_test ) {

		name = "";
		if ( !li->test.active ) {
			name += "//";
		}

		if ( !li->test.valid ) {
			name += "#" + String::num( li->uid ) + " ?";
			msg = "!valid";
		} else {
			name = "";
			if ( !li->test.active ) {
				name += "//";
			}
			name += "?";
		}

		switch( li->test.type ) {
			case Boid::TestType::BTEST_TYPE_ALWAYS_TRUE:
				name += "true ";
				break;
			case Boid::TestType::BTEST_TYPE_ATTRIBUTE:
				switch ( li->test.attr ) {
					case Boid::TestAttribute::BTEST_ATTR_ACTIVE:
					case Boid::TestAttribute::BTEST_ATTR_COLLISION:
					case Boid::TestAttribute::BTEST_ATTR_MOVING:
						name += prepare_for_display( enums["test_attr"][ int( li->test.attr ) ] ) + " ";
						name += prepare_for_display( enums["test_eval"][ int( li->test.eval ) ] ) + " ";
						if ( li->test.param_bool ) {
							name += "true ";
						} else {
							name += "false ";
						}
						break;
					case Boid::TestAttribute::BTEST_ATTR_GROUP:
						name += prepare_for_display( enums["test_attr"][ int( li->test.attr ) ] ) + " ";
						name += prepare_for_display( enums["test_eval"][ int( li->test.eval ) ] ) + " ";
						if ( !li->test.param_int.initialised ) {
							name += "? ";
						} else {
							String groupn;
							for ( int i = 0; i < 20; ++i ) {
								if ( (li->test.param_int.value & 1<<i) != 0 ) {
									if (!groupn.empty()) {
										groupn += ",";
									}
									groupn += get_group_name(i);
								}
							}
							if ( groupn.find(",") != -1 ) {
								groupn = "("+groupn+")";
							}
							name += groupn + " ";
						}
						break;
					case Boid::TestAttribute::BTEST_ATTR_ANIMATION:
						name += prepare_for_display( enums["test_attr"][ int( li->test.attr ) ] ) + " ";
						name += prepare_for_display( enums["test_eval"][ int( li->test.eval ) ] ) + " ";
						if ( !li->test.param_int.initialised ) {
							name += "? ";
						} else {
							String animn;
							for ( int i = 0; i < 20; ++i ) {
								if ( (li->test.param_int.value & 1<<i) != 0 ) {
									if (!animn.empty()) {
										animn += ",";
									}
									animn += get_anim_name(i);
								}
							}
							if ( animn.find(",") != -1 ) {
								animn = "("+animn+")";
							}
							name += animn + " ";
						}
						break;
					case Boid::TestAttribute::BTEST_ATTR_SPEED:
					case Boid::TestAttribute::BTEST_ATTR_WEIGHT:
					case Boid::TestAttribute::BTEST_ATTR_RADIUS:
					case Boid::TestAttribute::BTEST_ATTR_PUSH:
					case Boid::TestAttribute::BTEST_ATTR_TIMER:
					case Boid::TestAttribute::BTEST_ATTR_COUNTER:
					case Boid::TestAttribute::BTEST_ATTR_LIFETIME:
						name += prepare_for_display( enums["test_attr"][ int( li->test.attr ) ] ) + " ";
						name += prepare_for_display( enums["test_eval"][ int( li->test.eval ) ] ) + " ";
						if ( !li->test.param_float.initialised ) {
							name += "? ";
						} else {
							name += String::num( li->test.param_float.value ) + " ";
						}
						break;

					case Boid::TestAttribute::BTEST_ATTR_NONE:
					default:
						break;
				}
				break;
			case Boid::TestType::BTEST_TYPE_RANDOM:
				name += "rand ";
				name += prepare_for_display( enums["test_eval"][ int( li->test.eval ) ] ) + " ";
				if ( !li->test.param_float.initialised ) {
					name += "? ";
				} else {
					name += String::num( li->test.param_float.value ) + " ";
				}
				break;
			case Boid::TestType::BTEST_TYPE_NEIGHBORHOUD:
				name += prepare_for_display( enums["test_attr"][ int( li->test.attr ) ] ) + " ";
				name += prepare_for_display( enums["test_eval"][ int( li->test.eval ) ] ) + " ";
				if ( !li->test.param_int.initialised ) {
					name += "? ";
				} else {
					String groupn;
					for ( int i = 0; i < 20; ++i ) {
						if ( (li->test.param_int.value & 1<<i) != 0 ) {
							if (!groupn.empty()) {
								groupn += ",";
							}
							groupn += get_group_name(i);
						}
					}
					if ( groupn.find(",") != -1 ) {
						groupn = "("+groupn+")";
					}
					name += groupn + " ";
				}
				break;
			case Boid::TestType::BTEST_TYPE_INSIDE:
				name += "in:";
				if ( !li->test.param_ctrl.initialised ) {
					name += "? ";
				} else {
					for( int i = 0, imax = ctrl_infos.size(); i < imax; ++i ) {
						if ( ctrl_infos[i].UID == li->test.param_ctrl.control_UID ) {
							name += ctrl_infos[i].name + " ";
						}
					}
				}
				break;
			case Boid::TestType::BTEST_TYPE_OUTSIDE:
				name += "out:";
				if ( !li->test.param_ctrl.initialised ) {
					name += "? ";
				} else {
					for( int i = 0, imax = ctrl_infos.size(); i < imax; ++i ) {
						if ( ctrl_infos[i].UID == li->test.param_ctrl.control_UID ) {
							name += ctrl_infos[i].name + " ";
						}
					}
				}
				break;
			case Boid::TestType::BTEST_TYPE_EVENT:
				name += "ev:";
				switch ( li->test.event ) {
					case Boid::TestEvent::BTEST_EVENT_BORN:
					case Boid::TestEvent::BTEST_EVENT_TIMER_END:
					case Boid::TestEvent::BTEST_EVENT_MOVING_START:
					case Boid::TestEvent::BTEST_EVENT_MOVING_STOP:
						name += prepare_for_display( enums["test_event"][ int( li->test.event ) ] ) + " ";
						break;
					case Boid::TestEvent::BTEST_EVENT_NONE:
					default:
						name += "? ";
						break;
				}
				break;
			case Boid::TestType::BTEST_TYPE_NEIGHBOR_COUNT:
				name += prepare_for_display( enums["test_type"][ int( li->test.type ) ] ) + " ";
				name += prepare_for_display( enums["test_eval"][ int( li->test.eval ) ] ) + " ";
				if ( !li->test.param_int.initialised ) {
					name += "? ";
				} else {
					name += String::num( li->test.param_int.value ) + " ";
				}
				break;
			case Boid::TestType::BTEST_TYPE_NONE:
			default:
				break;
		}
		switch( li->test.repeat ) {
			case Boid::TestRepeat::BTEST_REPEAT_ONCE:
				name += "[1] ";
				break;
			case Boid::TestRepeat::BTEST_REPEAT_CONTINUOUS:
			default:
				break;
		}
		switch( li->test.after ) {
			case Boid::TestAfter::BTEST_AFTER_AND:
				name += "&&";
				break;
			case Boid::TestAfter::BTEST_AFTER_OR:
				name += "||";
				break;
			case Boid::TestAfter::BTEST_AFTER_EXIT:
				name += "_|";
				break;
			case Boid::TestAfter::BTEST_AFTER_GOTO:
				name += "->" + String::num( li->test.jumpto );
				break;

			default:
				break;
		}

	} else if ( li->is_action ) {

		if ( !li->action.valid ) {
			name = "#" + String::num( li->uid ) + " ";
			msg = "!valid";
		} else {
			name = "";
			if ( !li->test.active ) {
				name += "//";
			}
		}

		switch ( li->action.type ) {

			case Boid::ActionType::BACTION_TYPE_SET_ATTRIBUTE:

				switch( li->action.attr ) {

					case Boid::ActionAttribute::BACTION_ATTR_ACTIVE:
					case Boid::ActionAttribute::BACTION_ATTR_COLLISION:
						name += prepare_for_display( enums["action_attr"][ int( li->action.attr ) ] ) + " @ ";
						if ( li->action.param_bool ) {
							name += "true";
						} else {
							name += "false";
						}
						break;

					case Boid::ActionAttribute::BACTION_ATTR_GROUP:
						name += prepare_for_display( enums["action_attr"][ int( li->action.attr ) ] ) + " @ ";
						if ( !li->action.param_int.initialised ) {
							name += "?";
						} else {
							String groupn;
							for ( int i = 0; i < 20; ++i ) {
								if ( (li->action.param_int.value & 1<<i) != 0 ) {
									if (!groupn.empty()) {
										groupn += ",";
									}
									groupn += get_group_name(i);
								}
							}
							if ( groupn.find(",") != -1 ) {
								groupn = "("+groupn+")";
							}
							name += groupn;
						}
						break;

					case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
						name += prepare_for_display( enums["action_attr"][ int( li->action.attr ) ] ) + " @ ";
						if ( !li->action.param_int.initialised ) {
							name += "?";
						} else {
							String animn;
							for ( int i = 0; i < 20; ++i ) {
								if ( (li->action.param_int.value & 1<<i) != 0 ) {
									if (!animn.empty()) {
										animn += ",";
									}
									animn += get_anim_name(i);
								}
							}
							if ( animn.find(",") != -1 ) {
								animn = "("+animn+")";
							}
							name += animn;
						}
						break;


					case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
					case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
					case Boid::ActionAttribute::BACTION_ATTR_COHESION:
					case Boid::ActionAttribute::BACTION_ATTR_STEER:
					case Boid::ActionAttribute::BACTION_ATTR_SPEED:
					case Boid::ActionAttribute::BACTION_ATTR_PUSH_SPEED:
					case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
					case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
					case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
					case Boid::ActionAttribute::BACTION_ATTR_FRICTION:
					case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
					case Boid::ActionAttribute::BACTION_ATTR_TIMER:
						name += prepare_for_display( enums["action_attr"][ int( li->action.attr ) ] ) + " @ ";
						if ( !li->action.param_float.initialised ) {
							name += "?";
						} else {
							name += String::num( li->action.param_float.value ) + " ";
						}
						break;

					case Boid::ActionAttribute::BACTION_ATTR_AIM:
					case Boid::ActionAttribute::BACTION_ATTR_POSITION:
					case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
					case Boid::ActionAttribute::BACTION_ATTR_FRONT:
					case Boid::ActionAttribute::BACTION_ATTR_PUSH:
						name += prepare_for_display( enums["action_attr"][ int( li->action.attr ) ] ) + " @ ";
						if ( !li->action.param_vec3.initialised ) {
							name += "?";
						} else {
							name += "("+ String::num( li->action.param_vec3.value[0] ) + ",";
							name += String::num( li->action.param_vec3.value[1] ) + ",";
							name += String::num( li->action.param_vec3.value[2] ) + ")";
						}
						break;

					case Boid::ActionAttribute::BACTION_ATTR_COLOR:
						name += prepare_for_display( enums["action_attr"][ int( li->action.attr ) ] ) + " @ ";
						if ( !li->action.param_vec4.initialised ) {
							name += "?";
						} else {
							name += "("+ String::num( li->action.param_vec4.value[0] ) + ",";
							name += String::num( li->action.param_vec4.value[1] ) + ",";
							name += String::num( li->action.param_vec4.value[2] ) + ",";
							name += String::num( li->action.param_vec4.value[3] ) + ")";
						}
						break;

					case Boid::ActionAttribute::BACTION_ATTR_NONE:
					default:
						break;

				}

				break;

			case Boid::ActionType::BACTION_TYPE_INCREMENT_ATTRIBUTE:

				switch( li->action.attr ) {
					case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
					case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
					case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
					case Boid::ActionAttribute::BACTION_ATTR_COHESION:
					case Boid::ActionAttribute::BACTION_ATTR_STEER:
					case Boid::ActionAttribute::BACTION_ATTR_SPEED:
					case Boid::ActionAttribute::BACTION_ATTR_PUSH_SPEED:
					case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
					case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
					case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
					case Boid::ActionAttribute::BACTION_ATTR_FRICTION:
					case Boid::ActionAttribute::BACTION_ATTR_TIMER:
					case Boid::ActionAttribute::BACTION_ATTR_LIFETIME:
						name += prepare_for_display( enums["action_attr"][ int( li->action.attr ) ] );
						if ( li->action.type == Boid::ActionType::BACTION_TYPE_INCREMENT_ATTRIBUTE ) {
							name += "++";
						} else {
							name += "--";
						}
						break;

					case Boid::ActionAttribute::BACTION_ATTR_NONE:
					case Boid::ActionAttribute::BACTION_ATTR_ACTIVE:
					case Boid::ActionAttribute::BACTION_ATTR_COLLISION:
					case Boid::ActionAttribute::BACTION_ATTR_GROUP:
					case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
					case Boid::ActionAttribute::BACTION_ATTR_AIM:
					case Boid::ActionAttribute::BACTION_ATTR_COLOR:
					case Boid::ActionAttribute::BACTION_ATTR_POSITION:
					case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
					case Boid::ActionAttribute::BACTION_ATTR_FRONT:
					case Boid::ActionAttribute::BACTION_ATTR_PUSH:
					default:
						break;

				}

				break;

			case Boid::ActionType::BACTION_TYPE_RANDOMISE_ATTRIBUTE:
				switch( li->action.attr ) {

					case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
					case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
					case Boid::ActionAttribute::BACTION_ATTR_COHESION:
					case Boid::ActionAttribute::BACTION_ATTR_STEER:
					case Boid::ActionAttribute::BACTION_ATTR_SPEED:
					case Boid::ActionAttribute::BACTION_ATTR_PUSH_SPEED:
					case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
					case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
					case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
					case Boid::ActionAttribute::BACTION_ATTR_FRICTION:
					case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
					case Boid::ActionAttribute::BACTION_ATTR_TIMER:
					case Boid::ActionAttribute::BACTION_ATTR_LIFETIME:
						name += prepare_for_display( enums["action_attr"][ int( li->action.attr ) ] ) + "=";
						if ( !li->action.param_float.initialised ) {
							name += "?";
						} else {
							name += "rand(" + String::num( li->action.param_float.min ) + "," + String::num( li->action.param_float.max ) + ")";
						}
						break;

					case Boid::ActionAttribute::BACTION_ATTR_AIM:
					case Boid::ActionAttribute::BACTION_ATTR_POSITION:
					case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
					case Boid::ActionAttribute::BACTION_ATTR_FRONT:
					case Boid::ActionAttribute::BACTION_ATTR_PUSH:
						name += prepare_for_display( enums["action_attr"][ int( li->action.attr ) ] ) + " @ ";
						if ( !li->action.param_vec3.initialised ) {
							name += "?";
						} else {
							name += "rand(("+ String::num( li->action.param_vec3.min[0] ) + ",";
							name += String::num( li->action.param_vec3.min[1] ) + ",";
							name += String::num( li->action.param_vec3.min[2] ) + "),";
							name += "("+ String::num( li->action.param_vec3.max[0] ) + ",";
							name += String::num( li->action.param_vec3.max[1] ) + ",";
							name += String::num( li->action.param_vec3.max[2] ) + "))";
						}
						break;

					case Boid::ActionAttribute::BACTION_ATTR_COLOR:
						name += prepare_for_display( enums["action_attr"][ int( li->action.attr ) ] ) + " @ ";
						if ( !li->action.param_vec4.initialised ) {
							name += "?";
						} else {
							name += "rand(("+ String::num( li->action.param_vec4.min[0] ) + ",";
							name += String::num( li->action.param_vec4.min[1] ) + ",";
							name += String::num( li->action.param_vec4.min[2] ) + ",";
							name += String::num( li->action.param_vec4.min[3] ) + "),";
							name += "("+ String::num( li->action.param_vec4.max[0] ) + ",";
							name += String::num( li->action.param_vec4.max[1] ) + ",";
							name += String::num( li->action.param_vec4.max[2] ) + ",";
							name += String::num( li->action.param_vec4.max[3] ) + "))";
						}
						break;

					case Boid::ActionAttribute::BACTION_ATTR_NONE:
					case Boid::ActionAttribute::BACTION_ATTR_ACTIVE:
					case Boid::ActionAttribute::BACTION_ATTR_COLLISION:
					case Boid::ActionAttribute::BACTION_ATTR_GROUP:
					case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
					default:
						break;
				}
				break;

			case Boid::ActionType::BACTION_TYPE_ASSIGN_TARGET:
				name += "to:";
				if ( !li->action.param_ctrl.initialised ) {
					name += "? ";
				} else {
					for( int i = 0, imax = ctrl_infos.size(); i < imax; ++i ) {
						if ( ctrl_infos[i].UID == li->action.param_ctrl.control_UID ) {
							name += ctrl_infos[i].name + " ";
						}
					}
				}
				break;

			case Boid::ActionType::BACTION_TYPE_AIM_AT:
			case Boid::ActionType::BACTION_TYPE_REACH:
			case Boid::ActionType::BACTION_TYPE_ESCAPE:

				if ( li->action.type == Boid::ActionType::BACTION_TYPE_AIM_AT ) {
					name += "aim:";
				} else if ( li->action.type == Boid::ActionType::BACTION_TYPE_REACH ) {
					name += "reach:";
				} else if ( li->action.type == Boid::ActionType::BACTION_TYPE_ESCAPE ) {
					name += "esc:";
				}

				if ( !li->action.param_int.initialised ) {
					name += "?";
				} else {
					String groupn;
					for ( int i = 0; i < 20; ++i ) {
						if ( (li->action.param_int.value & 1<<i) != 0 ) {
							if (!groupn.empty()) {
								groupn += ",";
							}
							groupn += get_group_name(i);
						}
					}
					if ( groupn.find(",") != -1 ) {
						groupn = "("+groupn+")";
					}
					name += groupn + " ";
				}
				if ( li->action.param_float.initialised ) {
					name += "*" + String::num( li->action.param_float.value ) + " ";
				}
				if ( li->action.param_bool ) {
					name += "+ center";
				}
				break;

			case Boid::ActionType::BACTION_TYPE_COUPLE:
				name += "cpl:";
				if ( !li->action.param_int.initialised ) {
					name += "?";
				} else {
					String groupn;
					for ( int i = 0; i < 20; ++i ) {
						if ( (li->action.param_int.value & 1<<i) != 0 ) {
							if (!groupn.empty()) {
								groupn += ",";
							}
							groupn += get_group_name(i);
						}
					}
					if ( groupn.find(",") != -1 ) {
						groupn = "("+groupn+")";
					}
					name += groupn + " ";
				}
				break;

			case Boid::ActionType::BACTION_TYPE_LEAVE:
				name += "bye_bye";
				break;

			case Boid::ActionType::BACTION_TYPE_SIGNAL:
				name += "sig:";
				if ( !li->action.param_int.initialised ) {
					name += "?";
				} else {
					name += String::num(li->action.param_int.value);
				}
				break;

			case Boid::ActionType::BACTION_TYPE_NONE:
			default:
				break;

		}

	}

	if ( !li->action.active ) {
		name = "//" + name;
		if ( !msg.empty() ) {
			msg += "\n";
		}
		msg = "inactive";
	}

	li->name_btn->set_text( name );
	li->name_btn->set_tooltip( msg );

}

void BoidBehaviourEditor::prepare_vectr_atrr( Label* l, int axis ) {
	switch ( axis ) {
		case 0:
			l->add_color_override("font_color", Color(1,0,0,1));
			break;
		case 1:
			l->add_color_override("font_color", Color(0,1,0,1));
			break;
		case 2:
			l->add_color_override("font_color", Color(.2,.4,1,1));
			break;
		default:
			break;
	}
	l->add_style_override("normal", style_vector_attr());
}

String BoidBehaviourEditor::prepare_for_display( const String& s ) {

	String out = s.replace( " " , "");
	int start = 0;
	while ( out.find("[", start ) != -1 ) {
		start = out.find("[", start);
		int end = out.find("]", start);
		if ( end == -1 ) {
			s.replace( "[" , "");
			break;
		} else {
			end++;
			out = out.substr(0, start) + out.substr(end, out.size()-end);
			start = end;
		}
	}
	return out;

}

String BoidBehaviourEditor::get_group_name( const int& i ) {

	String name;

	if (ProjectSettings::get_singleton()->has_setting("layer_names/boid_group/layer_" + itos(i + 1) + "_name")) {
		name = ProjectSettings::get_singleton()->get("layer_names/boid_group/layer_" + itos(i + 1) + "_name");
	}

	if ( name.empty() ){
		name = String::num(i+1);
	}

	return name;

}

bool BoidBehaviourEditor::get_group_color( const int& i, Color& c ) {

	Color tmp;
	if (ProjectSettings::get_singleton()->has_setting("layer_names/boid_group/layer_" + itos(i + 1) + "_color")) {
		tmp = ProjectSettings::get_singleton()->get("layer_names/boid_group/layer_" + itos(i + 1) + "_color");
	}
	if ( tmp == Color(0,0,0,1) ) {
		return false;
	}
	c = tmp;
	return true;

}

String BoidBehaviourEditor::get_anim_name( const int& i ) {

	String name;

	if (ProjectSettings::get_singleton()->has_setting("layer_names/boid_animation/layer_" + itos(i + 1) + "_name")) {
		name = ProjectSettings::get_singleton()->get("layer_names/boid_animation/layer_" + itos(i + 1) + "_name");
	}

	if ( name.empty() ){
		name = String::num(i+1);
	}

	return name;

}

bool BoidBehaviourEditor::get_anim_color( const int& i, Color& c ) {

	Color tmp;
	if (ProjectSettings::get_singleton()->has_setting("layer_names/boid_animation/layer_" + itos(i + 1) + "_color")) {
		tmp = ProjectSettings::get_singleton()->get("layer_names/boid_animation/layer_" + itos(i + 1) + "_color");
	}
	if ( tmp == Color(0,0,0,1) ) {
		return false;
	}
	c = tmp;
	return true;

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\\\\ EDITORS ////////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidBehaviourEditor::prepare_fieldname( Label* l, const String& text ) {
	l->set_align(Label::ALIGN_LEFT);
	l->set_valign(Label::VALIGN_CENTER);
	l->add_color_override("font_color", fieldname_text_color);
	l->set_h_size_flags(SIZE_FILL);
	l->set_v_size_flags(SIZE_FILL);
	l->set_clip_contents(true);
	l->set_text( text );
}

void BoidBehaviourEditor::prepare_attrname( Label* l, const String& text ) {
	l->set_align(Label::ALIGN_LEFT);
	l->set_valign(Label::VALIGN_TOP);
	l->add_color_override("font_color", fieldname_text_color);
	l->set_h_size_flags(SIZE_FILL);
	l->set_v_size_flags(SIZE_FILL);
	l->add_style_override("normal", style_test_attribute());
	l->set_clip_contents(true);
	l->set_text( text );

}

void BoidBehaviourEditor::add_test_fieldname( Label* l, const String& text ) {
	prepare_fieldname( l, text );
	test_fields.form->add_child(l);
}

void BoidBehaviourEditor::add_test_attrname( Label* l, const String& text ) {
	prepare_attrname( l, text );
	test_fields.form->add_child(l);
}

void BoidBehaviourEditor::add_action_fieldname( Label* l, const String& text ) {
	prepare_fieldname( l, text );
	action_fields.form->add_child(l);
}

void BoidBehaviourEditor::add_action_attrname( Label* l, const String& text ) {
	prepare_attrname( l, text );
	action_fields.form->add_child(l);
}

void BoidBehaviourEditor::edit_close_all() {
	current_item = NULL;
	load_test_editor();
	load_action_editor();
}

//\\\\\\\\\\\\\\\\\\ TEST EDITOR

void BoidBehaviourEditor::build_test_editor( Control* parent ) {

	test_fields.panel = memnew(VBoxContainer);
	test_fields.panel->set_h_size_flags(SIZE_EXPAND_FILL);
	test_fields.panel->set_visible(false);
	parent->add_child(test_fields.panel);

	// reusables
	Label* lbl;
	ToolButton* tb;

	// menu
	PanelContainer* menu_wrp = memnew(PanelContainer);
	menu_wrp->add_style_override("panel", style_editor_menu());
	test_fields.panel->add_child(menu_wrp);

	HBoxContainer* menu =  memnew(HBoxContainer);
	menu->add_constant_override("separation",0);
	menu_wrp->add_child(menu);

	TextureRect* icon = memnew(TextureRect);
	icon->set_texture(enode->get_gui_base()->get_icon("BoidTest", "EditorIcons"));
	menu->add_child(icon);

	lbl = memnew(Label);
	lbl->set_text("BoidTest");
	lbl->set_h_size_flags(SIZE_EXPAND_FILL);
	menu->add_child(lbl);

	test_fields.active = memnew(CheckBox);
	prepare_button( test_fields.active );
	test_fields.active->connect("pressed",this, "edit_test_active");
	menu->add_child(test_fields.active);

	test_fields.clone = memnew(ToolButton);
	prepare_button( test_fields.clone );
	test_fields.clone->set_icon(enode->get_gui_base()->get_icon("Duplicate", "EditorIcons"));
	test_fields.clone->set_tooltip("Duplicate current test");
	test_fields.clone->connect("pressed",this, "edit_test_clone");
	menu->add_child(test_fields.clone);

	test_fields.del = memnew(ToolButton);
	prepare_button( test_fields.del );
	test_fields.del->set_icon(enode->get_gui_base()->get_icon("Remove", "EditorIcons"));
	test_fields.del->set_tooltip("Duplicate current test");
	test_fields.del->connect("pressed",this, "edit_test_delete");
	menu->add_child(test_fields.del);

	tb = memnew(ToolButton);
	prepare_button( tb );
	tb->set_icon(enode->get_gui_base()->get_icon("GuiClose", "EditorIcons"));
	tb->set_tooltip("Close test editor");
	tb->connect("pressed",this, "edit_close_all");
	menu->add_child(tb);

	// errors
	test_fields.error_lbl = memnew(Label);
	test_fields.error_lbl->set_text("errors");
	test_fields.panel->add_child(test_fields.error_lbl);
	test_fields.error = memnew(Label);
	prepare_error( test_fields.error );
	test_fields.error->set_text("all cool");
	test_fields.panel->add_child(test_fields.error);

	// preview
	lbl = memnew(Label);
	lbl->set_text("preview");
	lbl->set_h_size_flags(SIZE_EXPAND_FILL);
	test_fields.panel->add_child(lbl);

	ScrollContainer* preview_wrp = memnew(ScrollContainer);
	preview_wrp->set_h_size_flags(SIZE_EXPAND_FILL);
	preview_wrp->set_enable_h_scroll(true);
	preview_wrp->set_enable_v_scroll(false);
	test_fields.panel->add_child(preview_wrp);
	test_fields.preview = memnew(Label);
	prepare_preview( test_fields.preview );
	test_fields.preview->set_text("if ... ");
	preview_wrp->add_child(test_fields.preview);

	// fields
	test_fields.form = memnew(GridContainer);
	test_fields.form->set_columns(2);
	test_fields.panel->add_child(test_fields.form);

	// type
	test_fields.type_lbl = memnew(Label);
	add_test_fieldname( test_fields.type_lbl, "type" );
	test_fields.type = memnew(OptionButton);
	test_fields.type->set_h_size_flags(SIZE_EXPAND_FILL);
	test_fields.type->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	for ( int i = 0, imax = enums["test_type"].size(); i < imax; ++i ) {
		test_fields.type->add_item(enums["test_type"][i]);
	}
	test_fields.type->connect("item_selected",this, "edit_test_type");
	test_fields.form->add_child(test_fields.type);

	// attribute
	test_fields.attr_lbl = memnew(Label);
	add_test_attrname( test_fields.attr_lbl, "attr" );
	test_fields.attr = memnew(OptionButton);
	test_fields.attr->set_h_size_flags(SIZE_EXPAND_FILL);
	test_fields.attr->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	for ( int i = 0, imax = enums["test_attr"].size(); i < imax; ++i ) {
		test_fields.attr->add_item(enums["test_attr"][i]);
	}
	test_fields.attr->connect("item_selected",this, "edit_test_attribute");
	test_fields.form->add_child(test_fields.attr);

	// eval
	test_fields.eval_lbl = memnew(Label);
	add_test_fieldname( test_fields.eval_lbl, "eval" );
	test_fields.eval = memnew(OptionButton);
	test_fields.eval->set_h_size_flags(SIZE_EXPAND_FILL);
	test_fields.eval->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	for ( int i = 0, imax = enums["test_eval"].size(); i < imax; ++i ) {
		test_fields.eval->add_item(enums["test_eval"][i]);
	}
	test_fields.eval->connect("item_selected",this, "edit_test_eval");
	test_fields.form->add_child(test_fields.eval);

	// attr_bool
	test_fields.attr_bool_lbl = memnew(Label);
	add_test_attrname( test_fields.attr_bool_lbl, "boolean" );
	test_fields.attr_bool = memnew(CheckBox);
	test_fields.attr_bool->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	test_fields.attr_bool->connect("pressed",this, "edit_test_attr_bool");
	test_fields.form->add_child(test_fields.attr_bool);

	// attr_int
	test_fields.attr_int_lbl = memnew(Label);
	add_test_attrname( test_fields.attr_int_lbl, "int" );
	test_fields.attr_int = memnew(SpinBox);
	test_fields.attr_int->set_h_size_flags(SIZE_EXPAND_FILL);
	test_fields.attr_int->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	test_fields.attr_int->set_step(1);
	test_fields.attr_int->set_value(0);
	test_fields.attr_int->set_allow_greater(true);
	test_fields.attr_int->set_allow_lesser(true);
	test_fields.attr_int->connect("value_changed",this, "edit_test_attr_int");
	test_fields.form->add_child(test_fields.attr_int);

	// attr_group
	test_fields.attr_group_lbl = memnew(Label);
	add_test_attrname( test_fields.attr_group_lbl, "group" );
	test_fields.attr_group = memnew(GridContainer);
	test_fields.attr_group->set_h_size_flags(SIZE_FILL);
	test_fields.attr_group->add_constant_override("vseparation",0);
	test_fields.attr_group->add_constant_override("hseparation",0);
	test_fields.attr_group->set_columns(10);
	for (int i = 0; i < 20; i++) {
		test_fields.attr_group_checks[i] = memnew(CheckBox);
		test_fields.attr_group_checks[i]->set_pressed( false );
		test_fields.attr_group_checks[i]->connect("pressed", this, "edit_test_group", varray(i));
		prepare_group_checkbox( test_fields.attr_group_checks[i], i );
		test_fields.attr_group_checks[i]->set_tooltip("Group " + get_group_name(i));
		test_fields.attr_group->add_child(test_fields.attr_group_checks[i]);
	}
	test_fields.attr_group->set_size(Vector2(0,0), false);
	test_fields.form->add_child(test_fields.attr_group);

	// attr_anim
	test_fields.attr_anim_lbl = memnew(Label);
	add_test_attrname( test_fields.attr_anim_lbl, "animation" );
	test_fields.attr_anim = memnew(GridContainer);
	test_fields.attr_anim->set_h_size_flags(SIZE_FILL);
	test_fields.attr_anim->add_constant_override("vseparation",0);
	test_fields.attr_anim->add_constant_override("hseparation",0);
	test_fields.attr_anim->set_columns(10);
	for (int i = 0; i < 20; i++) {
		test_fields.attr_anim_checks[i] = memnew(CheckBox);
		test_fields.attr_anim_checks[i]->set_pressed( false );
		test_fields.attr_anim_checks[i]->connect("pressed", this, "edit_test_group", varray(i));
		prepare_anim_checkbox( test_fields.attr_anim_checks[i], i );
		test_fields.attr_anim_checks[i]->set_tooltip("Animation " + get_anim_name(i));
		test_fields.attr_anim->add_child(test_fields.attr_anim_checks[i]);
	}
	test_fields.attr_anim->set_size(Vector2(0,0), false);
	test_fields.form->add_child(test_fields.attr_anim);

	// attr_float
	test_fields.attr_float_lbl = memnew(Label);
	add_test_attrname( test_fields.attr_float_lbl, "float" );
	test_fields.attr_float = memnew(SpinBox);
	test_fields.attr_float->set_h_size_flags(SIZE_EXPAND_FILL);
	test_fields.attr_float->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	test_fields.attr_float->set_step(0.01);
	test_fields.attr_float->set_value(0);
	test_fields.attr_float->set_allow_greater(true);
	test_fields.attr_float->set_allow_lesser(true);
	test_fields.attr_float->connect("value_changed",this, "edit_test_attr_float");
	test_fields.form->add_child(test_fields.attr_float);

	// attr_control
	test_fields.attr_ctrl_lbl = memnew(Label);
	add_test_attrname( test_fields.attr_ctrl_lbl, "control" );
	test_fields.attr_ctrl = memnew(OptionButton);
	test_fields.attr_ctrl->set_h_size_flags(SIZE_EXPAND_FILL);
	test_fields.attr_ctrl->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	test_fields.attr_ctrl->connect("item_selected",this, "edit_test_attr_ctrl");
	test_fields.form->add_child(test_fields.attr_ctrl);

	// attr_event
	test_fields.attr_event_lbl = memnew(Label);
	add_test_attrname( test_fields.attr_event_lbl, "event" );
	test_fields.attr_event = memnew(OptionButton);
	test_fields.attr_event->set_h_size_flags(SIZE_EXPAND_FILL);
	test_fields.attr_event->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	for ( int i = 0, imax = enums["test_event"].size(); i < imax; ++i ) {
		test_fields.attr_event->add_item(enums["test_event"][i]);
	}
	test_fields.attr_event->connect("item_selected",this, "edit_test_attr_event");
	test_fields.form->add_child(test_fields.attr_event);

	// repeat
	test_fields.repeat_lbl = memnew(Label);
	add_test_fieldname( test_fields.repeat_lbl, "repeat" );
	test_fields.repeat = memnew(OptionButton);
	test_fields.repeat->set_h_size_flags(SIZE_EXPAND_FILL);
	test_fields.repeat->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	for ( int i = 0, imax = enums["test_repeat"].size(); i < imax; ++i ) {
		test_fields.repeat->add_item(enums["test_repeat"][i]);
	}
	test_fields.repeat->connect("item_selected",this, "edit_test_repeat");
	test_fields.form->add_child(test_fields.repeat);

	// after
	test_fields.after_lbl = memnew(Label);
	add_test_fieldname( test_fields.after_lbl, "after" );
	test_fields.after = memnew(OptionButton);
	test_fields.after->set_h_size_flags(SIZE_EXPAND_FILL);
	test_fields.after->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	for ( int i = 0, imax = enums["test_after"].size(); i < imax; ++i ) {
		test_fields.after->add_item(enums["test_after"][i]);
	}
	test_fields.after->connect("item_selected",this, "edit_test_after");
	test_fields.form->add_child(test_fields.after);

	// attr_goto
	test_fields.attr_goto_lbl = memnew(Label);
	add_test_attrname( test_fields.attr_goto_lbl, "goto" );
	test_fields.attr_goto = memnew(SpinBox);
	test_fields.attr_goto->set_h_size_flags(SIZE_EXPAND_FILL);
	test_fields.attr_goto->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	test_fields.attr_goto->set_step(1);
	test_fields.attr_goto->set_value(0);
	test_fields.attr_goto->set_allow_greater(true);
	test_fields.attr_goto->set_allow_lesser(true);
	test_fields.attr_goto->connect("value_changed",this, "edit_test_attr_goto");
	test_fields.form->add_child(test_fields.attr_goto);

}

void BoidBehaviourEditor::load_test_editor() {

	if ( current_item == NULL || !current_item->is_test ) {
		test_fields.panel->set_visible(false);
		return;
	}

	test_fields.panel->set_visible(true);
	adjust_test_editor();

}

void BoidBehaviourEditor::adjust_test_editor() {

	if ( boid_behaviour == NULL || current_item == NULL || !current_item->is_test ) {
		return;
	}

	int next_i = get_next_index( current_item->uid );
	if ( next_i == -1 ) {
		boid_behaviour->validate_test( &current_item->test, NULL );
	} else {
		list_item* next = list_items[next_i];
		boid_behaviour->validate_test( &current_item->test, &next->test );
	}

	adjust_item_display( current_item );

	while ( test_fields.form->get_child_count() > 0 ) {
		test_fields.form->remove_child( test_fields.form->get_child(0) );
	}
	
	// synching fields
	test_fields.active->set_pressed( current_item->test.active );
	test_fields.error_lbl->set_visible( !current_item->test.valid );
	test_fields.error->set_visible( !current_item->test.valid );
	test_fields.error->set_text( String( current_item->test.msg.c_str() ) );
	test_fields.type->select( int(current_item->test.type) );
	test_fields.attr->select( int(current_item->test.attr) );
	test_fields.eval->select( int(current_item->test.eval) );
	test_fields.attr_bool->set_pressed( current_item->test.param_bool );
	test_fields.attr_event->select( int(current_item->test.event) );
	test_fields.repeat->select( int(current_item->test.repeat) );
	test_fields.after->select( int(current_item->test.after) );
	test_fields.attr_goto->set_value( int(current_item->test.jumpto) );

	if ( current_item->test.param_int.initialised ) {
		if ( current_item->test.type == Boid::TestType::BTEST_TYPE_ATTRIBUTE ) {
			switch ( current_item->test.attr ) {
				case Boid::TestAttribute::BTEST_ATTR_GROUP:
					for ( int i = 0; i < 20; ++i ) {
						test_fields.attr_group_checks[i]->set_disabled(false);
						test_fields.attr_group_checks[i]->set_pressed((current_item->test.param_int.value & (1<<i)) != 0);
					}
					break;
				case Boid::TestAttribute::BTEST_ATTR_ANIMATION:
					for ( int i = 0; i < 20; ++i ) {
						test_fields.attr_anim_checks[i]->set_disabled(false);
						test_fields.attr_anim_checks[i]->set_pressed((current_item->test.param_int.value & (1<<i)) != 0);
					}
					break;
				default:
					break;
			}
		} else if ( current_item->test.type == Boid::TestType::BTEST_TYPE_NEIGHBORHOUD ) {
			for ( int i = 0; i < 20; ++i ) {
				test_fields.attr_group_checks[i]->set_disabled(false);
				test_fields.attr_group_checks[i]->set_pressed((current_item->test.param_int.value & (1<<i)) != 0);
			}
		}
	} else {
		for ( int i = 0; i < 20; ++i ) {
			test_fields.attr_group_checks[i]->set_pressed(false);
			test_fields.attr_group_checks[i]->set_disabled(true);
		}
	}

	if ( current_item->test.param_int.initialised ) {
		test_fields.attr_int->set_value( current_item->test.param_int.value );
	}

	if ( current_item->test.param_float.initialised ) {
		test_fields.attr_float->set_value( current_item->test.param_float.value );
	}

	if ( current_item->test.param_ctrl.initialised ) {
		test_fields.attr_ctrl->select( 0 );
		for ( int i = 0, imax = ctrl_infos.size(); i < imax; ++i ) {
			if ( current_item->test.param_ctrl.control_UID == ctrl_infos[i].UID ) {
				test_fields.attr_ctrl->select( i+1 );
			}
		}
	}

	// building form
	test_fields.form->add_child(test_fields.type_lbl);
	test_fields.form->add_child(test_fields.type);
	
	switch ( current_item->test.type ) {
		case Boid::TestType::BTEST_TYPE_NONE:
			break;
		case Boid::TestType::BTEST_TYPE_ALWAYS_TRUE:
			test_fields.form->add_child(test_fields.repeat_lbl);
			test_fields.form->add_child(test_fields.repeat);
			test_fields.form->add_child(test_fields.after_lbl);
			test_fields.form->add_child(test_fields.after);
			break;
		case Boid::TestType::BTEST_TYPE_ATTRIBUTE:
			test_fields.form->add_child(test_fields.attr_lbl);
			test_fields.form->add_child(test_fields.attr);
			test_fields.form->add_child(test_fields.eval_lbl);
			test_fields.form->add_child(test_fields.eval);
			switch ( current_item->test.attr ) {
				case Boid::TestAttribute::BTEST_ATTR_ACTIVE:
				case Boid::TestAttribute::BTEST_ATTR_COLLISION:
				case Boid::TestAttribute::BTEST_ATTR_MOVING:
					test_fields.form->add_child(test_fields.attr_bool_lbl);
					test_fields.form->add_child(test_fields.attr_bool);
					break;
				case Boid::TestAttribute::BTEST_ATTR_GROUP:
					test_fields.form->add_child(test_fields.attr_group_lbl);
					test_fields.form->add_child(test_fields.attr_group);
					break;
				case Boid::TestAttribute::BTEST_ATTR_ANIMATION:
					test_fields.form->add_child(test_fields.attr_anim_lbl);
					test_fields.form->add_child(test_fields.attr_anim);
					break;
				case Boid::TestAttribute::BTEST_ATTR_SPEED:
				case Boid::TestAttribute::BTEST_ATTR_WEIGHT:
				case Boid::TestAttribute::BTEST_ATTR_RADIUS:
				case Boid::TestAttribute::BTEST_ATTR_PUSH:
				case Boid::TestAttribute::BTEST_ATTR_COUNTER:
				case Boid::TestAttribute::BTEST_ATTR_TIMER:
				case Boid::TestAttribute::BTEST_ATTR_LIFETIME:
					test_fields.form->add_child(test_fields.attr_float_lbl);
					test_fields.form->add_child(test_fields.attr_float);
					break;
				case Boid::TestAttribute::BTEST_ATTR_NONE:
				default:
					break;
			}
			test_fields.form->add_child(test_fields.repeat_lbl);
			test_fields.form->add_child(test_fields.repeat);
			test_fields.form->add_child(test_fields.after_lbl);
			test_fields.form->add_child(test_fields.after);
			break;
		case Boid::TestType::BTEST_TYPE_RANDOM:
			test_fields.form->add_child(test_fields.eval_lbl);
			test_fields.form->add_child(test_fields.eval);
			test_fields.form->add_child(test_fields.attr_float_lbl);
			test_fields.form->add_child(test_fields.attr_float);
			test_fields.form->add_child(test_fields.repeat_lbl);
			test_fields.form->add_child(test_fields.repeat);
			test_fields.form->add_child(test_fields.after_lbl);
			test_fields.form->add_child(test_fields.after);
			break;
		case Boid::TestType::BTEST_TYPE_NEIGHBORHOUD:
			test_fields.form->add_child(test_fields.eval_lbl);
			test_fields.form->add_child(test_fields.eval);
			test_fields.form->add_child(test_fields.attr_group_lbl);
			test_fields.form->add_child(test_fields.attr_group);
			test_fields.form->add_child(test_fields.repeat_lbl);
			test_fields.form->add_child(test_fields.repeat);
			test_fields.form->add_child(test_fields.after_lbl);
			test_fields.form->add_child(test_fields.after);
			break;
		case Boid::TestType::BTEST_TYPE_INSIDE:
		case Boid::TestType::BTEST_TYPE_OUTSIDE:
			test_fields.form->add_child(test_fields.attr_ctrl_lbl);
			test_fields.form->add_child(test_fields.attr_ctrl);
			test_fields.form->add_child(test_fields.repeat_lbl);
			test_fields.form->add_child(test_fields.repeat);
			test_fields.form->add_child(test_fields.after_lbl);
			test_fields.form->add_child(test_fields.after);
			break;
		case Boid::TestType::BTEST_TYPE_EVENT:
			test_fields.form->add_child(test_fields.attr_event_lbl);
			test_fields.form->add_child(test_fields.attr_event);
			test_fields.form->add_child(test_fields.repeat_lbl);
			test_fields.form->add_child(test_fields.repeat);
			test_fields.form->add_child(test_fields.after_lbl);
			test_fields.form->add_child(test_fields.after);
			break;
		case Boid::TestType::BTEST_TYPE_NEIGHBOR_COUNT:
			test_fields.form->add_child(test_fields.eval_lbl);
			test_fields.form->add_child(test_fields.eval);
			test_fields.form->add_child(test_fields.attr_int_lbl);
			test_fields.form->add_child(test_fields.attr_int);
			test_fields.form->add_child(test_fields.repeat_lbl);
			test_fields.form->add_child(test_fields.repeat);
			test_fields.form->add_child(test_fields.after_lbl);
			test_fields.form->add_child(test_fields.after);
			break;
		default:
			break;
	}

	if ( current_item->test.type != Boid::TestType::BTEST_TYPE_NONE ) {
		switch ( current_item->test.after ) {
			case Boid::TestAfter::BTEST_AFTER_NONE:
			case Boid::TestAfter::BTEST_AFTER_EXIT:
			case Boid::TestAfter::BTEST_AFTER_AND:
			case Boid::TestAfter::BTEST_AFTER_OR:
				break;
			case Boid::TestAfter::BTEST_AFTER_GOTO:
				test_fields.form->add_child(test_fields.attr_goto_lbl);
				test_fields.form->add_child(test_fields.attr_goto);
				break;
			default:
				break;
		}
	}

	regenerate_test_preview();

}

void BoidBehaviourEditor::regenerate_test_preview() {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}

	// preview rendering
	String prev = "";
	bool std_body = true;
	if ( current_item->test.valid ) {

		if ( current_item->test.type == Boid::TestType::BTEST_TYPE_NONE ) {
			prev += "#" + String::num(current_item->uid) + " do nothing";
			std_body = false;

		} else {

			prev += "if ";

			if ( current_item->test.repeat == Boid::TestRepeat::BTEST_REPEAT_ONCE ) {
				prev += "!done and ";
			}

			if ( current_item->test.type == Boid::TestType::BTEST_TYPE_ALWAYS_TRUE ) {
				prev += "true ";

			} else if ( current_item->test.type == Boid::TestType::BTEST_TYPE_ATTRIBUTE ) {

				prev += "boid." + prepare_for_display(test_fields.attr->get_item_text(test_fields.attr->get_selected())) + " ";
				prev += prepare_for_display(test_fields.eval->get_item_text(test_fields.eval->get_selected())) + " ";

				switch ( current_item->test.attr ) {

					case Boid::TestAttribute::BTEST_ATTR_ACTIVE:
					case Boid::TestAttribute::BTEST_ATTR_COLLISION:
					case Boid::TestAttribute::BTEST_ATTR_MOVING:
						if ( current_item->test.param_bool ) {
							prev += "true ";
						} else {
							prev += "false ";
						}
						break;
					case Boid::TestAttribute::BTEST_ATTR_GROUP:
					case Boid::TestAttribute::BTEST_ATTR_ANIMATION:
					{
						String param = " ";
						for ( int i = 0; i < 20; ++i ) {
							if ((current_item->test.param_int.value&(1<<i)) != 0 ) {
								if ( !param.empty() ) {
									param += ",";
								}
								param += String::num( i );
							}
						}
						if ( param.empty() ) {
							param = "0";
						}
						prev += "("+param+") ";
					}
						break;
					case Boid::TestAttribute::BTEST_ATTR_SPEED:
					case Boid::TestAttribute::BTEST_ATTR_WEIGHT:
					case Boid::TestAttribute::BTEST_ATTR_RADIUS:
					case Boid::TestAttribute::BTEST_ATTR_PUSH:
					case Boid::TestAttribute::BTEST_ATTR_COUNTER:
					case Boid::TestAttribute::BTEST_ATTR_TIMER:
					case Boid::TestAttribute::BTEST_ATTR_LIFETIME:
						prev += String::num(test_fields.attr_float->get_value()) + " ";
						break;
					default:
						break;

				}

			} else if ( current_item->test.type == Boid::TestType::BTEST_TYPE_RANDOM ) {
				prev += "rand(0,1) ";
				prev += prepare_for_display(test_fields.eval->get_item_text(test_fields.eval->get_selected())) + " ";
				prev += String::num(test_fields.attr_float->get_value()) + " ";

			} else if ( current_item->test.type == Boid::TestType::BTEST_TYPE_NEIGHBORHOUD ) {
				prev += "neighbors ";
				prev += prepare_for_display(test_fields.eval->get_item_text(test_fields.eval->get_selected())) + " ";
				{
					String groupn;
					for ( int i = 0; i < 20; ++i ) {
						if ( (current_item->test.param_int.value & 1<<i) != 0 ) {
							if (!groupn.empty()) {
								groupn += ",";
							}
							groupn += get_group_name(i);
						}
					}
					if ( groupn.find(",") != -1 ) {
						groupn = "("+groupn+")";
					}
					prev += groupn + " ";
				}

			} else if ( current_item->test.type == Boid::TestType::BTEST_TYPE_INSIDE ) {
				prev += "inside(";
				if ( !current_item->test.param_ctrl.initialised ) {
					prev += "?) ";
				} else {
					prev += String::num( current_item->test.param_ctrl.control_UID ) + ") ";
				}

			} else if ( current_item->test.type == Boid::TestType::BTEST_TYPE_OUTSIDE ) {
				prev += "outside(";
				if ( !current_item->test.param_ctrl.initialised ) {
					prev += "?) ";
				} else {
					prev += String::num( current_item->test.param_ctrl.control_UID ) + ") ";
				}

			} else if ( current_item->test.type == Boid::TestType::BTEST_TYPE_EVENT ) {

				prev += "event ";
				prev += prepare_for_display(test_fields.eval->get_item_text(test_fields.eval->get_selected())) + " ";
				prev += prepare_for_display(test_fields.attr_event->get_item_text(test_fields.attr_event->get_selected())) + " ";

			} else if ( current_item->test.type == Boid::TestType::BTEST_TYPE_NEIGHBOR_COUNT ) {

				prev += "neighbor_count ";
				prev += prepare_for_display(test_fields.eval->get_item_text(test_fields.eval->get_selected())) + " ";
				prev += prepare_for_display(test_fields.attr_event->get_item_text(test_fields.attr_event->get_selected())) + " ";

			}
		}

		if ( std_body ) {
			if ( current_item->test.after == Boid::TestAfter::BTEST_AFTER_AND ) {
				prev += "and...";
			} else if ( current_item->test.after == Boid::TestAfter::BTEST_AFTER_OR ) {
				prev += "or...";
			} else {
				prev += ":";
				prev += "\n    actions()";
				if ( current_item->test.repeat == Boid::TestRepeat::BTEST_REPEAT_ONCE ) {
					prev += "\n   done = true";
				}
				if ( current_item->test.after == Boid::TestAfter::BTEST_AFTER_EXIT ) {
					prev += "\n    return";
				} else if ( current_item->test.after == Boid::TestAfter::BTEST_AFTER_GOTO ) {
					prev += "\n    goto(";
					prev += String::num(current_item->test.jumpto);
					prev += ")";
				}
			}
		}

	} else {
		prev = "fix errors first...";
	}
	
	test_fields.preview->set_text( prev );

}

void BoidBehaviourEditor::edit_test_active() {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	current_item->test.active = test_fields.active->is_pressed();
	adjust_item_display( current_item );
	save_behaviour( current_item->test.valid );

}

void BoidBehaviourEditor::edit_test_clone() {
	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	create_item(current_item);
}

void BoidBehaviourEditor::edit_test_delete() {
	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	remove_item( current_item->uid );
}

void BoidBehaviourEditor::edit_test_type(int i) {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	bool was_valid = current_item->test.valid;
	current_item->test.type = Boid::TestType(i);
	adjust_test_editor();
	regenerate_list();
	save_behaviour( was_valid || was_valid != current_item->test.valid );

}

void BoidBehaviourEditor::edit_test_attribute(int i) {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	bool was_valid = current_item->test.valid;
	current_item->test.attr = Boid::TestAttribute(i);
	adjust_test_editor();
	save_behaviour( was_valid || was_valid != current_item->test.valid );

}

void BoidBehaviourEditor::edit_test_eval(int i) {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	bool was_valid = current_item->test.valid;
	current_item->test.eval = Boid::TestEval(i);
	adjust_test_editor();
	regenerate_list();
	save_behaviour( was_valid || was_valid != current_item->test.valid );

}

void BoidBehaviourEditor::edit_test_attr_bool() {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	bool was_valid = current_item->test.valid;
	current_item->test.param_bool = test_fields.attr_bool->is_pressed();
	adjust_item_display( current_item );
	save_behaviour( was_valid || was_valid != current_item->test.valid );
}

void BoidBehaviourEditor::edit_test_attr_int(double d) {
	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	bool was_valid = current_item->test.valid;
	current_item->test.param_int.value = int(d);
	validate_item( current_item );
	if ( current_item->test.param_int.value != int(d) ) {
		test_fields.attr_int->set_value( current_item->test.param_int.value );
	}
	adjust_item_display( current_item );
	save_behaviour( was_valid || was_valid != current_item->test.valid );
}

void BoidBehaviourEditor::edit_test_attr_float(double d) {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	bool was_valid = current_item->test.valid;
	current_item->test.param_float.value = float(d);
	validate_item( current_item );
	if ( current_item->test.param_float.value != float(d) ) {
		test_fields.attr_float->set_value( current_item->test.param_float.value );
	}
	adjust_item_display( current_item );
	save_behaviour( was_valid || was_valid != current_item->test.valid );

}

void BoidBehaviourEditor::edit_test_attr_event(int i) {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	bool was_valid = current_item->test.valid;
	current_item->test.event = Boid::TestEvent(i);
	validate_item( current_item );
	adjust_item_display( current_item );
	save_behaviour( was_valid || was_valid != current_item->test.valid );

}

void BoidBehaviourEditor::edit_test_attr_ctrl(int i) {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	if ( current_item->test.param_ctrl.initialised ) {
		// none is selected
		if ( i == 0 ) {
			current_item->test.param_ctrl.control_UID = -1;
		} else {
			i--;
			if ( i >= ctrl_infos.size() ) {
				current_item->test.param_ctrl.control_UID = -1;
			} else {
				current_item->test.param_ctrl.control_UID = ctrl_infos[i].UID;
			}
		}
	}
	bool was_valid = current_item->test.valid;
	validate_item( current_item );
	adjust_item_display( current_item );
	save_behaviour( was_valid || was_valid != current_item->test.valid );

}

void BoidBehaviourEditor::edit_test_group(int index) {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	if ( current_item->test.param_int.initialised ) {
		current_item->test.param_int.value = 0;
		for ( int i = 0; i < 20; ++i ) {
			if ( test_fields.attr_group_checks[i]->is_pressed() ) {
				current_item->test.param_int.value+= 1 << i;
			}
		}
	}
	adjust_item_display( current_item );
	regenerate_test_preview();
	save_behaviour( current_item->test.valid );

}

void BoidBehaviourEditor::edit_test_repeat(int i) {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	current_item->test.repeat = Boid::TestRepeat(i);
	adjust_test_editor();
	save_behaviour( current_item->test.valid );

}

void BoidBehaviourEditor::edit_test_after(int i) {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	bool was_valid = current_item->test.valid;
	current_item->test.after = Boid::TestAfter(i);
	adjust_test_editor();
	regenerate_list();
	save_behaviour( was_valid || was_valid != current_item->test.valid );

}

void BoidBehaviourEditor::edit_test_attr_goto(double d) {

	if ( current_item == NULL || !current_item->is_test ) {
		return;
	}
	bool was_valid = current_item->test.valid;
	current_item->test.jumpto = int(d);
	validate_item( current_item );
	adjust_item_display( current_item );
	save_behaviour( was_valid || was_valid != current_item->test.valid );

}

//\\\\\\\\\\\\\\\\\\ ACTION EDITOR

void BoidBehaviourEditor::build_action_editor( Control* parent ) {

	action_fields.panel = memnew(VBoxContainer);
	action_fields.panel->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.panel->set_visible(false);
	parent->add_child(action_fields.panel);

	// reusables
	Label* lbl;
	ToolButton* tb;
	GridContainer* grid0;
	GridContainer* grid1;

	// menu
	PanelContainer* menu_wrp = memnew(PanelContainer);
	menu_wrp->add_style_override("panel", style_editor_menu());
	action_fields.panel->add_child(menu_wrp);

	HBoxContainer* menu =  memnew(HBoxContainer);
	menu->add_constant_override("separation",0);
	menu_wrp->add_child(menu);

	TextureRect* icon = memnew(TextureRect);
	icon->set_texture(enode->get_gui_base()->get_icon("BoidAction", "EditorIcons"));
	menu->add_child(icon);

	lbl = memnew(Label);
	lbl->set_text("BoidAction");
	lbl->set_h_size_flags(SIZE_EXPAND_FILL);
	menu->add_child(lbl);

	action_fields.active = memnew(CheckBox);
	prepare_button( action_fields.active );
	action_fields.active->connect("pressed",this, "edit_action_active");
	menu->add_child(action_fields.active);

	action_fields.clone = memnew(ToolButton);
	prepare_button( action_fields.clone );
	action_fields.clone->set_icon(enode->get_gui_base()->get_icon("Duplicate", "EditorIcons"));
	action_fields.clone->set_tooltip("Duplicate current test");
	action_fields.clone->connect("pressed",this, "edit_action_clone");
	menu->add_child(action_fields.clone);

	action_fields.del = memnew(ToolButton);
	prepare_button( action_fields.del );
	action_fields.del->set_icon(enode->get_gui_base()->get_icon("Remove", "EditorIcons"));
	action_fields.del->set_tooltip("Duplicate current test");
	action_fields.del->connect("pressed",this, "edit_action_delete");
	menu->add_child(action_fields.del);

	tb = memnew(ToolButton);
	prepare_button( tb );
	tb->set_icon(enode->get_gui_base()->get_icon("GuiClose", "EditorIcons"));
	tb->set_tooltip("Close test editor");
	tb->connect("pressed",this, "edit_close_all");
	menu->add_child(tb);

	// errors
	action_fields.error_lbl = memnew(Label);
	action_fields.error_lbl->set_text("errors");
	action_fields.panel->add_child(action_fields.error_lbl);
	action_fields.error = memnew(Label);
	prepare_error( action_fields.error );
	action_fields.error->set_text("all cool");
	action_fields.panel->add_child(action_fields.error);

	// form
	action_fields.form = memnew(GridContainer);
	action_fields.form->set_columns(2);
	action_fields.panel->add_child(action_fields.form);

	// type
	action_fields.type_lbl = memnew(Label);
	add_action_fieldname( action_fields.type_lbl, "type" );
	action_fields.type = memnew(OptionButton);
	action_fields.type->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.type->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	for ( int i = 0, imax = enums["action_type"].size(); i < imax; ++i ) { // @suppress("Method cannot be resolved")
		action_fields.type->add_item(enums["action_type"][i]); // @suppress("Invalid arguments")
	}
	action_fields.type->connect("item_selected",this, "edit_action_type");
	action_fields.form->add_child(action_fields.type);

	// attribute
	action_fields.attr_lbl = memnew(Label);
	add_action_attrname( action_fields.attr_lbl, "attr" );
	action_fields.attr = memnew(OptionButton);
	action_fields.attr->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.attr->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	for ( int i = 0, imax = enums["action_attr"].size(); i < imax; ++i ) { // @suppress("Method cannot be resolved")
		action_fields.attr->add_item(enums["action_attr"][i]); // @suppress("Invalid arguments")
	}
	action_fields.attr->connect("item_selected",this, "edit_action_attribute");
	action_fields.form->add_child(action_fields.attr);

	// attr_bool
	action_fields.attr_bool_lbl = memnew(Label);
	add_action_attrname( action_fields.attr_bool_lbl, "boolean" );
	action_fields.attr_bool = memnew(CheckBox);
	action_fields.attr_bool->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	action_fields.attr_bool->connect("pressed",this, "edit_action_attr_bool");
	action_fields.form->add_child(action_fields.attr_bool);

	// attr_int
	action_fields.attr_int_lbl = memnew(Label);
	add_action_attrname( action_fields.attr_int_lbl, "int" );
	action_fields.attr_int = memnew(SpinBox);
	action_fields.attr_int->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.attr_int->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	action_fields.attr_int->set_step(1);
	action_fields.attr_int->set_value(0);
	action_fields.attr_int->set_allow_greater(true);
	action_fields.attr_int->set_allow_lesser(true);
	action_fields.attr_int->connect("value_changed",this, "edit_action_attr_int");
	action_fields.form->add_child(action_fields.attr_int);

	// attr_group
	action_fields.attr_group_lbl = memnew(Label);
	add_action_attrname( action_fields.attr_group_lbl, "group" );
	action_fields.attr_group = memnew(GridContainer);
	action_fields.attr_group->set_h_size_flags(SIZE_FILL);
	action_fields.attr_group->add_constant_override("vseparation",0);
	action_fields.attr_group->add_constant_override("hseparation",0);
	action_fields.attr_group->set_columns(10);
	for (int i = 0; i < 20; i++) {
		action_fields.attr_group_checks[i] = memnew(CheckBox);
		action_fields.attr_group_checks[i]->connect("pressed", this, "edit_action_group", varray(i));
		prepare_group_checkbox( action_fields.attr_group_checks[i], i );
		action_fields.attr_group_checks[i]->set_tooltip("Group " + get_group_name(i));
		action_fields.attr_group->add_child(action_fields.attr_group_checks[i]);
	}
	action_fields.attr_group->set_size(Vector2(0,0), false);
	action_fields.form->add_child(action_fields.attr_group);

	// attr_anim
	action_fields.attr_anim_lbl = memnew(Label);
	add_action_attrname( action_fields.attr_anim_lbl, "animation" );
	action_fields.attr_anim = memnew(GridContainer);
	action_fields.attr_anim->set_h_size_flags(SIZE_FILL);
	action_fields.attr_anim->add_constant_override("vseparation",0);
	action_fields.attr_anim->add_constant_override("hseparation",0);
	action_fields.attr_anim->set_columns(10);
	for (int i = 0; i < 20; i++) {
		action_fields.attr_anim_checks[i] = memnew(CheckBox);
		action_fields.attr_anim_checks[i]->connect("pressed", this, "edit_action_anim", varray(i));
		prepare_anim_checkbox( action_fields.attr_anim_checks[i], i );
		action_fields.attr_anim_checks[i]->set_tooltip("Animation " + get_group_name(i));
		action_fields.attr_anim->add_child(action_fields.attr_anim_checks[i]);
	}
	action_fields.attr_anim->set_size(Vector2(0,0), false);
	action_fields.form->add_child(action_fields.attr_anim);

	// attr_float
	action_fields.attr_float_lbl = memnew(Label);
	add_action_attrname( action_fields.attr_float_lbl, "float" );
	action_fields.attr_float = memnew(SpinBox);
	action_fields.attr_float->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.attr_float->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	action_fields.attr_float->set_step(0.01);
	action_fields.attr_float->set_value(0);
	action_fields.attr_float->set_allow_greater(true);
	action_fields.attr_float->set_allow_lesser(true);
	action_fields.attr_float->connect("value_changed",this, "edit_action_attr_float");
	action_fields.form->add_child(action_fields.attr_float);

	// attr_vec3index
	action_fields.attr_vec3_lbl = memnew(Label);
	add_action_attrname( action_fields.attr_vec3_lbl, "vec3" );
	action_fields.attr_vec3 = memnew(GridContainer);
	action_fields.attr_vec3->set_columns(2);
	action_fields.attr_vec3->add_constant_override("vseparation",0);
	action_fields.attr_vec3->add_constant_override("hseparation",0);
	for ( int i = 0; i < 3; ++i ) {
		lbl = memnew(Label);
		switch(i){
			case 0:
				lbl->set_text("x");
				break;
			case 1:
				lbl->set_text("y");
				break;
			default:
				lbl->set_text("z");
				break;
		}
		prepare_vectr_atrr( lbl, i );
		action_fields.attr_vec3->add_child( lbl );
		action_fields.attr_vec3_spins[i] = memnew(SpinBox);
		action_fields.attr_vec3_spins[i]->set_h_size_flags(SIZE_EXPAND_FILL);
		action_fields.attr_vec3_spins[i]->set_focus_mode(Control::FocusMode::FOCUS_NONE);
		action_fields.attr_vec3_spins[i]->set_step(0.01);
		action_fields.attr_vec3_spins[i]->set_value(0);
		action_fields.attr_vec3_spins[i]->set_allow_greater(true);
		action_fields.attr_vec3_spins[i]->set_allow_lesser(true);
		action_fields.attr_vec3_spins[i]->connect("value_changed",this, "edit_action_vec3");
		action_fields.attr_vec3->add_child( action_fields.attr_vec3_spins[i] );
	}
	action_fields.form->add_child(action_fields.attr_vec3);

	// attr_vec4
	action_fields.attr_vec4_lbl = memnew(Label);
	add_action_attrname( action_fields.attr_vec4_lbl, "vec4" );
	action_fields.attr_vec4 = memnew(HBoxContainer);
	action_fields.attr_vec4->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.attr_vec4->add_constant_override("hseparation",0);
	action_fields.form->add_child(action_fields.attr_vec4);
	GridContainer* grd = memnew(GridContainer);
	grd->set_h_size_flags(SIZE_EXPAND_FILL);
	grd->set_columns(2);
	grd->add_constant_override("vseparation",0);
	grd->add_constant_override("hseparation",0);
	for ( int i = 0; i < 4; ++i ) {
		lbl = memnew(Label);
		switch(i){
			case 0:
				lbl->set_text("r");
				break;
			case 1:
				lbl->set_text("g");
				break;
			case 2:
				lbl->set_text("b");
				break;
			default:
				lbl->set_text("a");
				break;
		}
		prepare_vectr_atrr( lbl, i );
		grd->add_child( lbl );
		action_fields.attr_vec4_spins[i] = memnew(SpinBox);
		action_fields.attr_vec4_spins[i]->set_h_size_flags(SIZE_EXPAND_FILL);
		action_fields.attr_vec4_spins[i]->set_focus_mode(Control::FocusMode::FOCUS_NONE);
		action_fields.attr_vec4_spins[i]->set_step(0.01);
		action_fields.attr_vec4_spins[i]->set_value(1);
		action_fields.attr_vec4_spins[i]->set_allow_greater(true);
		action_fields.attr_vec4_spins[i]->set_allow_lesser(true);
		action_fields.attr_vec4_spins[i]->connect("value_changed",this, "edit_action_vec4");
		grd->add_child( action_fields.attr_vec4_spins[i] );
	}
	action_fields.attr_vec4->add_child(grd);
	action_fields.attr_vec4_preview = memnew(ColorRect);
	action_fields.attr_vec4_preview->set_h_size_flags(SIZE_FILL);
	action_fields.attr_vec4_preview->set_v_size_flags(SIZE_FILL);
	action_fields.attr_vec4_preview->set_custom_minimum_size(Vector2(50,50)*EDSCALE);
	action_fields.attr_vec4_preview->set_size(Vector2(50,50)*EDSCALE);
	action_fields.attr_vec4_preview->set_frame_color(Color(1,1,1,1));
	action_fields.attr_vec4->add_child(action_fields.attr_vec4_preview);

	// attr_control
	action_fields.attr_ctrl_lbl = memnew(Label);
	add_action_attrname( action_fields.attr_ctrl_lbl, "control" );
	action_fields.attr_ctrl = memnew(OptionButton);
	action_fields.attr_ctrl->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.attr_ctrl->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	action_fields.attr_ctrl->connect("item_selected",this, "edit_action_ctrl");
	action_fields.form->add_child(action_fields.attr_ctrl);

	// attr_rand
	action_fields.rand_lbl = memnew(Label);
	add_action_attrname( action_fields.rand_lbl, "random" );

	action_fields.rand_int = memnew(HBoxContainer);
	action_fields.rand_int->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.rand_int->set_v_size_flags(SIZE_FILL);
	action_fields.form->add_child(action_fields.rand_int);
	lbl = memnew(Label);
	lbl->set_text("min");
	action_fields.rand_int->add_child(lbl);
	action_fields.rand_int_min = memnew(SpinBox);
	action_fields.rand_int_min->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.rand_int_min->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	action_fields.rand_int_min->set_step(1);
	action_fields.rand_int_min->set_value(0);
	action_fields.rand_int_min->set_allow_greater(true);
	action_fields.rand_int_min->set_allow_lesser(true);
	action_fields.rand_int_min->connect("value_changed",this, "edit_action_rand_int");
	action_fields.rand_int->add_child(action_fields.rand_int_min);
	lbl = memnew(Label);
	lbl->set_text("max");
	action_fields.rand_int->add_child(lbl);
	action_fields.rand_int_max = memnew(SpinBox);
	action_fields.rand_int_max->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.rand_int_max->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	action_fields.rand_int_max->set_step(1);
	action_fields.rand_int_max->set_value(0);
	action_fields.rand_int_max->set_allow_greater(true);
	action_fields.rand_int_max->set_allow_lesser(true);
	action_fields.rand_int_max->connect("value_changed",this, "edit_action_rand_int");
	action_fields.rand_int->add_child(action_fields.rand_int_max);

	action_fields.rand_float = memnew(HBoxContainer);
	action_fields.rand_float->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.rand_float->set_v_size_flags(SIZE_FILL);
	action_fields.form->add_child(action_fields.rand_float);
	lbl = memnew(Label);
	lbl->set_text("min");
	action_fields.rand_float->add_child(lbl);
	action_fields.rand_float_min = memnew(SpinBox);
	action_fields.rand_float_min->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.rand_float_min->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	action_fields.rand_float_min->set_step(0.01);
	action_fields.rand_float_min->set_value(0);
	action_fields.rand_float_min->set_allow_greater(true);
	action_fields.rand_float_min->set_allow_lesser(true);
	action_fields.rand_float_min->connect("value_changed",this, "edit_action_rand_float");
	action_fields.rand_float->add_child(action_fields.rand_float_min);
	lbl = memnew(Label);
	lbl->set_text("max");
	action_fields.rand_float->add_child(lbl);
	action_fields.rand_float_max = memnew(SpinBox);
	action_fields.rand_float_max->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.rand_float_max->set_focus_mode(Control::FocusMode::FOCUS_NONE);
	action_fields.rand_float_max->set_step(0.01);
	action_fields.rand_float_max->set_value(0);
	action_fields.rand_float_max->set_allow_greater(true);
	action_fields.rand_float_max->set_allow_lesser(true);
	action_fields.rand_float_max->connect("value_changed",this, "edit_action_rand_float");
	action_fields.rand_float->add_child(action_fields.rand_float_max);

	action_fields.rand_vec3 = memnew(HBoxContainer);
	action_fields.rand_vec3->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.rand_vec3->set_v_size_flags(SIZE_FILL);
	action_fields.form->add_child(action_fields.rand_vec3);
	lbl = memnew(Label);
	lbl->set_valign(Label::VAlign::VALIGN_TOP);
	lbl->set_v_size_flags(SIZE_SHRINK_END);
	lbl->set_text("min");
	action_fields.rand_vec3->add_child(lbl);
	grid0 = memnew(GridContainer);
	grid0->set_h_size_flags(SIZE_EXPAND_FILL);
	grid0->set_v_size_flags(SIZE_FILL);
	grid0->set_columns(2);
	action_fields.rand_vec3->add_child(grid0);
	lbl = memnew(Label);
	lbl->set_valign(Label::VAlign::VALIGN_TOP);
	lbl->set_v_size_flags(SIZE_SHRINK_END);
	lbl->set_text("max");
	action_fields.rand_vec3->add_child(lbl);
	grid1 = memnew(GridContainer);
	grid1->set_h_size_flags(SIZE_EXPAND_FILL);
	grid1->set_v_size_flags(SIZE_FILL);
	grid1->set_columns(2);
	action_fields.rand_vec3->add_child(grid1);
	for ( int i = 0; i < 3; ++i ) {

		action_fields.rand_vec3_min[i] = memnew(SpinBox);
		action_fields.rand_vec3_min[i]->set_h_size_flags(SIZE_EXPAND_FILL);
		action_fields.rand_vec3_min[i]->set_focus_mode(Control::FocusMode::FOCUS_NONE);
		action_fields.rand_vec3_min[i]->set_step(0.01);
		action_fields.rand_vec3_min[i]->set_value(1);
		action_fields.rand_vec3_min[i]->set_allow_greater(true);
		action_fields.rand_vec3_min[i]->set_allow_lesser(true);
		action_fields.rand_vec3_min[i]->connect("value_changed",this, "edit_action_rand_vec3");

		action_fields.rand_vec3_max[i] = memnew(SpinBox);
		action_fields.rand_vec3_max[i]->set_h_size_flags(SIZE_EXPAND_FILL);
		action_fields.rand_vec3_max[i]->set_focus_mode(Control::FocusMode::FOCUS_NONE);
		action_fields.rand_vec3_max[i]->set_step(0.01);
		action_fields.rand_vec3_max[i]->set_value(1);
		action_fields.rand_vec3_max[i]->set_allow_greater(true);
		action_fields.rand_vec3_max[i]->set_allow_lesser(true);
		action_fields.rand_vec3_max[i]->connect("value_changed",this, "edit_action_rand_vec3");

		Label* lblmi = memnew(Label);
		Label* lblma = memnew(Label);
		switch(i){
			case 0:
				lblmi->set_text("x");
				lblma->set_text("x");
				break;
			case 1:
				lblmi->set_text("y");
				lblma->set_text("y");
				break;
			default:
				lblmi->set_text("z");
				lblma->set_text("z");
				break;
		}
		prepare_vectr_atrr( lblmi, i );
		prepare_vectr_atrr( lblma, i );
		grid0->add_child( lblmi );
		grid0->add_child( action_fields.rand_vec3_min[i] );
		grid1->add_child( lblma );
		grid1->add_child( action_fields.rand_vec3_max[i] );
	}

	action_fields.rand_vec4 = memnew(HBoxContainer);
	action_fields.rand_vec4->set_h_size_flags(SIZE_EXPAND_FILL);
	action_fields.rand_vec4->set_v_size_flags(SIZE_FILL);
	action_fields.form->add_child(action_fields.rand_vec4);
	lbl = memnew(Label);
	lbl->set_valign(Label::VAlign::VALIGN_TOP);
	lbl->set_v_size_flags(SIZE_SHRINK_END);
	lbl->set_text("min");
	action_fields.rand_vec4->add_child(lbl);
	grid0 = memnew(GridContainer);
	grid0->set_h_size_flags(SIZE_EXPAND_FILL);
	grid0->set_v_size_flags(SIZE_FILL);
	grid0->set_columns(2);
	action_fields.rand_vec4->add_child(grid0);
	lbl = memnew(Label);
	lbl->set_valign(Label::VAlign::VALIGN_TOP);
	lbl->set_v_size_flags(SIZE_SHRINK_END);
	lbl->set_text("max");
	action_fields.rand_vec4->add_child(lbl);
	grid1 = memnew(GridContainer);
	grid1->set_h_size_flags(SIZE_EXPAND_FILL);
	grid1->set_v_size_flags(SIZE_FILL);
	grid1->set_columns(2);
	action_fields.rand_vec4->add_child(grid1);
	for ( int i = 0; i < 4; ++i ) {

		action_fields.rand_vec4_min[i] = memnew(SpinBox);
		action_fields.rand_vec4_min[i]->set_h_size_flags(SIZE_EXPAND_FILL);
		action_fields.rand_vec4_min[i]->set_focus_mode(Control::FocusMode::FOCUS_NONE);
		action_fields.rand_vec4_min[i]->set_step(0.01);
		action_fields.rand_vec4_min[i]->set_value(1);
		action_fields.rand_vec4_min[i]->set_allow_greater(true);
		action_fields.rand_vec4_min[i]->set_allow_lesser(true);
		action_fields.rand_vec4_min[i]->connect("value_changed",this, "edit_action_rand_vec4");

		action_fields.rand_vec4_max[i] = memnew(SpinBox);
		action_fields.rand_vec4_max[i]->set_h_size_flags(SIZE_EXPAND_FILL);
		action_fields.rand_vec4_max[i]->set_focus_mode(Control::FocusMode::FOCUS_NONE);
		action_fields.rand_vec4_max[i]->set_step(0.01);
		action_fields.rand_vec4_max[i]->set_value(1);
		action_fields.rand_vec4_max[i]->set_allow_greater(true);
		action_fields.rand_vec4_max[i]->set_allow_lesser(true);
		action_fields.rand_vec4_max[i]->connect("value_changed",this, "edit_action_rand_vec4");

		Label* lblmi = memnew(Label);
		Label* lblma = memnew(Label);
		switch(i){
			case 0:
				lblmi->set_text("r");
				lblma->set_text("r");
				break;
			case 1:
				lblmi->set_text("g");
				lblma->set_text("g");
				break;
			case 2:
				lblmi->set_text("b");
				lblma->set_text("b");
				break;
			default:
				lblmi->set_text("a");
				lblma->set_text("a");
				break;
		}
		prepare_vectr_atrr( lblmi, i );
		prepare_vectr_atrr( lblma, i );
		grid0->add_child( lblmi );
		grid0->add_child( action_fields.rand_vec4_min[i] );
		grid1->add_child( lblma );
		grid1->add_child( action_fields.rand_vec4_max[i] );
	}

}

void BoidBehaviourEditor::load_action_editor() {

	if ( current_item == NULL || !current_item->is_action ) {
		action_fields.panel->set_visible(false);
		return;
	}

	action_fields.panel->set_visible(true);
	adjust_action_editor();

}

void BoidBehaviourEditor::adjust_action_editor() {

	if ( boid_behaviour == NULL || current_item == NULL || !current_item->is_action ) {
		return;
	}

	boid_behaviour->validate_action( &current_item->action );

	action_fields.active->set_pressed( current_item->action.active );
	action_fields.error_lbl->set_visible( !current_item->action.valid );
	action_fields.error->set_visible( !current_item->action.valid );

	while ( action_fields.form->get_child_count() > 0 ) {
		action_fields.form->remove_child( action_fields.form->get_child(0) );
	}

	// reset labels
	action_fields.attr_bool_lbl->set_text("boolean");
	action_fields.attr_int_lbl->set_text("int");
	action_fields.attr_float_lbl->set_text("float");

	// synching fields
	action_fields.type->select( int(current_item->action.type) );
	action_fields.attr->select( int(current_item->action.attr) );
	action_fields.attr_bool->set_pressed( int(current_item->action.param_bool) );

	switch ( current_item->action.type ) {
		case Boid::ActionType::BACTION_TYPE_SET_ATTRIBUTE:
			switch ( current_item->action.attr ) {
				case Boid::ActionAttribute::BACTION_ATTR_GROUP:
					if ( current_item->action.param_int.initialised ) {
						for ( int i = 0; i < 20; ++i ) {
							action_fields.attr_group_checks[i]->set_pressed((current_item->action.param_int.value&(1<<i)) != 0);
						}
					}
					break;

				case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
					if ( current_item->action.param_int.initialised ) {
						for ( int i = 0; i < 20; ++i ) {
							action_fields.attr_anim_checks[i]->set_pressed((current_item->action.param_int.value&(1<<i)) != 0);
						}
					}
					break;
				default:
					break;

			}
			break;
		case Boid::ActionType::BACTION_TYPE_AIM_AT:
		case Boid::ActionType::BACTION_TYPE_REACH:
		case Boid::ActionType::BACTION_TYPE_ESCAPE:
			action_fields.attr_bool_lbl->set_text("group center");
			action_fields.attr_float_lbl->set_text("strength");
			if ( current_item->action.param_int.initialised ) {
				for ( int i = 0; i < 20; ++i ) {
					action_fields.attr_group_checks[i]->set_pressed((current_item->action.param_int.value&(1<<i)) != 0);
				}
			}
			break;

		case Boid::ActionType::BACTION_TYPE_COUPLE:
			if ( current_item->action.param_int.initialised ) {
				for ( int i = 0; i < 20; ++i ) {
					action_fields.attr_group_checks[i]->set_pressed((current_item->action.param_int.value&(1<<i)) != 0);
				}
			}
			break;
		default:
			break;

	}

	if ( current_item->action.param_int.initialised ) {
		action_fields.attr_int->set_value( int(current_item->action.param_int.value) );
		action_fields.rand_int_min->set_value( int(current_item->action.param_int.min) );
		action_fields.rand_int_max->set_value( int(current_item->action.param_int.max) );
	}

	if ( current_item->action.param_float.initialised ) {
		action_fields.attr_float->set_value( current_item->action.param_float.value );
		action_fields.rand_float_min->set_value( int(current_item->action.param_float.min) );
		action_fields.rand_float_max->set_value( int(current_item->action.param_float.max) );
	}

	if ( current_item->action.param_vec3.initialised ) {
		for ( int i = 0; i < 3; ++i ) {
			action_fields.attr_vec3_spins[i]->set_value( current_item->action.param_vec3.value[i] );
			action_fields.rand_vec3_min[i]->set_value( current_item->action.param_vec3.min[i] );
			action_fields.rand_vec3_max[i]->set_value( current_item->action.param_vec3.max[i] );
		}
	}

	if ( current_item->action.param_vec4.initialised ) {
		for ( int i = 0; i < 4; ++i ) {
			action_fields.attr_vec4_spins[i]->set_value( current_item->action.param_vec4.value[i] );
			action_fields.rand_vec4_min[i]->set_value( current_item->action.param_vec4.min[i] );
			action_fields.rand_vec4_max[i]->set_value( current_item->action.param_vec4.max[i] );
		}
		action_fields.attr_vec4_preview->set_frame_color( current_item->action.param_vec4.value );
	}

	if ( current_item->action.param_ctrl.initialised ) {
		action_fields.attr_ctrl->select( 0 );
		for ( int i = 0, imax = ctrl_infos.size(); i < imax; ++i ) {
			if ( current_item->action.param_ctrl.control_UID == ctrl_infos[i].UID ) {
				action_fields.attr_ctrl->select( i+1 );
			}
		}
	}

	// building form
	action_fields.form->add_child(action_fields.type_lbl);
	action_fields.form->add_child(action_fields.type);

	switch ( current_item->action.type ) {

		case Boid::ActionType::BACTION_TYPE_SET_ATTRIBUTE:
			action_fields.form->add_child(action_fields.attr_lbl);
			action_fields.form->add_child(action_fields.attr);
			switch( current_item->action.attr ) {

				case Boid::ActionAttribute::BACTION_ATTR_ACTIVE:
				case Boid::ActionAttribute::BACTION_ATTR_COLLISION:
					action_fields.form->add_child(action_fields.attr_bool_lbl);
					action_fields.form->add_child(action_fields.attr_bool);
					break;

				case Boid::ActionAttribute::BACTION_ATTR_GROUP:
					action_fields.form->add_child(action_fields.attr_group_lbl);
					action_fields.form->add_child(action_fields.attr_group);
					break;

				case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
					action_fields.form->add_child(action_fields.attr_anim_lbl);
					action_fields.form->add_child(action_fields.attr_anim);
					break;

				case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
				case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
				case Boid::ActionAttribute::BACTION_ATTR_COHESION:
				case Boid::ActionAttribute::BACTION_ATTR_STEER:
				case Boid::ActionAttribute::BACTION_ATTR_SPEED:
				case Boid::ActionAttribute::BACTION_ATTR_PUSH_SPEED:
				case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
				case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
				case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
				case Boid::ActionAttribute::BACTION_ATTR_FRICTION:
				case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
				case Boid::ActionAttribute::BACTION_ATTR_TIMER:
				case Boid::ActionAttribute::BACTION_ATTR_LIFETIME:
					action_fields.attr_float_lbl->set_text( "float" );
					action_fields.form->add_child(action_fields.attr_float_lbl);
					action_fields.form->add_child(action_fields.attr_float);
					break;

				case Boid::ActionAttribute::BACTION_ATTR_COLOR:
					action_fields.form->add_child(action_fields.attr_vec4_lbl);
					action_fields.form->add_child(action_fields.attr_vec4);
					break;

				case Boid::ActionAttribute::BACTION_ATTR_AIM:
				case Boid::ActionAttribute::BACTION_ATTR_POSITION:
				case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
				case Boid::ActionAttribute::BACTION_ATTR_FRONT:
				case Boid::ActionAttribute::BACTION_ATTR_PUSH:
					action_fields.form->add_child(action_fields.attr_vec3_lbl);
					action_fields.form->add_child(action_fields.attr_vec3);
					break;

				case Boid::ActionAttribute::BACTION_ATTR_NONE:
				default:
					break;
			}
			break;

		case Boid::ActionType::BACTION_TYPE_INCREMENT_ATTRIBUTE:
			action_fields.form->add_child(action_fields.attr_lbl);
			action_fields.form->add_child(action_fields.attr);
			switch( current_item->action.attr ) {

				case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
				case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
				case Boid::ActionAttribute::BACTION_ATTR_COHESION:
				case Boid::ActionAttribute::BACTION_ATTR_STEER:
				case Boid::ActionAttribute::BACTION_ATTR_SPEED:
				case Boid::ActionAttribute::BACTION_ATTR_PUSH_SPEED:
				case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
				case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
				case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
				case Boid::ActionAttribute::BACTION_ATTR_FRICTION:
				case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
				case Boid::ActionAttribute::BACTION_ATTR_TIMER:
				case Boid::ActionAttribute::BACTION_ATTR_LIFETIME:
					action_fields.attr_float_lbl->set_text( "float/sec" );
					action_fields.form->add_child(action_fields.attr_float_lbl);
					action_fields.form->add_child(action_fields.attr_float);
					break;

				default:
					break;
			}
			break;

		case Boid::ActionType::BACTION_TYPE_RANDOMISE_ATTRIBUTE:
			action_fields.form->add_child(action_fields.attr_lbl);
			action_fields.form->add_child(action_fields.attr);
			action_fields.form->add_child(action_fields.rand_lbl);
			switch ( current_item->action.attr ) {
				case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
				case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
				case Boid::ActionAttribute::BACTION_ATTR_COHESION:
				case Boid::ActionAttribute::BACTION_ATTR_STEER:
				case Boid::ActionAttribute::BACTION_ATTR_SPEED:
				case Boid::ActionAttribute::BACTION_ATTR_PUSH_SPEED:
				case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
				case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
				case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
				case Boid::ActionAttribute::BACTION_ATTR_FRICTION:
				case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
				case Boid::ActionAttribute::BACTION_ATTR_TIMER:
				case Boid::ActionAttribute::BACTION_ATTR_LIFETIME:
					action_fields.form->add_child(action_fields.rand_float);
					break;
				case Boid::ActionAttribute::BACTION_ATTR_AIM:
				case Boid::ActionAttribute::BACTION_ATTR_POSITION:
				case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
				case Boid::ActionAttribute::BACTION_ATTR_FRONT:
				case Boid::ActionAttribute::BACTION_ATTR_PUSH:
					action_fields.form->add_child(action_fields.rand_vec3);
					break;
				case Boid::ActionAttribute::BACTION_ATTR_COLOR:
					action_fields.form->add_child(action_fields.rand_vec4);
					break;
				case Boid::ActionAttribute::BACTION_ATTR_NONE:
				default:
					action_fields.form->add_child(memnew(Label));
					break;
			}
			break;

		case Boid::ActionType::BACTION_TYPE_ASSIGN_TARGET:
			action_fields.form->add_child(action_fields.attr_ctrl_lbl);
			action_fields.form->add_child(action_fields.attr_ctrl);
			break;

		case Boid::ActionType::BACTION_TYPE_AIM_AT:
		case Boid::ActionType::BACTION_TYPE_REACH:
		case Boid::ActionType::BACTION_TYPE_ESCAPE:
			action_fields.form->add_child(action_fields.attr_group_lbl);
			action_fields.form->add_child(action_fields.attr_group);
			action_fields.form->add_child(action_fields.attr_float_lbl);
			action_fields.form->add_child(action_fields.attr_float);
			action_fields.form->add_child(action_fields.attr_bool_lbl);
			action_fields.form->add_child(action_fields.attr_bool);
			break;

		case Boid::ActionType::BACTION_TYPE_COUPLE:
			action_fields.form->add_child(action_fields.attr_group_lbl);
			action_fields.form->add_child(action_fields.attr_group);
			break;

		case Boid::ActionType::BACTION_TYPE_SIGNAL:
			action_fields.form->add_child(action_fields.attr_int_lbl);
			action_fields.form->add_child(action_fields.attr_int);
			break;

		case Boid::ActionType::BACTION_TYPE_LEAVE:
		case Boid::ActionType::BACTION_TYPE_NONE:
		default:
			break;

	}

	adjust_item_display( current_item );

}

void BoidBehaviourEditor::edit_action_active() {

	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	current_item->action.active = action_fields.active->is_pressed();
	adjust_item_display( current_item );
	save_behaviour(current_item->action.valid);

}

void BoidBehaviourEditor::edit_action_clone() {
	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	create_item(current_item);
}

void BoidBehaviourEditor::edit_action_delete() {
	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	remove_item( current_item->uid );
}

void BoidBehaviourEditor::edit_action_type(int i) {

	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	bool was_valid = current_item->action.valid;
	current_item->action.type = Boid::ActionType(i);
	adjust_action_editor();
	save_behaviour( was_valid || was_valid != current_item->action.valid );

}

void BoidBehaviourEditor::edit_action_attribute(int i) {

	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	bool was_valid = current_item->action.valid;
	current_item->action.attr = Boid::ActionAttribute(i);
	adjust_action_editor();
	save_behaviour( was_valid || was_valid != current_item->action.valid );

}

void BoidBehaviourEditor::edit_action_attr_bool() {

	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	current_item->action.param_bool = action_fields.attr_bool->is_pressed();
	adjust_item_display( current_item );
	save_behaviour( current_item->action.valid );

}

void BoidBehaviourEditor::edit_action_attr_int(double d) {

	if ( boid_behaviour == NULL || current_item == NULL || !current_item->is_action ) {
		return;
	}
	if ( current_item->action.param_int.initialised ) {
		current_item->action.param_int.value = int(d);
		boid_behaviour->validate_action(&current_item->action);
		if ( current_item->action.param_int.value != int(d) ) {
			action_fields.attr_int->set_value(current_item->action.param_int.value);
		}
	}
	adjust_item_display( current_item );
	save_behaviour( current_item->action.valid );

}

void BoidBehaviourEditor::edit_action_attr_float(double d) {

	if ( boid_behaviour == NULL || current_item == NULL || !current_item->is_action ) {
		return;
	}
	if ( current_item->action.param_float.initialised ) {
		current_item->action.param_float.value = float(d);
		boid_behaviour->validate_action(&current_item->action);
		if ( current_item->action.param_float.value != int(d) ) {
			action_fields.attr_float->set_value(current_item->action.param_float.value);
		}
	}
	adjust_item_display( current_item );
	save_behaviour( current_item->action.valid );

}

void BoidBehaviourEditor::edit_action_group(int index) {

	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	if ( current_item->action.param_int.initialised ) {
		current_item->action.param_int.value = 0;
		for ( int i = 0; i < 20; ++i ) {
			if ( action_fields.attr_group_checks[i]->is_pressed() ) {
				current_item->action.param_int.value+= 1 << i;
			}
		}
	}
	adjust_item_display( current_item );
	save_behaviour( current_item->action.valid );

}

void BoidBehaviourEditor::edit_action_anim(int index) {

	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	if ( current_item->action.param_int.initialised ) {
		current_item->action.param_int.value = 0;
		for ( int i = 0; i < 20; ++i ) {
			if ( action_fields.attr_anim_checks[i]->is_pressed() ) {
				current_item->action.param_int.value+= 1 << i;
			}
		}
	}
	adjust_item_display( current_item );
	save_behaviour( current_item->action.valid );

}

void BoidBehaviourEditor::edit_action_vec3(double d) {

	if ( boid_behaviour == NULL || current_item == NULL || !current_item->is_action ) {
		return;
	}
	if ( current_item->action.param_vec3.initialised ) {
		Vector3 tmp;
		for ( int i = 0; i < 3; ++i ) {
			tmp[i] = action_fields.attr_vec3_spins[i]->get_value();
			current_item->action.param_vec3.value[i] = tmp[i];
		}
		boid_behaviour->validate_action(&current_item->action);
		if ( tmp != current_item->action.param_vec3.value ) {
			for ( int i = 0; i < 3; ++i ) {
				action_fields.attr_vec3_spins[i]->set_value(current_item->action.param_vec3.value[i]);
			}
		}
	}
	adjust_item_display( current_item );
	save_behaviour( current_item->action.valid );

}

void BoidBehaviourEditor::edit_action_vec4(double d) {

	if ( boid_behaviour == NULL || current_item == NULL || !current_item->is_action ) {
		return;
	}
	if ( current_item->action.param_vec4.initialised ) {
		Color tmp;
		for ( int i = 0; i < 3; ++i ) {
			tmp[i] = action_fields.attr_vec4_spins[i]->get_value();
			current_item->action.param_vec4.value[i] = tmp[i];
		}
		boid_behaviour->validate_action(&current_item->action);
		if ( tmp != current_item->action.param_vec4.value ) {
			for ( int i = 0; i < 3; ++i ) {
				action_fields.attr_vec4_spins[i]->set_value(current_item->action.param_vec4.value[i]);
			}
		}
		action_fields.attr_vec4_preview->set_frame_color( current_item->action.param_vec4.value );
	}
	adjust_item_display( current_item );
	save_behaviour( current_item->action.valid );

}

void BoidBehaviourEditor::edit_action_ctrl(int i) {

	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	if ( current_item->action.param_ctrl.initialised ) {
		// none is selected
		if ( i == 0 ) {
			current_item->action.param_ctrl.control_UID = -1;
		} else {
			i--;
			if ( i >= ctrl_infos.size() ) {
				current_item->action.param_ctrl.control_UID = -1;
			} else {
				current_item->action.param_ctrl.control_UID = ctrl_infos[i].UID;
			}
		}
	}
	adjust_action_editor();
	bool was_valid = current_item->action.valid;
	validate_item( current_item );
	save_behaviour( was_valid || was_valid != current_item->action.valid );

}

void BoidBehaviourEditor::edit_action_rand_int(double d) {
	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	if ( current_item->action.param_int.initialised ) {
		current_item->action.param_int.min = action_fields.rand_int_min->get_value();
		current_item->action.param_int.max = action_fields.rand_int_max->get_value();
		regenerate_item_name(current_item);
		save_behaviour( current_item->action.valid );
	}
}

void BoidBehaviourEditor::edit_action_rand_float(double d) {
	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	if ( current_item->action.param_float.initialised ) {
		current_item->action.param_float.min = action_fields.rand_float_min->get_value();
		current_item->action.param_float.max = action_fields.rand_float_max->get_value();
		regenerate_item_name(current_item);
		save_behaviour( current_item->action.valid );
	}
}

void BoidBehaviourEditor::edit_action_rand_vec3(double d) {

	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	if ( current_item->action.param_vec3.initialised ) {
		for ( int i = 0; i < 3; ++i ) {
			current_item->action.param_vec3.min[i] = action_fields.rand_vec3_min[i]->get_value();
			current_item->action.param_vec3.max[i] = action_fields.rand_vec3_max[i]->get_value();
		}
		regenerate_item_name(current_item);
		save_behaviour( current_item->action.valid );
	}

}

void BoidBehaviourEditor::edit_action_rand_vec4(double d) {
	if ( current_item == NULL || !current_item->is_action ) {
		return;
	}
	if ( current_item->action.param_vec4.initialised ) {
		for ( int i = 0; i < 4; ++i ) {
			current_item->action.param_vec4.min[i] = action_fields.rand_vec4_min[i]->get_value();
			current_item->action.param_vec4.max[i] = action_fields.rand_vec4_max[i]->get_value();
		}
		regenerate_item_name(current_item);
		save_behaviour( current_item->action.valid );
	}
}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\\\ MANAGEMENT //////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidBehaviourEditor::add_test_item() {
	create_item();
}

void BoidBehaviourEditor::create_item( list_item* src, bool regenerate ) {

	list_item* li = memnew(list_item);
	li->uid = item_UID++;
	li->set = -1;

	if ( src != NULL ) {
		li->name = src->name;
		if ( li->name.find("[clone]") == -1 ) {
			li->name += " [clone]";
		}
		li->is_test = src->is_test;
		li->is_action = src->is_action;
		BoidBehaviour::clone_test( &src->test, &li->test );
		BoidBehaviour::clone_action( &src->action, &li->action );
	} else {
		String n = String::num( li->uid );
		while( n.length() < 3 ) {
			n = "0"+n;
		}
		li->name = String("test_") + n;
		li->is_test = true;
		li->is_action = false;
		BoidBehaviour::init_test( &li->test );
		BoidBehaviour::init_action( &li->action );
	}

	li->display = memnew(PanelContainer);
	li->display->set_h_size_flags(SIZE_EXPAND_FILL);
	li->display->add_style_override("panel", style_test_active_panel());

	HBoxContainer* vbox = memnew(HBoxContainer);
	vbox->set_h_size_flags(SIZE_EXPAND_FILL);
	vbox->add_constant_override("separation",0);
	li->display->add_child(vbox);

	TextureRect* icon = memnew(TextureRect);
	icon->set_texture(enode->get_gui_base()->get_icon("BoidTest", "EditorIcons"));
	vbox->add_child(icon);

	li->name_btn = memnew(Button);
	prepare_button( li->name_btn );
	li->name_btn->set_text( li->name );
	li->name_btn->set_text_align(Button::TextAlign::ALIGN_LEFT);
	li->name_btn->set_h_size_flags(SIZE_EXPAND_FILL);
	li->name_btn->set_clip_contents(true);
	li->name_btn->add_style_override("normal", style_button_normal());
	li->name_btn->add_style_override("hover", style_button_hover());
	li->name_btn->add_style_override("pressed", style_button_hover());
	li->name_btn->connect("pressed",this,"edit_item",varray(li->uid));
	vbox->add_child(li->name_btn);

	li->warning = memnew(TextureRect);
	li->warning->set_texture(enode->get_gui_base()->get_icon("NodeWarning", "EditorIcons"));
	vbox->add_child(li->warning);

	li->up = memnew(ToolButton);
	prepare_button( li->up );
	li->up->set_icon(enode->get_gui_base()->get_icon("GuiTreeArrowUp", "EditorIcons"));
	li->up->set_tooltip("Move up");
	li->up->connect("pressed",this,"move_up",varray(li->uid));
	vbox->add_child(li->up);

	li->down = memnew(ToolButton);
	prepare_button( li->down );
	li->down->set_icon(enode->get_gui_base()->get_icon("GuiTreeArrowDown", "EditorIcons"));
	li->down->set_tooltip("Move down");
	li->down->connect("pressed",this,"move_down",varray(li->uid));
	vbox->add_child(li->down);

	li->add = memnew(ToolButton);
	prepare_button( li->add );
	li->add->set_icon(enode->get_gui_base()->get_icon("Add", "EditorIcons"));
	li->add->set_tooltip("Add a new action");
	li->add->connect("pressed",this,"add_action_item",varray(li->uid));
	vbox->add_child(li->add);

	li->del = memnew(ToolButton);
	prepare_button( li->del );
	li->del->set_icon(enode->get_gui_base()->get_icon("Remove", "EditorIcons"));
	li->del->set_tooltip("Delete");
	li->del->connect("pressed",this,"remove_item",varray(li->uid));
	vbox->add_child(li->del);

	if ( src != NULL ) {
		list_items.insert( get_index(src->uid) + 1, li );
	} else {
		list_items.push_back(li);
	}

	if ( regenerate ) {
		regenerate_list();
		if ( li->is_test ) {
			save_behaviour( li->test.valid );
		} else if ( li->is_action ) {
			save_behaviour( li->action.valid );
		}
	}

}

void BoidBehaviourEditor::add_action_item( const int& id ) {

	int index = -1;
	for ( int i = 0, imax = list_items.size(); i < imax; ++i ) {
		if ( list_items[i]->uid == id ) {
			index = i;
			break;
		}
	}
	if (index == -1) {
		return;
	}

	list_item* li = memnew(list_item);
	li->uid = item_UID++;
	String n = String::num( li->uid );
	while( n.length() < 3 ) {
		n = "0"+n;
	}
	li->name = String("action_") + n;
	li->is_test = false;
	li->is_action = true;
	BoidBehaviour::init_test( &li->test );
	BoidBehaviour::init_action( &li->action );

	li->display = memnew(PanelContainer);
	li->display->set_h_size_flags(SIZE_EXPAND_FILL);
	li->display->add_style_override("panel", style_action_active_panel());

	HBoxContainer* vbox = memnew(HBoxContainer);
	vbox->set_h_size_flags(SIZE_EXPAND_FILL);
	vbox->add_constant_override("separation",0);
	li->display->add_child(vbox);

	TextureRect* icon = memnew(TextureRect);
	icon->set_texture(enode->get_gui_base()->get_icon("BoidAction", "EditorIcons"));
	vbox->add_child(icon);

	li->name_btn = memnew(Button);
	prepare_button( li->name_btn );
	li->name_btn->set_text( li->name );
	li->name_btn->set_h_size_flags(SIZE_EXPAND_FILL);
	li->name_btn->set_text_align(Button::TextAlign::ALIGN_LEFT);
	li->name_btn->set_clip_contents(true);
	li->name_btn->add_style_override("normal", style_button_normal());
	li->name_btn->add_style_override("hover", style_button_hover());
	li->name_btn->add_style_override("pressed", style_button_hover());
	li->name_btn->connect("pressed",this,"edit_item",varray(li->uid));
	vbox->add_child(li->name_btn);

	li->warning = memnew(TextureRect);
	li->warning->set_texture(enode->get_gui_base()->get_icon("NodeWarning", "EditorIcons"));
	vbox->add_child(li->warning);

	li->up = memnew(ToolButton);
	prepare_button( li->up );
	li->up->set_icon(enode->get_gui_base()->get_icon("GuiTreeArrowUp", "EditorIcons"));
	li->up->set_tooltip("Move up");
	li->up->connect("pressed",this,"move_up",varray(li->uid));
	vbox->add_child(li->up);

	li->down = memnew(ToolButton);
	prepare_button( li->down );
	li->down->set_icon(enode->get_gui_base()->get_icon("GuiTreeArrowDown", "EditorIcons"));
	li->down->set_tooltip("Move down");
	li->down->connect("pressed",this,"move_down",varray(li->uid));
	vbox->add_child(li->down);

	li->add = memnew(ToolButton);
	prepare_button( li->add );
	li->add->set_icon(enode->get_gui_base()->get_icon("Add", "EditorIcons"));
	li->add->set_tooltip("Add a new action");
	li->add->connect("pressed",this,"add_action_item",varray(li->uid));
	vbox->add_child(li->add);

	li->del = memnew(ToolButton);
	prepare_button( li->del );
	li->del->set_icon(enode->get_gui_base()->get_icon("Remove", "EditorIcons"));
	li->del->set_tooltip("Delete");
	li->del->connect("pressed",this,"remove_item",varray(li->uid));
	vbox->add_child(li->del);

	list_items.insert(index+1,li);

	current_item = NULL;
	load_test_editor();
	load_action_editor();
	regenerate_list();

}

void BoidBehaviourEditor::remove_item( const int& id ) {

	list_item* candidate = NULL;

	for ( int i = 0, imax = list_items.size(); i < imax; ++i ) {
		if ( list_items[i]->uid == id ) {
			candidate = list_items[i];
			break;
		}
	}

	if ( candidate != NULL ) {

		purge_item( candidate );
		candidate->name_btn->disconnect("pressed",this,"edit_item");
		candidate->add->disconnect("pressed",this,"add_action_item");
		candidate->up->disconnect("pressed",this,"move_up");
		candidate->down->disconnect("pressed",this,"move_down");
		candidate->del->disconnect("pressed",this,"remove_item");
		list_items.erase( candidate );
		regenerate_list();

		if (candidate->is_test) {
			std::cout << "remove test " << candidate->test.valid << "?" << std::endl;
			save_behaviour( candidate->test.valid );
		} else if (candidate->is_action) {
			std::cout << "remove test " << candidate->test.valid << "?" << std::endl;
			save_behaviour( candidate->action.valid );
		}

	}

}

void BoidBehaviourEditor::move_set_up( const int& id ) {

	if ( id < 1 || id >= item_sets.size() ) {
		return;
	}

	// releasing all parenting
	for ( int i = 0, imax = item_sets.size(); i < imax; ++i ) {
		if ( item_sets[i]->vbox == NULL ) {
			continue;
		}
		while( item_sets[i]->vbox->get_child_count() > 0 ) {
			item_sets[i]->vbox->remove_child( item_sets[i]->vbox->get_child(0) );
		}
	}

	item_set* s = item_sets[id];
	item_sets.remove(id);
	item_sets.insert(id-1, s);
	Vector<list_item*> new_list_items;
	for ( int i = 0, imax = item_sets.size(); i < imax; ++i ) {
		item_set* set = item_sets[i];
		for ( int j = 0, jmax = set->tests.size(); j < jmax; ++j ) {
			new_list_items.push_back( list_items[set->tests[j]] );
		}
		for ( int j = 0, jmax = set->actions.size(); j < jmax; ++j ) {
			new_list_items.push_back( list_items[set->actions[j]] );
		}
	}
	list_items.clear();
	list_items = new_list_items;
	regenerate_list();
	save_behaviour( true );

}

void BoidBehaviourEditor::move_set_down( const int& id ) {

	if ( id < 0 || id >= item_sets.size()-1 ) {
		return;
	}
	move_set_up( id+1 );

}

void BoidBehaviourEditor::move_up( const int& id ) {

	list_item* li = NULL;
	int index = -1;
	for ( int i = 1, imax = list_items.size(); i < imax; ++i ) {
		if ( list_items[i]->uid == id ) {
			index = i;
			li = list_items[i];
		}
	}

	if (index!=-1) {
		list_items.remove(index);
		list_items.insert(index-1, li);
		regenerate_list();
		save_behaviour( true );
	}

}

void BoidBehaviourEditor::move_down( const int& id ) {
	for ( int i = 0, imax = list_items.size(); i < imax; ++i ) {
		if ( list_items[i]->uid == id && i < imax-1 ) {
			move_up( list_items[i+1]->uid );
			return;
		}
	}
}

void BoidBehaviourEditor::create_new_set() {
	item_set* s = memnew( item_set );
	s->valid = true;
	s->panel = NULL;
	s->vbox = NULL;
	item_sets.push_back( s );
}

void BoidBehaviourEditor::regenerate_list() {

	// releasing list items
	for ( int i = 0, imax = item_sets.size(); i < imax; ++i ) {
		if ( item_sets[i]->vbox == NULL ) {
			continue;
		}
		while( item_sets[i]->vbox->get_child_count() > 0 ) {
			item_sets[i]->vbox->remove_child( item_sets[i]->vbox->get_child(0) );
		}
	}
	item_sets.clear();
	while( list_container->get_child_count() > 0 ) {
		list_container->remove_child( list_container->get_child(0) );
	}

	// stopping here if necessary
	if (!is_inside_tree() || boid_behaviour == NULL || desctuction_flag) {
		return;
	}

	// validating all items
	for ( int i = 0, imax = list_items.size(); i < imax; ++i ) {
		if ( list_items[i]->is_test && i < imax-1 ) {
			list_item* next = list_items[i+1];
			boid_behaviour->validate_test(&list_items[i]->test, &next->test);
		} else if ( list_items[i]->is_test ) {
			boid_behaviour->validate_test(&list_items[i]->test, NULL);
		} else if ( list_items[i]->is_action ) {
			boid_behaviour->validate_action(&list_items[i]->action);
		}
		adjust_item_display( list_items[i] );
		if ( i == 0 ) {
			list_items[i]->up->set_disabled(true);
			list_items[i]->down->set_disabled( imax == 1 );
		} else if ( i == imax-1 ) {
			list_items[i]->up->set_disabled(false);
			list_items[i]->down->set_disabled(true);
		} else {
			list_items[i]->up->set_disabled(false);
			list_items[i]->down->set_disabled(false);
		}
	}

	// regenerate sets
	create_new_set();
	item_set* current_set = item_sets[item_sets.size()-1];
	bool previous_andor = true;

	for ( int i = 0, imax = list_items.size(); i < imax; ++i ) {

		if ( list_items[i]->is_test ) {
			if ( !previous_andor ) {
				create_new_set();
				current_set = item_sets[item_sets.size()-1];
			}
			switch ( list_items[i]->test.after ) {
				case Boid::TestAfter::BTEST_AFTER_AND:
				case Boid::TestAfter::BTEST_AFTER_OR:
					previous_andor = true;
					break;
				case Boid::TestAfter::BTEST_AFTER_NONE:
				case Boid::TestAfter::BTEST_AFTER_EXIT:
				case Boid::TestAfter::BTEST_AFTER_GOTO:
				default:
					previous_andor = false;
					break;
			}
			current_set->valid = current_set->valid && list_items[i]->test.valid;
			current_set->tests.push_back( i );
			list_items[i]->set = list_items.size()-1;
		}

		if ( list_items[i]->is_action ) {
			current_set->valid = current_set->valid && !current_set->tests.empty();
			previous_andor = false;
			current_set->actions.push_back( i );
			list_items[i]->set = list_items.size()-1;
		}

	}
	if ( current_set->tests.empty() ) {
		item_sets.erase( current_set );
	}

	// rebuilding container & adjusting up & down visibility
	for ( int i = 0, imax = item_sets.size(); i < imax; ++i ) {

		item_set* set = item_sets[i];

		HBoxContainer* panel_wrp = memnew(HBoxContainer);
		panel_wrp->set_h_size_flags(SIZE_EXPAND_FILL);
		panel_wrp->add_constant_override("separation", 0);
		list_container->add_child( panel_wrp );

		VBoxContainer* set_menu = memnew(VBoxContainer);
		set_menu->add_constant_override("separation", 0);
		panel_wrp->add_child(set_menu);

		set->up = memnew(ToolButton);
		prepare_button( set->up );
		set->up->set_icon(enode->get_gui_base()->get_icon("GuiTreeArrowUp", "EditorIcons"));
		set->up->set_tooltip("Move up");
		set->up->set_disabled( i == 0 );
		set->up->connect("pressed",this,"move_set_up",varray(i));
		set_menu->add_child(set->up);

		set->down = memnew(ToolButton);
		prepare_button( set->down );
		set->down->set_icon(enode->get_gui_base()->get_icon("GuiTreeArrowDown", "EditorIcons"));
		set->down->set_tooltip("Move down");
		set->down->set_disabled( i == imax-1 );
		set->down->connect("pressed",this,"move_set_down",varray(i));
		set_menu->add_child(set->down);

		set->panel = memnew(PanelContainer);
		set->panel->set_h_size_flags(SIZE_EXPAND_FILL);
		set->panel->set_v_size_flags(SIZE_FILL);
		if ( set->valid ) {
			set->panel->add_style_override("panel", style_set_active());
		} else {
			set->panel->add_style_override("panel", style_set_inactive());
		}
		panel_wrp->add_child( set->panel );

		set->vbox = memnew(VBoxContainer);
		set->vbox->set_h_size_flags(SIZE_EXPAND_FILL);
		set->vbox->add_constant_override("separation", 0);
		set->panel->add_child( set->vbox );

		for ( int j = 0, jmax = set->tests.size(); j < jmax; ++j ) {
			list_item* li = list_items[set->tests[j]];
			set->vbox->add_child(li->display);
			li->name_btn->set_size(Vector2(0,0));
		}
		for ( int j = 0, jmax = set->actions.size(); j < jmax; ++j ) {
			list_item* li = list_items[set->actions[j]];
			set->vbox->add_child(li->display);
			li->name_btn->set_size(Vector2(0,0));
			// can not go before first test
			if ( i == 0 && j == 0 && set->tests.size() == 1 ) {
				li->up->set_disabled(true);
			}
		}

	}

}

void BoidBehaviourEditor::edit_item( const int& id ) {

	current_item = NULL;
	for ( int i = 0, imax = list_items.size(); i < imax; ++i ) {
		if ( list_items[i]->uid == id ) {
			current_item = list_items[i];
			break;
		}
	}

	if ( current_item == NULL || (!current_item->is_test && !current_item->is_action) ) {
		return;
	}

	load_test_editor();
	load_action_editor();

}

int BoidBehaviourEditor::get_index( const int& id ) {

	for ( int i = 0, imax = list_items.size(); i < imax; ++i ) {
		if ( list_items[i]->uid == id ) {
			return i;
		}
	}
	return -1;

}

int BoidBehaviourEditor::get_next_index( const int& id ) {

	for ( int i = 0, imax = list_items.size(); i < imax; ++i ) {
		if ( list_items[i]->uid == id && i < imax-1 ) {
			return i+1;
		}
	}
	return -1;

}

void BoidBehaviourEditor::validate_item( list_item* li ) {

	if ( boid_behaviour == NULL ) {
		return;
	}
	if ( li->is_test ) {
		int next_i = get_next_index( current_item->uid );
		if ( next_i == -1 ) {
			boid_behaviour->validate_test(&current_item->test, NULL);
		} else {
			list_item* next = list_items[next_i];
			boid_behaviour->validate_test(&current_item->test, &next->test);
		}
	} else if ( li->is_action ) {
		boid_behaviour->validate_action(&current_item->action);
	}

}

void BoidBehaviourEditor::purge_item( list_item* li ) {

	if ( li == NULL ) {
		return;
	}

	if ( current_item == li  ) {
		current_item = NULL;
		load_test_editor();
		load_action_editor();
	}

	BoidBehaviour::purge_test( &li->test );
	BoidBehaviour::purge_action( &li->action );

}

void BoidBehaviourEditor::purge_items() {

	item_UID = 0;
	// cleanup of inaccessible data in structures
	for ( int i = 0, imax = list_items.size(); i < imax; ++i ) {
		purge_item( list_items[i] );
	}
	list_items.clear();

}

void BoidBehaviourEditor::purge() {

	if ( boid_behaviour != NULL ) {
		boid_behaviour->disconnect("behaviour_info_updated", this, "regenerate_control_lists");
		boid_behaviour = NULL;
	}
	regenerate_list();

}

void BoidBehaviourEditor::load_behaviour() {

	if ( boid_behaviour == NULL ) {
		return;
	}

	purge_items();

	Vector<Boid::boid_behaviour_item> data;
	boid_behaviour->deserialise( data );

	for ( int i = 0, imax = data.size(); i < imax; ++i ) {
		create_item( NULL, false );
		list_item* li = list_items[i];
		if ( data[i].test != NULL ) {
			li->is_test = true;
			li->is_action = false;
			BoidBehaviour::clone_test( data[i].test, &li->test);
			BoidBehaviour::purge_test( data[i].test );
		} else if ( data[i].action != NULL ) {
			li->is_test = false;
			li->is_action = true;
			BoidBehaviour::clone_action( data[i].action, &li->action);
			BoidBehaviour::purge_action( data[i].action );
		}
		adjust_item_display( li );
	}

	regenerate_list();

}

void BoidBehaviourEditor::save_behaviour( bool reload ) {

	if ( boid_behaviour == NULL ) {
		return;
	}

	Vector<Boid::boid_behaviour_item> data;
	for ( int i = 0, imax = list_items.size(); i < imax; ++i ) {
		Boid::boid_behaviour_item bi;
		bi.test = NULL;
		bi.action = NULL;
		if ( list_items[i]->is_test ) {
			bi.test = &list_items[i]->test;
		} else if ( list_items[i]->is_action ) {
			bi.action = &list_items[i]->action;
		}
		data.push_back( bi );
	}

	boid_behaviour->serialise( data, reload );

}

void BoidBehaviourEditor::regenerate_control_lists() {

	if ( boid_behaviour == NULL ) {
		return;
	}

	ctrl_infos = boid_behaviour->get_control_info();

	test_fields.attr_ctrl->clear();
	action_fields.attr_ctrl->clear();

	test_fields.attr_ctrl->add_item("none");
	action_fields.attr_ctrl->add_item("none");

	for ( int i = 0, imax = ctrl_infos.size(); i < imax; ++i ) {
		const Boid::boid_control_info& bci = ctrl_infos[i];
		test_fields.attr_ctrl->add_item(bci.name + " ["+String::num(bci.UID)+"]");
		action_fields.attr_ctrl->add_item(bci.name + " ["+String::num(bci.UID)+"]");
		Ref<Texture> tex;
		switch( bci.type ) {
			case Boid::ControlType::BCTRL_TYPE_SOURCE:
				tex = enode->get_gui_base()->get_icon("BoidSource", "EditorIcons");
				break;
			case Boid::ControlType::BCTRL_TYPE_TARGET:
				tex = enode->get_gui_base()->get_icon("BoidTarget", "EditorIcons");
				break;
			case Boid::ControlType::BCTRL_TYPE_COLLIDER:
				tex = enode->get_gui_base()->get_icon("BoidCollider", "EditorIcons");
				break;
			case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
				tex = enode->get_gui_base()->get_icon("BoidAttractor", "EditorIcons");
				break;
			case Boid::ControlType::BCTRL_TYPE_NONE:
			default:
				tex = enode->get_gui_base()->get_icon("BoidControl", "EditorIcons");
				break;
		}
		test_fields.attr_ctrl->set_item_icon(i+1,tex);
		action_fields.attr_ctrl->set_item_icon(i+1,tex);
	}

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\ ENGINE LINKS //////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidBehaviourEditor::edit(BoidBehaviour *p_bbehaviour) {

	if ( boid_behaviour == p_bbehaviour ) {
		return;
	}

	if ( boid_behaviour != p_bbehaviour ) {
		purge();
	}

	boid_behaviour = p_bbehaviour;

	if ( boid_behaviour != NULL ) {
		regenerate_control_lists();
		boid_behaviour->connect("behaviour_info_updated", this, "regenerate_control_lists");
	}

}

void BoidBehaviourEditor::_node_removed(Node *p_node) {

	BoidBehaviour* bb = Object::cast_to<BoidBehaviour>(p_node);
	if ( bb != NULL && bb == boid_behaviour ) {
		purge();
	}

}

void BoidBehaviourEditor::_notification(int p_what) {

	switch (p_what) {

		case NOTIFICATION_PREDELETE:
			desctuction_flag = true;
			break;

		case NOTIFICATION_ENTER_TREE:
			get_tree()->connect("node_removed", this, "_node_removed");
			if ( boid_behaviour != NULL ) {
				load_behaviour();
			}
			break;

		case NOTIFICATION_EXIT_TREE:
			get_tree()->disconnect("node_removed", this, "_node_removed");
			break;

		case NOTIFICATION_PROCESS: {
			if (!boid_behaviour) {
				return;
			}
		} break;

		case NOTIFICATION_THEME_CHANGED: {
		} break;
	}

}

void BoidBehaviourEditor::_bind_methods() {

	ClassDB::bind_method("_node_removed", &BoidBehaviourEditor::_node_removed);
	ClassDB::bind_method("add_test_item", &BoidBehaviourEditor::add_test_item);

	ClassDB::bind_method(D_METHOD("edit_item", "uid"), &BoidBehaviourEditor::edit_item);
	ClassDB::bind_method(D_METHOD("add_action_item", "uid"), &BoidBehaviourEditor::add_action_item);
	ClassDB::bind_method(D_METHOD("remove_item", "uid"), &BoidBehaviourEditor::remove_item);
	ClassDB::bind_method(D_METHOD("move_up", "uid"), &BoidBehaviourEditor::move_up);
	ClassDB::bind_method(D_METHOD("move_down", "uid"), &BoidBehaviourEditor::move_down);
	ClassDB::bind_method(D_METHOD("move_set_up", "set_id"), &BoidBehaviourEditor::move_set_up);
	ClassDB::bind_method(D_METHOD("move_set_down", "set_id"), &BoidBehaviourEditor::move_set_down);

	ClassDB::bind_method("edit_close_all", &BoidBehaviourEditor::edit_close_all);
	ClassDB::bind_method("regenerate_control_lists", &BoidBehaviourEditor::regenerate_control_lists);

	ClassDB::bind_method("edit_test_active", &BoidBehaviourEditor::edit_test_active);
	ClassDB::bind_method("edit_test_clone", &BoidBehaviourEditor::edit_test_clone);
	ClassDB::bind_method("edit_test_delete", &BoidBehaviourEditor::edit_test_delete);
	ClassDB::bind_method("edit_test_type", &BoidBehaviourEditor::edit_test_type);
	ClassDB::bind_method("edit_test_attribute", &BoidBehaviourEditor::edit_test_attribute);
	ClassDB::bind_method("edit_test_eval", &BoidBehaviourEditor::edit_test_eval);
	ClassDB::bind_method("edit_test_attr_bool", &BoidBehaviourEditor::edit_test_attr_bool);
	ClassDB::bind_method("edit_test_attr_int", &BoidBehaviourEditor::edit_test_attr_int);
	ClassDB::bind_method("edit_test_attr_float", &BoidBehaviourEditor::edit_test_attr_float);
	ClassDB::bind_method("edit_test_attr_event", &BoidBehaviourEditor::edit_test_attr_event);
	ClassDB::bind_method("edit_test_attr_ctrl", &BoidBehaviourEditor::edit_test_attr_ctrl);
	ClassDB::bind_method("edit_test_repeat", &BoidBehaviourEditor::edit_test_repeat);
	ClassDB::bind_method("edit_test_after", &BoidBehaviourEditor::edit_test_after);
	ClassDB::bind_method("edit_test_attr_goto", &BoidBehaviourEditor::edit_test_attr_goto);
	ClassDB::bind_method("edit_test_group", &BoidBehaviourEditor::edit_test_group);

	ClassDB::bind_method("edit_action_active", &BoidBehaviourEditor::edit_action_active);
	ClassDB::bind_method("edit_action_clone", &BoidBehaviourEditor::edit_action_clone);
	ClassDB::bind_method("edit_action_delete", &BoidBehaviourEditor::edit_action_delete);
	ClassDB::bind_method("edit_action_type", &BoidBehaviourEditor::edit_action_type);
	ClassDB::bind_method("edit_action_attribute", &BoidBehaviourEditor::edit_action_attribute);
	ClassDB::bind_method("edit_action_attr_bool", &BoidBehaviourEditor::edit_action_attr_bool);
	ClassDB::bind_method("edit_action_attr_int", &BoidBehaviourEditor::edit_action_attr_int);
	ClassDB::bind_method("edit_action_attr_float", &BoidBehaviourEditor::edit_action_attr_float);
	ClassDB::bind_method("edit_action_group", &BoidBehaviourEditor::edit_action_group);
	ClassDB::bind_method("edit_action_anim", &BoidBehaviourEditor::edit_action_anim);
	ClassDB::bind_method("edit_action_vec3", &BoidBehaviourEditor::edit_action_vec3);
	ClassDB::bind_method("edit_action_vec4", &BoidBehaviourEditor::edit_action_vec4);
	ClassDB::bind_method("edit_action_ctrl", &BoidBehaviourEditor::edit_action_ctrl);
	ClassDB::bind_method("edit_action_rand_int", &BoidBehaviourEditor::edit_action_rand_int);
	ClassDB::bind_method("edit_action_rand_float", &BoidBehaviourEditor::edit_action_rand_float);
	ClassDB::bind_method("edit_action_rand_vec3", &BoidBehaviourEditor::edit_action_rand_vec3);
	ClassDB::bind_method("edit_action_rand_vec4", &BoidBehaviourEditor::edit_action_rand_vec4);

}

BoidBehaviourEditor::BoidBehaviourEditor(EditorNode *p_editor) :
	undo_redo(NULL),
	enode(NULL),
	boid_behaviour(NULL),
	menu(NULL),
	add_test(NULL),
	list_scroll(NULL),
	list_container(NULL),
	item_UID(0),
	current_item(NULL),
	desctuction_flag(false)
	{

	enode = p_editor;

	// extracting enums
	HashMap<String,String> enum_strings;
	BoidBehaviour::get_behaviour_enums( enum_strings );
	List<String> keys;
	enum_strings.get_key_list( &keys );
	for ( int i = 0, imax = keys.size(); i < imax; ++i ) {
		enums[keys[i]] = enum_strings[keys[i]].split(",");
	}

	int mw = EDITOR_DEF("editors/boid_behaviour/palette_min_width", 230);

//	spatial_editor_hb = memnew(HBoxContainer);
//	spatial_editor_hb->set_h_size_flags(SIZE_EXPAND_FILL);
//	spatial_editor_hb->set_alignment(BoxContainer::ALIGN_END);
//	spatial_editor_hb->add_style_override("panel", style_main_panel());
//	SpatialEditor::get_singleton()->add_control_to_menu_panel(spatial_editor_hb);

	PanelContainer* main = memnew(PanelContainer);
	main->set_v_size_flags(SIZE_EXPAND_FILL);
	main->set_h_size_flags(SIZE_EXPAND_FILL);
	main->add_style_override("panel", style_main_panel());
	add_child(main);

	// main container
	VBoxContainer* vbc = memnew(VBoxContainer);
	vbc->set_v_size_flags(SIZE_EXPAND_FILL);
	vbc->set_h_size_flags(SIZE_EXPAND_FILL);
	vbc->set_custom_minimum_size(Size2(mw, 0) * EDSCALE);
	main->add_child(vbc);

	// menu

	PanelContainer* menu_wrp = memnew(PanelContainer);
	menu_wrp->add_style_override("panel", style_editor_menu());
	vbc->add_child(menu_wrp);

	menu = memnew(HBoxContainer);
	menu->set_h_size_flags(SIZE_EXPAND_FILL);
	menu->add_constant_override("separation",0);
	menu_wrp->add_child(menu);

	TextureRect* icon = memnew(TextureRect);
	icon->set_texture(enode->get_gui_base()->get_icon("BoidBehaviour", "EditorIcons"));
	menu->add_child(icon);

	Label* lbl = memnew(Label);
	lbl->set_text("List");
	lbl->set_h_size_flags(SIZE_EXPAND_FILL);
	menu->add_child(lbl);

	add_test = memnew(ToolButton);
	prepare_button( add_test );
	add_test->set_icon(enode->get_gui_base()->get_icon("Add", "EditorIcons"));
	add_test->set_tooltip("Add a new test");
	add_test->connect("pressed",this, "add_test_item");
	menu->add_child(add_test);

	// list of tests & actions
	list_scroll = memnew(ScrollContainer);
	list_scroll->set_h_size_flags(SIZE_EXPAND_FILL);
	list_scroll->set_v_size_flags(SIZE_FILL);
	list_scroll->set_enable_h_scroll(true);
	list_scroll->set_enable_v_scroll(false);
	vbc->add_child(list_scroll);

	list_container = memnew(VBoxContainer);
	list_container->set_h_size_flags(SIZE_EXPAND_FILL);
	list_container->set_v_size_flags(SIZE_FILL);
	list_container->add_constant_override("separation",4);
	list_scroll->add_child(list_container);

	// test editor
	build_test_editor(vbc);

	// action editor
	build_action_editor(vbc);

}

BoidBehaviourEditor::~BoidBehaviourEditor() {
	purge();
}

