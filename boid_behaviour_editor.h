/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 * boid_behaviour_editor.h
 *
 *  Created on: Mar 16, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_BOIDS_BOID_BEHAVIOUR_EDITOR_H_
#define MODULES_BOIDS_BOID_BEHAVIOUR_EDITOR_H_

#include <iostream>

#include "core/class_db.h"
#include "core/color.h"
#include "core/hash_map.h"
#include "core/list.h"
#include "core/math/math_defs.h"
#include "core/math/vector2.h"
#include "core/math/vector3.h"
#include "core/object.h"
#include "core/os/memory.h"
#include "core/project_settings.h"
#include "core/reference.h"
#include "core/string_name.h"
#include "core/typedefs.h"
#include "core/ustring.h"
#include "core/variant.h"
#include "core/vector.h"
#include "editor/editor_scale.h"
#include "editor/editor_node.h"
#include "editor/editor_plugin.h"
#include "editor/pane_drag.h"
#include "editor/plugins/spatial_editor_plugin.h"
#include "scene/gui/color_rect.h"
#include "editor/editor_settings.h"
#include "scene/gui/box_container.h"
#include "scene/gui/button.h"
#include "scene/gui/check_box.h"
#include "scene/gui/control.h"
#include "scene/gui/grid_container.h"
#include "scene/gui/label.h"
#include "scene/gui/line_edit.h"
#include "scene/gui/option_button.h"
#include "scene/gui/panel_container.h"
#include "scene/gui/scroll_container.h"
#include "scene/gui/spin_box.h"
#include "scene/gui/texture_rect.h"
#include "scene/gui/tool_button.h"
#include "scene/main/node.h"
#include "scene/main/scene_tree.h"
#include "scene/resources/style_box.h"

#include "boid_common.h"
#include "boid_behaviour.h"

class BoidBehaviourEditor : public VBoxContainer {
	GDCLASS(BoidBehaviourEditor, VBoxContainer);

	Ref<StyleBoxFlat> main_panel;
	Ref<StyleBoxFlat> button_normal;
	Ref<StyleBoxFlat> button_hover;
	Ref<StyleBoxFlat> test_active_panel;
	Ref<StyleBoxFlat> test_inactive_panel;
	Ref<StyleBoxFlat> action_active_panel;
	Ref<StyleBoxFlat> action_inactive_panel;
	Ref<StyleBoxFlat> section_panel;
	Ref<StyleBoxFlat> editor_menu;
	Ref<StyleBoxFlat> test_preview;
	Ref<StyleBoxFlat> panel_error;
	Ref<StyleBoxFlat> test_attribute;
	Ref<StyleBoxFlat> no_margin;
	Ref<StyleBoxFlat> vector_attr;
	Ref<StyleBoxFlat> set_active;
	Ref<StyleBoxFlat> set_inactive;

	struct list_item {
		int uid;
		String name;
		bool is_test;
		bool is_action;
		Boid::boid_behaviour_test test;
		Boid::boid_behaviour_action action;
		PanelContainer* display;
		TextureRect* warning;
		Button* name_btn;
		ToolButton* up;
		ToolButton* down;
		ToolButton* add;
		ToolButton* del;
		int set;
	};

	struct item_set {
		bool valid;
		Vector<int> tests;
		Vector<int> actions;
		PanelContainer* panel;
		VBoxContainer* vbox;
		ToolButton* up;
		ToolButton* down;
	};

	struct test_field_list {
		VBoxContainer* panel;
		Label* error_lbl;
		Label* error;
		CheckBox* active;
		ToolButton* clone;
		ToolButton* del;
		Label* preview;
		GridContainer* form;

		Label* type_lbl;
		OptionButton* type;

		Label* attr_lbl;
		OptionButton* attr;

		Label* eval_lbl;
		OptionButton* eval;

		Label* attr_bool_lbl;
		CheckBox* attr_bool;

		Label* attr_int_lbl;
		SpinBox* attr_int;

		Label* attr_group_lbl;
		GridContainer* attr_group;
		CheckBox* attr_group_checks[20];

		Label* attr_anim_lbl;
		GridContainer* attr_anim;
		CheckBox* attr_anim_checks[20];

		Label* attr_float_lbl;
		SpinBox* attr_float;

		Label* attr_ctrl_lbl;
		OptionButton* attr_ctrl;

		Label* attr_event_lbl;
		OptionButton* attr_event;

		Label* repeat_lbl;
		OptionButton* repeat;

		Label* after_lbl;
		OptionButton* after;

		Label* attr_goto_lbl;
		SpinBox* attr_goto;
	};

	struct action_field_list {
		VBoxContainer* panel;
		Label* error_lbl;
		Label* error;
		CheckBox* active;
		ToolButton* clone;
		ToolButton* del;
		GridContainer* form;

		Label* type_lbl;
		OptionButton* type;

		Label* attr_lbl;
		OptionButton* attr;

		Label* attr_bool_lbl;
		CheckBox* attr_bool;

		Label* attr_int_lbl;
		SpinBox* attr_int;

		Label* attr_group_lbl;
		GridContainer* attr_group;
		CheckBox* attr_group_checks[20];

		Label* attr_anim_lbl;
		GridContainer* attr_anim;
		CheckBox* attr_anim_checks[20];

		Label* attr_float_lbl;
		SpinBox* attr_float;

		Label* attr_vec3_lbl;
		GridContainer* attr_vec3;
		SpinBox* attr_vec3_spins[3];

		Label* attr_vec4_lbl;
		HBoxContainer* attr_vec4;
		ColorRect* attr_vec4_preview;
		SpinBox* attr_vec4_spins[4];

		Label* rand_lbl;

		HBoxContainer* rand_int;
		SpinBox* rand_int_min;
		SpinBox* rand_int_max;

		HBoxContainer* rand_float;
		SpinBox* rand_float_min;
		SpinBox* rand_float_max;

		HBoxContainer* rand_vec3;
		SpinBox* rand_vec3_min[3];
		SpinBox* rand_vec3_max[3];

		HBoxContainer* rand_vec4;
		SpinBox* rand_vec4_min[4];
		SpinBox* rand_vec4_max[4];

		Label* attr_ctrl_lbl;
		OptionButton* attr_ctrl;
	};

	UndoRedo *undo_redo;

	HashMap<String,Vector<String>> enums;
	Vector<Boid::boid_control_info> ctrl_infos;

	EditorNode* enode;

	BoidBehaviour* boid_behaviour;

	HBoxContainer* menu;
	ToolButton* add_test;
	ScrollContainer* list_scroll;
	VBoxContainer* list_container;

	test_field_list test_fields;
	action_field_list action_fields;

	int item_UID;
	Vector<list_item*> list_items;
	Vector<item_set*> item_sets;
	list_item* current_item;
	bool desctuction_flag;
	bool behaviour_loaded;

	const Ref<StyleBoxFlat>& style_main_panel();
	const Ref<StyleBoxFlat>& style_button_normal();
	const Ref<StyleBoxFlat>& style_button_hover();
	const Ref<StyleBoxFlat>& style_test_active_panel();
	const Ref<StyleBoxFlat>& style_test_inactive_panel();
	const Ref<StyleBoxFlat>& style_action_active_panel();
	const Ref<StyleBoxFlat>& style_action_inactive_panel();
	const Ref<StyleBoxFlat>& style_section_panel();
	const Ref<StyleBoxFlat>& style_editor_menu();
	const Ref<StyleBoxFlat>& style_test_preview();
	const Ref<StyleBoxFlat>& style_error();
	const Ref<StyleBoxFlat>& style_test_attribute();
	const Ref<StyleBoxFlat>& style_no_margin();
	const Ref<StyleBoxFlat>& style_vector_attr();
	const Ref<StyleBoxFlat>& style_set_active();
	const Ref<StyleBoxFlat>& style_set_inactive();

	void prepare_button( Button* b );
	void prepare_error( Label* l );
	void prepare_preview( Label* l );
	void prepare_group_checkbox( CheckBox* cb, const int& i );
	void prepare_anim_checkbox( CheckBox* cb, const int& i );
	void adjust_item_display( list_item* li );
	void prepare_vectr_atrr( Label* l, int axis );

protected:
	void _notification(int p_what);
	static void _bind_methods();

	void prepare_fieldname( Label* l, const String& text );
	void prepare_attrname( Label* l, const String& text );
	void add_test_fieldname( Label* l, const String& text );
	void add_test_attrname( Label* l, const String& text );
	void add_action_fieldname( Label* l, const String& text );
	void add_action_attrname( Label* l, const String& text );

	void build_test_editor( Control* parent );
	void load_test_editor();
	void adjust_test_editor();
	void regenerate_test_preview();

	void build_action_editor( Control* parent );
	void load_action_editor();
	void adjust_action_editor();

	void create_new_set();
	void regenerate_list();
	int get_index( const int& id );
	int get_next_index( const int& id );
	void load_behaviour();
	void save_behaviour( bool reload );
	String prepare_for_display( const String& s );
	String get_group_name( const int& i );
	bool get_group_color( const int& i, Color& c );
	String get_anim_name( const int& i );
	bool get_anim_color( const int& i, Color& c );
	void regenerate_item_name( list_item* li );
	void rename_item( list_item* li );
	void create_item( list_item* src = NULL, bool regenerate = true );
	void validate_item( list_item* li );
	void purge_item( list_item* li );
	void purge_items();
	void purge();

public:

	void edit(BoidBehaviour *p_bbehaviour);
	void _node_removed(Node *p_node);

	// main
	void add_test_item();
	void add_action_item( const int& id );
	void edit_item( const int& id );
	void remove_item( const int& id );
	void move_up( const int& id );
	void move_down( const int& id );
	void move_set_up( const int& id );
	void move_set_down( const int& id );

	// all editors
	void regenerate_control_lists();
	void edit_close_all();

	// test editor
	void edit_test_active();
	void edit_test_clone();
	void edit_test_delete();
	void edit_test_type(int);
	void edit_test_attribute(int);
	void edit_test_eval(int);
	void edit_test_attr_bool();
	void edit_test_attr_int(double);
	void edit_test_attr_float(double);
	void edit_test_attr_event(int);
	void edit_test_group(int);
	void edit_test_repeat(int);
	void edit_test_after(int);
	void edit_test_attr_goto(double);
	void edit_test_attr_ctrl(int);

	// action editor
	void edit_action_active();
	void edit_action_clone();
	void edit_action_delete();
	void edit_action_type(int);
	void edit_action_attribute(int);
	void edit_action_attr_bool();
	void edit_action_attr_int(double);
	void edit_action_attr_float(double);
	void edit_action_group(int);
	void edit_action_anim(int);
	void edit_action_vec3(double);
	void edit_action_vec4(double);
	void edit_action_ctrl(int);
	void edit_action_rand_int(double d);
	void edit_action_rand_float(double d);
	void edit_action_rand_vec3(double d);
	void edit_action_rand_vec4(double d);

	BoidBehaviourEditor(EditorNode *p_editor);
	virtual ~BoidBehaviourEditor();

};

#endif /* MODULES_BOIDS_BOID_BEHAVIOUR_EDITOR_H_ */
