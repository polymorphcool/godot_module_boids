/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_collider.cpp
 *
 *  Created on: Feb 24, 2021
 *      Author: frankiezafe
 */

#include "boid_collider.h"

void BoidCollider::set_weight( const float& p_weight ) {
	data.weight = p_weight;
}

const float& BoidCollider::get_weight() const {
	return data.weight;
}

void BoidCollider::set_influence_direction( const bool& p_dir ) {
	data.influence_direction = p_dir;
}

const bool& BoidCollider::is_influence_direction() const {
	return data.influence_direction;
}

void BoidCollider::set_influence_front( const bool& p_dir ) {
	data.influence_front = p_dir;
}

const bool& BoidCollider::is_influence_front() const {
	return data.influence_front;
}

void BoidCollider::set_strength( float p_strength ) {
	data.force = p_strength;
}

const float& BoidCollider::get_strength() const {
	return data.force;
}

void BoidCollider::_bind_methods() {

	ClassDB::bind_method(D_METHOD("set_weight", "weight"), &BoidCollider::set_weight);
	ClassDB::bind_method(D_METHOD("get_weight"), &BoidCollider::get_weight);

	ClassDB::bind_method(D_METHOD("set_influence_direction", "influence_direction"), &BoidCollider::set_influence_direction);
	ClassDB::bind_method(D_METHOD("is_influence_direction"), &BoidCollider::is_influence_direction);

	ClassDB::bind_method(D_METHOD("set_influence_front", "influence_front"), &BoidCollider::set_influence_front);
	ClassDB::bind_method(D_METHOD("is_influence_front"), &BoidCollider::is_influence_front);

	ClassDB::bind_method(D_METHOD("set_strength", "strength"), &BoidCollider::set_strength);
	ClassDB::bind_method(D_METHOD("get_strength"), &BoidCollider::get_strength);

	ClassDB::bind_method(D_METHOD("set_ground", "ground"), &BoidCollider::set_ground);
	ClassDB::bind_method(D_METHOD("is_ground"), &BoidCollider::is_ground);

	ADD_PROPERTY( PropertyInfo(Variant::REAL, "weight", PROPERTY_HINT_RANGE,
					"0,10,0.001,or_greater"), "set_weight", "get_weight");
	ADD_PROPERTY( PropertyInfo(Variant::BOOL, "influence_direction"), "set_influence_direction", "is_influence_direction");
	ADD_PROPERTY( PropertyInfo(Variant::BOOL, "influence_front"), "set_influence_front", "is_influence_front");
	ADD_PROPERTY( PropertyInfo(Variant::REAL, "strength", PROPERTY_HINT_RANGE,
					"0,10,0.001,or_greater,or_lesser"), "set_strength", "get_strength");
	ADD_PROPERTY( PropertyInfo(Variant::BOOL, "ground"), "set_ground", "is_ground");

}

BoidCollider::BoidCollider() {
	data.owner = this;
	data.type = Boid::ControlType::BCTRL_TYPE_COLLIDER;
	data.group = 31775; // [0,4] + [10,14]
}

BoidCollider::~BoidCollider() {
	// TODO Auto-generated destructor stub
}

