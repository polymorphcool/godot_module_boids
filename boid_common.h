/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_common.h
 *
 *  Created on: Feb 8, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_BOIDS_BOID_COMMON_H_
#define MODULES_BOIDS_BOID_COMMON_H_

#include <string>
#include "core/color.h"
#include "core/math/transform.h"

const int boid_GROUP_COUNT = 21;
const float boid_PI = 3.14159265358979323846264;
const float boid_TAU = boid_PI*2;

// update request flags
const int boid_DISPLAY_DOMAIN = 				1;
const int boid_REFRESH_DOMAIN = 				2;
const int boid_DISPLAY_CELL = 					4;
const int boid_REFRESH_CELL = 					8;
const int boid_DISPLAY_MODIFIER_BOIDS = 		16;
const int boid_REFRESH_MODIFIER_BOIDS_DEBUG = 	32;
const int boid_UPDATE_SHADER = 					64;
const int boid_INIT_DOMAIN = 					128;
const int boid_UPDATE_DOMAIN_PLANE = 			256;
const int boid_REGENERATE_MODIFIER_BOIDS = 		512;
const int boid_INIT_TEXTURES = 					1024;

// list of vertices used by box controller, order is important
const Vector3 box_vertices[8] = {
	// top face
	Vector3(1,1,1), Vector3(-1,1,1), Vector3(-1,1,-1), Vector3(1,1,-1),
	// bottom face
	Vector3(1,-1,1), Vector3(-1,-1,1), Vector3(-1,-1,-1), Vector3(1,-1,-1)
	};

#endif /* MODULES_BOIDS_BOID_COMMON_H_ */
