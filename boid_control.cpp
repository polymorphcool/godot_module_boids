/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_control.cpp
 *
 *  Created on: Feb 8, 2021
 *      Author: frankiezafe
 */

#include "boid_control.h"
#include "boid_server.h"

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\\\ INTERNAL ////////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidControl::_parented() {

	system_unregister();

	BoidSystem* prev_bs = bsystem;
	BoidMesh* prev_bm = bmesh;

	Node *n = get_parent();

	if (n != NULL) {

		if (Object::cast_to<BoidSystem>(n)) {

			// control is a child of a system
			bsystem = Object::cast_to<BoidSystem>(n);
			bmesh = NULL;

		} else if (Object::cast_to<BoidMesh>(n)) {

			// control is a child of a mesh
			bmesh = Object::cast_to<BoidMesh>(n);
			bsystem = bmesh->get_boid_system();

		} else if (Object::cast_to<BoidControl>(n)) {

			// control is a child of another control
			BoidControl* ctrl = Object::cast_to<BoidControl>(n);
			bsystem = ctrl->get_boid_system();
			bmesh = ctrl->get_boid_mesh();

		}

	}

	data.mesh = bmesh;

	// connections management
	if ( prev_bs != NULL && prev_bs != bsystem ) {
		prev_bs->disconnect("boid_control_updated", this, "system_control_updated");
	}
	if ( bsystem != NULL && prev_bs != bsystem ) {
		bsystem->connect("boid_control_updated", this, "system_control_updated");
	}
	if ( prev_bm != NULL && prev_bm != bmesh ) {
		prev_bm->disconnect("boid_system_changed", this, "system_changed");
	}
	if ( bmesh != NULL && prev_bm != bmesh ) {
		bmesh->connect("boid_system_changed", this, "system_changed");
	}

	system_register();

	update_configuration_warning();

}

void BoidControl::system_control_updated() {
}

BoidSystem* BoidControl::get_boid_system() {
	return bsystem;
}

BoidMesh* BoidControl::get_boid_mesh() {
	return bmesh;
}

void BoidControl::_shape_changed() {

	if (shape.is_null()) {

		data.shape = Boid::ControlShape::BCTRL_SHAPE_POINT;
		data.dimension = Vector3(0, 0, 0);
		data.volume = 0;
		data.margin = 0;

	} else if (Object::cast_to<SphereShape>(*shape)) {

		data.shape = Boid::ControlShape::BCTRL_SHAPE_SPHERE;
		data.dimension = Vector3(1, 1, 1) * Object::cast_to<SphereShape>(*shape)->get_radius();
		data.volume = 4 * boid_PI * Math::pow(data.dimension.x, 3) / 3.0;
		data.margin = shape->get_margin();

	} else if (Object::cast_to<BoxShape>(*shape)) {

		data.shape = Boid::ControlShape::BCTRL_SHAPE_BOX;
		data.dimension = Object::cast_to<BoxShape>(*shape)->get_extents();
		data.volume = data.dimension[0] * data.dimension[1] * data.dimension[2];
		data.margin = shape->get_margin();

	}

	if (bsystem) {
		data.regenerate_domain = true;
		bsystem->update_control(&data);
	}

	if (is_inside_tree() && !debug_shape_dirty) {
		debug_shape_dirty = true;
		call_deferred("_update_debug_shape");
	}

}

void BoidControl::purge_debug_mesh() {
	if (mi != NULL) {
		remove_child(mi);
		mi = NULL;
	}
}

void BoidControl::_update_debug_shape() {

	debug_shape_dirty = false;

	Ref<Shape> s = get_shape();
	if (s.is_null()) {
		purge_debug_mesh();
		return;
	}

	Ref<Mesh> mesh = s->get_debug_mesh();
	if (mi == NULL) {
		mi = memnew(MeshInstance); // @suppress("Abstract class cannot be instantiated")
		add_child(mi);
	}
	mi->set_mesh(mesh);

}

void BoidControl::update_data() {

	if (is_inside_tree()) {
#ifdef TOOLS_ENABLED
		data.name = get_name();
#endif
		data.active = active;
		Transform t = get_global_transform();
		if (data.global != t) {
			data.regenerate_domain = true;
			data.global = t;
		}
	} else {
		data.active = false;
	}

}

void BoidControl::system_changed() {
	_parented();
}

void BoidControl::system_register() {

	if ( bsystem ) {
		update_data();
		bsystem->update_control(&data);
		// boid system set the UID automatically
		set_UID( data.UID );
	}
	for (int i = 0, max = get_child_count(); i < max; ++i) {
		BoidControl* ctrl = Object::cast_to<BoidControl>(get_child(i));
		if ( ctrl ) {
			ctrl->_parented();
		}
	}

}

void BoidControl::system_unregister() {

	if (bsystem) {
		bsystem->remove_control(&data);
		bsystem = NULL;
	}
	for (int i = 0, max = get_child_count(); i < max; ++i) {
		BoidControl* ctrl = Object::cast_to<BoidControl>(get_child(i));
		if ( ctrl ) {
			ctrl->system_unregister();
		}
	}

}

void BoidControl::system_update() {

	if (bsystem) {
		update_data();
		bsystem->update_control(&data);
		for (int i = 0, max = get_child_count(); i < max; ++i) {
			BoidControl* ctrl = Object::cast_to<BoidControl>(get_child(i));
			if ( ctrl ) {
				ctrl->system_update();
			}
		}
	}

}

void BoidControl::system_purge() {

	if (bsystem) {
		BoidBehaviour::purge( &data.behaviour );
		bsystem->remove_control(&data);
	}
	if ( bsystem != NULL ) {
		bsystem->disconnect("boid_control_updated", this, "system_control_updated");
	}
	if ( bmesh != NULL ) {
		bmesh->disconnect("boid_system_changed", this, "system_changed");
	}
	bsystem = NULL;
	bmesh = NULL;
	data.mesh = NULL;
	for (int i = 0, max = get_child_count(); i < max; ++i) {
		BoidControl* ctrl = Object::cast_to<BoidControl>(get_child(i));
		if ( ctrl ) {
			ctrl->system_purge();
		}
	}

}

void BoidControl::internal_process() {}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\ SETTERS & GETTERS ///////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidControl::set_UID(const int& p_uid) {

	if ( p_uid < 0 ) {
		return;
	}

	if ( bsystem == NULL ) {
		UID = p_uid;
		data.UID = UID;
	} else {
		// making sure that UID is unique!
		data.UID = p_uid;
		bsystem->validate_control_uid( &data );
		UID = data.UID;
	}

}

const int& BoidControl::get_UID() const {
	return UID;
}

void BoidControl::set_shape(const Ref<Shape> &p_shape) {

	if (!shape.is_null()) {
		shape->unregister_owner(this);
		shape->disconnect("changed", this, "_shape_changed");
	}

	if (p_shape.is_null() or
	Object::cast_to<BoxShape>(*p_shape) != NULL or
	Object::cast_to<SphereShape>(*p_shape) != NULL
	) {
		shape = p_shape;
		if (!shape.is_null()) {
			shape->register_owner(this);
			shape->connect("changed", this, "_shape_changed");
		}
	} else {
		shape.unref();
	}

	update_gizmo();

	_shape_changed();

}

Ref<Shape> BoidControl::get_shape() const {
	return shape;
}

void BoidControl::set_active(const bool &p_active) {
	active = p_active;
	if (data.active != p_active) {
		data.active = p_active;
		if (bsystem) {
			bsystem->update_control(&data);
		}
		update_gizmo();
	}
}

const bool& BoidControl::is_active() const {
	return data.active;
}

void BoidControl::set_subdivision(int p_subdiv) {
	if (p_subdiv < 1) {
		return;
	}
	if (data.subdivision != p_subdiv) {
		data.subdivision = p_subdiv;
		if (bsystem) {
			data.regenerate_domain = true;
			bsystem->update_control(&data);
		}
	}

}

const int& BoidControl::get_subdivision() {
	return data.subdivision;
}

void BoidControl::set_static(const bool &p_static) {
	if (data.is_static != p_static) {
		data.is_static = p_static;
		if (bsystem) {
			data.regenerate_domain = true;
			bsystem->update_control(&data);
		}
		update_gizmo();
	}
}

const bool& BoidControl::is_static() const {
	return data.is_static;
}

void BoidControl::set_ground( const bool& p_ground ) {
	data.ground = p_ground;
}

const bool& BoidControl::is_ground() const {
	return data.ground;
}

void BoidControl::set_group_mask(uint32_t p_mask) { // @suppress("Member declaration not found") // @suppress("Type cannot be resolved")
	data.group = p_mask;
	if (bsystem) {
		bsystem->update_control(&data);
	}
}

const uint32_t& BoidControl::get_group_mask() const { // @suppress("Member declaration not found") // @suppress("Type cannot be resolved")
	return data.group;
}

void BoidControl::set_group_mask_bit(int p_group, bool p_enable) {
	ERR_FAIL_INDEX(p_group, 32); // @suppress("Invalid arguments")
	if (p_enable) {
		set_group_mask(data.group | (1 << p_group)); // @suppress("Invalid arguments")
	} else {
		set_group_mask(data.group & (~(1 << p_group))); // @suppress("Invalid arguments")
	}
}

bool BoidControl::get_group_mask_bit(int p_group) const {
	ERR_FAIL_INDEX_V(p_group, 32, false); // @suppress("Invalid arguments")
	return (data.group & (1 << p_group));
}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\ SERVER LINKS //////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidControl::server_update( int32_t tree_id ) { // @suppress("Type cannot be resolved")

	const BoidServer::boid_tree* _btree = BoidServer::get_tree(tree_id);
	if ( _btree == NULL ) {
		return;
	}

	std::cout << "BoidControl::server_update" << std::endl;
	std::cout << "\tsystem " << _btree->system << " <> " << this << std::endl;
	std::cout << "\tmeshes " << _btree->mesh_count << std::endl;
	std::cout << "\tcontrols " << _btree->control_count << std::endl;

}

void BoidControl::server_clear() {

	std::cout << "BoidControl::server_clear" << std::endl;

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\ ENGINE LINKS //////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

String BoidControl::get_configuration_warning() const {

	String warnings;

	// checking if a boid_mesh is visible
	if (bsystem == NULL) {
		if (warnings != String()) {
			warnings += "\n";
		}
		warnings += "- " + TTR("BoidControl must be parented to BoidSystem to be effective.");
	}

	return warnings;

}

void BoidControl::_bind_methods() {

	// mesh signal
	ClassDB::bind_method(D_METHOD("system_changed"), &BoidControl::system_changed);
	ClassDB::bind_method(D_METHOD("system_control_updated"), &BoidControl::system_control_updated);

	ClassDB::bind_method(D_METHOD("set_UID", "UID"), &BoidControl::set_UID);
	ClassDB::bind_method(D_METHOD("get_UID"), &BoidControl::get_UID);

	ClassDB::bind_method(D_METHOD("set_static", "static"),
			&BoidControl::set_static);
	ClassDB::bind_method(D_METHOD("is_static"), &BoidControl::is_static);

	ClassDB::bind_method(D_METHOD("set_shape", "shape"),
			&BoidControl::set_shape);
	ClassDB::bind_method(D_METHOD("get_shape"), &BoidControl::get_shape);

	ClassDB::bind_method(D_METHOD("set_active", "active"),
			&BoidControl::set_active);
	ClassDB::bind_method(D_METHOD("is_active"), &BoidControl::is_active);

	ClassDB::bind_method(D_METHOD("_shape_changed"),
			&BoidControl::_shape_changed);
	ClassDB::bind_method(D_METHOD("_update_debug_shape"),
			&BoidControl::_update_debug_shape);

	ClassDB::bind_method(D_METHOD("set_subdivision", "box_subdivision"),
			&BoidControl::set_subdivision);
	ClassDB::bind_method(D_METHOD("get_subdivision"),
			&BoidControl::get_subdivision);

	ClassDB::bind_method(D_METHOD("set_group_mask", "group"),
			&BoidControl::set_group_mask); // @suppress("Invalid arguments")
	ClassDB::bind_method(D_METHOD("get_group_mask"),
			&BoidControl::get_group_mask); // @suppress("Invalid arguments")
	ClassDB::bind_method(D_METHOD("set_group_mask_bit", "group", "enabled"),
			&BoidControl::set_group_mask_bit);
	ClassDB::bind_method(D_METHOD("get_group_mask_bit", "group"),
			&BoidControl::get_group_mask_bit);

	ADD_GROUP("Shape", "");

	ADD_PROPERTY(
			PropertyInfo(Variant::OBJECT, "shape", PROPERTY_HINT_RESOURCE_TYPE,
					"BoxShape,SphereShape"), "set_shape", "get_shape");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "box_subdivision", PROPERTY_HINT_RANGE,
					"1,10,1,or_greater"), "set_subdivision", "get_subdivision");

	ADD_GROUP("Config", "");

	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "static"), "set_static",
			"is_static");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "active"), "set_active",
			"is_active");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "group",
					PROPERTY_HINT_LAYERS_3D_PHYSICS), "set_group_mask",
			"get_group_mask");
	ADD_PROPERTY(PropertyInfo(Variant::INT, "UID"), "set_UID", "get_UID");

}

void BoidControl::_notification(int p_what) {

	switch (p_what) {

		case NOTIFICATION_INTERNAL_PROCESS:
			internal_process();
			break;

		case NOTIFICATION_PARENTED:
			BoidServer::link( this );
			_parented();
			update_gizmo();
			break;

		case NOTIFICATION_UNPARENTED:
			BoidServer::unlink( this );
			system_purge();
			break;

		case NOTIFICATION_TRANSFORM_CHANGED:
			system_update();
			break;

		case NOTIFICATION_ENTER_TREE:
			BoidServer::link( this );
			update_gizmo();
			break;

		case NOTIFICATION_EXIT_TREE:
			BoidServer::unlink( this );
			break;

//		case NOTIFICATION_INTERNAL_PROCESS:
//			internal_process();
//			break;
//
//		case NOTIFICATION_PREDELETE:
//			break;
//
//		case NOTIFICATION_PARENTED:
//			_parented();
//			break;
//
//		case NOTIFICATION_UNPARENTED:
//			break;
//
//		case NOTIFICATION_ENTER_TREE:
//			if (get_tree()->is_debugging_collisions_hint()) {
//				_update_debug_shape();
//			}
//			break;
//
//		case NOTIFICATION_EXIT_TREE:
//			break;
//
//		case NOTIFICATION_TRANSFORM_CHANGED:
//			system_update();
//			break;

	}

}

BoidControl::BoidControl() :
		UID(-1), active(true), mi(NULL), debug_shape_dirty(true),
		v3_zero(0, 0, 0), bsystem(NULL), bmesh(NULL) {

	data.UID = 0;
	data.type = Boid::ControlType::BCTRL_TYPE_NONE;
	data.owner = this;
	data.active = active;
	data.group = 1;
	data.shape = Boid::ControlShape::BCTRL_SHAPE_POINT;
	data.subdivision = 1;
	data.is_static = false;
	data.volume = 0;
	data.mesh = NULL;
	data.regenerate_domain = true;
	data.direction = Boid::ControlDirection::BCTRL_DIRECTION_RADIAL;
	data.falloff = Boid::ControlFalloff::BCTRL_FALLOFF_NONE;
	data.emission_rate = 0;
	data.elapsed_time = 0;
	BoidBehaviour::init( &data.behaviour );
	data.on_enter = Boid::ControlOnEnter::BCTRL_ONENTER_NOTHING;
	data.force = 1;
	data.pull = 0;
	data.influence_direction = false;
	data.influence_front = false;
	data.weight = 1;
	data.ground = false;
	data.margin = 0;

	set_notify_transform(true);

#ifdef TOOLS_ENABLED
	data.name = get_name();
	set_process_internal(true);
#endif

}

BoidControl::~BoidControl() {

	purge_debug_mesh();

}

