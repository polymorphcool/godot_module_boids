/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_control.h
 *
 *  Created on: Feb 8, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_BOIDS_BOID_CONTROL_H_
#define MODULES_BOIDS_BOID_CONTROL_H_

#include <iostream>

#include "scene/3d/visual_instance.h"
#include "scene/3d/mesh_instance.h"
#include "scene/resources/box_shape.h"
#include "scene/resources/sphere_shape.h"
#include "scene/3d/immediate_geometry.h"

#include "boid_common.h"
#include "boid.h"
#include "boid_system.h"
#include "boid_mesh.h"

class BoidServer;

class BoidControl : public Spatial {

private:
	GDCLASS(BoidControl, Spatial); // @suppress("Symbol is not resolved")
	OBJ_CATEGORY("Boid");

private:

	int UID;
	bool active;
	Ref<Shape> shape;
	MeshInstance* mi;
	void purge_debug_mesh();
	bool debug_shape_dirty;

	// usefull
	Vector3 v3_zero;

protected:

	static void _bind_methods();
	void _notification(int p_what);
	void internal_process();

	void update_data();
	void _update_debug_shape();
	void _shape_changed();

	// must be accessible to daugther classes
	BoidSystem* bsystem;
	BoidMesh* bmesh;
	Boid::boid_control_data data;

public:

	// server connection
	void server_update( int32_t tree_id ); // @suppress("Type cannot be resolved")
	void server_clear();

	// updates bsystem and bmesh + connect signals
	void _parented();

	void system_changed();
	// connected to "boid_control_updated" in bsystem
	virtual void system_control_updated();
	void system_register();
	void system_unregister();
	void system_update();
	void system_purge();

	BoidSystem* get_boid_system();
	BoidMesh* get_boid_mesh();

	void set_UID(const int& p_uid);
	const int& get_UID() const;

	void set_shape(const Ref<Shape> &p_shape);
	Ref<Shape> get_shape() const;

	void set_active( const bool& p_active );
	const bool& is_active() const;

	// exposed in editor only for attractors, targets & collider (not sources)
	void set_subdivision( int p_subdiv );
	const int& get_subdivision();

	void set_static( const bool& p_static );
	const bool& is_static() const;

	void set_ground( const bool& p_ground );
	const bool& is_ground() const;

	void set_group_mask(uint32_t p_mask); // @suppress("Type cannot be resolved")
	const uint32_t& get_group_mask() const; // @suppress("Type cannot be resolved")

	void set_group_mask_bit(int p_group, bool p_enable);
	bool get_group_mask_bit(int p_group) const;

	virtual String get_configuration_warning() const;

	BoidControl();
	virtual ~BoidControl();

};

#endif /* MODULES_BOIDS_BOID_CONTROL_H_ */
