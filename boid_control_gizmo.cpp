/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boids_modifier_gizmo.cpp
 *
 *  Created on: Feb 8, 2021
 *      Author: frankiezafe
 */

#include "boid_control_gizmo.h"

BoidControlGizmo::BoidControlGizmo() {

	const Color gizmo_color = EDITOR_DEF("editors/3d_gizmos/gizmo_colors/shape", Color(0.1, 1, 1));
	create_material("shape_material", gizmo_color);
	const float gizmo_value = gizmo_color.get_v();
	const Color gizmo_color_disabled = Color(gizmo_value, gizmo_value, gizmo_value, 0.65);
	create_material("shape_material_disabled", gizmo_color_disabled);
	create_handle_material("handles");

}

bool BoidControlGizmo::has_gizmo(Spatial *p_spatial) {
	return Object::cast_to<BoidControl>(p_spatial) != NULL;
}

String BoidControlGizmo::get_name() const {
	return "BoidControl";
}

int BoidControlGizmo::get_priority() const {
	return -1;
}

String BoidControlGizmo::get_handle_name(const EditorSpatialGizmo *p_gizmo, int p_idx) const {

	const BoidControl *bm = Object::cast_to<BoidControl>(p_gizmo->get_spatial_node());
	Ref<Shape> s = bm->get_shape();
	if (!s.is_null()) {
		if (Object::cast_to<SphereShape>(*s)) {
			return "Radius";
		} else if (Object::cast_to<BoxShape>(*s)) {
			return "Extents";
		}
	}
	return "";
}

Variant BoidControlGizmo::get_handle_value(EditorSpatialGizmo *p_gizmo, int p_idx) const {

	BoidControl *bm = Object::cast_to<BoidControl>(p_gizmo->get_spatial_node());
	Ref<Shape> s = bm->get_shape();
	if (!s.is_null()) {
		if (Object::cast_to<SphereShape>(*s)) {
			Ref<SphereShape> ss = s;
			return ss->get_radius();
		} else if (Object::cast_to<BoxShape>(*s)) {
			Ref<BoxShape> bs = s;
			return bs->get_extents();
		}
	}
	return Variant();

}

void BoidControlGizmo::set_handle(EditorSpatialGizmo *p_gizmo, int p_idx, Camera *p_camera, const Point2 &p_point) {

	BoidControl *bm = Object::cast_to<BoidControl>(p_gizmo->get_spatial_node());
	Ref<Shape> s = bm->get_shape();
	if (s.is_null())
		return;

	Transform gt = bm->get_global_transform();
	Transform gi = gt.affine_inverse();
	Vector3 ray_from = p_camera->project_ray_origin(p_point);
	Vector3 ray_dir = p_camera->project_ray_normal(p_point);
	Vector3 sg[2] = { gi.xform(ray_from), gi.xform(ray_from + ray_dir * 4096) };

	if (Object::cast_to<SphereShape>(*s)) {

		Ref<SphereShape> ss = s;
		Vector3 ra, rb;
		Geometry::get_closest_points_between_segments(Vector3(), Vector3(4096, 0, 0), sg[0], sg[1], ra, rb);
		float d = ra.x;
		if (SpatialEditor::get_singleton()->is_snap_enabled()) {
			d = Math::stepify(d, SpatialEditor::get_singleton()->get_translate_snap());
		}
		if (d < 0.001)
			d = 0.001;
		ss->set_radius(d);

	} else if (Object::cast_to<BoxShape>(*s)) {

		Vector3 axis;
		axis[p_idx] = 1.0;
		Ref<BoxShape> bs = s;
		Vector3 ra, rb;
		Geometry::get_closest_points_between_segments(Vector3(), axis * 4096, sg[0], sg[1], ra, rb);
		float d = ra[p_idx];
		if (SpatialEditor::get_singleton()->is_snap_enabled()) {
			d = Math::stepify(d, SpatialEditor::get_singleton()->get_translate_snap());
		}
		if (d < 0.001)
			d = 0.001;
		Vector3 he = bs->get_extents();
		he[p_idx] = d;
		bs->set_extents(he);

	}

}

void BoidControlGizmo::commit_handle(EditorSpatialGizmo *p_gizmo, int p_idx, const Variant &p_restore, bool p_cancel) {

	BoidControl *bm = Object::cast_to<BoidControl>(p_gizmo->get_spatial_node());

	Ref<Shape> s = bm->get_shape();
	if (s.is_null())
		return;

	if (Object::cast_to<SphereShape>(*s)) {

		Ref<SphereShape> ss = s;
		if (p_cancel) {
			ss->set_radius(p_restore);
			return;
		}
		UndoRedo *ur = SpatialEditor::get_singleton()->get_undo_redo();
		ur->create_action(TTR("Change Sphere Shape Radius"));
		ur->add_do_method(ss.ptr(), "set_radius", ss->get_radius());
		ur->add_undo_method(ss.ptr(), "set_radius", p_restore);
		ur->commit_action();

	} else if (Object::cast_to<BoxShape>(*s)) {

		Ref<BoxShape> ss = s;
		if (p_cancel) {
			ss->set_extents(p_restore);
			return;
		}
		UndoRedo *ur = SpatialEditor::get_singleton()->get_undo_redo();
		ur->create_action(TTR("Change Box Shape Extents"));
		ur->add_do_method(ss.ptr(), "set_extents", ss->get_extents());
		ur->add_undo_method(ss.ptr(), "set_extents", p_restore);
		ur->commit_action();

	}

}

void BoidControlGizmo::redraw(EditorSpatialGizmo *p_gizmo) {

	BoidControl *bm = Object::cast_to<BoidControl>(p_gizmo->get_spatial_node());

	p_gizmo->clear();

	Ref<Shape> s = bm->get_shape();
	if (s.is_null())
		return;

	const Ref<Material> material =
			get_material(bm->is_active() ? "shape_material" : "shape_material_disabled", p_gizmo);
	Ref<Material> handles_material = get_material("handles");

	if (Object::cast_to<SphereShape>(*s)) {

		Ref<SphereShape> sp = s;
		float r = sp->get_radius();
		Vector<Vector3> points;
		for (int i = 0; i <= 360; i++) {
			float ra = Math::deg2rad((float)i);
			float rb = Math::deg2rad((float)i + 1);
			Point2 a = Vector2(Math::sin(ra), Math::cos(ra)) * r;
			Point2 b = Vector2(Math::sin(rb), Math::cos(rb)) * r;
			points.push_back(Vector3(a.x, 0, a.y));
			points.push_back(Vector3(b.x, 0, b.y));
			points.push_back(Vector3(0, a.x, a.y));
			points.push_back(Vector3(0, b.x, b.y));
			points.push_back(Vector3(a.x, a.y, 0));
			points.push_back(Vector3(b.x, b.y, 0));
		}
		Vector<Vector3> collision_segments;
		for (int i = 0; i < 64; i++) {
			float ra = i * Math_PI * 2.0 / 64.0;
			float rb = (i + 1) * Math_PI * 2.0 / 64.0;
			Point2 a = Vector2(Math::sin(ra), Math::cos(ra)) * r;
			Point2 b = Vector2(Math::sin(rb), Math::cos(rb)) * r;
			collision_segments.push_back(Vector3(a.x, 0, a.y));
			collision_segments.push_back(Vector3(b.x, 0, b.y));
			collision_segments.push_back(Vector3(0, a.x, a.y));
			collision_segments.push_back(Vector3(0, b.x, b.y));
			collision_segments.push_back(Vector3(a.x, a.y, 0));
			collision_segments.push_back(Vector3(b.x, b.y, 0));
		}
		p_gizmo->add_lines(points, material);
		p_gizmo->add_collision_segments(collision_segments);
		Vector<Vector3> handles;
		handles.push_back(Vector3(r, 0, 0));
		p_gizmo->add_handles(handles, handles_material);

	}

	if (Object::cast_to<BoxShape>(*s)) {

		Ref<BoxShape> bs = s;
		Vector<Vector3> lines;
		AABB aabb;
		aabb.position = -bs->get_extents();
		aabb.size = aabb.position * -2;
		for (int i = 0; i < 12; i++) {
			Vector3 a, b;
			aabb.get_edge(i, a, b);
			lines.push_back(a);
			lines.push_back(b);
		}
		Vector<Vector3> handles;
		for (int i = 0; i < 3; i++) {

			Vector3 ax;
			ax[i] = bs->get_extents()[i];
			handles.push_back(ax);
		}
		p_gizmo->add_lines(lines, material);
		p_gizmo->add_collision_segments(lines);
		p_gizmo->add_handles(handles, handles_material);

	}

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\ SPECIALISATION /////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////


BoidAttractorGizmo::BoidAttractorGizmo() {
	create_icon_material("icon", SpatialEditor::get_singleton()->get_icon("GizmoBoidAttractor", "EditorIcons"));
	create_icon_material("icon_off", SpatialEditor::get_singleton()->get_icon("GizmoBoidAttractorOff", "EditorIcons"));
	create_icon_material("icon_static", SpatialEditor::get_singleton()->get_icon("GizmoBoidAttractorStatic", "EditorIcons"));
	create_icon_material("icon_static_off", SpatialEditor::get_singleton()->get_icon("GizmoBoidAttractorStaticOff", "EditorIcons"));
}

bool BoidAttractorGizmo::has_gizmo(Spatial *p_spatial) {
	return Object::cast_to<BoidAttractor>(p_spatial) != NULL;
}

void BoidAttractorGizmo::redraw(EditorSpatialGizmo *p_gizmo) {
	BoidControlGizmo::redraw(p_gizmo);
	BoidAttractor* bm =  Object::cast_to<BoidAttractor>(p_gizmo->get_spatial_node());
	Ref<Material> icon;
	if (bm->is_static() && !bm->is_active()) {
		icon = get_material("icon_static_off", p_gizmo);
	} else if (bm->is_static() && bm->is_active()) {
		icon = get_material("icon_static", p_gizmo);
	} else if (!bm->is_active()) {
		icon = get_material("icon_off", p_gizmo);
	} else {
		icon = get_material("icon", p_gizmo);
	}
	p_gizmo->add_unscaled_billboard(icon, 0.01);
}

BoidSourceGizmo::BoidSourceGizmo() {
	create_icon_material("icon", SpatialEditor::get_singleton()->get_icon("GizmoBoidSource", "EditorIcons"));
	create_icon_material("icon_off", SpatialEditor::get_singleton()->get_icon("GizmoBoidSourceOff", "EditorIcons"));
}

bool BoidSourceGizmo::has_gizmo(Spatial *p_spatial) {
	return Object::cast_to<BoidSource>(p_spatial) != NULL;
}

void BoidSourceGizmo::redraw(EditorSpatialGizmo *p_gizmo) {
	BoidControlGizmo::redraw(p_gizmo);
	BoidSource* bm =  Object::cast_to<BoidSource>(p_gizmo->get_spatial_node());
	Ref<Material> icon;
	if (!bm->is_active()) {
		icon = get_material("icon_off", p_gizmo);
	} else {
		icon = get_material("icon", p_gizmo);
	}
	p_gizmo->add_unscaled_billboard(icon, 0.01);
}

BoidTargetGizmo::BoidTargetGizmo() {
	create_icon_material("icon", SpatialEditor::get_singleton()->get_icon("GizmoBoidTarget", "EditorIcons"));
	create_icon_material("icon_off", SpatialEditor::get_singleton()->get_icon("GizmoBoidTargetOff", "EditorIcons"));
	create_icon_material("icon_static", SpatialEditor::get_singleton()->get_icon("GizmoBoidTargetStatic", "EditorIcons"));
	create_icon_material("icon_static_off", SpatialEditor::get_singleton()->get_icon("GizmoBoidTargetStaticOff", "EditorIcons"));
}

bool BoidTargetGizmo::has_gizmo(Spatial *p_spatial) {
	return Object::cast_to<BoidTarget>(p_spatial) != NULL;
}

void BoidTargetGizmo::redraw(EditorSpatialGizmo *p_gizmo) {
	BoidControlGizmo::redraw(p_gizmo);
	BoidTarget* bm =  Object::cast_to<BoidTarget>(p_gizmo->get_spatial_node());
	Ref<Material> icon;
	if (bm->is_static() && !bm->is_active()) {
		icon = get_material("icon_static_off", p_gizmo);
	} else if (bm->is_static() && bm->is_active()) {
		icon = get_material("icon_static", p_gizmo);
	} else if (!bm->is_active()) {
		icon = get_material("icon_off", p_gizmo);
	} else {
		icon = get_material("icon", p_gizmo);
	}
	p_gizmo->add_unscaled_billboard(icon, 0.01);
}

BoidColliderGizmo::BoidColliderGizmo() {
	create_icon_material("icon", SpatialEditor::get_singleton()->get_icon("GizmoBoidCollider", "EditorIcons"));
	create_icon_material("icon_off", SpatialEditor::get_singleton()->get_icon("GizmoBoidColliderOff", "EditorIcons"));
	create_icon_material("icon_static", SpatialEditor::get_singleton()->get_icon("GizmoBoidColliderStatic", "EditorIcons"));
	create_icon_material("icon_static_off", SpatialEditor::get_singleton()->get_icon("GizmoBoidColliderStaticOff", "EditorIcons"));
}

bool BoidColliderGizmo::has_gizmo(Spatial *p_spatial) {
	return Object::cast_to<BoidCollider>(p_spatial) != NULL;
}

void BoidColliderGizmo::redraw(EditorSpatialGizmo *p_gizmo) {
	BoidControlGizmo::redraw(p_gizmo);
	BoidCollider* bm =  Object::cast_to<BoidCollider>(p_gizmo->get_spatial_node());
	Ref<Material> icon;
	if (bm->is_static() && !bm->is_active()) {
		icon = get_material("icon_static_off", p_gizmo);
	} else if (bm->is_static() && bm->is_active()) {
		icon = get_material("icon_static", p_gizmo);
	} else if (!bm->is_active()) {
		icon = get_material("icon_off", p_gizmo);
	} else {
		icon = get_material("icon", p_gizmo);
	}
	p_gizmo->add_unscaled_billboard(icon, 0.01);
}
