/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boids_modifier_gizmo.h
 *
 *  Created on: Feb 8, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_BOIDS_BOID_CONTROL_GIZMO_H_
#define MODULES_BOIDS_BOID_CONTROL_GIZMO_H_

#include <iostream>

#include "editor/plugins/spatial_editor_plugin.h"
#include "scene/3d/camera.h"

#include "boid_control.h"
#include "boid_attractor.h"
#include "boid_source.h"
#include "boid_target.h"
#include "boid_collider.h"

class BoidControlGizmo : public EditorSpatialGizmoPlugin {

private:
	GDCLASS(BoidControlGizmo, EditorSpatialGizmoPlugin);

public:

	bool has_gizmo(Spatial *p_spatial);
	String get_name() const;
	int get_priority() const;
	void redraw(EditorSpatialGizmo *p_gizmo);

	String get_handle_name(const EditorSpatialGizmo *p_gizmo, int p_idx) const;
	Variant get_handle_value(EditorSpatialGizmo *p_gizmo, int p_idx) const;
	void set_handle(EditorSpatialGizmo *p_gizmo, int p_idx, Camera *p_camera, const Point2 &p_point);
	void commit_handle(EditorSpatialGizmo *p_gizmo, int p_idx, const Variant &p_restore, bool p_cancel = false);

	BoidControlGizmo();

};

// specialisation

class BoidAttractorGizmo : public BoidControlGizmo {
private:
	GDCLASS(BoidAttractorGizmo, BoidControlGizmo);
public:
	bool has_gizmo(Spatial *p_spatial);
	void redraw(EditorSpatialGizmo *p_gizmo);
	BoidAttractorGizmo();
};

class BoidSourceGizmo : public BoidControlGizmo {
private:
	GDCLASS(BoidSourceGizmo, BoidControlGizmo);
public:
	bool has_gizmo(Spatial *p_spatial);
	void redraw(EditorSpatialGizmo *p_gizmo);
	BoidSourceGizmo();
};

class BoidTargetGizmo : public BoidControlGizmo {
private:
	GDCLASS(BoidTargetGizmo, BoidControlGizmo);
public:
	bool has_gizmo(Spatial *p_spatial);
	void redraw(EditorSpatialGizmo *p_gizmo);
	BoidTargetGizmo();
};

class BoidColliderGizmo : public BoidControlGizmo {
private:
	GDCLASS(BoidColliderGizmo, BoidControlGizmo);
public:
	bool has_gizmo(Spatial *p_spatial);
	void redraw(EditorSpatialGizmo *p_gizmo);
	BoidColliderGizmo();
};



#endif /* MODULES_BOIDS_BOID_CONTROL_GIZMO_H_ */
