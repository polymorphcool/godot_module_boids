/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boids_editor_plugin.cpp
 *
 *  Created on: Feb 8, 2021
 *      Author: frankiezafe
 */

#include "boid_editor_plugin.h"

bool EditorInspectorPluginBoidBehaviour::can_handle(Object *p_object) {

	BoidBehaviour *bb = Object::cast_to<BoidBehaviour>(p_object);
	return (bb != NULL);

}

void EditorInspectorPluginBoidBehaviour::parse_begin(Object *p_object) {

	BoidBehaviour *bb = Object::cast_to<BoidBehaviour>(p_object);
	if (!bb) {
		return;
	}
	Ref<BoidBehaviour> m(bb);
	BoidBehaviourEditor *editor = memnew(BoidBehaviourEditor(node));
	editor->edit(bb);
	add_custom_control(editor);

}

EditorInspectorPluginBoidBehaviour::EditorInspectorPluginBoidBehaviour(EditorNode *p_node) {

	node = p_node;

}

BoidBehaviourEditorPlugin::BoidBehaviourEditorPlugin(EditorNode *p_node) {

	EditorInspectorPluginBoidBehaviour* e = memnew(EditorInspectorPluginBoidBehaviour(p_node));
	Ref<EditorInspectorPluginBoidBehaviour> plugin( e );
	add_inspector_plugin(plugin);

//	EDITOR_DEF("editors/boid_behaviour/editor_side", 1);
//	EditorSettings::get_singleton()->add_property_hint(PropertyInfo(Variant::INT, "editors/boid_behaviour/editor_side", PROPERTY_HINT_ENUM, "Left,Right"));

}

BoidEditorPlugin::BoidEditorPlugin(EditorNode *p_node) {

//	ProjectSettings::get_singleton()->set_custom_property_info( "layer_names/boid_groups/layer_1" )
//	void set_custom_property_info(const String &p_prop, const PropertyInfo &p_info);
//	EditorSettings::get_singleton()->add_property_hint(PropertyInfo(Variant::String, "editors/boid_behaviour/editor_side", PROPERTY_HINT_ENUM, "Left,Right"));

	for (int i = 0; i < 20; i++) {
		GLOBAL_DEF("layer_names/boid_group/layer_" + itos(i + 1) + "_name", "");
		GLOBAL_DEF("layer_names/boid_group/layer_" + itos(i + 1) + "_color", Color(0,0,0,1));
		GLOBAL_DEF("layer_names/boid_animation/layer_" + itos(i + 1) + "_name", "");
		GLOBAL_DEF("layer_names/boid_animation/layer_" + itos(i + 1) + "_color", Color(0,0,0,1));
	}

    Ref<BoidAttractorGizmo> boid_attractor_gizmo = Ref<BoidAttractorGizmo>(memnew(BoidAttractorGizmo));
    SpatialEditor::get_singleton()->add_gizmo_plugin(boid_attractor_gizmo);

    Ref<BoidSourceGizmo> boid_source_gizmo = Ref<BoidSourceGizmo>(memnew(BoidSourceGizmo));
    SpatialEditor::get_singleton()->add_gizmo_plugin(boid_source_gizmo);

    Ref<BoidTargetGizmo> boid_target_gizmo = Ref<BoidTargetGizmo>(memnew(BoidTargetGizmo));
    SpatialEditor::get_singleton()->add_gizmo_plugin(boid_target_gizmo);

    Ref<BoidColliderGizmo> boid_collider_gizmo = Ref<BoidColliderGizmo>(memnew(BoidColliderGizmo));
    SpatialEditor::get_singleton()->add_gizmo_plugin(boid_collider_gizmo);

}

String BoidMaterialConversionPlugin::converts_to() const {
	return "ShaderMaterial";
}

bool BoidMaterialConversionPlugin::handles(const Ref<Resource> &p_resource) const {
	Ref<BoidMaterial> mat = p_resource;
	return mat.is_valid();
}

Ref<Resource> BoidMaterialConversionPlugin::convert(const Ref<Resource> &p_resource) const {

	Ref<BoidMaterial> mat = p_resource;
	ERR_FAIL_COND_V(!mat.is_valid(), Ref<Resource>());

	RID shader_id = mat->get_shader()->get_rid();

	Ref<ShaderMaterial> smat;
	smat.instance();
	Ref<Shader> shader;
	shader.instance();
	String code = VS::get_singleton()->shader_get_code(shader_id);
	shader->set_code(code);
	smat->set_shader(shader);

	List<PropertyInfo> params;
	VS::get_singleton()->shader_get_param_list(shader_id, &params);

	for (List<PropertyInfo>::Element *E = params.front(); E; E = E->next()) {
		Variant value = VS::get_singleton()->material_get_param(mat->get_rid(), E->get().name);
		smat->set_shader_param(E->get().name, value);
	}

	smat->set_render_priority(mat->get_render_priority());
	return smat;
}
