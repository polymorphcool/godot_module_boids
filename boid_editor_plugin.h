/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boids_editor_plugin.h
 *
 *  Created on: Feb 8, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_BOIDS_BOID_EDITOR_PLUGIN_H_
#define MODULES_BOIDS_BOID_EDITOR_PLUGIN_H_

#include "core/project_settings.h"
#include "editor/editor_plugin.h"
#include "editor/property_editor.h"
#include "boid_control_gizmo.h"
#include "boid_material.h"
#include "boid_behaviour.h"
#include "boid_behaviour_editor.h"

class EditorInspectorPluginBoidBehaviour : public EditorInspectorPlugin {
	GDCLASS(EditorInspectorPluginBoidBehaviour, EditorInspectorPlugin);
	EditorNode* node;
public:
	virtual bool can_handle(Object *p_object);
	virtual void parse_begin(Object *p_object);
	EditorInspectorPluginBoidBehaviour(EditorNode *p_node);
};

class BoidBehaviourEditorPlugin : public EditorPlugin {
    GDCLASS( BoidBehaviourEditorPlugin, EditorPlugin )
public:
	virtual String get_name() const { return "BoidBehaviour"; }
	BoidBehaviourEditorPlugin(EditorNode *p_editor);
};

class BoidEditorPlugin : public EditorPlugin {
    GDCLASS( BoidEditorPlugin, EditorPlugin )
public:
	virtual String get_name() const { return "Boid"; }
	BoidEditorPlugin(EditorNode *p_editor);
};

class BoidMaterialConversionPlugin : public EditorResourceConversionPlugin {
	GDCLASS(BoidMaterialConversionPlugin, EditorResourceConversionPlugin);

public:
	virtual String converts_to() const;
	virtual bool handles(const Ref<Resource> &p_resource) const;
	virtual Ref<Resource> convert(const Ref<Resource> &p_resource) const;
};

#endif /* MODULES_BOIDS_BOID_EDITOR_PLUGIN_H_ */
