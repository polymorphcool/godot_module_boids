/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 * boid_material.cpp
 *
 *  Created on: Mar 14, 2021
 *      Author: frankiezafe
 */

#include "boid_material.h"

void BoidMaterial::new_shader() {

	String code = "";
	code += "shader_type particles;\n"
			"uniform int texture_width = 1;\n"
			"uniform int texture_height = 1;\n"
			"uniform sampler2D texture_data : hint_black;\n";

	if ( depth_enabled ) {
	code += "uniform sampler2D texture_depth : hint_black;\n"
			"uniform vec3 domain_offset = vec3(0.);\n"
			"uniform vec3 domain_size = vec3(200.);\n";
	}

	code += "float rand_from_seed(inout uint seed) {\n"
			"	int k;\n"
			"	int s = int(seed);\n"
			"	if (s == 0) { s = 305420679; }\n"
			"	k = s / 127773;\n"
			"	s = 16807 * (s - k * 127773) - 2836 * k;\n"
			"	if (s < 0) { s += 2147483647; }\n"
			"	seed = uint(s);\n"
			"	return float(seed % uint(65536)) / 65535.0;\n"
			"}\n"
			"float rand_from_seed_m1_p1(inout uint seed) {\n"
			"	return rand_from_seed(seed) * 2.0 - 1.0;\n"
			"}\n"
			"uint hash(uint x) {\n"
			"	x = ((x >> uint(16)) ^ x) * uint(73244475);\n"
			"	x = ((x >> uint(16)) ^ x) * uint(73244475);\n"
			"	x = (x >> uint(16)) ^ x;\n"
			"	return x;\n"
			"}\n"
			"void vertex() {\n"
			"	uint alt_seed = hash(NUMBER + uint(1) + RANDOM_SEED);\n"
			"	int tx = INDEX % texture_width;\n"
			"	int ty = INDEX / texture_width;\n"
			"	vec2 tuv = ( vec2(.5) + vec2( float(tx),float(ty) ) ) / vec2( float(texture_width), float(texture_height) );\n"
			"	vec3 pos = 	texture( texture_data, tuv ).xyz;\n"
			"	vec4 dir = 	texture( texture_data, tuv + vec2(0.0,0.25) );\n"
			"	CUSTOM = 	texture( texture_data, tuv + vec2(0.0,0.5) );\n"
			"	COLOR = 	texture( texture_data, tuv + vec2(0.0,0.75) );\n"
			"	ACTIVE = true;\n"
			"	VELOCITY = vec3( 0.0, 0.0, 0.0 );\n"
			"	vec3 forward = normalize( dir.xyz );\n"
			"	vec3 right = normalize( cross( vec3( 0.0, 1.0, 0.0 ), forward ) );\n"
			"	if (forward.y == -1.0 || forward.y == 1.0) {\n"
			"		right = normalize( cross( vec3( 0.0, 0.0, -forward.y ), forward ) );\n"
			"	}\n"
			"	vec3 up = normalize( cross( forward, right ) );\n"
			"	right = cross(up,forward);\n";

	if ( depth_enabled ) {
	code += "	vec2 depth_uv = (pos.xz+domain_offset.xz)/domain_size.xz;\n"
			"	pos.y += (texture(texture_depth,depth_uv).x+domain_offset.y)*domain_size.y;\n";
	}

	code += "	TRANSFORM[0] = vec4( right, 	0.0 );\n"
			"	TRANSFORM[1] = vec4( up, 		0.0 );\n"
			"	TRANSFORM[2] = vec4( forward, 	0.0 );\n"
			"	TRANSFORM[3] = vec4( pos, 		0.0 );\n"
			"}";


	local_shader->set_code( code );
	ShaderMaterial::set_shader( local_shader );

}

void BoidMaterial::_bind_methods() {

	ClassDB::bind_method(D_METHOD("is_depth_enable"), &BoidMaterial::is_depth_enable);
	ClassDB::bind_method(D_METHOD("depth_enable", "depth_enabled"), &BoidMaterial::depth_enable);

	ADD_GROUP("Depth", "");
	ADD_PROPERTY( PropertyInfo(Variant::BOOL, "depth_enabled"), "depth_enable", "is_depth_enable");

}

bool BoidMaterial::is_depth_enable() const {
	return depth_enabled;
}

void BoidMaterial::depth_enable( bool _enable ) {
	if ( depth_enabled != _enable ) {
		depth_enabled = _enable;
		new_shader();
	}
}

BoidMaterial::BoidMaterial() : depth_enabled(false) {
	local_shader.instance();
	new_shader();
}

BoidMaterial::~BoidMaterial() {
	local_shader.unref();
}
