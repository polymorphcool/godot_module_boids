/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_mesh.cpp
 *
 *  Created on: Mar 9, 2021
 *      Author: frankiezafe
 */

#include "boid_mesh.h"
#include "boid_server.h"

void BoidMesh::clear_textures() {
	if ( tex_data.is_valid() ) {
		tex_data.unref();
		im_data.unref();
	}
}

void BoidMesh::update_inactive_color() {
	if ( bsystem && is_inside_tree() ) {
		Camera *c = get_viewport()->get_camera(); // @suppress("Method cannot be resolved")
		if (c) {
			Vector3 dir = c->get_global_transform().basis.get_axis(2) * c->get_zfar() * 2;
			Vector3 inactive_position = c->get_global_transform().get_origin() + dir;
			inactive_position = bsystem->to_local(inactive_position);
			inactive_color = Color(inactive_position.x,inactive_position.y,inactive_position.z);
		}
	}
}

void BoidMesh::prepare_material() {

	if (!process_material.is_valid() || bset.count < 1) {
		// no update until a process material is not set
		return;
	}

	clear_textures();

	if ( bset.count <= 512 ) {
		im_width = bset.count;
		im_lines = 1;
		im_height = TEXT_DATA_LAYER_COUNT;
	} else {
		im_width = 512;
		im_lines = int(Math::ceil(bset.count / 512.0f));
		im_height = im_lines * TEXT_DATA_LAYER_COUNT;
	}

	update_inactive_color();

	im_data.instance();
	im_data->create(im_width, im_height, false, Image::FORMAT_RGBAF);
	im_data->fill( inactive_color );

	tex_data.instance();
	tex_data->create_from_image(im_data, 0); // @suppress("Invalid arguments")

	process_material->set_shader_param("texture_width", im_width);
	process_material->set_shader_param("texture_height", im_height);
	process_material->set_shader_param("texture_data", tex_data);

}

void BoidMesh::update_textures() {

	if (process_material.is_null()) {
		// no update until a process material is not set
		return;
	}

	if ( !im_data.is_valid() ) {
		return;
	}

#ifdef TOOLS_ENABLED
	// fucking error due to references cleared while saving the object
	Ref<ImageTexture> rit = process_material->get_shader_param("texture_data");
	if ( rit->get_rid() != tex_data->get_rid() ) {
		start_particles();
	}
#endif

	serialise_boids();

	VS::get_singleton()->texture_set_data(tex_data->get_rid(), im_data);

}

void BoidMesh::update_set() {

	if ( bsystem != NULL ) {
		bset.count = boid_amount;
		// time to update boid set
		if ( boid_amount <= 0 ) {
			bsystem->remove_set( bset );
		} else {
			bsystem->update_set( bset );
			VS::get_singleton()->particles_set_amount(particles, boid_amount);
		}
	}

	if ( process_material.is_valid() && bset.boids != NULL ) {
		start_particles();
	} else {
		stop_particles();
	}

}

float BoidMesh::get_channel_info( Boid::boid* b, const ColorChannelData& tc ) {
	switch ( tc ) {
		case ColorChannelData::CCDATA_GROUP:
			return b->group;
		case ColorChannelData::CCDATA_REACH_GROUP:
			return b->reach_group;
		case ColorChannelData::CCDATA_ESCAPE_GROUP:
			return b->escape_group;
			break;
		case ColorChannelData::CCDATA_ANIMATION:
			return b->animation;
			break;
		case ColorChannelData::CCDATA_POSITION_X:
			return b->local_pos.x;
			break;
		case ColorChannelData::CCDATA_POSITION_Y:
			return b->local_pos.y;
			break;
		case ColorChannelData::CCDATA_POSITION_Z:
			return b->local_pos.z;
			break;
		case ColorChannelData::CCDATA_MOTION_X:
			return b->motion.x;
			break;
		case ColorChannelData::CCDATA_MOTION_Y:
			return b->motion.y;
			break;
		case ColorChannelData::CCDATA_MOTION_Z:
			return b->motion.z;
			break;
		case ColorChannelData::CCDATA_MOTION_LENGTH:
			return b->motion.length();
			break;
		case ColorChannelData::CCDATA_DIR_X:
			return b->dir.x;
			break;
		case ColorChannelData::CCDATA_DIR_Y:
			return b->dir.y;
			break;
		case ColorChannelData::CCDATA_DIR_Z:
			return b->dir.z;
			break;
		case ColorChannelData::CCDATA_FRONT_X:
			return b->mat3x3[2].x;
			break;
		case ColorChannelData::CCDATA_FRONT_Y:
			return b->mat3x3[2].y;
			break;
		case ColorChannelData::CCDATA_FRONT_Z:
			return b->mat3x3[2].z;
			break;
		case ColorChannelData::CCDATA_PUSH_X:
			return b->push.x;
			break;
		case ColorChannelData::CCDATA_PUSH_Y:
			return b->push.y;
			break;
		case ColorChannelData::CCDATA_PUSH_Z:
			return b->push.z;
			break;
		case ColorChannelData::CCDATA_TRAVEL_DISTANCE:
			return b->travel_distance;
			break;
		case ColorChannelData::CCDATA_LIFETIME:
			return b->lifetime;
			break;
		case ColorChannelData::CCDATA_TIMER:
			return b->timer;
			break;
		case ColorChannelData::CCDATA_COUNTER:
			return b->counter;
			break;
		case ColorChannelData::CCDATA_COUNTER_PREVIOUS:
			return b->prev_counter;
			break;
		case ColorChannelData::CCDATA_COLOR_R:
			return b->color.r;
			break;
		case ColorChannelData::CCDATA_COLOR_G:
			return b->color.g;
			break;
		case ColorChannelData::CCDATA_COLOR_B:
			return b->color.b;
			break;
		case ColorChannelData::CCDATA_COLOR_A:
			return b->color.a;
			break;
		case ColorChannelData::CCDATA_NEIGHBOR_COUNT:
			return b->neighbor_count;
			break;
		case ColorChannelData::CCDATA_MOTION_DOT_RIGHT:
			if ( b->motion.length_squared() == 0 ) {
				return 0;
			}
			return b->mat3x3[0].dot( b->motion.normalized() );
			break;
		case ColorChannelData::CCDATA_MOTION_DOT_UP:
			if ( b->motion.length_squared() == 0 ) {
				return 0;
			}
			return b->mat3x3[1].dot( b->motion.normalized() );
			break;
		case ColorChannelData::CCDATA_MOTION_DOT_FRONT:
			if ( b->motion.length_squared() == 0 ) {
				return 0;
			}
			return b->mat3x3[2].dot( b->motion.normalized() );
			break;
		case ColorChannelData::CCDATA_GROUND_ROTATION:
			return b->ground_rotation;
			break;
		case ColorChannelData::CCDATA_UID:
			return b->UID;
		case ColorChannelData::CCDATA_NONE:
		default:
			return 0;
	}

}

void BoidMesh::serialise_boids() {

	if ( !im_data.is_valid() ) {
		return;
	}

	update_inactive_color();

	// positon: 	layer 0
	// direction: 	layer 1
	// custom: 		layer 2
	// color: 		layer 3

	im_data->lock();
	int x = 0;
	int y = 0;
	for (int i = 0; i < bset.count; ++i) {
		Boid::boid* b = bset.boids[i];
		if ( !b->lock && b->active && b->lifetime > 0) {
			im_data->set_pixel(x, y, Color(b->local_pos.x, b->local_pos.y, b->local_pos.z));
			if ( b->aiming ) {
				im_data->set_pixel(x, y+im_lines, Color(b->aim.x, b->aim.y, b->aim.z, b->motion.length()));
			} else {
				im_data->set_pixel(x, y+im_lines, Color(b->front.x, b->front.y, b->front.z, b->motion.length()));
			}
		} else {
			im_data->set_pixel(x, y, inactive_color);
		}
		im_data->set_pixel(x, y+im_lines*2, Color (
				get_channel_info(b,ccd_custom_r),
				get_channel_info(b,ccd_custom_g),
				get_channel_info(b,ccd_custom_b),
				get_channel_info(b,ccd_custom_a)
				));
		im_data->set_pixel(x, y+im_lines*3, Color (
				get_channel_info(b,ccd_color_r),
				get_channel_info(b,ccd_color_g),
				get_channel_info(b,ccd_color_b),
				get_channel_info(b,ccd_color_a)
				));
		x++;
		if (x >= im_width) {
			x = 0;
			y++;
		}
	}
	im_data->unlock();

}

void BoidMesh::update_boids() {

	if ( bset.boids != NULL ) {

		update_textures();

		if ( dynamic_aabb ) {
			set_visibility_aabb( get_aabb() );
		}

		// send events
		for (int i = 0; i < bset.count; ++i) {
			Boid::boid* b = bset.boids[i];
			if ( b->event_enabled ) {
				emit_signal( "boid_event", i, b->event );
				b->event_enabled = false;
			}
		}

	}

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\\\ BOID NODE ///////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidMesh::seek_system() {

	BoidSystem* prev_bsys = bsystem;
	Node* n = get_parent();
	if ( n != NULL && Object::cast_to<BoidSystem>(n)) {
		bsystem = Object::cast_to<BoidSystem>(n);
	} else {
		bsystem = NULL;
	}

	if ( prev_bsys != NULL && prev_bsys != bsystem ) {
		prev_bsys->remove_set( bset );
		prev_bsys->disconnect("boid_updated", this, "update_boids");
	}
	if ( bsystem != NULL ) {
		bsystem->connect("boid_updated", this, "update_boids");
	}

	update_set();
	update_configuration_warning();
	emit_signal("boid_system_changed");

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\ SETTERS & GETTERS ///////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

BoidSystem* BoidMesh::get_boid_system() {
	return bsystem;
}

AABB BoidMesh::get_aabb() const {
	if ( dynamic_aabb ) {
		return AABB( bset.min, bset.max - bset.min );
	} else {
		return visibility_aabb;
	}

}

PoolVector<Face3> BoidMesh::get_faces(uint32_t p_usage_flags) const { // @suppress("Member declaration not found") // @suppress("Type cannot be resolved")
	return PoolVector<Face3>();
}

void BoidMesh::start_particles() {

	if ( particles.is_valid() && process_material.is_valid()) {
		prepare_material();
		RID material_rid = process_material->get_rid();
		VS::get_singleton()->particles_set_process_material(particles,material_rid);
	}

}

void BoidMesh::stop_particles() {

	clear_textures();
	if ( particles.is_valid() ) {
		VS::get_singleton()->particles_set_process_material(particles,RID());
	}

}

void BoidMesh::set_amount(int p_amount) {

	boid_amount = p_amount;
	update_set();

}

const int& BoidMesh::get_amount() const {
	return boid_amount;
}

void BoidMesh::set_speed(float p_speed) {
	boid_speed = p_speed;
	if ( bset.boids != NULL ) {
		for ( int i = 0; i < bset.count; ++i ) {
			bset.boids[i]->speed = boid_speed;
		}
	}
}

const float& BoidMesh::get_speed() const {
	return boid_speed;
}

void BoidMesh::set_radius(float p_br) {
	boid_radius = p_br;
	if ( bset.boids != NULL ) {
		for ( int i = 0; i < bset.count; ++i ) {
			bset.boids[i]->radius = boid_radius;
		}
	}
}

const float& BoidMesh::get_radius() const {
	return boid_radius;
}

void BoidMesh::set_weight(float p_br) {
	boid_weight = p_br;
	if ( bset.boids != NULL ) {
		for ( int i = 0; i < bset.count; ++i ) {
			bset.boids[i]->weight = boid_weight;
		}
	}
}

const float& BoidMesh::get_weight() const {
	return boid_weight;
}

void BoidMesh::set_separation_weight(float p_sw) {
	boid_separation_weight = p_sw;
	if ( bset.boids != NULL ) {
		for ( int i = 0; i < bset.count; ++i ) {
			bset.boids[i]->sac_weights[0] = boid_separation_weight;
		}
	}
}

const float& BoidMesh::get_separation_weight() const {
	return boid_separation_weight;
}

void BoidMesh::set_alignment_weight(float p_aw) {
	boid_alignment_weight = p_aw;
	if ( bset.boids != NULL ) {
		for ( int i = 0; i < bset.count; ++i ) {
			bset.boids[i]->sac_weights[1] = boid_alignment_weight;
		}
	}
}

const float& BoidMesh::get_alignment_weight() const {
	return boid_alignment_weight;
}

void BoidMesh::set_cohesion_weight(float p_cw) {
	boid_cohesion_weight = p_cw;
	if ( bset.boids != NULL ) {
		for ( int i = 0; i < bset.count; ++i ) {
			bset.boids[i]->sac_weights[2] = boid_alignment_weight;
		}
	}
}

const float& BoidMesh::get_cohesion_weight() const {
	return boid_cohesion_weight;
}

void BoidMesh::set_custom_red(BoidMesh::ColorChannelData p_tc) {
	ccd_custom_r = p_tc;
}

BoidMesh::ColorChannelData BoidMesh::get_custom_red() const {
	return ccd_custom_r;
}

void BoidMesh::set_custom_green(BoidMesh::ColorChannelData p_tc) {
	ccd_custom_g = p_tc;
}

BoidMesh::ColorChannelData BoidMesh::get_custom_green() const {
	return ccd_custom_g;
}

void BoidMesh::set_custom_blue(BoidMesh::ColorChannelData p_tc) {
	ccd_custom_b = p_tc;
}

BoidMesh::ColorChannelData BoidMesh::get_custom_blue() const {
	return ccd_custom_b;
}

void BoidMesh::set_custom_alpha(BoidMesh::ColorChannelData p_tc) {
	ccd_custom_a = p_tc;
}

BoidMesh::ColorChannelData BoidMesh::get_custom_alpha() const {
	return ccd_custom_a;
}

void BoidMesh::set_color_red(BoidMesh::ColorChannelData p_tc) {
	ccd_color_r = p_tc;
}

BoidMesh::ColorChannelData BoidMesh::get_color_red() const {
	return ccd_color_r;
}

void BoidMesh::set_color_green(BoidMesh::ColorChannelData p_tc) {
	ccd_color_g = p_tc;
}

BoidMesh::ColorChannelData BoidMesh::get_color_green() const {
	return ccd_color_g;
}

void BoidMesh::set_color_blue(BoidMesh::ColorChannelData p_tc) {
	ccd_color_b = p_tc;
}

BoidMesh::ColorChannelData BoidMesh::get_color_blue() const {
	return ccd_color_b;
}

void BoidMesh::set_color_alpha(BoidMesh::ColorChannelData p_tc) {
	ccd_color_a = p_tc;
}

BoidMesh::ColorChannelData BoidMesh::get_color_alpha() const {
	return ccd_color_a;
}

void BoidMesh::verify_process_material() {
	if (!process_material.is_valid()) {
		Ref<BoidMaterial> new_mat = memnew(BoidMaterial);
		set_process_material(new_mat);
	}
}

void BoidMesh::set_visibility_aabb(const AABB &p_aabb) {
	visibility_aabb = p_aabb;
	VS::get_singleton()->particles_set_custom_aabb(particles, visibility_aabb);
	update_gizmo();
	_change_notify("visibility_aabb");
}

AABB BoidMesh::get_visibility_aabb() const {
	return visibility_aabb;
}

void BoidMesh::set_dynamic_aabb(const bool &dyn_aabb) {
	dynamic_aabb = dyn_aabb;
}

bool BoidMesh::is_dynamic_aabb() const {
	return dynamic_aabb;
}

void BoidMesh::set_process_material(const Ref<Material> &p_material) {

	process_material = p_material;
	if ( process_material.is_valid() && bset.boids != NULL ) {
		start_particles();
	} else {
		stop_particles();
	}
	update_configuration_warning();

}

Ref<Material> BoidMesh::get_process_material() const {
	return process_material;
}

void BoidMesh::set_draw_passes(int p_count) {
	ERR_FAIL_COND(p_count < 1);
	draw_passes.resize(p_count);
	VS::get_singleton()->particles_set_draw_passes(particles, p_count);
	_change_notify();
}

int BoidMesh::get_draw_passes() const {
	return draw_passes.size();
}

void BoidMesh::set_draw_pass_mesh(int p_pass, const Ref<Mesh> &p_mesh) {
	ERR_FAIL_INDEX(p_pass, draw_passes.size()); // @suppress("Invalid arguments")
	draw_passes.write[p_pass] = p_mesh;
	RID mesh_rid;
	if (p_mesh.is_valid())
		mesh_rid = p_mesh->get_rid();
	VS::get_singleton()->particles_set_draw_pass_mesh(particles, p_pass,
			mesh_rid);
	update_configuration_warning();
}

Ref<Mesh> BoidMesh::get_draw_pass_mesh(int p_pass) const {
	ERR_FAIL_INDEX_V(p_pass, draw_passes.size(), Ref<Mesh>()); // @suppress("Invalid arguments")
	return draw_passes[p_pass];
}

String BoidMesh::get_configuration_warning() const {

	if (OS::get_singleton()->get_current_video_driver()
			== OS::VIDEO_DRIVER_GLES2) {
		return TTR(
				"GPU-based particles are not supported by the GLES2 video driver.\nUse the CPUParticles node instead. You can use the \"Convert to BoidCPUMesh\" option for this purpose.");
	}

	String warnings;

	bool meshes_found = false;
	bool anim_material_found = false;

	for (int i = 0; i < draw_passes.size(); i++) {
		if (draw_passes[i].is_valid()) {
			meshes_found = true;
			for (int j = 0; j < draw_passes[i]->get_surface_count(); j++) {
				anim_material_found = Object::cast_to<ShaderMaterial>(
						draw_passes[i]->surface_get_material(j).ptr()) != NULL;
				SpatialMaterial *spat = Object::cast_to<SpatialMaterial>(
						draw_passes[i]->surface_get_material(j).ptr());
				anim_material_found =
						anim_material_found
								|| (spat
										&& spat->get_billboard_mode()
												== SpatialMaterial::BILLBOARD_PARTICLES);
			}
			if (anim_material_found)
				break;
		}
	}

	anim_material_found = anim_material_found
			|| Object::cast_to<ShaderMaterial>(get_material_override().ptr())
					!= NULL;
	SpatialMaterial *spat = Object::cast_to<SpatialMaterial>(
			get_material_override().ptr());
	anim_material_found = anim_material_found
			|| (spat
					&& spat->get_billboard_mode()
							== SpatialMaterial::BILLBOARD_PARTICLES);

	if (!meshes_found) {
		if (warnings != String())
			warnings += "\n";
		warnings +=
				"- "
						+ TTR(
								"Nothing is visible because meshes have not been assigned to draw passes.");
	}

	if (process_material.is_null()) {
		if (warnings != String())
			warnings += "\n";
		warnings +=
				"- "
						+ TTR(
								"A material to process boids is not assigned, so no behavior is imprinted.");
	}

	if (bsystem == NULL) {
		if (warnings != String())
			warnings += "\n";
		warnings += "- " + TTR("BoidMesh must be parented to BoidSystem to be effective.");
	}

	return warnings;
}

AABB BoidMesh::capture_aabb() const {
	return VS::get_singleton()->particles_get_current_aabb(particles);
}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\ SERVER LINKS //////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidMesh::server_update( int32_t tree_id ) { // @suppress("Type cannot be resolved")

	const BoidServer::boid_tree* _btree = BoidServer::get_tree(tree_id);
	if ( _btree == NULL ) {
		return;
	}
	std::cout << "BoidMesh::server_update" << std::endl;
	std::cout << "\tsystem " << _btree->system << " <> " << this << std::endl;
	std::cout << "\tmeshes " << _btree->mesh_count << std::endl;
	std::cout << "\tcontrols " << _btree->control_count << std::endl;

}

void BoidMesh::server_clear() {

	bsystem = NULL;
	std::cout << "BoidMesh::server_clear" << std::endl;

}

//\\\\\\\\\\\\\\\\\\\\/////////////////////////
//\\\\\\\\\\\\\\\ SCRIPTING //////////////////
//\\\\\\\\\\\\\\\\\\\\///////////////////////

int BoidMesh::get_boid_index( const int& uid ) {

	for ( int i = 0; i < boid_amount; ++i ) {
		if ( bset.boids[i]->UID == uid ) {
			return i;
		}
	}
	return -1;

}

bool BoidMesh::is_boid_active( const int& i ) {
	if ( i < 0 || i >= boid_amount ) {
		return false;
	}
	return bset.boids[i]->active;
}

Variant BoidMesh::get_boid_data( const int& i, BoidData data ) {
	if ( i < 0 || i >= boid_amount ) {
		return Variant();
	}
	Boid::boid* b = bset.boids[i];
	switch( data ) {
		case BoidData::BD_UID:
			return Variant(b->UID);
		case BoidData::BD_TYPE:
			return Variant(b->type);
		case BoidData::BD_ACTIVE:
			return Variant(b->active);
		case BoidData::BD_LOCK:
			return Variant(b->lock);
		case BoidData::BD_COLLISION:
			return Variant(b->collision);
		case BoidData::BD_GROUP:
			return Variant(b->group);
		case BoidData::BD_REACH_GROUP:
			return Variant(b->reach_group);
		case BoidData::BD_REACH_STRENGTH:
			return Variant(b->reach_strength);
		case BoidData::BD_REACH_GROUP_CENTER:
			return Variant(b->reach_group_center);
		case BoidData::BD_ESCAPE_GROUP:
			return Variant(b->escape_group);
		case BoidData::BD_ESCAPE_STRENGTH:
			return Variant(b->escape_strength);
		case BoidData::BD_ESCAPE_GROUP_CENTER:
			return Variant(b->escape_group_center);
		case BoidData::BD_AIM_AT_GROUP:
			return Variant(b->aim_at_group);
		case BoidData::BD_AIM_AT_STRENGTH:
			return Variant(b->aim_at_group);
		case BoidData::BD_AIM_GROUP_CENTER:
			return Variant(b->aim_at_group_center);
		case BoidData::BD_ANIMATION:
			return Variant(b->animation);
		case BoidData::BD_PLANE:
			return Variant(b->plane);
		case BoidData::BD_DOMAIN_POS:
			return Variant(b->domain_pos);
		case BoidData::BD_LOCAL_POS:
			return Variant(b->local_pos);
		case BoidData::BD_PREVIOUS_POS:
			return Variant(b->previous_pos);
		case BoidData::BD_MOTION:
			return Variant(b->motion);
		case BoidData::BD_DIR:
			return Variant(b->dir);
		case BoidData::BD_FRONT:
			return Variant(b->front);
		case BoidData::BD_AIM:
			return Variant(b->aim);
		case BoidData::BD_PREV_FRONT:
			return Variant(b->prev_front);
		case BoidData::BD_PUSH:
			return Variant(b->push);
		case BoidData::BD_MAT3X3:
			return Variant(b->mat3x3);
		case BoidData::BD_INERTIA:
			return Variant(b->inertia);
		case BoidData::BD_SPEED:
			return Variant(b->speed);
		case BoidData::BD_PUSH_SPEED:
			return Variant(b->push_speed);
		case BoidData::BD_INTERNAL_SPEED:
			return Variant(b->internal_speed);
		case BoidData::BD_WEIGHT:
			return Variant(b->weight);
		case BoidData::BD_RADIUS:
			return Variant(b->radius);
		case BoidData::BD_STEER:
			return Variant(b->steer);
		case BoidData::BD_FRICTION:
			return Variant(b->friction);
		case BoidData::BD_INDEX0:
			return Variant(b->index[0]);
		case BoidData::BD_INDEX1:
			return Variant(b->index[1]);
		case BoidData::BD_INDEX2:
			return Variant(b->index[2]);
		case BoidData::BD_CELLID:
			return Variant(b->cellid);
		case BoidData::BD_NEXT:
			if ( b->next != NULL ) {
				return Variant(b->next->UID);
			}
			return Variant();
		case BoidData::BD_TARGET_OFFSET:
			return Variant(b->target_offset);
		case BoidData::BD_SEPARATION:
			return Variant(b->sac_weights[0]);
		case BoidData::BD_ALIGNMENT:
			return Variant(b->sac_weights[1]);
		case BoidData::BD_COHESION:
			return Variant(b->sac_weights[2]);
		case BoidData::BD_TRAVEL_DISTANCE:
			return Variant(b->travel_distance);
		case BoidData::BD_LIFETIME:
			return Variant(b->lifetime);
		case BoidData::BD_TIMER:
			return Variant(b->timer);
		case BoidData::BD_COUNTER:
			return Variant(b->counter);
		case BoidData::BD_PREV_COUNTER:
			return Variant(b->prev_counter);
		case BoidData::BD_COLOR:
			return Variant(b->color);
		case BoidData::BD_TOUCH_GROUND:
			return Variant(b->touch_ground);
		case BoidData::BD_AIMING:
			return Variant(b->aiming);
		case BoidData::BD_TIMER_END:
			return Variant(b->timer_end);
		case BoidData::BD_MOVING:
			return Variant(b->moving);
		case BoidData::BD_MOVING_START:
			return Variant(b->moving_start);
		case BoidData::BD_MOVING_STOP:
			return Variant(b->moving_stop);
		case BoidData::BD_NEIGHBOR_COUNT:
			return Variant(b->neighbor_count);
		// via color channels
		case BoidData::BD_MOTION_DOT_RIGHT:
			return Variant( get_channel_info( b, ColorChannelData::CCDATA_MOTION_DOT_RIGHT ) );
		case BoidData::BD_MOTION_DOT_UP:
			return Variant( get_channel_info( b, ColorChannelData::CCDATA_MOTION_DOT_UP ) );
		case BoidData::BD_MOTION_DOT_FRONT:
			return Variant( get_channel_info( b, ColorChannelData::CCDATA_MOTION_DOT_FRONT ) );
		case BoidData::BD_MOTION_LENGTH:
			return Variant( get_channel_info( b, ColorChannelData::CCDATA_MOTION_LENGTH ) );
		case BoidData::BD_GROUND_ROTATION:
			return Variant( get_channel_info( b, ColorChannelData::CCDATA_GROUND_ROTATION ) );
		default:
			return Variant();
	}
}

void BoidMesh::set_boid_data( const int& i, BoidData data, Variant value ) {
	if ( i < 0 || i >= boid_amount ) {
		return;
	}
	Boid::boid* b = bset.boids[i];
	switch( data ) {
		case BoidData::BD_UID:
			// UID is read only!
			break;
		case BoidData::BD_TYPE:
		{
			int v = value;
			b->type = Boid::Type( v );
		}
			break;
		case BoidData::BD_ACTIVE:
			b->active = value;
			break;
		case BoidData::BD_LOCK:
			b->lock = value;
			break;
		case BoidData::BD_COLLISION:
			b->collision = value;
			break;
		case BoidData::BD_GROUP:
			b->group = value;
			break;
		case BoidData::BD_REACH_GROUP:
			b->reach_group = value;
			break;
		case BoidData::BD_REACH_STRENGTH:
			b->reach_strength = value;
			break;
		case BoidData::BD_REACH_GROUP_CENTER:
			b->reach_group_center = value;
			break;
		case BoidData::BD_ESCAPE_GROUP:
			b->escape_group = value;
			break;
		case BoidData::BD_ESCAPE_STRENGTH:
			b->escape_strength = value;
			break;
		case BoidData::BD_ESCAPE_GROUP_CENTER:
			b->escape_group_center = value;
			break;
		case BoidData::BD_AIM_AT_GROUP:
			b->aim_at_group = value;
			break;
		case BoidData::BD_AIM_AT_STRENGTH:
			b->aim_at_group = value;
			break;
		case BoidData::BD_AIM_GROUP_CENTER:
			b->aim_at_group_center = value;
			break;
		case BoidData::BD_ANIMATION:
			b->animation = value;
			break;
		case BoidData::BD_PLANE:
		{
			int v = value;
			b->plane = Boid::DomainPlanes( int(v) );
		}
			break;
		case BoidData::BD_DOMAIN_POS:
			b->domain_pos = value;
			break;
		case BoidData::BD_LOCAL_POS:
			b->local_pos = value;
			if ( bsystem != NULL ) {
				bsystem->relocate( b );
			}
			break;
		case BoidData::BD_PREVIOUS_POS:
			b->previous_pos = value;
			break;
		case BoidData::BD_MOTION:
			b->motion = value;
			break;
		case BoidData::BD_DIR:
			b->dir = value;
			break;
		case BoidData::BD_FRONT:
			b->front = value;
			break;
		case BoidData::BD_AIM:
			b->aim = value;
			break;
		case BoidData::BD_PREV_FRONT:
			b->prev_front = value;
			break;
		case BoidData::BD_PUSH:
			b->push = value;
			break;
		case BoidData::BD_MAT3X3:
			b->mat3x3 = value;
			break;
		case BoidData::BD_INERTIA:
			b->inertia = value;
			break;
		case BoidData::BD_SPEED:
			b->speed = value;
			break;
		case BoidData::BD_PUSH_SPEED:
			b->push_speed = value;
			break;
		case BoidData::BD_INTERNAL_SPEED:
			b->internal_speed = value;
			break;
		case BoidData::BD_WEIGHT:
			b->weight = value;
			break;
		case BoidData::BD_RADIUS:
			b->radius = value;
			break;
		case BoidData::BD_STEER:
			b->steer = value;
			break;
		case BoidData::BD_FRICTION:
			b->friction = value;
			break;
		case BoidData::BD_INDEX0:
			b->index[0] = value;
			break;
		case BoidData::BD_INDEX1:
			b->index[1] = value;
			break;
		case BoidData::BD_INDEX2:
			b->index[2] = value;
			break;
		case BoidData::BD_CELLID:
			b->cellid = value;
			break;
		case BoidData::BD_TARGET_OFFSET:
			b->target_offset = value;
			break;
		case BoidData::BD_SEPARATION:
			b->sac_weights[0] = value;
			break;
		case BoidData::BD_ALIGNMENT:
			b->sac_weights[1] = value;
			break;
		case BoidData::BD_COHESION:
			b->sac_weights[2] = value;
			break;
		case BoidData::BD_TRAVEL_DISTANCE:
			b->travel_distance = value;
			break;
		case BoidData::BD_LIFETIME:
			b->lifetime = value;
			break;
		case BoidData::BD_TIMER:
			b->timer = value;
			break;
		case BoidData::BD_COUNTER:
			b->counter = value;
			break;
		case BoidData::BD_PREV_COUNTER:
			b->prev_counter = value;
			break;
		case BoidData::BD_COLOR:
			b->color = value;
			break;
		case BoidData::BD_TOUCH_GROUND:
			b->touch_ground = value;
			break;
		case BoidData::BD_AIMING:
			b->aiming = value;
			break;
		case BoidData::BD_TIMER_END:
			b->timer_end = value;
			break;
		case BoidData::BD_MOVING:
			b->moving = value;
			break;
		case BoidData::BD_MOVING_START:
			b->moving_start = value;
			break;
		case BoidData::BD_MOVING_STOP:
			b->moving_stop = value;
			break;
		case BoidData::BD_NEIGHBOR_COUNT:
			b->neighbor_count = value;
			break;
		default:
			return;
	}
}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\ ENGINE LINKS //////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidMesh::_validate_property(PropertyInfo &property) const {
	if (property.name.begins_with("draw_pass_")) {
		int index = property.name.get_slicec('_', 2).to_int() - 1;
		if (index >= draw_passes.size()) {
			property.usage = 0;
			return;
		}
	}
}

void BoidMesh::_notification(int p_what) {

	switch (p_what) {

		case NOTIFICATION_VISIBILITY_CHANGED:
			// make sure particles are updated before rendering occurs if they were active before
			if (is_visible_in_tree() && !VS::get_singleton()->particles_is_inactive(particles)) {
				VS::get_singleton()->particles_request_process(particles);
			}
			break;

		case NOTIFICATION_PREDELETE:
			set_amount(0); // << destroying everything in boid system
			break;

		case NOTIFICATION_PARENTED:
			BoidServer::link( this );
			seek_system();
			update_gizmo();
			break;

		case NOTIFICATION_UNPARENTED:
			BoidServer::unlink( this );
			if ( bsystem ) {
				bsystem->remove_set(bset);
				bsystem = NULL;
				emit_signal("boid_system_changed");
			}
			break;

		case NOTIFICATION_ENTER_WORLD:
			verify_process_material();
			break;

		case NOTIFICATION_LOCAL_TRANSFORM_CHANGED:
			set_transform(transform_identity);
			break;

		case NOTIFICATION_ENTER_TREE:
			BoidServer::link( this );
			break;

		case NOTIFICATION_EXIT_TREE:
			BoidServer::unlink( this );
			break;

//		case NOTIFICATION_ENTER_TREE:
//		case NOTIFICATION_EXIT_TREE:
//		case NOTIFICATION_TRANSFORM_CHANGED:
//		case NOTIFICATION_INTERNAL_PROCESS:
//		case NOTIFICATION_PAUSED:
//		case NOTIFICATION_UNPAUSED:

	}

}

void BoidMesh::_bind_methods() {

	ADD_SIGNAL(MethodInfo("boid_system_changed"));
	ADD_SIGNAL(MethodInfo("boid_event",PropertyInfo(Variant::INT,"boid"),PropertyInfo(Variant::INT,"event")));

	ClassDB::bind_method(D_METHOD("get_boid_index"), &BoidMesh::get_boid_index);
	ClassDB::bind_method(D_METHOD("is_boid_active"), &BoidMesh::is_boid_active);
	ClassDB::bind_method(D_METHOD("get_boid_data"), &BoidMesh::get_boid_data);
	ClassDB::bind_method(D_METHOD("set_boid_data"), &BoidMesh::set_boid_data);

	ClassDB::bind_method(D_METHOD("update_boids"), &BoidMesh::update_boids);
//	ClassDB::bind_method(D_METHOD("capture_aabb"), &BoidMesh::capture_aabb);

	// boids related
	ClassDB::bind_method(D_METHOD("set_amount", "amount"), &BoidMesh::set_amount);
	ClassDB::bind_method(D_METHOD("get_amount"), &BoidMesh::get_amount);
	ClassDB::bind_method(D_METHOD("set_speed", "speed"), &BoidMesh::set_speed);
	ClassDB::bind_method(D_METHOD("get_speed"), &BoidMesh::get_speed);
	ClassDB::bind_method(D_METHOD("set_radius", "boid_radius"),
			&BoidMesh::set_radius);
	ClassDB::bind_method(D_METHOD("get_radius"), &BoidMesh::get_radius);
	ClassDB::bind_method(D_METHOD("set_weight", "boid_weight"), &BoidMesh::set_weight);
	ClassDB::bind_method(D_METHOD("get_weight"), &BoidMesh::get_weight);
	ClassDB::bind_method(D_METHOD("set_separation_weight", "separation_weight"),
			&BoidMesh::set_separation_weight);
	ClassDB::bind_method(D_METHOD("get_separation_weight"),
			&BoidMesh::get_separation_weight);
	ClassDB::bind_method(D_METHOD("set_alignment_weight", "alignment_weight"),
			&BoidMesh::set_alignment_weight);
	ClassDB::bind_method(D_METHOD("get_alignment_weight"),
			&BoidMesh::get_alignment_weight);
	ClassDB::bind_method(D_METHOD("set_cohesion_weight", "cohesion_weight"),
			&BoidMesh::set_cohesion_weight);
	ClassDB::bind_method(D_METHOD("get_cohesion_weight"),
			&BoidMesh::get_cohesion_weight);

	// texture related
	ClassDB::bind_method(D_METHOD("set_custom_red", "custom_red"), &BoidMesh::set_custom_red);
	ClassDB::bind_method(D_METHOD("get_custom_red"), &BoidMesh::get_custom_red);
	ClassDB::bind_method(D_METHOD("set_custom_green", "custom_green"), &BoidMesh::set_custom_green);
	ClassDB::bind_method(D_METHOD("get_custom_green"), &BoidMesh::get_custom_green);
	ClassDB::bind_method(D_METHOD("set_custom_blue", "custom_blue"), &BoidMesh::set_custom_blue);
	ClassDB::bind_method(D_METHOD("get_custom_blue"), &BoidMesh::get_custom_blue);
	ClassDB::bind_method(D_METHOD("set_custom_alpha", "custom_alpha"), &BoidMesh::set_custom_alpha);
	ClassDB::bind_method(D_METHOD("get_custom_alpha"), &BoidMesh::get_custom_alpha);

	ClassDB::bind_method(D_METHOD("set_color_red", "color_red"), &BoidMesh::set_color_red);
	ClassDB::bind_method(D_METHOD("get_color_red"), &BoidMesh::get_color_red);
	ClassDB::bind_method(D_METHOD("set_color_green", "color_green"), &BoidMesh::set_color_green);
	ClassDB::bind_method(D_METHOD("get_color_green"), &BoidMesh::get_color_green);
	ClassDB::bind_method(D_METHOD("set_color_blue", "color_blue"), &BoidMesh::set_color_blue);
	ClassDB::bind_method(D_METHOD("get_color_blue"), &BoidMesh::get_color_blue);
	ClassDB::bind_method(D_METHOD("set_color_alpha", "color_alpha"), &BoidMesh::set_color_alpha);
	ClassDB::bind_method(D_METHOD("get_color_alpha"), &BoidMesh::get_color_alpha);

	// rendering related
	ClassDB::bind_method(D_METHOD("set_process_material", "material"),
			&BoidMesh::set_process_material);
	ClassDB::bind_method(D_METHOD("get_process_material"),
			&BoidMesh::get_process_material);

	ClassDB::bind_method(D_METHOD("set_draw_passes", "passes"), &BoidMesh::set_draw_passes);
	ClassDB::bind_method(D_METHOD("get_draw_passes"), &BoidMesh::get_draw_passes);

	ClassDB::bind_method(D_METHOD("get_draw_pass_mesh", "pass"), &BoidMesh::get_draw_pass_mesh);
	ClassDB::bind_method(D_METHOD("set_draw_pass_mesh", "pass", "mesh"), &BoidMesh::set_draw_pass_mesh);

	ClassDB::bind_method(D_METHOD("set_visibility_aabb", "visibility_aabb"),
			&BoidMesh::set_visibility_aabb);
	ClassDB::bind_method(D_METHOD("get_visibility_aabb"),
			&BoidMesh::get_visibility_aabb);

	ClassDB::bind_method(D_METHOD("set_dynamic_aabb", "dynamic_aabb"),
			&BoidMesh::set_dynamic_aabb);
	ClassDB::bind_method(D_METHOD("is_dynamic_aabb"),
			&BoidMesh::is_dynamic_aabb);

	ClassDB::bind_method(D_METHOD("capture_aabb"), &BoidMesh::capture_aabb);

	// section BOIDS

	ADD_GROUP("Boids", "");

	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "amount", PROPERTY_HINT_EXP_RANGE,
					"1,1000000,1"), "set_amount", "get_amount");
	ADD_PROPERTY(
			PropertyInfo(Variant::REAL, "speed", PROPERTY_HINT_RANGE,
					"0,1000,0.001"), "set_speed", "get_speed");
	ADD_PROPERTY(
			PropertyInfo(Variant::REAL, "boid_radius", PROPERTY_HINT_RANGE,
					"0,10,0.01,or_greater"), "set_radius", "get_radius");
	ADD_PROPERTY(
			PropertyInfo(Variant::REAL, "boid_weight", PROPERTY_HINT_RANGE,
					"0,10,0.01,or_greater,or_lesser"), "set_weight", "get_weight");
	ADD_PROPERTY(
			PropertyInfo(Variant::REAL, "separation_weight",
					PROPERTY_HINT_RANGE, "0,1,0.01,or_greater"),
			"set_separation_weight", "get_separation_weight");
	ADD_PROPERTY(
			PropertyInfo(Variant::REAL, "alignment_weight", PROPERTY_HINT_RANGE,
					"0,1,0.01,or_greater"), "set_alignment_weight",
			"get_alignment_weight");
	ADD_PROPERTY(
			PropertyInfo(Variant::REAL, "cohesion_weight", PROPERTY_HINT_RANGE,
					"0,1,0.01,or_greater"), "set_cohesion_weight",
			"get_cohesion_weight");

	// section INFO MAPPING

	String info_list =
			"none,"
			"group,"
			"reach,"
			"escape,"
			"animation,"
			"position.x,"
			"position.y,"
			"position.z,"
			"motion.x,"
			"motion.y,"
			"motion.z,"
			"motion.length,"
			"direction.x,"
			"direction.y,"
			"direction.z,"
			"front.x,"
			"front.y,"
			"front.z,"
			"push.x,"
			"push.y,"
			"push.z,"
			"travel distance,"
			"lifetime,"
			"timer,"
			"counter,"
			"color.red,"
			"color.green,"
			"color.blue,"
			"color.alpha,"
			"neighbor count,"
			"motion.right,"
			"motion.up,"
			"motion.front,"
			"ground.rotation,"
			"UID,"
			"previous counter";

	ADD_GROUP("Custom mapping", "");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "custom_red", PROPERTY_HINT_ENUM,info_list),
			"set_custom_red", "get_custom_red");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "custom_green", PROPERTY_HINT_ENUM,info_list),
			"set_custom_green", "get_custom_green");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "custom_blue", PROPERTY_HINT_ENUM,info_list),
			"set_custom_blue", "get_custom_blue");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "custom_alpha", PROPERTY_HINT_ENUM,info_list),
			"set_custom_alpha", "get_custom_alpha");

	ADD_GROUP("Color mapping", "");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "color_red", PROPERTY_HINT_ENUM,info_list),
			"set_color_red", "get_color_red");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "color_green", PROPERTY_HINT_ENUM,info_list),
			"set_color_green", "get_color_green");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "color_blue", PROPERTY_HINT_ENUM,info_list),
			"set_color_blue", "get_color_blue");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "color_alpha", PROPERTY_HINT_ENUM,info_list),
			"set_color_alpha", "get_color_alpha");

	// section DRAWING

	ADD_GROUP("Drawing", "");

	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "dynamic_aabb"),
			"set_dynamic_aabb", "is_dynamic_aabb");

	ADD_PROPERTY(PropertyInfo(Variant::AABB, "visibility_aabb"),
			"set_visibility_aabb", "get_visibility_aabb");

	ADD_GROUP("Process Material", "");
	ADD_PROPERTY(
			PropertyInfo(Variant::OBJECT, "process_material",
					PROPERTY_HINT_RESOURCE_TYPE,
					"BoidMaterial,ShaderMaterial"), "set_process_material",
			"get_process_material");

	ADD_GROUP("Draw Passes", "draw_");

	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "draw_passes", PROPERTY_HINT_RANGE, "0," + itos(MAX_DRAW_PASSES) + ",1"), "set_draw_passes", "get_draw_passes");
	for (int i = 0; i < MAX_DRAW_PASSES; i++) {
		ADD_PROPERTYI( PropertyInfo(Variant::OBJECT, "draw_pass_" + itos(i + 1), PROPERTY_HINT_RESOURCE_TYPE, "Mesh"), "set_draw_pass_mesh", "get_draw_pass_mesh", i);
	}

	BIND_CONSTANT(MAX_DRAW_PASSES);

	BIND_ENUM_CONSTANT(CCDATA_NONE);
	BIND_ENUM_CONSTANT(CCDATA_GROUP);
	BIND_ENUM_CONSTANT(CCDATA_REACH_GROUP);
	BIND_ENUM_CONSTANT(CCDATA_ESCAPE_GROUP);
	BIND_ENUM_CONSTANT(CCDATA_ANIMATION);
	BIND_ENUM_CONSTANT(CCDATA_POSITION_X);
	BIND_ENUM_CONSTANT(CCDATA_POSITION_Y);
	BIND_ENUM_CONSTANT(CCDATA_POSITION_Z);
	BIND_ENUM_CONSTANT(CCDATA_MOTION_X);
	BIND_ENUM_CONSTANT(CCDATA_MOTION_Y);
	BIND_ENUM_CONSTANT(CCDATA_MOTION_Z);
	BIND_ENUM_CONSTANT(CCDATA_MOTION_LENGTH);
	BIND_ENUM_CONSTANT(CCDATA_DIR_X);
	BIND_ENUM_CONSTANT(CCDATA_DIR_Y);
	BIND_ENUM_CONSTANT(CCDATA_DIR_Z);
	BIND_ENUM_CONSTANT(CCDATA_FRONT_X);
	BIND_ENUM_CONSTANT(CCDATA_FRONT_Y);
	BIND_ENUM_CONSTANT(CCDATA_FRONT_Z);
	BIND_ENUM_CONSTANT(CCDATA_PUSH_X);
	BIND_ENUM_CONSTANT(CCDATA_PUSH_Y);
	BIND_ENUM_CONSTANT(CCDATA_PUSH_Z);
	BIND_ENUM_CONSTANT(CCDATA_TRAVEL_DISTANCE);
	BIND_ENUM_CONSTANT(CCDATA_LIFETIME);
	BIND_ENUM_CONSTANT(CCDATA_TIMER);
	BIND_ENUM_CONSTANT(CCDATA_COUNTER);
	BIND_ENUM_CONSTANT(CCDATA_COLOR_R);
	BIND_ENUM_CONSTANT(CCDATA_COLOR_G);
	BIND_ENUM_CONSTANT(CCDATA_COLOR_B);
	BIND_ENUM_CONSTANT(CCDATA_COLOR_A);
	BIND_ENUM_CONSTANT(CCDATA_NEIGHBOR_COUNT);
	BIND_ENUM_CONSTANT(CCDATA_MOTION_DOT_RIGHT);
	BIND_ENUM_CONSTANT(CCDATA_MOTION_DOT_UP);
	BIND_ENUM_CONSTANT(CCDATA_MOTION_DOT_FRONT);
	BIND_ENUM_CONSTANT(CCDATA_GROUND_ROTATION);
	BIND_ENUM_CONSTANT(CCDATA_UID);
	BIND_ENUM_CONSTANT(CCDATA_COUNTER_PREVIOUS);

	BIND_ENUM_CONSTANT(BD_UID);
	BIND_ENUM_CONSTANT(BD_TYPE);
	BIND_ENUM_CONSTANT(BD_ACTIVE);
	BIND_ENUM_CONSTANT(BD_LOCK);
	BIND_ENUM_CONSTANT(BD_COLLISION);
	BIND_ENUM_CONSTANT(BD_GROUP);
	BIND_ENUM_CONSTANT(BD_REACH_GROUP);
	BIND_ENUM_CONSTANT(BD_REACH_STRENGTH);
	BIND_ENUM_CONSTANT(BD_REACH_GROUP_CENTER);
	BIND_ENUM_CONSTANT(BD_ESCAPE_GROUP);
	BIND_ENUM_CONSTANT(BD_ESCAPE_STRENGTH);
	BIND_ENUM_CONSTANT(BD_ESCAPE_GROUP_CENTER);
	BIND_ENUM_CONSTANT(BD_AIM_AT_GROUP);
	BIND_ENUM_CONSTANT(BD_AIM_AT_STRENGTH);
	BIND_ENUM_CONSTANT(BD_AIM_GROUP_CENTER);
	BIND_ENUM_CONSTANT(BD_ANIMATION);
	BIND_ENUM_CONSTANT(BD_PLANE);
	BIND_ENUM_CONSTANT(BD_DOMAIN_POS);
	BIND_ENUM_CONSTANT(BD_LOCAL_POS);
	BIND_ENUM_CONSTANT(BD_PREVIOUS_POS);
	BIND_ENUM_CONSTANT(BD_MOTION);
	BIND_ENUM_CONSTANT(BD_DIR);
	BIND_ENUM_CONSTANT(BD_FRONT);
	BIND_ENUM_CONSTANT(BD_AIM);
	BIND_ENUM_CONSTANT(BD_PREV_FRONT);
	BIND_ENUM_CONSTANT(BD_PUSH);
	BIND_ENUM_CONSTANT(BD_MAT3X3);
	BIND_ENUM_CONSTANT(BD_INERTIA);
	BIND_ENUM_CONSTANT(BD_SPEED);
	BIND_ENUM_CONSTANT(BD_PUSH_SPEED);
	BIND_ENUM_CONSTANT(BD_INTERNAL_SPEED);
	BIND_ENUM_CONSTANT(BD_WEIGHT);
	BIND_ENUM_CONSTANT(BD_RADIUS);
	BIND_ENUM_CONSTANT(BD_STEER);
	BIND_ENUM_CONSTANT(BD_FRICTION);
	BIND_ENUM_CONSTANT(BD_INDEX0);
	BIND_ENUM_CONSTANT(BD_INDEX1);
	BIND_ENUM_CONSTANT(BD_INDEX2);
	BIND_ENUM_CONSTANT(BD_CELLID);
	BIND_ENUM_CONSTANT(BD_NEXT);
	BIND_ENUM_CONSTANT(BD_TARGET_OFFSET);
	BIND_ENUM_CONSTANT(BD_SEPARATION);
	BIND_ENUM_CONSTANT(BD_ALIGNMENT);
	BIND_ENUM_CONSTANT(BD_COHESION);
	BIND_ENUM_CONSTANT(BD_TRAVEL_DISTANCE);
	BIND_ENUM_CONSTANT(BD_LIFETIME);
	BIND_ENUM_CONSTANT(BD_TIMER);
	BIND_ENUM_CONSTANT(BD_COUNTER);
	BIND_ENUM_CONSTANT(BD_PREV_COUNTER);
	BIND_ENUM_CONSTANT(BD_COLOR);
	BIND_ENUM_CONSTANT(BD_TOUCH_GROUND);
	BIND_ENUM_CONSTANT(BD_AIMING);
	BIND_ENUM_CONSTANT(BD_TIMER_END);
	BIND_ENUM_CONSTANT(BD_MOVING);
	BIND_ENUM_CONSTANT(BD_MOVING_START);
	BIND_ENUM_CONSTANT(BD_MOVING_STOP);
	BIND_ENUM_CONSTANT(BD_NEIGHBOR_COUNT);
	BIND_ENUM_CONSTANT(BD_MOTION_DOT_RIGHT);
	BIND_ENUM_CONSTANT(BD_MOTION_DOT_UP);
	BIND_ENUM_CONSTANT(BD_MOTION_DOT_FRONT);
	BIND_ENUM_CONSTANT(BD_MOTION_LENGTH);
	BIND_ENUM_CONSTANT(BD_GROUND_ROTATION);

}

BoidMesh::BoidMesh():
		bsystem(NULL),
		im_width(0), im_height(0), im_lines(0),
		boid_amount(100),
		boid_speed(1), boid_radius(1), boid_weight(1),
		boid_separation_weight(1),
		boid_alignment_weight(1),
		boid_cohesion_weight(1),
		ccd_custom_r(ColorChannelData::CCDATA_UID),
		ccd_custom_g(ColorChannelData::CCDATA_TRAVEL_DISTANCE),
		ccd_custom_b(ColorChannelData::CCDATA_MOTION_LENGTH),
		ccd_custom_a(ColorChannelData::CCDATA_GROUND_ROTATION),
		ccd_color_r(ColorChannelData::CCDATA_COLOR_R),
		ccd_color_g(ColorChannelData::CCDATA_COLOR_G),
		ccd_color_b(ColorChannelData::CCDATA_COLOR_B),
		ccd_color_a(ColorChannelData::CCDATA_COLOR_A),
		dynamic_aabb(true)
		{

	transform_identity = Transform(
			Basis(Vector3(1, 0, 0), Vector3(0, 1, 0), Vector3(0, 0, 1)),
			Vector3(0, 0, 0));

	bset.owner = this;
	bset.group = 1;
	bset.boids = NULL;
	bset.count = 0;
	bset.active = 0;

	particles = VS::get_singleton()->particles_create();
	set_base(particles);
	VS::get_singleton()->particles_set_emitting(particles, true);
	VS::get_singleton()->particles_set_one_shot(particles, false);
	VS::get_singleton()->particles_set_amount(particles, boid_amount);
	VS::get_singleton()->particles_set_lifetime(particles, .01);
	VS::get_singleton()->particles_set_fixed_fps(particles, 0);
	VS::get_singleton()->particles_set_fractional_delta(particles, true);
	VS::get_singleton()->particles_set_pre_process_time(particles, 0);
	VS::get_singleton()->particles_set_explosiveness_ratio(particles, 1);
	VS::get_singleton()->particles_set_randomness_ratio(particles, 0);
	VS::get_singleton()->particles_set_use_local_coordinates(particles, true);
	VS::get_singleton()->particles_set_draw_order(particles, VS::ParticlesDrawOrder::PARTICLES_DRAW_ORDER_VIEW_DEPTH);

	set_visibility_aabb(AABB(Vector3(-4, -4, -4), Vector3(8, 8, 8)));
	set_draw_passes(1);

}

BoidMesh::~BoidMesh() {

	stop_particles();
	VS::get_singleton()->free(particles);
	clear_textures();

}

