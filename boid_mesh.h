/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_mesh.h
 *
 *  Created on: Mar 9, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_BOIDS_BOID_MESH_H_
#define MODULES_BOIDS_BOID_MESH_H_

#include <iostream>
#include <algorithm>
#include <math.h>

#include "core/os/os.h"
#include "core/rid.h"
#include "core/image.h"
#include "core/math/transform.h"
#include "core/math/math_funcs.h"
#include "scene/3d/camera.h"
#include "scene/3d/visual_instance.h"
#include "scene/3d/particles.h"
#include "scene/3d/immediate_geometry.h"
#include "scene/main/viewport.h"
#include "scene/resources/material.h"

#include "boid_common.h"
#include "boid_system.h"
#include "boid_material.h"

#define TEXT_DATA_LAYER_COUNT 4

class BoidServer;

class BoidMesh : public GeometryInstance {

private:
	GDCLASS(BoidMesh, GeometryInstance); // @suppress("Symbol is not resolved")
	OBJ_CATEGORY("Boid");

public:
	enum {
		MAX_DRAW_PASSES = 4
	};

	enum ColorChannelData {
		CCDATA_NONE,
		CCDATA_GROUP,				// group flag [0,1048575]
		CCDATA_REACH_GROUP,			// reach flag [0,1048575]
		CCDATA_ESCAPE_GROUP,		// escape flag [0,1048575]
		CCDATA_ANIMATION,			// animation flag [0,1048575]
		CCDATA_POSITION_X,			// local position x
		CCDATA_POSITION_Y,			// local position y
		CCDATA_POSITION_Z,			// local position z
		CCDATA_MOTION_X,			// last motion x
		CCDATA_MOTION_Y,			// last motion y
		CCDATA_MOTION_Z,			// last motion z
		CCDATA_MOTION_LENGTH,		// last motion length
		CCDATA_DIR_X,				// current direction x
		CCDATA_DIR_Y,				// current direction y
		CCDATA_DIR_Z,				// current direction z
		CCDATA_FRONT_X,				// current front x
		CCDATA_FRONT_Y,				// current front y
		CCDATA_FRONT_Z,				// current front z
		CCDATA_PUSH_X,				// current push x
		CCDATA_PUSH_Y,				// current push y
		CCDATA_PUSH_Z,				// current push z
		CCDATA_TRAVEL_DISTANCE,		// travel_distance [float]
		CCDATA_LIFETIME,			// lifetime [float]
		CCDATA_TIMER,				// timer [float]
		CCDATA_COUNTER,				// counter [float]
		CCDATA_COLOR_R,
		CCDATA_COLOR_G,
		CCDATA_COLOR_B,
		CCDATA_COLOR_A,
		CCDATA_NEIGHBOR_COUNT,		// number of neighbors
		CCDATA_MOTION_DOT_RIGHT, 	// motion dot product with right or aim right if enabled
		CCDATA_MOTION_DOT_UP, 		// motion dot product with up or aim right if enabled
		CCDATA_MOTION_DOT_FRONT, 	// motion dot product with front or aim if enabled
		CCDATA_GROUND_ROTATION, 	// orientation of boid motion in XZ plane
		CCDATA_UID,					// unique ID of the boid
		CCDATA_COUNTER_PREVIOUS		// previous counter [float]
	};

	enum BoidData {
		BD_UID,
		BD_TYPE,
		BD_ACTIVE,
		BD_LOCK,
		BD_COLLISION,
		BD_GROUP,
		BD_REACH_GROUP,
		BD_REACH_STRENGTH,
		BD_REACH_GROUP_CENTER,
		BD_ESCAPE_GROUP,
		BD_ESCAPE_STRENGTH,
		BD_ESCAPE_GROUP_CENTER,
		BD_AIM_AT_GROUP,
		BD_AIM_AT_STRENGTH,
		BD_AIM_GROUP_CENTER,
		BD_ANIMATION,
		BD_PLANE,
		BD_DOMAIN_POS,
		BD_LOCAL_POS,
		BD_PREVIOUS_POS,
		BD_MOTION,
		BD_DIR,
		BD_FRONT,
		BD_AIM,
		BD_PREV_FRONT,
		BD_PUSH,
		BD_MAT3X3,
		BD_INERTIA,
		BD_SPEED,
		BD_PUSH_SPEED,
		BD_INTERNAL_SPEED,
		BD_WEIGHT,
		BD_RADIUS,
		BD_STEER,
		BD_FRICTION,
		BD_INDEX0,
		BD_INDEX1,
		BD_INDEX2,
		BD_CELLID,
		BD_NEXT,
		BD_TARGET_OFFSET,
		BD_SEPARATION,
		BD_ALIGNMENT,
		BD_COHESION,
		BD_TRAVEL_DISTANCE,
		BD_LIFETIME,
		BD_TIMER,
		BD_COUNTER,
		BD_PREV_COUNTER,
		BD_COLOR,
		BD_TOUCH_GROUND,
		BD_AIMING,
		BD_TIMER_END,
		BD_MOVING,
		BD_MOVING_START,
		BD_MOVING_STOP,
		BD_NEIGHBOR_COUNT,
		// non existing in boid bu computed in color channel
		BD_MOTION_DOT_RIGHT,
		BD_MOTION_DOT_UP,
		BD_MOTION_DOT_FRONT,
		BD_MOTION_LENGTH,
		BD_GROUND_ROTATION
	};

private:

	BoidSystem* bsystem;

	// communication objects
	RID particles;
	int im_width;
	int im_height;
	int im_lines;
	Ref<Image> im_data;
	Ref<ImageTexture> tex_data;
	Vector3 inactive_position;
	Color inactive_color;

	// boids related
	Boid::boid_set bset;
	int boid_amount;			// actual number of boids
	float boid_speed;
	float boid_radius;
	float boid_weight;
	float boid_separation_weight;
	float boid_alignment_weight;
	float boid_cohesion_weight;

	// texture related
	ColorChannelData ccd_custom_r;
	ColorChannelData ccd_custom_g;
	ColorChannelData ccd_custom_b;
	ColorChannelData ccd_custom_a;

	ColorChannelData ccd_color_r;
	ColorChannelData ccd_color_g;
	ColorChannelData ccd_color_b;
	ColorChannelData ccd_color_a;

	// rendering related
	AABB visibility_aabb;
	bool dynamic_aabb;
	Ref<ShaderMaterial> process_material;
	Particles::DrawOrder draw_order;
	Vector<Ref<Mesh> > draw_passes;

	// usefull
	Transform transform_identity;

protected:

	static void _bind_methods();
	void _notification(int p_what);
	virtual void _validate_property(PropertyInfo &property) const;

	void seek_system();
	void clear_textures();
	void update_inactive_color();
	void update_textures();
	void prepare_material();
	void update_set();
	static float get_channel_info( Boid::boid* b, const ColorChannelData& tc );
	void serialise_boids();

	void start_particles();
	void stop_particles();

	void verify_process_material();

public:

	// server connection
	void server_update( int32_t tree_id ); // @suppress("Type cannot be resolved")
	void server_clear();

	BoidSystem* get_boid_system();

	// mandatory for GeometryInstance

	AABB get_aabb() const;
	PoolVector<Face3> get_faces(uint32_t p_usage_flags) const; // @suppress("Type cannot be resolved")

	// boids related

	void set_amount(int p_amount);
	const int& get_amount() const;

	void set_speed(float p_speed);
	const float& get_speed() const;

	void set_radius(float p_br);
	const float& get_radius() const;

	void set_weight(float p_br);
	const float& get_weight() const;

	void set_separation_weight(float p_sw);
	const float& get_separation_weight() const;

	void set_alignment_weight(float p_aw);
	const float& get_alignment_weight() const;

	void set_cohesion_weight(float p_cw);
	const float& get_cohesion_weight() const;

	// texture related

	void set_custom_red(BoidMesh::ColorChannelData p_tc);
	BoidMesh::ColorChannelData get_custom_red() const;

	void set_custom_green(BoidMesh::ColorChannelData p_tc);
	BoidMesh::ColorChannelData get_custom_green() const;

	void set_custom_blue(BoidMesh::ColorChannelData p_tc);
	BoidMesh::ColorChannelData get_custom_blue() const;

	void set_custom_alpha(BoidMesh::ColorChannelData p_tc);
	BoidMesh::ColorChannelData get_custom_alpha() const;

	void set_color_red(BoidMesh::ColorChannelData p_tc);
	BoidMesh::ColorChannelData get_color_red() const;

	void set_color_green(BoidMesh::ColorChannelData p_tc);
	BoidMesh::ColorChannelData get_color_green() const;

	void set_color_blue(BoidMesh::ColorChannelData p_tc);
	BoidMesh::ColorChannelData get_color_blue() const;

	void set_color_alpha(BoidMesh::ColorChannelData p_tc);
	BoidMesh::ColorChannelData get_color_alpha() const;

	// scripting interface
	int get_boid_index( const int& uid );
	bool is_boid_active( const int& i );
	Variant get_boid_data( const int& i, BoidData data );
	void set_boid_data( const int& i, BoidData data, Variant value );

	// rendering related

	void update_boids();

	void set_visibility_aabb(const AABB &p_aabb);
	AABB get_visibility_aabb() const;

	void set_dynamic_aabb(const bool &dyn_aabb);
	bool is_dynamic_aabb() const;

	void set_process_material(const Ref<Material> &p_material);
	Ref<Material> get_process_material() const;

	void set_draw_passes(int p_count);
	int get_draw_passes() const;

	void set_draw_pass_mesh(int p_pass, const Ref<Mesh> &p_mesh);
	Ref<Mesh> get_draw_pass_mesh(int p_pass) const;

	virtual String get_configuration_warning() const;

	AABB capture_aabb() const;

	BoidMesh();
	virtual ~BoidMesh();

};

VARIANT_ENUM_CAST(BoidMesh::ColorChannelData)
VARIANT_ENUM_CAST(BoidMesh::BoidData)

#endif /* MODULES_BOIDS_BOID_MESH_H_ */
