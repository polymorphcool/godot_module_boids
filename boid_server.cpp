/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_server.cpp
 *
 *  Created on: Jul 7, 2021
 *      Author: frankiezafe
 */

#include "boid_server.h"

static BoidServer* singleton = NULL;

// \\\\\\\\\\\\\\\\\\ UTILS //////////////////

int32_t BoidServer::find_tree( BoidSystem* _system ) {

//	for ( uint16_t i = 0, imax = trees.size(); i < imax; ++i ) {
//		if ( trees[i].system == _system ) {
//			return i;
//		}
//	}
	return -1;

}

int32_t BoidServer::find_tree( BoidMesh* _mesh ) {

//	for ( uint16_t i = 0, imax = trees.size(); i < imax; ++i ) {
//		for ( uint16_t j = 0; j < trees[i].mesh_count; ++j ) {
//			if ( trees[i].meshes[j] == _mesh ) {
//				return i;
//			}
//		}
//	}
	return -1;

}

int32_t BoidServer::find_tree( BoidControl* _ctrl ) {

//	for ( uint16_t i = 0, imax = trees.size(); i < imax; ++i ) {
//		for ( uint16_t j = 0; j < trees[i].control_count; ++j ) {
//			if ( trees[i].controls[j] == _ctrl ) {
//				return i;
//			}
//		}
//	}
	return -1;

}

int32_t BoidServer::seek_system( Node* _node ) {

//	Node* n = _node->get_parent();
//	while ( n != NULL ) {
//		if ( Object::cast_to<BoidSystem>(n)) {
//			return find_tree( Object::cast_to<BoidSystem>(n) );
//		}
//		n = n->get_parent();
//	}
	return -1;

}

bool BoidServer::seek_parent( Node* _parent, Node* _node ) {

//	Node* n = _node->get_parent();
//	while ( n != NULL ) {
//		if ( n == _parent ) {
//			return true;
//		}
//		n = n->get_parent();
//	}
	return false;

}

void BoidServer::clear( int32_t tid, Node* _node ) {

//	if ( tid == -1 ) {
//		return;
//	}
//
//	boid_tree & t = trees.write[ tid ];
//	boid_tree zombies;
//
//	// gathering zombies
//	for ( uint32_t i = 0; i < t.mesh_count; ++i ) {
//		if ( seek_parent( _node, t.meshes[i] ) ) {
//			zombies.meshes.push_back( t.meshes[i] );
//		}
//	}
//	for ( uint32_t i = 0; i < t.control_count; ++i ) {
//		if ( seek_parent( _node, t.controls[i] ) ) {
//			zombies.controls.push_back( t.controls[i] );
//		}
//	}
//	zombies.mesh_count = zombies.meshes.size();
//	zombies.control_count = zombies.controls.size();
//
//	// disconnecting zombies
//	for ( uint32_t i = 0; i < zombies.mesh_count; ++i ) {
//		t.meshes.erase( zombies.meshes[i] );
//		warn_mesh( zombies.meshes[i], -1 );
//	}
//	for ( uint32_t i = 0; i < zombies.control_count; ++i ) {
//		t.controls.erase( zombies.controls[i] );
//		warn_control( zombies.controls[i], -1 );
//	}
//
//	t.mesh_count = t.meshes.size();
//	t.control_count = t.controls.size();
//	warn_system( t.system, tid );

}

const BoidServer::boid_tree* BoidServer::get_tree( int32_t tree_id ) {

//	if ( singleton == NULL || tree_id < 0 || tree_id >= singleton->trees.size() ) {
//		return NULL;
//	}
//
//	return &singleton->trees[ tree_id ];

	return NULL;

}

// \\\\\\\\\\\\\\\\\\ SYSTEM MANAGEMENT //////////////////

void BoidServer::warn_system( BoidSystem* _system, int32_t tid ) {

//	if ( tid == -1 ) {
//		_system->server_clear();
//	} else {
//		_system->server_update( tid );
//	}

}

void BoidServer::link_system( BoidSystem* _system, bool enable ) {

//	int32_t tid = find_tree( _system );
//
//	if ( !enable && tid == -1 ) {
//		return;
//	} else if ( enable && tid != -1 ) {
//		return;
//	}
//
//	if ( !enable ) {
//
//		boid_tree & tree = trees.write[ tid ];
//		for ( uint32_t i = 0; i < tree.mesh_count; ++i ) {
//			warn_mesh( tree.meshes[i], -1 );
//		}
//		for ( uint32_t i = 0; i < tree.control_count; ++i ) {
//			warn_control( tree.controls[i], -1 );
//		}
//		tree.meshes.clear();
//		tree.controls.clear();
//		tree.mesh_count = 0;
//		tree.control_count = 0;
//		warn_system( _system, -1 );
//
//	} else {
//
//		boid_tree t;
//		t.system = _system;
//		t.mesh_count = 0;
//		t.control_count = 0;
//		tid = trees.size();
//		trees.push_back( t );
//		warn_system( _system, tid );
//
//	}

}

void BoidServer::link( BoidSystem* _system ) {
//	singleton->link_system( _system, true );
}

void BoidServer::unlink( BoidSystem* _system ) {
//	singleton->link_system( _system, false );
}

// \\\\\\\\\\\\\\\\\\ MESH MANAGEMENT //////////////////

void BoidServer::warn_mesh( BoidMesh* _mesh, int32_t tid ) {

//	if ( tid == -1 ) {
//		_mesh->server_clear();
//	} else {
//		_mesh->server_update( tid );
//	}

}

void BoidServer::link_mesh( BoidMesh* _mesh, bool enable ) {

//	int32_t tid = find_tree( _mesh );
//	int32_t sid = seek_system( _mesh );
//
//	if ( !enable and tid == -1 ) {
//		return;
//	} else if ( enable and tid != -1 and tid == sid ) {
//		return;
//	}
//
//	if ( !enable ) {
//
//		boid_tree & t = trees.write[ tid ];
//		t.meshes.erase( _mesh );
//		t.mesh_count = t.meshes.size();
//		clear( tid, _mesh );
//
//	} else {
//
//		if ( tid != -1 ) {
//			boid_tree & t = trees.write[ tid ];
//			t.meshes.erase( _mesh );
//			t.mesh_count = t.meshes.size();
//			clear( tid, _mesh );
//		}
//
//		if ( sid != -1 ) {
//			boid_tree & t = trees.write[ sid ];
//			t.meshes.push_back( _mesh );
//			t.mesh_count = t.meshes.size();
//			warn_mesh( _mesh, sid );
//			warn_system( t.system, sid );
//		}
//
//	}

}

void BoidServer::link( BoidMesh* _mesh ) {
//	singleton->link_mesh( _mesh, true );
}

void BoidServer::unlink( BoidMesh* _mesh ) {
//	singleton->link_mesh( _mesh, false );
}

// \\\\\\\\\\\\\\\\\\ CONTROL MANAGEMENT //////////////////

void BoidServer::warn_control( BoidControl* _ctrl, int32_t tid ) {

//	if ( tid == -1 ) {
//		_ctrl->server_clear();
//	} else {
//		_ctrl->server_update( tid );
//	}

}

void BoidServer::link_control( BoidControl* _ctrl, bool enable ) {

//	int32_t tid = find_tree( _ctrl );
//	int32_t sid = seek_system( _ctrl );
//
//	if ( !enable and tid == -1 ) {
//		return;
//	} else if ( enable and tid != -1 and tid == sid ) {
//		return;
//	}
//
//	if ( !enable ) {
//
//		boid_tree & t = trees.write[ tid ];
//		t.controls.erase( _ctrl );
//		t.control_count = t.controls.size();
//		clear( tid, _ctrl );
//
//	} else {
//
//		if ( tid != -1 ) {
//			boid_tree & t = trees.write[ tid ];
//			t.controls.erase( _ctrl );
//			t.control_count = t.controls.size();
//			clear( tid, _ctrl );
//		}
//
//		if ( sid != -1 ) {
//			boid_tree & t = trees.write[ sid ];
//			t.controls.push_back( _ctrl );
//			t.control_count = t.controls.size();
//			warn_control( _ctrl, sid );
//			warn_system( t.system, sid );
//		}
//
//	}

}

void BoidServer::link( BoidControl* _ctrl ) {
//	singleton->link_control( _ctrl, true );
}

void BoidServer::unlink( BoidControl* _ctrl ) {
//	singleton->link_control( _ctrl, false );
}

// \\\\\\\\\\\\\\\\\\ STD //////////////////

void BoidServer::_bind_methods() {

}

void BoidServer::_notification(int p_what) {

}

BoidServer* BoidServer::get_singleton() {

	if ( singleton == NULL ) {
		singleton = memnew(BoidServer);
	}
	return singleton;

}

void BoidServer::kill() {

	if ( singleton != NULL ) {
		memdelete(singleton);
		singleton = NULL;
	}

}

BoidServer::BoidServer() {
}

