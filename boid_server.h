/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_server.h
 *
 *  Created on: Jul 7, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_BOIDS_BOID_SERVER_H_
#define MODULES_BOIDS_BOID_SERVER_H_

#include "core/object.h"
#include "core/hash_map.h"

#include "boid_common.h"
#include "boid.h"
#include "boid_mesh.h"
#include "boid_system.h"
#include "boid_control.h"

class BoidServer : public Object {
	GDCLASS(BoidServer, Object);

public:
	typedef struct boid_tree {
		BoidSystem* system;
		Vector< BoidMesh* > meshes;
		Vector< BoidControl* > controls;
		uint16_t mesh_count;
		uint16_t control_count;
	} boid_tree;

protected:

	BoidServer();
	static void _bind_methods();
	void _notification(int p_what);

	int32_t find_tree( BoidSystem* _system );
	int32_t find_tree( BoidMesh* _mesh );
	int32_t find_tree( BoidControl* _ctrl );

	int32_t seek_system( Node* _node );
	bool seek_parent( Node* _parent, Node* _node );

	void clear( int32_t tid, Node* _node );

	void link_system( BoidSystem* _system, bool enable );
	void link_mesh( BoidMesh* _mesh, bool enable );
	void link_control( BoidControl* _ctrl, bool enable );

	void warn_system( BoidSystem* _system, int32_t tid );
	void warn_mesh( BoidMesh* _mesh, int32_t tid );
	void warn_control( BoidControl* _ctrl, int32_t tid );

	Vector< boid_tree > trees;

public:

	static const BoidServer::boid_tree* get_tree( int32_t tree_id );

	static void link( BoidSystem* _system );
	static void link( BoidMesh* _mesh );
	static void link( BoidControl* _ctrl );

	static void unlink( BoidSystem* _system );
	static void unlink( BoidMesh* _mesh );
	static void unlink( BoidControl* _ctrl );

	static BoidServer *get_singleton();
	static void kill();

};

#endif /* MODULES_BOIDS_BOID_SERVER_H_ */
