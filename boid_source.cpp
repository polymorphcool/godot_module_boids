/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_source.cpp
 *
 *  Created on: Feb 13, 2021
 *      Author: frankiezafe
 */

#include "boid_source.h"

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\\\ INTERNAL ////////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidSource::behaviour_updated() {

	behaviour_edited();
	if ( behaviour.is_valid() ) {
		behaviour->load( &data.behaviour );
	}

	if ( bsystem != NULL ) {
		bsystem->update_behaviour( &data );
	}

}

void BoidSource::behaviour_edited() {

	if ( behaviour.is_valid() ) {
		if ( bsystem == NULL ) {
			behaviour->set_control_info( Vector<Boid::boid_control_info>() );
		} else {
			behaviour->set_control_info( bsystem->get_behaviour_info() );
		}
	}

}

void BoidSource::system_control_updated() {

	behaviour_edited();
	behaviour_updated();
	BoidControl::system_control_updated();

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\ SETTERS & GETTERS ///////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidSource::set_rate( float p_rate ) {
	if ( p_rate <= 0 ) {
		rate = 0;
	} else {
		rate = p_rate;
	}
	data.emission_rate = rate;
}

const float& BoidSource::get_rate() const {
	return rate;
}

void BoidSource::set_direction( Boid::ControlDirection p_dir ) {
	dir = p_dir;
	data.direction = dir;
}

Boid::ControlDirection BoidSource::get_direction() const {
	return dir;
}

void BoidSource::set_behaviour(const Ref<BoidBehaviour> &p_behaviour) {

	if ( behaviour == p_behaviour ) {
		return;
	}
	if ( behaviour.is_valid() ) {
		behaviour->disconnect("behaviour_updated", this, "behaviour_updated");
		behaviour->disconnect("behaviour_edited", this, "behaviour_edited");
	}
	BoidBehaviour::purge( &data.behaviour );
	behaviour = p_behaviour;
	if ( behaviour.is_valid() ) {
		behaviour->connect("behaviour_updated", this, "behaviour_updated");
		behaviour->connect("behaviour_edited", this, "behaviour_edited");
	}
	behaviour_updated();

}

Ref<BoidBehaviour> BoidSource::get_behaviour() const {
	return behaviour;
}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\ ENGINE LINKS //////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

String BoidSource::get_configuration_warning() const {

	String warnings = BoidControl::get_configuration_warning();

	// checking if a boid_mesh is visible
	if ( bmesh == NULL ) {
		if (warnings != String())
			warnings += "\n";
		warnings += "- " + TTR("BoidSource must be parented to BoidMesh to emit boids.");
	}

	return warnings;

}

void BoidSource::_notification(int p_what) {

	switch (p_what) {
		case NOTIFICATION_PARENTED:
			// reloading the behaviour
			behaviour_updated();
			break;
	}

	BoidControl::_notification(p_what);

}

void BoidSource::_bind_methods() {

	ClassDB::bind_method(D_METHOD("set_rate", "rate"), &BoidSource::set_rate);
	ClassDB::bind_method(D_METHOD("get_rate"), &BoidSource::get_rate);

	ClassDB::bind_method(D_METHOD("set_direction", "direction"), &BoidSource::set_direction);
	ClassDB::bind_method(D_METHOD("get_direction"), &BoidSource::get_direction);

	ClassDB::bind_method(D_METHOD("behaviour_updated"), &BoidSource::behaviour_updated);
	ClassDB::bind_method(D_METHOD("behaviour_edited"), &BoidSource::behaviour_edited);

	ClassDB::bind_method(D_METHOD("set_behaviour", "behaviour"), &BoidSource::set_behaviour);
	ClassDB::bind_method(D_METHOD("get_behaviour"), &BoidSource::get_behaviour);

	ADD_PROPERTY(
			PropertyInfo(Variant::REAL, "rate", PROPERTY_HINT_RANGE,
					"0,100,1,or_greater"), "set_rate", "get_rate");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "direction", PROPERTY_HINT_ENUM,
					"random, radial, right (X), left (-X), up (Y), down (-Y), forward (Z), backward (-Z)"), "set_direction",
			"get_direction");

	ADD_PROPERTY(
			PropertyInfo(Variant::OBJECT, "behaviour", PROPERTY_HINT_RESOURCE_TYPE,
					"BoidBehaviour"), "set_behaviour", "get_behaviour");

}

BoidSource::BoidSource() {

	data.owner = this;
	data.type = Boid::ControlType::BCTRL_TYPE_SOURCE;

	set_rate(10);

}

BoidSource::~BoidSource() {
}
