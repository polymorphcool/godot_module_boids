/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_source.h
 *
 *  Created on: Feb 13, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_BOIDS_BOID_SOURCE_H_
#define MODULES_BOIDS_BOID_SOURCE_H_

#include "boid_common.h"
#include "boid_control.h"
#include "boid_behaviour.h"

class BoidSource : public BoidControl {

private:
	GDCLASS(BoidSource, BoidControl);
	OBJ_CATEGORY("Boid");

private:

	float rate;
	Boid::ControlDirection dir;
	Ref<BoidBehaviour> behaviour;

protected:

	static void _bind_methods();
	void _notification(int p_what);

public:

	void behaviour_updated();
	void behaviour_edited();
	virtual void system_control_updated();

	void set_rate( float p_rate );
	const float& get_rate() const;

	void set_direction( Boid::ControlDirection p_dir );
	Boid::ControlDirection get_direction() const;

	void set_behaviour(const Ref<BoidBehaviour> &p_behaviour);
	Ref<BoidBehaviour> get_behaviour() const;

	virtual String get_configuration_warning() const;

	BoidSource();
	virtual ~BoidSource();

};

#endif /* MODULES_BOIDS_BOID_SOURCE_H_ */
