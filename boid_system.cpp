/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_system.cpp
 *
 *  Created on: Mar 9, 2021
 *      Author: frankiezafe
 */

#include "boid_system.h"
#include "boid_server.h"

static Vector3 Xaxis = Vector3(1,0,0);
static Vector3 Yaxis = Vector3(0,1,0);

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\\\\\ UTILS /////////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidSystem::get_cell_axis(const Vector3 &position, Vector3 &cell_axis) {
	cell_axis = Vector3(-1, -1, -1);
	for (int i = 0; i < 3; ++i) {
		if (position[i] < 0 || position[i] > domain_size[i]) {
			cell_axis[0] = -1;
			return;
		}
		cell_axis[i] = int(position[i] / cell_size);
		if (cell_axis[i] < 0 || cell_axis[i] >= domain_dim[i]) {
			cell_axis[0] = -1;
			return;
		}
	}
}

int BoidSystem::get_cell_id(const Vector3 &cell_axis) const {
	if (cell_axis[0] < 0 || cell_axis[1] < 0 || cell_axis[2] < 0) {
		return -1;
	}
	int id = cell_axis[0] + cell_axis[1] * domain_x
			+ cell_axis[2] * domain_x * domain_y;
	if (id >= 0 && id < cell_num) {
		return id;
	}
	return -1;
}

Vector3 BoidSystem::keep_in_cell(const Vector3 &v) {
	float l = Math::abs(v[0]);
	for (int i = 1; i < 3; ++i) {
		float a = Math::abs(v[i]);
		if (l < a) {
			l = a;
		}
	}
	if (l > cell_size) {
		return (v / l) * cell_size;
	}
	return Vector3(v);
}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\ BOIDS PROCESS /////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidSystem::clear_boids() {
	if (boid_amount > 0) {
		for (int i = 0; i < boid_amount; ++i) {
			if ( boids[i] != NULL ) {
				boids[i]->behaviour = NULL;
				sync_boid_tests(boids[i]);
				delete( boids[i] );
			}
		}
		if ( boids != NULL ) {
			delete [] boids;
		}
		if ( neighbors != NULL ) {
			delete [] neighbors;
		}
		boid_amount = 0;
		neighbor_count = 0;
		boids = NULL;
		neighbors = NULL;
	}
}

void BoidSystem::clear_static_boids() {
	if (static_boids_amount != 0) {
		for (int i = 0; i < static_boids_amount; ++i) {
			if ( static_boids[i] != NULL ) {
				delete (static_boids[i]);
			}
		}
		delete [] static_boids;
		static_boids_amount = 0;
		static_boids = NULL;
	}
}

void BoidSystem::clear_domain() {
	if (cell_num > 0) {
		if ( domain != NULL ) {
			delete [] domain;
		}
		if ( domain_cells != NULL ) {
			delete [] domain_cells;
		}
		if ( domain_dim != NULL ) {
			delete [] domain_dim;
		}
		cell_num = 0;
		domain = NULL;
		domain_cells = NULL;
		domain_dim = NULL;
	}
}

void BoidSystem::init_boid(Boid::boid *b) {

	b->owner = NULL;
	b->lock = false;
	reset_boid( b );

}

void BoidSystem::reset_boid(Boid::boid* b) {

	b->type = Boid::Type::BTYPE_BOID;
	b->active = false;
	b->lock = false;
	b->collision = true;
	b->group = 1;

	b->reach_group = 0;
	b->reach_strength = 1;
	b->reach_group_center = false;

	b->escape_group = 0;
	b->escape_strength = 1;
	b->escape_group_center = false;

	b->aim_at_group = 0;
	b->aim_at_strength = 1;
	b->aim_at_group_center = false;

	b->animation = 0;
	b->plane = domain_plane;
	b->domain_pos *= 0;
	b->local_pos *= 0;
	b->previous_pos *= 0;
	b->motion *= 0;
	b->dir = Vector3(0,0,1);
	b->front = b->dir;
	b->aim = b->front;
	b->prev_front = b->front;
	b->mat3x3 = Basis( 1,0,0, 0,1,0, 0,0,1 );
	b->push *= 0;
	b->inertia = 0;
	b->speed = 1;
	b->push_speed = 1;
	b->internal_speed = 0;
	b->internal_push_speed = 0;
	b->weight = 1;
	b->radius = 1;
	b->steer = 1;
	b->friction = 0.2;
	b->index[0] = -1;
	b->index[1] = -1;
	b->index[2] = -1;
	b->cellid = cell_num;
	b->next = NULL;
	b->control = NULL;
	b->target_offset *= 0;
	b->sac_weights = Vector3(1,1,1);
	b->behaviour = NULL;
	b->travel_distance = 0;
	b->lifetime = 0;
	b->timer = 0;
	b->counter = 0;
	b->prev_counter = 0;
	b->color = color_white;
	b->touch_ground = false;
	b->aiming = false;
	b->timer_end = false;
	b->moving = false;
	b->moving_start = false;
	b->moving_stop = false;
	b->test_count = 0;
	b->tests = NULL;
	b->neighbor_count = 0;
	b->ground_rotation = 0;
	b->event_enabled = false;
	b->event = 0;

}

void BoidSystem::spawn_boid(Boid::boid *b) {

	b->domain_pos = Vector3(Math::random(0.0f, 1.0f), Math::random(0.0f, 1.0f),
			Math::random(0.0f, 1.0f)) * domain_size;
	b->dir = Vector3(Math::random(-1.0f, 1.0f), Math::random(-1.0f, 1.0f),
			Math::random(-1.0f, 1.0f));
	switch (b->plane) {
	case Boid::DomainPlanes::DOMAIN_PLANES_ALL:
		break;
	case Boid::DomainPlanes::DOMAIN_PLANES_FLOOR:
		b->domain_pos[1] = 0;
		b->dir[1] = 0;
		break;
	}
	b->dir.normalize();
	b->front = b->dir;

	b->type = Boid::Type::BTYPE_BOID;
	b->active = true;
	b->collision = true;
	b->group = 1;

	b->reach_group = 0;
	b->reach_strength = 1;
	b->reach_group_center = false;
	b->escape_group = 0;
	b->escape_strength = 1;
	b->escape_group_center = false;

	b->animation = 0;
	b->local_pos = b->domain_pos + domain_offset;
	b->previous_pos = b->local_pos;
	b->motion *= 0;
	b->dir.normalize();
	b->front = b->dir;
	b->aim = b->dir;
	b->prev_front = b->dir;
	b->push *= 0;
	b->inertia = 0;
	b->internal_speed = 0;
	b->control = NULL;
	b->travel_distance = 0;
	b->lifetime = 0;
	b->timer = 0;
	b->counter = 0;
	b->color = color_white;
	b->timer_end = false;
	b->moving = false;
	b->moving_start = false;
	b->moving_stop = false;
	b->neighbor_count = 0;

}

void BoidSystem::sync_boid_tests(Boid::boid* b) {

	if ( b->behaviour == NULL ) {
		if ( b->tests != NULL ) {
			delete [] b->tests;
		}
		b->test_count = 0;
		b->tests = NULL;
		return;
	}

	if ( b->test_count != b->behaviour->test_count ) {
		if ( b->tests != NULL ) {
			delete [] b->tests;
			b->tests = NULL;
		}
		b->test_count = b->behaviour->test_count;
		if ( b->test_count > 0 ) {
			b->tests = new bool[b->test_count];
		}
	}

	for ( int i = 0; i < b->test_count; ++i ) {
		b->tests[i] = false;
	}

}

void BoidSystem::source_boid(const Boid::boid_control_data *src, Boid::boid *b) {

	reset_boid(b);

	b->domain_pos = src->local.origin;
	b->domain_pos += src->local.basis.xform(
			Vector3(src->dimension[0] * Math::random(-1.0f, 1.0f),
					src->dimension[1] * Math::random(-1.0f, 1.0f),
					src->dimension[2] * Math::random(-1.0f, 1.0f)));

	switch (src->direction) {
	case Boid::ControlDirection::BCTRL_DIRECTION_RADIAL:
		b->dir = b->domain_pos - src->local.origin;
		break;
	case Boid::ControlDirection::BCTRL_DIRECTION_X:
		b->dir = src->local.basis.xform(Vector3(1, 0, 0));
		break;
	case Boid::ControlDirection::BCTRL_DIRECTION_Xi:
		b->dir = src->local.basis.xform(Vector3(-1, 0, 0));
		break;
	case Boid::ControlDirection::BCTRL_DIRECTION_Y:
		b->dir = src->local.basis.xform(Vector3(0, 1, 0));
		break;
	case Boid::ControlDirection::BCTRL_DIRECTION_Yi:
		b->dir = src->local.basis.xform(Vector3(0, -1, 0));
		break;
	case Boid::ControlDirection::BCTRL_DIRECTION_Z:
		b->dir = src->local.basis.xform(Vector3(0, 0, 1));
		break;
	case Boid::ControlDirection::BCTRL_DIRECTION_Zi:
		b->dir = src->local.basis.xform(Vector3(0, 0, -1));
		break;
	default:
	case Boid::ControlDirection::BCTRL_DIRECTION_RANDOM:
		b->dir = Vector3(Math::random(-1.0f, 1.0f), Math::random(-1.0f, 1.0f),
				Math::random(-1.0f, 1.0f));
		break;
	}

	switch ( domain_plane ) {
		case Boid::DomainPlanes::DOMAIN_PLANES_FLOOR:
			b->dir[1] = 0;
			break;
		default:
			break;
	}

	// set
	b->active = true;
	b->collision = true;
	b->group = src->group;

	b->local_pos = b->domain_pos + domain_offset;
	b->previous_pos = b->local_pos;
	b->motion *= 0;
	b->dir.normalize();
	b->front = b->dir;

	b->behaviour = &src->behaviour;
	sync_boid_tests(b);
	behave( b );

}

void BoidSystem::kill_boid(Boid::boid *b) {
	b->behaviour = NULL;
	sync_boid_tests(b);
	b->control = NULL;
	b->active = false;
	b->cellid = cell_num;
}

void BoidSystem::init_domain() {

	uint32_t total_cells = domain_x * domain_y * domain_z; // @suppress("Type cannot be resolved")
	domain_size = Vector3(domain_x, domain_y, domain_z) * cell_size;
	Vector3 po = domain_offset;
	domain_offset = domain_size * -.5;
	Vector3 offset_delta = po - domain_offset;

	push_request(boid_UPDATE_SHADER);

	if (total_cells == cell_num) {
		return;
	}

	clear_domain();

	cell_num = total_cells;
	domain = new Boid::boid*[cell_num];
	domain_cells = new int[cell_num];
	domain_dim = new int[3];
	domain_dim[0] = domain_x;
	domain_dim[1] = domain_y;
	domain_dim[2] = domain_z;

	// moving all boids already running
	if (boids != 0) {
		for (int i = 0; i < boid_amount; ++i) {
			boids[i]->domain_pos += offset_delta;
		}
	}

}

Boid::boid* BoidSystem::get_inactive_boid( Boid::boid_control_data* ctrl ) {
	for (int i = 0; i < boid_amount; ++i) {
		Boid::boid* b = boids[i];
		// not the right mesh -> skipping the boid
		if ( b->lock || b->owner != ctrl->mesh ) {
			continue;
		}
		if (!b->active) {
			return b;
		}
	}
	return 0;
}

void BoidSystem::reset_boid_groups() {
	for (int i = 0; i < boid_GROUP_COUNT; ++i) {
		boid_groups[i].center = v3_zero;
		boid_groups[i].count = 0;
	}
}

void BoidSystem::reset_boid_sets() {
	for (int i = 0, imax = boid_sets.size(); i < imax; ++i) {
		boid_sets[i]->active = 0;
	}
}

void BoidSystem::increment_set( Boid::boid* b ) {
	if ( boid_meshset.has( b->owner ) ) {
		Boid::boid_set* s = boid_meshset[ b->owner ];
		if ( s->active == 0 ) {
			s->min = b->local_pos - v3_one*b->radius;
			s->max = b->local_pos + v3_one*b->radius;
		}
		s->active++;
		for ( int i = 0; i < 3; ++i ) {
			if ( s->min[i] > b->local_pos[i] - b->radius ) {
				s->min[i] = b->local_pos[i] - b->radius;
			}
			if ( s->max[i] < b->local_pos[i] + b->radius ) {
				s->max[i] = b->local_pos[i] + b->radius;
			}
		}
	}
}

void BoidSystem::render_boid_groups() {
	for (int i = 0; i < boid_GROUP_COUNT; ++i) {
		if (boid_groups[i].count != 0) {
			boid_groups[i].center /= boid_groups[i].count;
		}
	}
}

void BoidSystem::update_boid_groups( Boid::boid* b ) {
	uint32_t k = 0; // @suppress("Type cannot be resolved")
	for ( uint32_t i = 0; i < boid_GROUP_COUNT; ++i ) { // @suppress("Type cannot be resolved")
		k = 1 << i;
		if ( b->group == k || (k & b->group) != 0 ) {
			boid_groups[i].center += b->domain_pos; // @suppress("Field cannot be resolved")
			boid_groups[i].count++; // @suppress("Field cannot be resolved")
		}
	}
}

void BoidSystem::apply_domain_plane(int start, int end) {

	if (start < 0) {
		start = 0;
	}
	if (end == -1) {
		end = boid_amount;
	} else if (end > boid_amount) {
		end = boid_amount;
	} else if (end <= start) {
		return;
	}
	for (int i = start; i < end; ++i) {
		boids[i]->plane = domain_plane;
	}

}

void BoidSystem::update_sources() {

	float delta = get_process_delta_time() * speed;
	if (delta == 0) {
		return;
	}

	// initialising boid requests
	std::map<void*, boid_sourcing> sourcing;
	for (int i = 0, imax = boid_sets.size(); i < imax; ++i) {
		sourcing[ boid_sets[i]->owner ].available = boid_sets[i]->count - boid_sets[i]->active;
		sourcing[ boid_sets[i]->owner ].requested = 0;
	}

	// storing requests
	for (int i = 0; i < sources.count; ++i) {
		Boid::boid_control_data *source = sources.list[i];
		if (!source->active || source->mesh == NULL) {
			continue;
		}
		source->elapsed_time += source->emission_rate * delta;
		// how many boids should be generated
		int req = int( source->elapsed_time );
		source->elapsed_time -= req;
		req = std::min( int(source->emission_rate), req );
		sourcing[ source->mesh ].requested += req;
		sourcing[ source->mesh ].source_req[ source ] = req;
	}

	// checking if there is enough boids in sets <> sources requests
	for (int i = 0, imax = boid_sets.size(); i < imax; ++i) {
		boid_sourcing& bs = sourcing[ boid_sets[i]->owner ];
		std::map<Boid::boid_control_data*, int>::iterator it;
		std::map<Boid::boid_control_data*, int>::iterator ite;
		it = bs.source_req.begin();
		ite = bs.source_req.end();
		for (; it != ite; ++it) {
			int emit = it->second;
			// weighting the requests
			if ( bs.requested > bs.available ) {
				emit = int( emit * bs.available / float(bs.requested) );
			}
			for ( int j = 0; j < emit; ++j ) {
				Boid::boid *b = get_inactive_boid( it->first );
				if ( !b ) {
					break;
				}
				source_boid(it->first, b);
			}
		}
	}

}

void BoidSystem::constrain_boid(Boid::boid *b) {

	if (domain_border == Boid::DomainBorder::BORDER_NONE) {
		return;
	}

	switch (domain_border) {
	case Boid::DomainBorder::BORDER_BOUNCE:
		for (int g = 0; g < 3; ++g) {
			if (b->domain_pos[g] < 0) {
				if (b->domain_pos[g] < -cell_size) {
					b->domain_pos[g] = 0;
				} else {
					b->domain_pos[g] *= -1;
				}
				if (b->front[g] < 0) {
					b->front[g] *= -1;
				}
				if (b->dir[g] < 0) {
					b->dir[g] *= -1;
				}
			} else if (b->domain_pos[g] > domain_size[g]) {
				if (b->domain_pos[g] > domain_size[g] + cell_size) {
					b->domain_pos[g] = domain_size[g];
				} else {
					b->domain_pos[g] = domain_size[g] - (b->domain_pos[g] - domain_size[g]);
				}
				if (b->front[g] > 0) {
					b->front[g] *= -1;
				}
				if (b->dir[g] > 0) {
					b->dir[g] *= -1;
				}
			}
		}
		b->cellid = cell_num;
		break;
	case Boid::DomainBorder::BORDER_PUSH:
		// second part of the border management
		for (int g = 0; g < 3; ++g) {
			if (b->domain_pos[g] < 0) {
				b->domain_pos[g] = 0;
			} else if (b->domain_pos[g] > domain_size[g]) {
				b->domain_pos[g] = domain_size[g];
				b->cellid = cell_num; // will fall outside...
			}
		}
		break;
	case Boid::DomainBorder::BORDER_ATTRACT:
		for (int g = 0; g < 3; ++g) {
			if (b->domain_pos[g] < 0) {
				b->dir[g] += cell_size;
			} else if (b->domain_pos[g] > domain_size[g]) {
				b->dir[g] -= cell_size;
			}
		}
		b->cellid = cell_num;
		break;
	case Boid::DomainBorder::BORDER_RESPAWN:
		spawn_boid(b);
		for (int g = 0; g < 3; ++g) {
			b->index[g] = int(b->domain_pos[g] / cell_size);
		}
		break;
	case Boid::DomainBorder::BORDER_TELEPORT:
		for (int g = 0; g < 3; ++g) {
			while (b->domain_pos[g] < 0) {
				b->domain_pos[g] += domain_size[g];
			}
			while (b->domain_pos[g] >= domain_size[g]) {
				b->domain_pos[g] -= domain_size[g];
			}
			b->index[g] = int(b->domain_pos[g] / cell_size);
		}
		break;
	case Boid::DomainBorder::BORDER_DEACTIVATE:
	default:
		b->active = false;
		b->cellid = cell_num;
		break;
	}

}

void BoidSystem::update_boids() {

	float delta = get_process_delta_time() * speed;
	if (delta == 0) {
		return;
	}

	Vector3 bmin(0,0,0);
	Vector3 bmax(0,0,0);
	Vector3 pos_diff;
	Vector3 push_norm;
	Vector3 total_motion;
	Vector3 x_axis;
	Vector3 y_axis;
	Vector3 z_axis;
	Vector2 ground_prev_front;
	Vector2 ground_front;
	float floor_y = -domain_offset[1];
	float p_speed;
	float max_speed;
	float push_length;
	float ground_theta;
	float front_motion_theta;
	float ground_theta_diff;
	float ground_mix = std::min( 1.0f, delta * 5 );
	float gm;

	for (int i = 0; i < boid_amount; ++i) {

		Boid::boid* b = boids[i];

		// resetting next
		b->next = NULL;

		// boid has been flagged as locked
		// the only thing to do is update min/max
		// and sets
		if (b->lock) {
			if ( b->active ) {
				for (int g = 0; g < 3; ++g) {
					// checking min & max
					if (i == 0) {
						bmin[g] = b->domain_pos[g] - b->radius;
						bmax[g] = b->domain_pos[g] + b->radius;
					} else {
						if (bmin[g] > b->domain_pos[g] - b->radius) {
							bmin[g] = b->domain_pos[g] - b->radius;
						}
						if (bmax[g] < b->domain_pos[g] + b->radius) {
							bmax[g] = b->domain_pos[g] + b->radius;
						}
					}
				}
				if (b->cellid < cell_num) {
					update_boid_groups( b );
					increment_set( b );
				}
			}
			continue;
		}

		// nothing else if boid is inactive
		if (!b->active) {
			// preventing all events
			b->aiming = false;
			b->timer_end = false;
			b->moving = false;
			b->moving_start = false;
			b->moving_stop = false;
			b->event_enabled = false;
			continue;
		}

		// traction by the target
		if (b->control != NULL && b->control->type == Boid::ControlType::BCTRL_TYPE_TARGET) {
			b->dir +=
					(b->control->local.origin + b->control->local.basis.xform( b->target_offset) - b->domain_pos).normalized() *
					b->control->force * delta;
		}

		// backup > moved at the end of process_boid!
//		b->previous_pos = b->local_pos;
//		if ( b->aiming ) {
//			b->prev_front = b->aim;
//		} else {
//			b->prev_front = b->front;
//		}

		// moving boid
		switch (b->plane) {
		case Boid::DomainPlanes::DOMAIN_PLANES_ALL:
			break;
		case Boid::DomainPlanes::DOMAIN_PLANES_FLOOR:
			b->dir[1] = 0;
			if (b->domain_pos[1] <= floor_y) {
				b->touch_ground = true;
			}
			break;
		}

		// steering towards direction
		b->dir.normalize();
		b->front = b->front.move_toward( b->dir, std::min(1.0f, b->steer * delta));
		b->front.normalize();

		// apply gravitational forces
		if ( !b->touch_ground && b->weight != 0 ) {
			b->domain_pos += delta * gravity;
		}

		// adjusting front speed depending on inertia
		if ( b->inertia <= 1e-5 ) {
			b->internal_speed = b->speed;
		} else if ( b->internal_speed < b->speed ) {
			b->internal_speed = std::min( b->speed, b->internal_speed + (1/b->inertia) * delta );
		} else if ( b->internal_speed > b->speed ) {
			b->internal_speed = std::max( b->speed, b->internal_speed - (1/b->inertia) * delta );
		}

		// removing friction from push
		if (b->friction > 0) {
			b->push -= b->push * std::min(1.f, b->friction * delta );
		}

		// adjusting push speed depending on inertia
		push_length = b->push.length();
		if ( push_length > 1 ) {
			b->push /= push_length;
			push_length = 1;
		}
		p_speed = push_length > 1e-5 ? b->push_speed : 0;
		if ( b->inertia <= 1e-5 ) {
			b->internal_push_speed = p_speed;
		} else if ( b->internal_push_speed < p_speed ) {
			b->internal_push_speed = std::min( p_speed, b->internal_push_speed + (1/b->inertia) * delta );
		} else if ( b->internal_push_speed > p_speed ) {
			b->internal_push_speed = std::max( p_speed, b->internal_push_speed - (1/b->inertia) * delta );
		}

		// moving forward
		total_motion =
				b->push * std::min(1.0f, delta * b->internal_push_speed ) +
				b->front * std::min(1.0f, delta * b->internal_speed );
		// not too fast!
		max_speed = std::max( b->internal_speed, b->internal_push_speed );
		if ( total_motion.length_squared() > max_speed * max_speed * delta ) {
			total_motion = total_motion.normalized() * max_speed * delta;
		}
		b->domain_pos += total_motion;

		// removing consumed force from push
		b->push -= b->push * std::min(1.0f, delta * b->internal_push_speed );

		// teleport above the ground
		switch (b->plane) {
			case Boid::DomainPlanes::DOMAIN_PLANES_ALL:
				break;
			case Boid::DomainPlanes::DOMAIN_PLANES_FLOOR:
				if (b->domain_pos[1] < floor_y) {
					b->domain_pos[1] = floor_y;
				}
				break;
		}

		// position in domain
		b->cellid = 0;
		bool outside = false;

		for (int g = 0; g < 3; ++g) {

			// reaction on
			if (domain_border == Boid::DomainBorder::BORDER_PUSH) {
				if (b->domain_pos[g] < domain_border_radius) {
					float r = Math::abs(
							(b->domain_pos[g] - domain_border_radius)
									/ domain_border_radius);
					b->dir[g] += r * 2;
					b->front[g] += Math::pow(r, 2);
				} else if (b->domain_pos[g]
						> domain_size[g] - domain_border_radius) {
					float r = Math::abs(
							(b->domain_pos[g]
									- (domain_size[g] - domain_border_radius))
									/ domain_border_radius);
					b->dir[g] -= r * 2;
					b->front[g] -= Math::pow(r, 2);
				}
			}
			if (b->domain_pos[g] < 0 || b->domain_pos[g] >= domain_size[g]) {
				outside = true;
			} else {
				// render grid index
				b->index[g] = int(b->domain_pos[g] / cell_size);
			}

			// checking min & max
			if (i == 0) {
				bmin[g] = b->domain_pos[g] - b->radius;
				bmax[g] = b->domain_pos[g] + b->radius;
			} else {
				if (bmin[g] > b->domain_pos[g] - b->radius) {
					bmin[g] = b->domain_pos[g] - b->radius;
				}
				if (bmax[g] < b->domain_pos[g] + b->radius) {
					bmax[g] = b->domain_pos[g] + b->radius;
				}
			}

		}

		if (outside) {
			constrain_boid(boids[i]);
		}

		// render cellid
		if (b->cellid == 0) {
			b->cellid = b->index[0] + b->index[1] * domain_x
					+ b->index[2] * domain_x * domain_y;
			if (b->cellid >= cell_num) {
				b->active = false;
				b->cellid = cell_num;
			}

		}

		// it's a valid boid!
		if (b->cellid < cell_num) {

			b->lifetime += delta;
			b->local_pos = b->domain_pos + domain_offset;
			pos_diff = (b->local_pos - b->previous_pos);
			switch (b->plane) {
				case Boid::DomainPlanes::DOMAIN_PLANES_FLOOR:
					pos_diff[1] = 0;
					break;
				default:
					break;
			}
			float molen = pos_diff.length();
			// updating motion: in unit/sec
			b->travel_distance += molen;
			b->motion = molen > 0 ? pos_diff/std::max(0.00001f, delta) : pos_diff;

			update_boid_groups( b );
			increment_set( b );

			// reset aiming
			b->aiming = b->aim_at_group != 0;

			// preparing events
			if ( b->timer > 0 ) {
				b->timer -= delta;
				if ( b->timer <= 0 ) {
					b->timer_end = true;
				}
			} else if (b->timer_end) {
				b->timer_end = false;
			}
			bool was_moving = b->moving;
			b->moving = molen != 0;
			b->moving_start = false;
			b->moving_stop = false;
			if ( was_moving != b->moving ) {
				if ( b->moving ) {
					b->moving_start = true;
				} else {
					b->moving_stop = true;
				}
			}

			// now we can compute the basis for this boid
			{
				if ( b->aiming ) {
					z_axis = b->aim.normalized();
				} else {
					z_axis = b->front.normalized();
				}
				x_axis = Yaxis.cross( z_axis ).normalized();
				if ( z_axis[1] == 1 || z_axis[1] == -1 ) {
					x_axis = Vector3(0,0,-z_axis[1]).cross( z_axis ).normalized();
				}
				y_axis = z_axis.cross( x_axis ).normalized();
				x_axis = b->mat3x3[1].cross( z_axis ).normalized();
				b->mat3x3.set_axis( 0, x_axis );
				b->mat3x3.set_axis( 1, y_axis );
				b->mat3x3.set_axis( 2, z_axis );
			}

			// updating ground rotation
			switch (b->plane) {
				case Boid::DomainPlanes::DOMAIN_PLANES_FLOOR:
					{
						ground_prev_front.x = b->prev_front.z;
						ground_prev_front.y = b->prev_front.x;
						if (b->aiming) {
							ground_front.x = b->aim.z;
							ground_front.y = b->aim.x;
						} else {
							ground_front.x = b->front.z;
							ground_front.y = b->front.x;
						}
						// normalisation
						ground_prev_front /= ground_prev_front.length();
						ground_front /= ground_front.length();
						ground_theta = ground_prev_front.angle_to(ground_front)/std::max(0.00001f, delta);
						// constrain rotation in range [ -PI/2, PI/2 ]
						gm = std::min( 1.0f, float(Math::abs( ground_theta ) / Math_PI) );
						ground_theta -= ground_theta > 0.0 ? Math_PI*gm*gm : Math_PI*-1.0*gm*gm;
						// front <> motion
						if ( molen > 0 ) {
							ground_prev_front.x = b->motion.z;
							ground_prev_front.y = b->motion.x;
							// instantaneous => no division by deltatime
							front_motion_theta = ground_front.angle_to(ground_prev_front);
							if ( Math::abs(ground_theta) < Math::abs(front_motion_theta) ) {
								ground_theta = front_motion_theta;
							}
						}
						// smoothing
						ground_theta_diff = Math::abs(ground_theta - b->ground_rotation);
						if ( ground_theta_diff > Math_PI ) {
							ground_theta = fmodf( ground_theta + Math_TAU, Math_PI );
						}
// TODO: avoid glitches by limiting maximum rotation delta per call
						b->ground_rotation = b->ground_rotation * (1-ground_mix) + ground_theta * ground_mix;
					}
					break;
				default:
					b->ground_rotation = 0;
					break;
			}

		}

	}

	boids_aabb = AABB(bmin, bmax - bmin);

}

void BoidSystem::link_in_cell(Boid::boid *b) {

	// getting first boid of the cell
	Boid::boid *src = domain[b->cellid];
	// empty cell? -> storing the boid
	if (src == NULL) {
		domain[b->cellid] = b;
	} else {
		// already a boid in this cell? -> going to next boid
		// until next is NULL
		while (src->next != NULL) {
			src = src->next;
		}
		src->next = b;
	}
	domain_cells[b->cellid]++;

}

void BoidSystem::link_domain() {

	// resetting all cells
	for (uint32_t c = 0; c < cell_num; ++c) { // @suppress("Type cannot be resolved")
		domain[c] = NULL;
		domain_cells[c] = 0;
	}
	// placing modifier boids in the grid
	for (int i = 0; i < static_boids_amount; ++i) {
		Boid::boid *b = static_boids[i];
		b->next = NULL;
		if (!b->control->active) {
			continue;
		}
		link_in_cell(b);
	}
	// placing standard boids in the grid
	for (int i = 0; i < boid_amount; ++i) {
		Boid::boid* b = boids[i];
		if (!b->active || b->cellid == cell_num) {
			continue;
		}
		link_in_cell(boids[i]);
	}

}

void BoidSystem::boid_collision(Boid::boid *a, Boid::boid *b, const Vector3 &dist) {


	if (!a->active || !b->active) {
		return;
	}

	float l = std::max(0.01f, dist.length());
	float minl = a->radius + b->radius;
	// pushing each other
	if (l < minl) {
		collision_count++;
		float wt = a->weight + b->weight;
		float force = minl - l;
		if ( b->weight != 0 ) {
			a->push += dist / l * (b->weight / wt) * force;
		}
		if ( a->weight != 0 ) {
			b->push -= dist / l * (a->weight / wt) * force;
		}
		a->dir += dist * force;
		b->dir -= dist * force;
	}
	// avoiding collision -> only if cross product is negative (facing each other)
	float c = std::max(0.f, a->front.dot(b->front) * -1.f);
	if ( c == 0 ) {
		// division by 0!
		return;
	}
	a->dir += std::min(1.0f, a->sac_weights[1]) * dist / l * c * c;
	b->dir -= std::min(1.0f, b->sac_weights[1]) * dist / l * c * c;

}

bool BoidSystem::boid_inside_modifier(Boid::boid *b, Boid::boid_control_data *mod) {


	if (b->type != Boid::Type::BTYPE_BOID || mod == NULL) {
		return false;
	}

	switch (mod->shape) {
	case Boid::ControlShape::BCTRL_SHAPE_POINT:
		return false;
	case Boid::ControlShape::BCTRL_SHAPE_SPHERE: {
		Vector3 dist = mod->local_inv.xform(b->domain_pos);
		if (dist.length_squared() < mod->dimension[0] * mod->dimension[0]) {
			collision_count++;
			return true;
		}
	}
		return false;
	case Boid::ControlShape::BCTRL_SHAPE_BOX: {
		Vector3 loc = mod->local_inv.xform(b->domain_pos);
		if (loc[0] < -mod->dimension[0] || loc[0] > mod->dimension[0]) {
			return false;
		}
		if (loc[1] < -mod->dimension[1] || loc[1] > mod->dimension[1]) {
			return false;
		}
		if (loc[2] < -mod->dimension[2] || loc[2] > mod->dimension[2]) {
			return false;
		}
		collision_count++;
		return true;
	}
		return false;
	default:
		break;
	}

	return false;

}

void BoidSystem::process_boids_attractor(Boid::boid *b, Boid::boid_control_data *bmd,
		const float &delta) {

	bool inside = false;
	float mult = 0;

	switch (bmd->shape) {
		case Boid::ControlShape::BCTRL_SHAPE_POINT:
			inside = true;
			break;
		case Boid::ControlShape::BCTRL_SHAPE_SPHERE:
			inside = boid_inside_modifier(b, bmd);
			if (inside && bmd->falloff != Boid::ControlFalloff::BCTRL_FALLOFF_NONE) {
				mult = (bmd->local.origin - b->domain_pos).length() / bmd->dimension[0];
			}
			break;
		case Boid::ControlShape::BCTRL_SHAPE_BOX:
			inside = boid_inside_modifier(b, bmd);
			if (inside && bmd->falloff != Boid::ControlFalloff::BCTRL_FALLOFF_NONE) {
				float l = bmd->dimension[0];
				if (l < bmd->dimension[1]) {
					l = bmd->dimension[1];
				}
				if (l < bmd->dimension[2]) {
					l = bmd->dimension[2];
				}
				mult = (bmd->local.origin - b->domain_pos).length() / l;
			}
			break;
		default:
			break;
	}

	if (!inside) {
		return;
	}

	if (mult != 0) {
		switch (bmd->falloff) {
		case Boid::Boid::ControlFalloff::BCTRL_FALLOFF_LINEAR:
			mult = 1 - mult;
			break;
		case Boid::Boid::ControlFalloff::BCTRL_FALLOFF_SQRT:
			mult = 1 - mult;
			mult *= mult;
			break;
		case Boid::Boid::ControlFalloff::BCTRL_FALLOFF_SQRTi:
			mult = 1 - (mult * mult);
			break;
		case Boid::Boid::ControlFalloff::BCTRL_FALLOFF_NONE:
		default:
			break;
		}
	} else {
		mult = 1;
	}

	Vector3 d;

	switch (bmd->direction) {
		case Boid::ControlDirection::BCTRL_DIRECTION_RADIAL:
			d = (bmd->local.origin - b->domain_pos).normalized();
			break;
		case Boid::ControlDirection::BCTRL_DIRECTION_X:
			d = bmd->local.basis.xform(Vector3(1, 0, 0)).normalized();
			break;
		case Boid::ControlDirection::BCTRL_DIRECTION_Xi:
			d = bmd->local.basis.xform(Vector3(-1, 0, 0)).normalized();
			break;
		case Boid::ControlDirection::BCTRL_DIRECTION_Y:
			d = bmd->local.basis.xform(Vector3(0, 1, 0)).normalized();
			break;
		case Boid::ControlDirection::BCTRL_DIRECTION_Yi:
			d = bmd->local.basis.xform(Vector3(0, -1, 0)).normalized();
			break;
		case Boid::ControlDirection::BCTRL_DIRECTION_Z:
			d = bmd->local.basis.xform(Vector3(0, 0, 1)).normalized();
			break;
		case Boid::ControlDirection::BCTRL_DIRECTION_Zi:
			d = bmd->local.basis.xform(Vector3(0, 0, -1)).normalized();
			break;
		case Boid::ControlDirection::BCTRL_DIRECTION_RANDOM:
			d = Vector3(Math::random(-1.0f, 1.0f), Math::random(-1.0f, 1.0f),Math::random(-1.0f, 1.0f));
			break;
		default:
			return;
	}

	b->dir += d * bmd->force * mult * delta;
	b->push += d * bmd->pull * mult * delta;

}

void BoidSystem::push_boids_collider(
		Boid::boid* b, Boid::boid_control_data* bmd,
		const Vector3& normal, const float& push_percent,
		const float &delta) {

	float pc = std::min(1.0f, b->steer * delta);

	if (bmd->influence_front) {
		b->front = ( ( b->front * b->weight * (1 - pc) ) + ( normal * bmd->weight * pc ) ).normalized();
	}
	if (bmd->influence_direction) {
		b->dir = ( ( b->dir * b->weight * (1 - pc) ) + ( normal * bmd->weight * pc ) ).normalized();
	}

	b->push += normal * bmd->force * push_percent;

}

void BoidSystem::process_boids_collider(Boid::boid *b, Boid::boid_control_data *bmd, const float &delta) {

	Vector3 normal, pos, radii, margins;

	switch (bmd->shape) {

		case Boid::ControlShape::BCTRL_SHAPE_SPHERE:
		{

			pos = bmd->local_inv.xform(b->domain_pos);
			radii[0] = b->radius / bmd->local.basis.get_scale()[0];
			margins[0] = bmd->margin / bmd->local.basis.get_scale()[0] + radii[0];

			float rad_margin = bmd->dimension[0] + margins[0];
			float rad_short = bmd->dimension[0] + radii[0];
			float pl = pos.length();
			if ( pl > rad_margin ) {
				return;
			}

			normal = (b->domain_pos - bmd->local.origin).normalized();
			push_boids_collider( b, bmd, normal, std::min(1.0f,std::max(0.0f,(pl-bmd->dimension[0])/margins[0])), delta );

			if ( pl > rad_short ) {
				return;
			}

			// collision!!!
			collision_count++;
			b->domain_pos = bmd->local.xform(pos);

		}
			break;

		case Boid::ControlShape::BCTRL_SHAPE_BOX:
		{

			pos = bmd->local_inv.xform(b->domain_pos);
			radii = Vector3( b->radius, b->radius, b->radius ) / bmd->local_inv.basis.get_scale();
			margins = Vector3( bmd->margin, bmd->margin, bmd->margin ) / bmd->local_inv.basis.get_scale() + radii;

			if (pos[0] < -(bmd->dimension[0]+margins[0]) || pos[0] > bmd->dimension[0]+margins[0] ) {
				return;
			}
			if (pos[1] < -(bmd->dimension[1]+margins[1]) || pos[1] > bmd->dimension[1]+margins[1] ) {
				return;
			}
			if (pos[2] < -(bmd->dimension[2]+margins[2]) || pos[2] > bmd->dimension[2]+margins[2] ) {
				return;
			}

			normal = Vector3(1, 0, 0);
			int better_axis = 0;
			if (bmd->dimension[1] - Math::abs(pos[1]) < bmd->dimension[better_axis] - Math::abs(pos[better_axis])) {
				better_axis = 1;
				normal = Vector3(0, 1, 0);
			}
			if (bmd->dimension[2] - Math::abs(pos[2]) < bmd->dimension[better_axis] - Math::abs(pos[better_axis])) {
				better_axis = 2;
				normal = Vector3(0, 0, 1);
			}
			if (pos[better_axis] < 0) {
				normal *= -1;
			}
			float pc = std::min(1.0f, std::max(0.0f, ( Math::abs( pos[better_axis] ) - bmd->dimension[better_axis] ) / margins[better_axis] ));
			// orientation of normal
			normal = bmd->local.basis.xform(normal).normalized();
			push_boids_collider( b, bmd, normal, std::min(1.0f,std::max(0.0f,1.0f-pc)), delta );

			// collisions ?
			if (pos[0] < -(bmd->dimension[0]+radii[0]) || pos[0] > bmd->dimension[0]+radii[0] ) {
				return;
			}
			if (pos[1] < -(bmd->dimension[1]+radii[1]) || pos[1] > bmd->dimension[1]+radii[1] ) {
				return;
			}
			if (pos[2] < -(bmd->dimension[2]+radii[2]) || pos[2] > bmd->dimension[2]+radii[2] ) {
				return;
			}
			// collision!!!
			if (pos[better_axis] < 0) {
				pos[better_axis] = -(bmd->dimension[better_axis]+radii[better_axis]);
			} else {
				pos[better_axis] = (bmd->dimension[better_axis]+radii[better_axis]);
			}
			collision_count++;
			b->domain_pos = bmd->local.xform(pos);

		}
			break;

		default:
			return;
	}

	if (bmd->ground) {
		b->touch_ground = true;
	}

	Vector3 lp = b->local_pos;
	b->local_pos = b->domain_pos + domain_offset;
	// teleport!
	b->previous_pos += b->local_pos - lp;

}

void BoidSystem::process_boids_target(Boid::boid *b, Boid::boid_control_data *bmd) {

	// for targets, test is done only if current modifier is the boid target
	if (b->control != bmd) {
		return;
	}
	// ok! boid is very close from its target
	if (b->control->on_enter != Boid::ControlOnEnter::BCTRL_ONENTER_NOTHING
			&& boid_inside_modifier(b, b->control)) {
		switch (b->control->on_enter) {
		case Boid::ControlOnEnter::BCTRL_ONENTER_FREE:
			b->control = NULL;
			break;
		case Boid::ControlOnEnter::BCTRL_ONENTER_STOP:
			std::cout << "BCTRL_ONENTER_STOP to implement" << std::endl;
			break;
		case Boid::ControlOnEnter::BCTRL_ONENTER_DEACTIVATE:
			kill_boid(b);
			break;
		case Boid::ControlOnEnter::BCTRL_ONENTER_NOTHING:
		default:
			break;
		}
	}

}

void BoidSystem::process_boids_modifier(Boid::boid *b, Boid::boid_control_data *mod, const float &delta) {

	if (b->type != Boid::Type::BTYPE_BOID || mod == NULL || !mod->active) {
		return;
	}

	switch (mod->type) {
		case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
			process_boids_attractor(b, mod, delta);
			break;
		case Boid::ControlType::BCTRL_TYPE_COLLIDER:
			if ( b->collision ) {
				process_boids_collider(b, mod, delta);
			}
			break;
		case Boid::ControlType::BCTRL_TYPE_TARGET:
			process_boids_target(b, mod);
			break;
		case Boid::ControlType::BCTRL_TYPE_SOURCE:
		case Boid::ControlType::BCTRL_TYPE_NONE:
		default:
			break;
	}

}

bool BoidSystem::behave_attr( Boid::boid* b,Boid::boid_behaviour_test* test ) {

	if ( test->type != Boid::TestType::BTEST_TYPE_ATTRIBUTE ) {
		return false;
	}

	float float_value = 0;
	bool float_test = false;
	int int_value = 0;
	bool int_test = false;

	switch ( test->attr ) {

		case Boid::TestAttribute::BTEST_ATTR_ACTIVE:
			return b->active;
		case Boid::TestAttribute::BTEST_ATTR_COLLISION:
			return b->collision;
		case Boid::TestAttribute::BTEST_ATTR_MOVING:
			return b->moving;

		case Boid::TestAttribute::BTEST_ATTR_GROUP:
			switch ( test->eval ) {
				case Boid::TestEval::BTEST_EVAL_BITWISE_AND:
					return ( ( b->group & test->param_int.value ) != 0 );
				case Boid::TestEval::BTEST_EVAL_BITWISE_XOR:
					std::cout << "BoidSystem::behave_attr, BTEST_EVAL_LOGICAL_XOR" << std::endl;
					return ( ( b->group ^ test->param_int.value ) != 0 );
				case Boid::TestEval::BTEST_EVAL_NONE:
				case Boid::TestEval::BTEST_EVAL_EQUAL:
				case Boid::TestEval::BTEST_EVAL_NOT_EQUAL:
				case Boid::TestEval::BTEST_EVAL_GREATER:
				case Boid::TestEval::BTEST_EVAL_GREATER_EQUAL:
				case Boid::TestEval::BTEST_EVAL_LESSER:
				case Boid::TestEval::BTEST_EVAL_LESSER_EQUAL:
				case Boid::TestEval::BTEST_EVAL_MODULO:
				default:
					break;
			}
			return false;

		case Boid::TestAttribute::BTEST_ATTR_ANIMATION:
			switch ( test->eval ) {
				case Boid::TestEval::BTEST_EVAL_BITWISE_AND:
					return ( ( b->animation & test->param_int.value ) != 0 );
				case Boid::TestEval::BTEST_EVAL_BITWISE_XOR:
					std::cout << "BoidSystem::behave_attr, BTEST_EVAL_LOGICAL_XOR" << std::endl;
					return ( ( b->animation ^ test->param_int.value ) != 0 );
				case Boid::TestEval::BTEST_EVAL_NONE:
				case Boid::TestEval::BTEST_EVAL_EQUAL:
				case Boid::TestEval::BTEST_EVAL_NOT_EQUAL:
				case Boid::TestEval::BTEST_EVAL_GREATER:
				case Boid::TestEval::BTEST_EVAL_GREATER_EQUAL:
				case Boid::TestEval::BTEST_EVAL_LESSER:
				case Boid::TestEval::BTEST_EVAL_LESSER_EQUAL:
				case Boid::TestEval::BTEST_EVAL_MODULO:
				default:
					break;
			}
			return false;

		case Boid::TestAttribute::BTEST_ATTR_SPEED:
			float_value = b->speed;
			float_test = true;
			break;

		case Boid::TestAttribute::BTEST_ATTR_PUSH_SPEED:
			float_value = b->push_speed;
			float_test = true;
			break;

		case Boid::TestAttribute::BTEST_ATTR_WEIGHT:
			float_value = b->weight;
			float_test = true;
			break;

		case Boid::TestAttribute::BTEST_ATTR_RADIUS:
			float_value = b->radius;
			float_test = true;
			break;

		case Boid::TestAttribute::BTEST_ATTR_PUSH:
			float_value = b->push.length();
			float_test = true;
			break;

		case Boid::TestAttribute::BTEST_ATTR_COUNTER:
			float_value = b->counter;
			float_test = true;
			break;

		case Boid::TestAttribute::BTEST_ATTR_TIMER:
			float_value = b->timer;
			float_test = true;
			break;

		case Boid::TestAttribute::BTEST_ATTR_LIFETIME:
			float_value = b->lifetime;
			float_test = true;
			break;

		case Boid::TestAttribute::BTEST_ATTR_NONE:
		default:
			return false;

	}

	if ( float_test ) {
		return behave_eval_float( float_value, test );
	} else if ( int_test ) {
		return behave_eval_int( int_value, test );
	}

	return false;

}

bool BoidSystem::behave_eval_int( const int& i, Boid::boid_behaviour_test* test ) {
	switch( test->eval ) {
		case Boid::TestEval::BTEST_EVAL_EQUAL:
			return i == test->param_int.value;
		case Boid::TestEval::BTEST_EVAL_NOT_EQUAL:
			return i != test->param_int.value;
		case Boid::TestEval::BTEST_EVAL_GREATER:
			return i > test->param_int.value;
		case Boid::TestEval::BTEST_EVAL_GREATER_EQUAL:
			return i >= test->param_int.value;
		case Boid::TestEval::BTEST_EVAL_LESSER:
			return i < test->param_int.value;
		case Boid::TestEval::BTEST_EVAL_LESSER_EQUAL:
			return i <= test->param_int.value;
		case Boid::TestEval::BTEST_EVAL_MODULO:
			return (i % test->param_int.value) == 0;
		case Boid::TestEval::BTEST_EVAL_NONE:
		case Boid::TestEval::BTEST_EVAL_BITWISE_AND:
		case Boid::TestEval::BTEST_EVAL_BITWISE_XOR:
		default:
			return false;
	}
	return false;
}

bool BoidSystem::behave_eval_float( const float& f, Boid::boid_behaviour_test* test ) {
	switch( test->eval ) {
		case Boid::TestEval::BTEST_EVAL_EQUAL:
			return f == test->param_float.value;
		case Boid::TestEval::BTEST_EVAL_NOT_EQUAL:
			return f != test->param_float.value;
		case Boid::TestEval::BTEST_EVAL_GREATER:
			return f > test->param_float.value;
		case Boid::TestEval::BTEST_EVAL_GREATER_EQUAL:
			return f >= test->param_float.value;
		case Boid::TestEval::BTEST_EVAL_LESSER:
			return f < test->param_float.value;
		case Boid::TestEval::BTEST_EVAL_LESSER_EQUAL:
			return f <= test->param_float.value;
		case Boid::TestEval::BTEST_EVAL_NONE:
		case Boid::TestEval::BTEST_EVAL_MODULO:
		case Boid::TestEval::BTEST_EVAL_BITWISE_AND:
		case Boid::TestEval::BTEST_EVAL_BITWISE_XOR:
		default:
			return false;
	}
	return false;
}

bool BoidSystem::behave_neighborhoud( const Boid::boid* b, const int& group ) {

	if ( neighbor_count == 0 ) {
		return false;
	}
	for ( int i = 0 ; i < neighbor_count; ++i ) {
		Boid::boid* n = neighbors[i];
		if ( (n->group & group) != 0 ) {
			Vector3 dist = b->domain_pos - n->domain_pos;
			if (dist.length_squared() / cell_size_pow2 <= 1) {
				return true;
			}
		}
	}
	return false;

}

bool BoidSystem::behave_inside( Boid::boid* b, Boid::boid_control_data* ctrl ) {

	if ( ctrl == NULL ) {
		return false;
	}
	if ( !ctrl->active ) {
		return false;
	}

	switch (ctrl->type) {
		case Boid::ControlType::BCTRL_TYPE_COLLIDER:
			if ( b->collision ) {
				return boid_inside_modifier(b, ctrl);
			}
			return false;
		case Boid::ControlType::BCTRL_TYPE_TARGET:
			return boid_inside_modifier(b, ctrl);
		case Boid::ControlType::BCTRL_TYPE_SOURCE:
		case Boid::ControlType::BCTRL_TYPE_NONE:
		default:
			break;
	}

	return false;

}

void BoidSystem::behave_action( Boid::boid* b, const Boid::boid_behaviour_action* action, const float& delta ) {

	if ( !action->active || !action->valid ) {
		return;
	}

	switch( action->type ) {
		case Boid::ActionType::BACTION_TYPE_SET_ATTRIBUTE:
			switch ( action->attr ) {
				case Boid::ActionAttribute::BACTION_ATTR_ACTIVE:
					b->active = action->param_bool;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_COLLISION:
					b->collision = action->param_bool;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_GROUP:
					b->group = action->param_int.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
					b->animation = action->param_int.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
					b->sac_weights[0] = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
					b->sac_weights[1] = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_COHESION:
					b->sac_weights[2] = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_STEER:
					b->steer = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_SPEED:
					b->speed = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_PUSH_SPEED:
					b->push_speed = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
					b->inertia = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
					b->weight = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
					b->radius = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_FRICTION:
					b->friction = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_COLOR:
					b->color = action->param_vec4.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_AIM:
					b->aim = action->param_vec3.value;
					b->aiming = true;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
					b->prev_counter = b->counter;
					b->counter = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_TIMER:
					b->timer = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_LIFETIME:
					b->lifetime = action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_POSITION:
					b->domain_pos = action->param_vec3.value - domain_offset;;
					b->local_pos = action->param_vec3.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
					b->dir = action->param_vec3.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_FRONT:
					b->front = action->param_vec3.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_PUSH:
					b->push = action->param_vec3.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_NONE:
				default:
					break;
			}
			break;
		case Boid::ActionType::BACTION_TYPE_INCREMENT_ATTRIBUTE:
			switch ( action->attr ) {
				case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
					b->sac_weights[0] += action->param_float.value*delta;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
					b->sac_weights[1] += action->param_float.value*delta;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_COHESION:
					b->sac_weights[2] += action->param_float.value*delta;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_STEER:
					b->steer += action->param_float.value*delta;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_SPEED:
					b->speed += action->param_float.value*delta;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
					b->inertia += action->param_float.value*delta;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
					b->weight += action->param_float.value*delta;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
					b->radius += action->param_float.value*delta;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
					b->prev_counter = b->counter;
					b->counter += action->param_float.value;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_TIMER:
					b->timer += action->param_float.value*delta;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_LIFETIME:
					b->lifetime += action->param_float.value*delta;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_NONE:
				case Boid::ActionAttribute::BACTION_ATTR_ACTIVE:
				case Boid::ActionAttribute::BACTION_ATTR_COLLISION:
				case Boid::ActionAttribute::BACTION_ATTR_GROUP:
				case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
				case Boid::ActionAttribute::BACTION_ATTR_COLOR:
				case Boid::ActionAttribute::BACTION_ATTR_AIM:
				case Boid::ActionAttribute::BACTION_ATTR_POSITION:
				case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
				case Boid::ActionAttribute::BACTION_ATTR_FRONT:
				case Boid::ActionAttribute::BACTION_ATTR_PUSH:
				default:
					break;
			}
			break;
		case Boid::ActionType::BACTION_TYPE_RANDOMISE_ATTRIBUTE:
			switch ( action->attr ) {
				case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
					b->sac_weights[0] = action->param_float.min + (action->param_float.max-action->param_float.min) * Math::randf();
					break;
				case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
					b->sac_weights[1] = action->param_float.min + (action->param_float.max-action->param_float.min) * Math::randf();
					break;
				case Boid::ActionAttribute::BACTION_ATTR_COHESION:
					b->sac_weights[2] = action->param_float.min + (action->param_float.max-action->param_float.min) * Math::randf();
					break;
				case Boid::ActionAttribute::BACTION_ATTR_STEER:
					b->steer = action->param_float.min + (action->param_float.max-action->param_float.min) * Math::randf();
					break;
				case Boid::ActionAttribute::BACTION_ATTR_SPEED:
					b->speed = action->param_float.min + (action->param_float.max-action->param_float.min) * Math::randf();
					break;
				case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
					b->inertia = action->param_float.min + (action->param_float.max-action->param_float.min) * Math::randf();
					break;
				case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
					b->weight = action->param_float.min + (action->param_float.max-action->param_float.min) * Math::randf();
					break;
				case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
					b->radius = action->param_float.min + (action->param_float.max-action->param_float.min) * Math::randf();
					break;
				case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
					b->prev_counter = b->counter;
					b->counter = action->param_float.min + (action->param_int.max-action->param_int.min) * Math::randf();
					break;
				case Boid::ActionAttribute::BACTION_ATTR_TIMER:
					b->timer = action->param_float.min + (action->param_float.max-action->param_float.min) * Math::randf();
					break;
				case Boid::ActionAttribute::BACTION_ATTR_POSITION:
					for ( int i = 0; i < 3; ++i ) {
						b->local_pos[i] = action->param_vec3.min[i] + (action->param_vec3.max[i]-action->param_vec3.min[i]) * Math::randf();
					}
					b->domain_pos = b->local_pos - domain_offset;
					break;
				case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
					for ( int i = 0; i < 3; ++i ) {
						b->dir[i] = action->param_vec3.min[i] + (action->param_vec3.max[i]-action->param_vec3.min[i]) * Math::randf();
					}
					b->dir.normalize();
					break;
				case Boid::ActionAttribute::BACTION_ATTR_FRONT:
					for ( int i = 0; i < 3; ++i ) {
						b->front[i] = action->param_vec3.min[i] + (action->param_vec3.max[i]-action->param_vec3.min[i]) * Math::randf();
					}
					b->front.normalize();
					break;
				case Boid::ActionAttribute::BACTION_ATTR_PUSH:
					for ( int i = 0; i < 3; ++i ) {
						b->push[i] = action->param_vec3.min[i] + (action->param_vec3.max[i]-action->param_vec3.min[i]) * Math::randf();
					}
					break;
				case Boid::ActionAttribute::BACTION_ATTR_COLOR:
					for ( int i = 0; i < 4; ++i ) {
						b->color[i] = action->param_vec4.min[i] + (action->param_vec4.max[i]-action->param_vec4.min[i]) * Math::randf();
					}
					break;
				case Boid::ActionAttribute::BACTION_ATTR_AIM:
					b->aiming = true;
					for ( int i = 0; i < 3; ++i ) {
						b->aim[i] = action->param_vec3.min[i] + (action->param_vec3.max[i]-action->param_vec3.min[i]) * Math::randf();
					}
					break;
				case Boid::ActionAttribute::BACTION_ATTR_NONE:
				case Boid::ActionAttribute::BACTION_ATTR_ACTIVE:
				case Boid::ActionAttribute::BACTION_ATTR_COLLISION:
				case Boid::ActionAttribute::BACTION_ATTR_GROUP:
				case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
				default:
					break;
			}
			break;
		case Boid::ActionType::BACTION_TYPE_ASSIGN_TARGET:
			b->control = action->param_ctrl.value;
			break;
		case Boid::ActionType::BACTION_TYPE_AIM_AT:
			b->aim_at_group = action->param_int.value;
			b->aim_at_strength = action->param_float.value;
			b->aim_at_group_center = action->param_bool;
			b->aiming = true;
			break;
		case Boid::ActionType::BACTION_TYPE_REACH:
			b->reach_group = action->param_int.value;
			b->reach_strength = action->param_float.value;
			b->reach_group_center = action->param_bool;
			break;
		case Boid::ActionType::BACTION_TYPE_ESCAPE:
			b->escape_group = action->param_int.value;
			b->escape_strength = action->param_float.value;
			b->escape_group_center = action->param_bool;
			break;
		case Boid::ActionType::BACTION_TYPE_COUPLE:
			break;
			b->group = 0;
			b->reach_group = 0;
			b->escape_group = 0;
			break;
		case Boid::ActionType::BACTION_TYPE_SIGNAL:
			b->event_enabled = true;
			b->event = action->param_int.value;
			break;
		case Boid::ActionType::BACTION_TYPE_NONE:
		default:
			break;
	}

}

void BoidSystem::behave( Boid::boid* b ) {

	if ( b->behaviour == NULL || !b->behaviour->active ) {
		return;
	}

	float delta = get_process_delta_time() * speed;
	Boid::boid_behaviour_test* test = b->behaviour->test;
	Boid::boid_behaviour_action* action = NULL;

	bool do_actions = false;
	bool prev_do_actions = false;
	bool skip_next = false; // set to true when AND + test=false
	bool after_or = false;
	int after_goto = -1;
	// random value used for tests, actions have their own
	float rand = Math::randf();
	int test_index = 0;

	while( test != NULL ) {

		do_actions = false;

		if ( !skip_next && !b->tests[test_index] && test->active && test->valid ) {

			switch ( test->type ) {

				case Boid::TestType::BTEST_TYPE_ALWAYS_TRUE:
					do_actions = true;
					break;

				case Boid::TestType::BTEST_TYPE_ATTRIBUTE:
					do_actions = behave_attr( b, test );
					break;

				case Boid::TestType::BTEST_TYPE_RANDOM:
					do_actions = behave_eval_float( rand, test );
					break;

				case Boid::TestType::BTEST_TYPE_NEIGHBORHOUD:
					do_actions = behave_neighborhoud( b, test->param_int.value );
					break;

				case Boid::TestType::BTEST_TYPE_INSIDE:
					do_actions = behave_inside( b, test->param_ctrl.value );
					break;

				case Boid::TestType::BTEST_TYPE_OUTSIDE:
					do_actions = !behave_inside( b, test->param_ctrl.value );
					break;

				case Boid::TestType::BTEST_TYPE_EVENT:
					switch ( test->event ) {

						case Boid::TestEvent::BTEST_EVENT_BORN:
							do_actions = b->lifetime == 0;
							break;

						case Boid::TestEvent::BTEST_EVENT_TIMER_END:
							do_actions = b->timer_end;
							break;

						case Boid::TestEvent::BTEST_EVENT_MOVING_START:
							do_actions = b->moving_start;
							break;

						case Boid::TestEvent::BTEST_EVENT_MOVING_STOP:
							do_actions = b->moving_stop;
							break;

						case Boid::TestEvent::BTEST_EVENT_NONE:
						default:
							do_actions = false;
							break;
					}
					break;

				case Boid::TestType::BTEST_TYPE_NEIGHBOR_COUNT:
					do_actions = behave_eval_int( b->neighbor_count, test );
					break;

				case Boid::TestType::BTEST_TYPE_NONE:
				default:
					break;

			}

			if ( after_or ) {
				do_actions = do_actions || prev_do_actions;
			}

			if (do_actions) {

				action = test->action;
				while ( action != NULL ) {
					behave_action( b, action, delta );
					action = action->next;
				}

				switch ( test->after ) {
					case Boid::TestAfter::BTEST_AFTER_EXIT:
						return;
					case Boid::TestAfter::BTEST_AFTER_GOTO:
						after_goto = test->jumpto;
						break;
					case Boid::TestAfter::BTEST_AFTER_NONE:
					case Boid::TestAfter::BTEST_AFTER_AND:
					case Boid::TestAfter::BTEST_AFTER_OR:
					default:
						after_goto = -1;
						break;
				}

			}

			switch ( test->repeat ) {
				case Boid::TestRepeat::BTEST_REPEAT_ONCE:
					b->tests[test_index] = do_actions;
					break;
				case Boid::TestRepeat::BTEST_REPEAT_CONTINUOUS:
				default:
					break;

			}

			while ( after_goto > 0 ) {

				after_goto--;
				test = test->next;
				test_index++;
				if ( test == NULL ) {
					return;
				}

			}

		}

		switch ( test->after ) {
			case Boid::TestAfter::BTEST_AFTER_AND:
				skip_next = !do_actions;
				break;
			case Boid::TestAfter::BTEST_AFTER_OR:
				after_or = true;
				break;
			case Boid::TestAfter::BTEST_AFTER_NONE:
			case Boid::TestAfter::BTEST_AFTER_EXIT:
			case Boid::TestAfter::BTEST_AFTER_GOTO:
			default:
				skip_next = false;
				after_or = false;
				break;
		}

		prev_do_actions = do_actions;

		test = test->next;
		test_index++;

	}

}

void BoidSystem::process_boids() {

	if (cell_num == 0) {
		return;
	}

	boid_active_amount = 0;

	float delta = get_process_delta_time() * speed;
	if (delta == 0) {
		return;
	}

	int dynmods = dynamic_modifiers.size();

	for (int i = 0; i < boid_amount; ++i) {

		Boid::boid* b = boids[i];

		if ( b->lock ) {
			if ( b->active ) {
				boid_active_amount++;
			}
			continue;
		}

		if (!b->active || b->cellid == cell_num) {
			continue;
		}

		// backup for next update_boids call!
		b->local_pos = b->domain_pos + domain_offset;
		b->previous_pos = b->local_pos;
		if ( b->aiming ) {
			b->prev_front = b->aim;
		} else {
			b->prev_front = b->front;
		}

		// some reset
		b->touch_ground = false;

		int bcount = 0;
		Vector3 separation = Vector3(0, 0, 0);
		Vector3 alignment = Vector3(0, 0, 0);
		Vector3 cohesion = Vector3(0, 0, 0);
		Vector3 aim_at = Vector3(0, 0, 0);
		int aim_at_count = 0;
		Vector3 reach = Vector3(0, 0, 0);
		int reach_count = 0;
		Vector3 escape = Vector3(0, 0, 0);
		int escape_count = 0;

		for (int m = 0; m < dynmods; ++m) {
			// trying to avoid extra calls
			if (!dynamic_modifiers[m]->active || (dynamic_modifiers[m]->group & b->group) == 0) {
				continue;
			}
			process_boids_modifier(b, dynamic_modifiers[m], delta);
			// after this test, boid might be inactive
			if (!b->active) {
				goto NEXT_BOID;
			}
		}

		neighbor_count = 0;

		for (int z = b->index[2] - 1; z < b->index[2] + 2; ++z) {
			if (z < 0 || z >= domain_z) {
				continue;
			}
			for (int y = b->index[1] - 1; y < b->index[1] + 2;
					++y) {
				if (y < 0 || y >= domain_y) {
					continue;
				}
				for (int x = b->index[0] - 1; x < b->index[0] + 2;
						++x) {
					if (x < 0 || x >= domain_x) {
						continue;
					}
					uint32_t cindex = x + y * domain_x // @suppress("Type cannot be resolved")
							+ z * domain_x * domain_y;
					if (cindex >= cell_num) {
						continue;
					}
					Boid::boid *n = domain[cindex];
					while (n != 0) {
						if (n->active && n != b) {
							switch (n->type) {
							case Boid::Type::BTYPE_BOID:
							{
								Vector3 dist = b->domain_pos - n->domain_pos;
								float ratio = (dist.length_squared() / cell_size_pow2);
								if (ratio <= 1) {
									// collisions are group independant
									if ( n->collision && b->collision ) {
										boid_collision(b, n, dist);
									}
									if ( (n->group & b->aim_at_group ) != 0 ) {
										aim_at_count++;
										aim_at += dist * -(1-ratio);
									}
									if ( (n->group & b->reach_group ) != 0 ) {
										reach_count++;
										reach += dist * -(1-ratio);
									}
									if ( (n->group & b->escape_group) != 0 ) {
										escape_count++;
										escape += dist * (1-ratio);
									}
									// to form a flock, the 2 boids must share at least 1 group
									if ( (n->group & b->group) != 0 ) {
										separation += dist / cell_size;
										alignment += n->front;
										cohesion += n->domain_pos;
										bcount++;
									}
								}
								neighbors[neighbor_count++] = n;

							}
								break;
							case Boid::Type::BTYPE_STATIC:
							case Boid::Type::BTYPE_DYNAMIC:
								// testing modifiers belonging to same group
								if ((n->group & b->group) == 0) {
									break;
								}
								process_boids_modifier(b, n->control, delta);
								// after this test, boid might be inactive
								if (!b->active) {
									goto NEXT_BOID;
								}
								break;
							case Boid::Type::BTYPE_NONE:
							default:
								break;
							}

						}
						n = n->next;
					}
				}
			}
		}

		b->neighbor_count = neighbor_count;

		behave(b);
		// after this test, boid might be inactive
		if (!b->active) {
			goto NEXT_BOID;
		}

		switch (b->type) {

			case Boid::Type::BTYPE_BOID:

				if (bcount > 0) {
					separation /= bcount;
					alignment /= bcount;
					// direction to neighbours barycentre
					cohesion /= bcount;
					cohesion -= b->domain_pos;
					cohesion /= cell_size;
					b->dir += separation * b->sac_weights[0];
					b->dir += alignment * b->sac_weights[1];
					b->dir += cohesion * b->sac_weights[2];
				}
				// aim at
				if ( b->aim_at_group != 0 ) {
					b->aim *= 0;
					if ( reach_count == 0 && b->aim_at_group_center ) {
						for ( int g = 0; g < boid_GROUP_COUNT; ++g ) {
							if ( boid_groups[g].count == 0 ) {
								continue;
							}
							if ( (b->aim_at_group & (1<<g)) != 0 ) {
								b->aim += (boid_groups[g].center-b->domain_pos).normalized() * b->aim_at_strength;
							}
						}
					}
					if ( aim_at_count != 0 ) {
						b->aim += aim_at * b->aim_at_strength / aim_at_count;
					}
					b->aim.normalize();
					b->aiming = true;
				}
				// reach
				if ( b->reach_group != 0 ) {
					if ( reach_count == 0 && b->reach_group_center ) {
						for ( int g = 0; g < boid_GROUP_COUNT; ++g ) {
							if ( boid_groups[g].count == 0 ) {
								continue;
							}
							if ( (b->reach_group & (1<<g)) != 0 ) {
								b->dir += (boid_groups[g].center-b->domain_pos).normalized() * b->reach_strength;
							}
						}
					}
					if ( reach_count != 0 ) {
						b->dir += reach * b->reach_strength / reach_count;
					}
				}
				// escape
				if ( b->escape_group != 0 ) {
					if ( escape_count == 0 && b->escape_group_center ) {
						for ( int g = 0; g < boid_GROUP_COUNT; ++g ) {
							if ( boid_groups[g].count == 0 ) {
								continue;
							}
							if ( (b->escape_group & (1<<g)) != 0 ) {
								b->dir += (b->domain_pos-boid_groups[g].center).normalized() * b->escape_strength;
							}
						}
					}
					if ( escape_count != 0 ) {
						b->dir += escape * b->reach_strength / escape_count;
					}
				}
				break;

			case Boid::Type::BTYPE_NONE:
			case Boid::Type::BTYPE_STATIC:
			case Boid::Type::BTYPE_DYNAMIC:
			default:
				break;
		}

		boid_active_amount++;

		NEXT_BOID: continue;

	}

}

void BoidSystem::refresh_domain_debug() {

	if (domain_debug != NULL) {
		domain_debug->clear();
		domain_debug->begin(Mesh::PrimitiveType::PRIMITIVE_LINES);
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(0,1,0));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(1,1,0));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(1,1,0));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(1,1,1));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(1,1,1));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(0,1,1));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(0,1,1));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(0,1,0));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(0,0,0));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(1,0,0));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(1,0,0));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(1,0,1));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(1,0,1));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(0,0,1));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(0,0,1));
		domain_debug->add_vertex( domain_offset + domain_size * Vector3(0,0,0));
		domain_debug->end();
	}

}

void BoidSystem::refresh_cell_debug() {

	if (cell_debug != NULL) {
		cell_debug->clear();
		cell_debug->begin(Mesh::PrimitiveType::PRIMITIVE_LINES);
		for (int z = 1; z < domain_z; ++z) {
			for (int x = 1; x < domain_x; ++x) {
				cell_debug->add_vertex(
						domain_offset + Vector3(x, 0, z) * cell_size);
				cell_debug->add_vertex(
						domain_offset + Vector3(x, domain_y, z) * cell_size);
			}
		}
		for (int z = 1; z < domain_z; ++z) {
			for (int y = 1; y < domain_y; ++y) {
				cell_debug->add_vertex(
						domain_offset + Vector3(0, y, z) * cell_size);
				cell_debug->add_vertex(
						domain_offset + Vector3(domain_x, y, z) * cell_size);
			}
		}
		for (int y = 1; y < domain_y; ++y) {
			for (int x = 1; x < domain_x; ++x) {
				cell_debug->add_vertex(
						domain_offset + Vector3(x, y, 0) * cell_size);
				cell_debug->add_vertex(
						domain_offset + Vector3(x, y, domain_z) * cell_size);
			}
		}
		cell_debug->end();
	}

}

void BoidSystem::refresh_modifier_debug() {

	if (modifier_boid_debug != NULL) {
		modifier_boid_debug->clear();
		modifier_boid_debug->begin(Mesh::PrimitiveType::PRIMITIVE_LINES);
		for (int i = 0; i < static_boids_amount; ++i) {
			Vector3 p = domain_offset + static_boids[i]->domain_pos;
			float s = .15;
			if (!static_boids[i]->active) {
				s = .02;
			}
			modifier_boid_debug->add_vertex(p - Vector3(s, 0, 0) * cell_size);
			modifier_boid_debug->add_vertex(p - Vector3(-s, 0, 0) * cell_size);
			modifier_boid_debug->add_vertex(p - Vector3(0, s, 0) * cell_size);
			modifier_boid_debug->add_vertex(p - Vector3(0, -s, 0) * cell_size);
			modifier_boid_debug->add_vertex(p - Vector3(0, 0, s) * cell_size);
			modifier_boid_debug->add_vertex(p - Vector3(0, 0, -s) * cell_size);
		}
		modifier_boid_debug->end();
	}

}

void BoidSystem::push_request(int p_req) {

	if (update_request == -1) {
		return;
	}
	update_request |= p_req;

}

void BoidSystem::process_system() {

	if (boid_amount <= 0) {
		return;
	}

	reset_boid_groups();
	reset_boid_sets();
	update_boids();
	render_boid_groups();
	link_domain();

	if (sources.count > 0) {
		neighbor_count = 0;
		update_sources();
	}

	process_boids();

}

void BoidSystem::internal_process() {

	internal_process_time = 0;
	collision_count = 0;

	if (update_request == -1) {
		return;
	}

	uint64_t start = OS::get_singleton()->get_system_time_msecs(); // @suppress("Type cannot be resolved")

	if (update_request != 0) {
		if ((update_request & boid_DISPLAY_DOMAIN) && domain_debug == NULL) {
			Ref<SpatialMaterial> mat;
			mat.instance();
			mat->set_feature(SpatialMaterial::Feature::FEATURE_TRANSPARENT,
					true);
			mat->set_flag(SpatialMaterial::Flags::FLAG_UNSHADED, true);
			mat->set_flag(SpatialMaterial::Flags::FLAG_DONT_RECEIVE_SHADOWS,
					true);
			mat->set_flag(SpatialMaterial::Flags::FLAG_DISABLE_AMBIENT_LIGHT,
					true);
			mat->set_albedo(Color(1, 0, 1, .3));
			domain_debug = new ImmediateGeometry(); // @suppress("Abstract class cannot be instantiated")
			domain_debug->set_material_override(mat);
			add_child(domain_debug);
		}
		if ((update_request & boid_DISPLAY_CELL) && cell_debug == NULL) {
			Ref<SpatialMaterial> mat;
			mat.instance();
			mat->set_feature(SpatialMaterial::Feature::FEATURE_TRANSPARENT,
					true);
			mat->set_flag(SpatialMaterial::Flags::FLAG_UNSHADED, true);
			mat->set_flag(SpatialMaterial::Flags::FLAG_DONT_RECEIVE_SHADOWS,
					true);
			mat->set_flag(SpatialMaterial::Flags::FLAG_DISABLE_AMBIENT_LIGHT,
					true);
			mat->set_albedo(Color(1, 0, 1, .3));
			cell_debug = new ImmediateGeometry(); // @suppress("Abstract class cannot be instantiated")
			cell_debug->set_material_override(mat);
			add_child(cell_debug);
		}
		if ((update_request & boid_DISPLAY_MODIFIER_BOIDS) && modifier_boid_debug == NULL) {
			Ref<SpatialMaterial> mat;
			mat.instance();
			mat->set_feature(SpatialMaterial::Feature::FEATURE_TRANSPARENT,
					true);
			mat->set_flag(SpatialMaterial::Flags::FLAG_UNSHADED, true);
			mat->set_flag(SpatialMaterial::Flags::FLAG_DONT_RECEIVE_SHADOWS,
					true);
			mat->set_flag(SpatialMaterial::Flags::FLAG_DISABLE_AMBIENT_LIGHT,
					true);
			mat->set_albedo(Color(0, 1, 1, .9));
			modifier_boid_debug = new ImmediateGeometry(); // @suppress("Abstract class cannot be instantiated")
			modifier_boid_debug->set_material_override(mat);
			add_child(modifier_boid_debug);
		}
		if (update_request & boid_INIT_DOMAIN) {
			init_domain();
			if (is_inside_tree()) {
				position_control(NULL);
			}
		}
		if (update_request & boid_REFRESH_DOMAIN) {
			refresh_domain_debug();
		}
		if (update_request & boid_REFRESH_CELL) {
			refresh_cell_debug();
		}
		if (update_request & boid_UPDATE_DOMAIN_PLANE) {
			apply_domain_plane();
		}
		if (update_request & boid_REGENERATE_MODIFIER_BOIDS) {
			regenerate_control_boids();
		}
		if (update_request & boid_REFRESH_MODIFIER_BOIDS_DEBUG) {
			refresh_modifier_debug();
		}
		update_request = 0;
	}

	if (active) {
		process_system();
		emit_signal("boid_updated");
	}

	uint64_t end = OS::get_singleton()->get_system_time_msecs(); // @suppress("Type cannot be resolved")

	internal_process_time = end - start;

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\\\\\ MESH //////////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidSystem::link_to_set( Boid::boid_set& p_set, Boid::boid* b ) {

	init_boid( b );
	b->owner = p_set.owner;
	b->group = p_set.group;

}

void BoidSystem::relink_sets() {

	std::map< void*, Boid::boid_set* > set_owners;
	std::map< Boid::boid_set*, int > set_indices;
	for (int i = 0, imax = boid_sets.size(); i < imax; ++i ) {
		Boid::boid_set* bs = boid_sets[i];
		if ( bs->boids != NULL ) {
			delete [] bs->boids;
		}
		if (bs->count < 1) {
			continue;
		}
		bs->boids = new Boid::boid*[bs->count];
		set_owners[bs->owner] = bs;
		set_indices[bs] = 0;
	}

	for ( int i = 0; i < boid_amount; ++i ) {
		Boid::boid* b = boids[i];
		Boid::boid_set* bs = set_owners[b->owner];
		if ( bs == NULL ) {
			continue;
		}
		bs->boids[set_indices[bs]++] = b;
	}

}

void BoidSystem::update_set( Boid::boid_set& p_set ) {

	if ( boid_sets.find(&p_set) == -1 ) {
		boid_sets.push_back( &p_set );
		boid_meshset[p_set.owner] = &p_set;
		p_set.active = 0;
	}

	// new request => setting the boids pointer to NULL
	if ( p_set.boids != NULL ) {
		delete [] p_set.boids;
		p_set.boids = NULL;
	}

	if ( boid_amount == 0 && p_set.count == 0 ) {
		// nothing to do here
		return;
	}

	// no boids yet
	if ( boid_amount == 0 && p_set.count > 0 ) {
		boid_amount = p_set.count;
		boids = new Boid::boid*[boid_amount];
		neighbors = new Boid::boid*[boid_amount];
		p_set.boids = new Boid::boid*[boid_amount];
		for ( int i = 0; i < boid_amount; ++i ) {
			boids[i] = new Boid::boid();
			boids[i]->UID = i;
			link_to_set( p_set, boids[i] );
			p_set.boids[i] = boids[i];
		}
		return;
	}

	// storing boids not link to this set
	Boid::boid** tmp_boids = new Boid::boid*[boid_amount];
	int tmp_amount = 0;
	// storing boids already linked to this set
	Boid::boid** curr_set = new Boid::boid*[boid_amount];
	int set_amount = 0;
	for ( int i = 0; i < boid_amount; ++i ) {
		if ( boids[i]->owner == p_set.owner ) {
			curr_set[ set_amount++ ] = boids[i];
		} else {
			tmp_boids[ tmp_amount++ ] = boids[i];
		}
		boids[i] = NULL;
	}

	if ( set_amount == p_set.count ) {
		// nothing to be done
		delete [] tmp_boids;
		delete [] curr_set;
		return;
	}

	// from here, we will reorganise the memory:
	// all boids link to the current set will be moved at the end of the array
	// and pass to the boid_set

	if ( set_amount > p_set.count ) {
		// too much boids in this set -> let's delete some
		for ( int i = p_set.count; i < set_amount; ++i ) {
			delete(curr_set[i]);
			curr_set[i] = NULL;
		}
	} else if ( set_amount < p_set.count ) {
		Boid::boid** new_set = new Boid::boid*[p_set.count];
		// not enough boids for this set -> let's first recycle the one usable
		for ( int i = 0; i < set_amount; ++i ) {
			new_set[i] = curr_set[i];
			curr_set[i] = NULL;
		}
		// now we create of the missing boids
		for ( int i = 0, imax = p_set.count-set_amount; i < imax; ++i ) {
			new_set[set_amount+i] = new Boid::boid();
			link_to_set(p_set, new_set[set_amount+i]);
		}
		// swapping pointers
		delete [] curr_set;
		curr_set = new_set;
	}

	set_amount = p_set.count;

	// no more boids!
	if ( set_amount == 0 && tmp_amount == 0 ) {
		clear_boids();
		return;
	}

	// new amount of boids
	boid_amount = tmp_amount+set_amount;
	if ( boids == NULL ) {
		delete [] boids;
		delete [] neighbors;
	}
	boids = new Boid::boid*[boid_amount];
	neighbors = new Boid::boid*[boid_amount];
	// restoring boids array
	for ( int i = 0; i < tmp_amount; ++i) {
		boids[i] = tmp_boids[i];
		boids[i]->UID = i;
		tmp_boids[i] = NULL;
	}
	for ( int i = 0; i < set_amount; ++i) {
		boids[i+tmp_amount] = curr_set[i];
		boids[i+tmp_amount]->UID = i+tmp_amount;
		curr_set[i] = NULL;
	}

	delete [] tmp_boids;
	delete [] curr_set;

	relink_sets();

}

void BoidSystem::purge_set( Boid::boid_set& p_set ) {

	// purging set itself
	if ( p_set.boids != NULL ) {
		delete [] p_set.boids;
		p_set.boids = NULL;
	}

	Boid::boid** tmp_boids = new Boid::boid*[boid_amount];
	int tmp_amount = 0;
	for ( int i = 0; i < boid_amount; ++i ) {
		if ( boids[i]->owner == p_set.owner ) {
			delete(boids[i]);
			boids[i] = NULL;
		} else {
			tmp_boids[ tmp_amount++ ] = boids[i];
		}
		boids[i] = NULL;
	}

	if ( tmp_amount == 0 ) {
		clear_boids();
		return;
	}

	boid_amount = tmp_amount;
	if ( boids != NULL ) {
		delete [] boids;
		delete [] neighbors;
	}
	boids = new Boid::boid*[boid_amount];
	neighbors = new Boid::boid*[boid_amount];
	for ( int i = 0; i < tmp_amount; ++i) {
		boids[i] = tmp_boids[i];
		tmp_boids[i] = NULL;
	}

	delete [] tmp_boids;

	relink_sets();

}

void BoidSystem::remove_set( Boid::boid_set& p_set ) {

	if ( boid_sets.find(&p_set) != -1 ) {
		p_set.count = 0;
		purge_set( p_set );
		boid_sets.erase(&p_set);
		boid_meshset.erase(p_set.owner);
	}

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\\\ CONTROLS ////////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

// public
void BoidSystem::update_control(Boid::boid_control_data *p_data) {

	if (controls.find(p_data) == -1) {

		controls.push_back(p_data);
		unique_control(p_data);

		switch (p_data->type) {
			case Boid::ControlType::BCTRL_TYPE_TARGET:
			case Boid::ControlType::BCTRL_TYPE_COLLIDER:
			case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
				sort_controls();
				break;
			case Boid::ControlType::BCTRL_TYPE_SOURCE:
				sort_controls();
				update_behaviour( p_data );
				break;
			case Boid::ControlType::BCTRL_TYPE_NONE:
			default:
				break;

		}


		emit_signal("boid_control_updated");

	}

	if (is_inside_tree()) {

		position_control(p_data);

		switch (p_data->type) {
			case Boid::ControlType::BCTRL_TYPE_TARGET:
			case Boid::ControlType::BCTRL_TYPE_COLLIDER:
			case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
				if (p_data->regenerate_domain) {
					push_regenerate_request(p_data);
					p_data->regenerate_domain = false;
				} else if (p_data->is_static) {
					refresh_control(p_data);
				}
				break;
			case Boid::ControlType::BCTRL_TYPE_SOURCE:
				break;
			case Boid::ControlType::BCTRL_TYPE_NONE:
			default:
				break;
		}

	}

}

// public
void BoidSystem::remove_control(Boid::boid_control_data *p_data) {

	if (controls.find(p_data) == -1) {
		return;
	}

	// unlinking boids using this control
	for ( int i = 0; i < boid_amount; ++i ) {
		if ( boids[i]->behaviour != NULL && boids[i]->behaviour == &p_data->behaviour ) {
			boids[i]->behaviour = NULL;
			sync_boid_tests(boids[i]);
		}
		if ( boids[i]->control == p_data ) {
			boids[i]->control = NULL;
		}
	}

	controls.erase(p_data);

	if ( p_data->type == Boid::ControlType::BCTRL_TYPE_NONE ) {
		// nothing else to do
		return;
	}

	sort_controls();

	switch (p_data->type) {
		case Boid::ControlType::BCTRL_TYPE_TARGET:
		case Boid::ControlType::BCTRL_TYPE_COLLIDER:
		case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
			if (p_data->is_static) {
				push_regenerate_request(p_data);
			} else {
				unregister_dynamic_control(p_data);
			}
			break;
		case Boid::ControlType::BCTRL_TYPE_SOURCE:
		case Boid::ControlType::BCTRL_TYPE_NONE:
		default:
			break;
	}

	emit_signal("boid_control_updated");

}

// public
void BoidSystem::validate_control_uid( Boid::boid_control_data* p_data ) {
	unique_control( p_data );
}

void BoidSystem::init_control_groups() {
	sources.list = 0;
	sources.count = 0;
	targets.list = 0;
	targets.count = 0;
	attractors.list = 0;
	attractors.count = 0;
	colliders.list = 0;
	colliders.count = 0;
}

void BoidSystem::clear_control_group(Boid::boid_control_group &p_group) {
	if (p_group.count > 0) {
		delete [] p_group.list;
		p_group.count = 0;
		p_group.list = 0;
	}
}

void BoidSystem::populate_control_group(Boid::boid_control_group &p_group) {
	if (p_group.count > 0) {
		p_group.list = new Boid::boid_control_data*[p_group.count];
	}
}

void BoidSystem::sort_controls() {

	clear_control_group(sources);
	clear_control_group(targets);
	clear_control_group(colliders);
	clear_control_group(attractors);

	for (int i = 0, max = controls.size(); i < max; ++i) {
		switch (controls[i]->type) {
		case Boid::ControlType::BCTRL_TYPE_SOURCE:
			sources.count += 1;
			break;
		case Boid::ControlType::BCTRL_TYPE_TARGET:
			targets.count += 1;
			break;
		case Boid::ControlType::BCTRL_TYPE_COLLIDER:
			colliders.count += 1;
			break;
		case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
			attractors.count += 1;
			break;
		default:
			break;
		}
	}

	populate_control_group(sources);
	populate_control_group(targets);
	populate_control_group(colliders);
	populate_control_group(attractors);

	int si = 0;
	int ti = 0;
	int ci = 0;
	int ai = 0;

	for (int i = 0, max = controls.size(); i < max; ++i) {
		switch (controls[i]->type) {
		case Boid::ControlType::BCTRL_TYPE_SOURCE:
			sources.list[si++] = controls[i];
			break;
		case Boid::ControlType::BCTRL_TYPE_TARGET:
			targets.list[ti++] = controls[i];
			break;
		case Boid::ControlType::BCTRL_TYPE_COLLIDER:
			colliders.list[ci++] = controls[i];
			break;
		case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
			attractors.list[ai++] = controls[i];
			break;
		default:
			break;
		}
	}

}

Boid::boid_control_data* BoidSystem::get_control_by_id( const int& uid ) {

	for ( int i = 0, imax = controls.size(); i < imax; ++i ) {
		if ( controls[i]->UID == uid ) {
			return controls[i];
		}
	}
	return NULL;

}

void BoidSystem::clear_target(Boid::boid_control_data *p_data) {

	for (int i = 0; i < boid_amount; ++i) {
		if (boids[i]->control == p_data) {
			boids[i]->control = NULL;
		}
	}

}

void BoidSystem::position_control(Boid::boid_control_data *p_data) {

	Transform gi = get_global_transform().affine_inverse();

	if (p_data != NULL && p_data->active) {

		p_data->local = gi * p_data->global;
		p_data->local.origin -= domain_offset;
		p_data->local_inv = p_data->local.affine_inverse();

	} else {

		for (int i = 0, max = controls.size(); i < max; ++i) {
			p_data = controls[i];
			p_data->local = gi * p_data->global;
			p_data->local.origin -= domain_offset;
			p_data->local_inv = p_data->local.affine_inverse();
		}

	}

}

void BoidSystem::refresh_control(Boid::boid_control_data *p_data) {

	switch (p_data->type) {

	case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
	case Boid::ControlType::BCTRL_TYPE_COLLIDER:
	case Boid::ControlType::BCTRL_TYPE_TARGET:
		if (p_data->is_static) {
			for (int i = 0; i < static_boids_amount; ++i) {
				Boid::boid *b = static_boids[i];
				if (b->control == p_data) {
					b->group = p_data->group;
					b->active = p_data->active;
				}
			}
			push_request(boid_REFRESH_MODIFIER_BOIDS_DEBUG);
		}
		break;
	case Boid::ControlType::BCTRL_TYPE_SOURCE:
	case Boid::ControlType::BCTRL_TYPE_NONE:
	default:
		break;
	}

}

void BoidSystem::push_regenerate_request(const Boid::boid_control_data *p_data) {

	switch (p_data->type) {
	case Boid::ControlType::BCTRL_TYPE_TARGET:
	case Boid::ControlType::BCTRL_TYPE_COLLIDER:
	case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
		push_request(boid_REGENERATE_MODIFIER_BOIDS);
		break;
	case Boid::ControlType::BCTRL_TYPE_SOURCE:
	case Boid::ControlType::BCTRL_TYPE_NONE:
	default:
		break;
	}

}

bool BoidSystem::unique_control( Boid::boid_control_data* p_data ) {

	int uid = p_data->UID;

	if ( p_data->UID < 0 ) {
		// compose unique UID
		p_data->UID = 0;
	}
	// validate that UID is unique
	bool go_on = true;
	while( go_on ) {
		go_on = false;
		for ( int i = 0, imax = controls.size(); i < imax; ++i ) {
			if ( controls[i] != p_data && controls[i]->UID == p_data->UID ) {
				p_data->UID++;
				go_on = true;
				break;
			}
		}
	}

	return uid != p_data->UID;

}

void BoidSystem::create_static_boid(Boid::boid_control_data *bmd, Vector<Boid::boid*> &list,
		const Vector3 &position, const Vector3 &cell) {
	Boid::boid *b = new Boid::boid();
	init_boid(b);
	b->type = Boid::Type::BTYPE_STATIC;
	b->control = bmd;
	b->active = bmd->active;
	b->domain_pos = position;
	b->group = bmd->group;
	for (int i = 0; i < 3; ++i) {
		b->index[i] = cell[i];
	}
	b->cellid = get_cell_id(cell);
	;
	list.push_back(b);
}

void BoidSystem::get_static_boids_sphere(Boid::boid_control_data *bmd, Vector<Boid::boid*> &list) {

	if (bmd->shape != Boid::ControlShape::BCTRL_SHAPE_SPHERE) {
		return;
	}

	float dsquared = Math::pow(bmd->dimension[0] + cell_size * .5, 2);
	Vector3 min, max, cell_axis, center;
	get_cell_axis(bmd->local.origin - bmd->dimension, min);
	get_cell_axis(bmd->local.origin + bmd->dimension, max);
	max += Vector3(1, 1, 1);

	for (cell_axis[0] = min[0]; cell_axis[0] < max[0]; ++cell_axis[0]) {
		for (cell_axis[1] = min[1]; cell_axis[1] < max[1]; ++cell_axis[1]) {
			for (cell_axis[2] = min[2]; cell_axis[2] < max[2]; ++cell_axis[2]) {
				int id = get_cell_id(cell_axis);
				if (id == -1) {
					continue;
				}
				center = (cell_axis + Vector3(.5, .5, .5)) * cell_size;
				float d = (center - bmd->local.origin).length_squared();
				if (d < dsquared) {
					create_static_boid(bmd, list, center, cell_axis);
				}
			}
		}
	}
}

void BoidSystem::get_static_boids_box(Boid::boid_control_data *bmd, Vector<Boid::boid*> &list) {

	if (bmd->shape != Boid::ControlShape::BCTRL_SHAPE_BOX) {
		return;
	}

	Vector3 *pts = new Vector3[8];
	for (int p = 0; p < 8; ++p) {
		pts[p] = bmd->local.xform(box_vertices[p] * bmd->dimension);
	}

	Vector3 diffx = pts[1] - pts[0];
	Vector3 dirx = keep_in_cell(diffx);
	float rx = diffx.length() / dirx.length();
	int jumpsx = int(Math::ceil(rx)) * bmd->subdivision;
	// reajusting the direction based on jumps
	dirx = diffx / jumpsx;

	Vector3 diffz = pts[2] - pts[1];
	Vector3 dirz = keep_in_cell(diffz);
	float rz = diffz.length() / dirz.length();
	int jumpsz = int(Math::ceil(rz)) * bmd->subdivision;
	// reajusting the direction based on jumps
	dirz = diffz / jumpsz;

	Vector3 diffy = pts[4] - pts[0];
	Vector3 diry = keep_in_cell(diffy);
	float ry = diffy.length() / diry.length();
	int jumpsy = int(Math::ceil(ry)) * bmd->subdivision;
	// reajusting the direction based on jumps
	diry = diffy / jumpsy;

	int previous_id = -1;
	Array cell_list;
	Vector3 cell_axis;

	for (int x = 0, xmax = jumpsx + 1; x < xmax; ++x) {
		Vector3 vx = pts[0] + dirx * x;
		for (int z = 0, zmax = jumpsz + 1; z < zmax; ++z) {
			Vector3 vxz = vx + dirz * z;
			for (int y = 0, ymax = jumpsy + 1; y < ymax; ++y) {
				Vector3 v = vxz + diry * y;
				get_cell_axis(v, cell_axis);
				int id = get_cell_id(cell_axis);
				if (id == -1 || id == previous_id) {
					continue;
				}
				previous_id = id;
				if (!cell_list.has(id)) {
					create_static_boid(bmd, list,
							(cell_axis + Vector3(.5, .5, .5)) * cell_size,
							cell_axis);
				}
			}
		}
	}

	delete [] pts;

}

void BoidSystem::get_static_boids(Boid::boid_control_data *bmd, Vector<Boid::boid*> &list) {
	switch (bmd->shape) {
	case Boid::ControlShape::BCTRL_SHAPE_SPHERE:
		get_static_boids_sphere(bmd, list);
		break;
	case Boid::ControlShape::BCTRL_SHAPE_BOX:
		get_static_boids_box(bmd, list);
		break;
	case Boid::ControlShape::BCTRL_SHAPE_POINT:
	default:
		break;
	}
}

void BoidSystem::register_dynamic_control(Boid::boid_control_data *p_data) {
	if (dynamic_modifiers.find(p_data) > -1) {
		return;
	}
	dynamic_modifiers.push_back(p_data);
}

void BoidSystem::unregister_dynamic_control(Boid::boid_control_data *p_data) {
	dynamic_modifiers.erase(p_data);
}

void BoidSystem::regenerate_control_group(const Boid::boid_control_group &group,
		Vector<Boid::boid*> &list) {
	for (int i = 0; i < group.count; ++i) {
		if (group.list[i]->is_static) {
			unregister_dynamic_control(group.list[i]);
			get_static_boids(group.list[i], list);
		} else {
			register_dynamic_control(group.list[i]);
		}
	}
}

void BoidSystem::regenerate_control_boids() {

	clear_static_boids();

	Vector<Boid::boid*> temporary_boids;
	regenerate_control_group(targets, temporary_boids);
	regenerate_control_group(attractors, temporary_boids);
	regenerate_control_group(colliders, temporary_boids);

	static_boids_amount = temporary_boids.size();
	static_boids = new Boid::boid*[static_boids_amount];
	for (int i = 0; i < static_boids_amount; ++i) {
		static_boids[i] = temporary_boids[i];
	}

	push_request(boid_REFRESH_MODIFIER_BOIDS_DEBUG);

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\\ BEHAVIOUR ////////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

// public
void BoidSystem::update_behaviour( Boid::boid_control_data* p_data ) {

	if ( p_data != NULL ) {

		// linking control data for test & action
		Boid::boid_behaviour_test* t = p_data->behaviour.test;
		while( t != NULL ) {
			if (t->param_ctrl.initialised) {
				t->param_ctrl.value = get_control_by_id( t->param_ctrl.control_UID );
				t->valid = t->param_ctrl.value != NULL;
				if ( !t->valid ) {
					WARN_PRINT( "test dactivated > inside UID:" + String::num(t->param_ctrl.control_UID) + " not found" );
				}
			}
			Boid::boid_behaviour_action* a = t->action;
			while( a != NULL ) {
				if (a->param_ctrl.initialised) {
					a->param_ctrl.value = get_control_by_id( a->param_ctrl.control_UID );
					a->valid = a->param_ctrl.value != NULL;
					if ( !a->valid ) {
						WARN_PRINT( "action dactivated > inside UID:" + String::num(a->param_ctrl.control_UID) + " not found" );
					}
				}
				a = a->next;
			}
			t = t->next;
		}


		// synching boids linked to behaviour
		switch (p_data->type) {
			case Boid::ControlType::BCTRL_TYPE_SOURCE:
				for ( int i = 0; i < boid_amount; ++i ) {
					if ( boids[i]->behaviour != NULL && boids[i]->behaviour == &p_data->behaviour ) {
						sync_boid_tests(boids[i]);
					}
				}
				break;
			case Boid::ControlType::BCTRL_TYPE_TARGET:
			case Boid::ControlType::BCTRL_TYPE_COLLIDER:
			case Boid::ControlType::BCTRL_TYPE_ATTRACTOR:
			case Boid::ControlType::BCTRL_TYPE_NONE:
			default:
				break;
		}
	}

}

Vector<Boid::boid_control_info> BoidSystem::get_behaviour_info() {
	Vector<Boid::boid_control_info> infos;
	for ( int i = 0, imax = controls.size(); i < imax; ++i ) {
		Boid::boid_control_info bci;
		bci.UID = controls[i]->UID;
		bci.name = controls[i]->name;
		bci.type = controls[i]->type;
		infos.push_back( bci );
	}
	return infos;
}

void BoidSystem::relocate( Boid::boid* b ) {

	b->domain_pos = b->local_pos - domain_offset;
	bool outside = false;
	for ( int g = 0; g < 3; ++g ) {
		if (b->domain_pos[g] < 0 || b->domain_pos[g] >= domain_size[g]) {
			outside = true;
		} else {
			// render grid index
			b->index[g] = int(b->domain_pos[g] / cell_size);
		}
	}
	if (outside) {
		constrain_boid(b);
	}
	b->cellid = b->index[0] + b->index[1] * domain_x + b->index[2] * domain_x * domain_y;
	if (b->cellid >= cell_num) {
		b->active = false;
		b->cellid = cell_num;
	}

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\ SETTERS & GETTERS ///////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidSystem::set_cell_size(float p_csize) {

	if (cell_size == 0) {
		WARN_PRINT( "Cell size can not be equal to 0" );
		return;
	} else if (cell_size < 0) {
		WARN_PRINT( "Cell size is smaller than 0, we gonna make it positive for you." );
		p_csize *= -1;
	}

	if (cell_size == p_csize) {
		return;
	}
	cell_size = p_csize;
	cell_size_pow2 = p_csize*p_csize;
	push_request(boid_INIT_DOMAIN);
	push_request(boid_REFRESH_DOMAIN);
	push_request(boid_REFRESH_CELL);
	push_request(boid_REGENERATE_MODIFIER_BOIDS);
	push_request(boid_REFRESH_MODIFIER_BOIDS_DEBUG);
}

const float& BoidSystem::get_cell_size() const {
	return cell_size;
}

void BoidSystem::set_domain_x(int p_dx) {
	if (domain_x == p_dx) {
		return;
	}
	domain_x = p_dx;
	if (domain_x <= 0) {
		domain_x = 1;
	}
	push_request(boid_INIT_DOMAIN);
	push_request(boid_REFRESH_DOMAIN);
	push_request(boid_REFRESH_CELL);
	push_request(boid_REGENERATE_MODIFIER_BOIDS);
	push_request(boid_REFRESH_MODIFIER_BOIDS_DEBUG);
}

const int& BoidSystem::get_domain_x() const {
	return domain_x;
}

void BoidSystem::set_domain_y(int p_dy) {
	if (domain_y == p_dy) {
		return;
	}
	domain_y = p_dy;
	if (domain_y <= 0) {
		domain_y = 1;
	}
	push_request(boid_INIT_DOMAIN);
	push_request(boid_REFRESH_DOMAIN);
	push_request(boid_REFRESH_CELL);
	push_request(boid_REGENERATE_MODIFIER_BOIDS);
	push_request(boid_REFRESH_MODIFIER_BOIDS_DEBUG);
}

const int& BoidSystem::get_domain_y() const {
	return domain_y;
}

void BoidSystem::set_domain_z(int p_dz) {
	if (domain_z == p_dz) {
		return;
	}
	domain_z = p_dz;
	if (domain_z <= 0) {
		domain_z = 1;
	}
	push_request(boid_INIT_DOMAIN);
	push_request(boid_REFRESH_DOMAIN);
	push_request(boid_REFRESH_CELL);
	push_request(boid_REGENERATE_MODIFIER_BOIDS);
	push_request(boid_REFRESH_MODIFIER_BOIDS_DEBUG);

}

const int& BoidSystem::get_domain_z() const {
	return domain_z;
}

void BoidSystem::set_domain_visible(bool p_dv) {
	if ((!p_dv && domain_debug == NULL) || (p_dv && domain_debug != NULL)) {
		return;
	}
	if (!p_dv) {
		remove_child(domain_debug);
		domain_debug = NULL;
	} else {
		push_request(boid_DISPLAY_DOMAIN);
		push_request(boid_REFRESH_DOMAIN);
	}
}

bool BoidSystem::is_domain_visible() {
	return domain_debug != NULL;
}

void BoidSystem::set_cell_visible(bool p_dv) {
	if ((!p_dv && cell_debug == NULL) || (p_dv && cell_debug != NULL)) {
		return;
	}
	if (!p_dv) {
		remove_child(cell_debug);
		cell_debug = NULL;
	} else {
		push_request(boid_DISPLAY_CELL);
		push_request(boid_REFRESH_CELL);
	}
}

bool BoidSystem::is_cell_visible() {
	return cell_debug != NULL;
}

void BoidSystem::set_modifier_boids_visible(bool p_dv) {
	if ((!p_dv && modifier_boid_debug == NULL)
			|| (p_dv && modifier_boid_debug != NULL)) {
		return;
	}
	if (!p_dv) {
		remove_child(modifier_boid_debug);
		modifier_boid_debug = NULL;
	} else {
		push_request(boid_DISPLAY_MODIFIER_BOIDS);
		push_request(boid_REFRESH_MODIFIER_BOIDS_DEBUG);
	}
}

bool BoidSystem::is_modifier_boids_visible() {
	return modifier_boid_debug != NULL;
}

void BoidSystem::set_domain_border(Boid::DomainBorder p_border) {
	domain_border = p_border;
}

Boid::DomainBorder BoidSystem::get_domain_border() const {
	return domain_border;
}

void BoidSystem::set_domain_plane(Boid::DomainPlanes p_plane) {
	if (domain_plane != p_plane) {
		domain_plane = p_plane;
		push_request(boid_UPDATE_DOMAIN_PLANE);
	}
}

Boid::DomainPlanes BoidSystem::get_domain_plane() const {
	return domain_plane;
}

void BoidSystem::set_domain_border_radius(float p_brad) {
	if (p_brad == 0) {
		return;
	}
	domain_border_radius = Math::abs(p_brad);
}

const float& BoidSystem::get_domain_border_radius() const {
	return domain_border_radius;
}

void BoidSystem::set_active(const bool& p_proc) {
	active = p_proc;
	if ( !active ) {
		for ( int i = 0; i < boid_amount; ++i ) {
			boids[i]->active = false;
		}
	}
}

bool BoidSystem::is_active() const {
	return active;
}

void BoidSystem::set_gravity(Vector3 p_gravity) {
	gravity = p_gravity;
}

void BoidSystem::set_speed(float p_speed) {
	speed = abs(p_speed);
}

const float& BoidSystem::get_speed() {
	return speed;
}

const Vector3& BoidSystem::get_gravity() const {
	return gravity;
}

const Vector3& BoidSystem::get_domain_offset() const {
	return domain_offset;
}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\ BOIDS SCRIPTING ////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

const int& BoidSystem::get_internal_process_time() const {
	return internal_process_time;
}

const int& BoidSystem::get_collision_count() const {
	return collision_count;
}

Dictionary BoidSystem::get_boid( const int& i ) const {

	if ( i < 0 || i >= boid_amount ) {
		return Dictionary();
	}

	Boid::boid* b = boids[i];
	Dictionary d;
	d["type"] = b->type;
	d["active"] = b->active;
	d["collision"] = b->collision;
	d["group"] = b->group;
	d["reach_group"] = b->reach_group;
	d["reach_strength"] = b->reach_strength;
	d["reach_group_center"] = b->reach_group_center;
	d["escape_group"] = b->escape_group;
	d["escape_strength"] = b->escape_strength;
	d["escape_group_center"] = b->escape_group_center;
	d["aim_at_group"] = b->aim_at_group;
	d["aim_at_strength"] = b->aim_at_strength;
	d["aim_at_group_center"] = b->aim_at_group_center;
	d["animation"] = b->animation;
	d["plane"] = b->plane;
	d["domain_pos"] = b->domain_pos;
	d["local_pos"] = b->local_pos;
	d["previous_pos"] = b->previous_pos;
	d["motion"] = b->motion;
	d["dir"] = b->dir;
	d["front"] = b->front;
	d["push"] = b->push;
	d["aim"] = b->aim;
	d["inertia"] = b->inertia;
	d["speed"] = b->speed;
	d["internal_speed"] = b->internal_speed;
	d["weight"] = b->weight;
	d["radius"] = b->radius;
	d["steer"] = b->steer;
	d["friction"] = b->friction;
	d["cellid"] = b->cellid;
	d["target_offset"] = b->target_offset;
	d["sac_weights"] = b->sac_weights;
	d["travel_distance"] = b->travel_distance;
	d["lifetime"] = b->lifetime;
	d["timer"] = b->timer;
	d["counter"] = b->counter;
	d["color"] = b->color;
	d["aiming"] = b->aiming;
	d["timer_end"] = b->timer_end;
	d["moving"] = b->moving;
	d["moving_start"] = b->moving_start;
	d["moving_stop"] = b->moving_stop;
	d["test_count"] = b->test_count;
	d["neighbor_count"] = b->neighbor_count;

	return d;

}

Variant BoidSystem::get_boid_attribute( const int& i, Boid::ActionAttribute attr ) const {

	if ( i < 0 || i >= boid_amount ) {
		return Variant();
	}

	Boid::boid* b = boids[i];

	switch ( attr ) {

		case Boid::ActionAttribute::BACTION_ATTR_ACTIVE:
			return b->active;

		case Boid::ActionAttribute::BACTION_ATTR_COLLISION:
			return b->collision;

		case Boid::ActionAttribute::BACTION_ATTR_GROUP:
			return int(b->group);

		case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
			return int(b->animation);

		case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
			return b->sac_weights[0];

		case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
			return b->sac_weights[1];

		case Boid::ActionAttribute::BACTION_ATTR_COHESION:
			return b->sac_weights[2];

		case Boid::ActionAttribute::BACTION_ATTR_STEER:
			return b->steer;

		case Boid::ActionAttribute::BACTION_ATTR_SPEED:
			return b->internal_speed;

		case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
			return b->inertia;

		case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
			return b->weight;

		case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
			return b->radius;

		case Boid::ActionAttribute::BACTION_ATTR_FRICTION:
			return b->friction;

		case Boid::ActionAttribute::BACTION_ATTR_COLOR:
			return b->color;

		case Boid::ActionAttribute::BACTION_ATTR_AIM:
			return b->aim;

		case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
			return b->counter;

		case Boid::ActionAttribute::BACTION_ATTR_TIMER:
			return b->timer;

		case Boid::ActionAttribute::BACTION_ATTR_LIFETIME:
			return b->lifetime;

		case Boid::ActionAttribute::BACTION_ATTR_POSITION:
			return b->local_pos;

		case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
			return b->dir;

		case Boid::ActionAttribute::BACTION_ATTR_FRONT:
			return b->front;

		case Boid::ActionAttribute::BACTION_ATTR_PUSH:
			return b->push;

		case Boid::ActionAttribute::BACTION_ATTR_NONE:
		default:
			return Variant();

	}

}

void BoidSystem::set_boid_attribute( const int& i, Boid::ActionAttribute attr, Variant v ) const {

	if ( i < 0 || i >= boid_amount ) {
		return ;
	}

	Boid::boid* b = boids[i];

	switch ( attr ) {

		case Boid::ActionAttribute::BACTION_ATTR_ACTIVE:
			b->active = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_COLLISION:
			b->collision = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_GROUP:
			b->group = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_ANIMATION:
			b->animation = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_SEPARATION:
			b->sac_weights[0] = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_ALIGNMENT:
			b->sac_weights[1] = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_COHESION:
			b->sac_weights[2] = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_STEER:
			b->steer = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_SPEED:
			b->speed = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_INERTIA:
			b->inertia = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_WEIGHT:
			b->weight = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_RADIUS:
			b->radius = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_FRICTION:
			b->friction = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_COLOR:
			b->color = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_AIM:
			b->aim = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_COUNTER:
			b->prev_counter = b->counter;
			b->counter = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_TIMER:
			b->timer = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_LIFETIME:
			b->lifetime = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_POSITION:
			b->local_pos = v;
			b->domain_pos = b->local_pos - domain_offset;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_DIRECTION:
			b->dir = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_FRONT:
			b->front = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_PUSH:
			b->push = v;
			break;

		case Boid::ActionAttribute::BACTION_ATTR_NONE:
		default:
			break;

	}

}

void BoidSystem::apply_action( const int& i, Object* action ) {

	if ( i < 0 || i >= boid_amount || action == NULL ) {
		return;
	}
	if ( !boids[i]->active ) {
		return;
	}
	BoidAction* aa = Object::cast_to<BoidAction>( action );
	if ( aa == NULL ) {
		return;
	}
	const Boid::boid_behaviour_action* a = aa->get_data();
	if ( !a->valid || !a->active ) {
		return;
	}
	behave_action( boids[i], a, get_process_delta_time() * speed );

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\ SERVER LINKS //////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidSystem::server_update( int32_t tree_id ) {  // @suppress("Type cannot be resolved")

	const BoidServer::boid_tree* _btree = BoidServer::get_tree(tree_id);
	if ( _btree == NULL ) {
		return;
	}

	std::cout << "BoidSystem::server_update" << std::endl;
	std::cout << "\tsystem " << _btree->system << " <> " << this << std::endl;
	std::cout << "\tmeshes " << _btree->mesh_count << std::endl;
	std::cout << "\tcontrols " << _btree->control_count << std::endl;

}

void BoidSystem::server_clear() {

	std::cout << "BoidSystem::server_clear" << std::endl;

}

//\\\\\\\\\\\\\\\\\\\\\\//////////////////////////
//\\\\\\\\\\\\\\\ ENGINE LINKS //////////////////
//\\\\\\\\\\\\\\\\\\\\\\////////////////////////

void BoidSystem::_validate_property(PropertyInfo &property) const {

}

void BoidSystem::_notification(int p_what) {

	switch( p_what ) {

		case NOTIFICATION_INTERNAL_PROCESS:
			internal_process();
			break;

		case NOTIFICATION_PREDELETE:
			update_request = -1;
			set_domain_visible(false);
			set_cell_visible(false);
			set_modifier_boids_visible(false);
			break;

		case NOTIFICATION_PAUSED:
		case NOTIFICATION_UNPAUSED:
			break;

		case NOTIFICATION_ENTER_WORLD:
			break;

		case NOTIFICATION_VISIBILITY_CHANGED:
			break;

		case NOTIFICATION_ENTER_TREE:
			BoidServer::link( this );
			break;

		case NOTIFICATION_EXIT_TREE:
			BoidServer::unlink( this );
			break;

		case NOTIFICATION_PARENTED:
			BoidServer::link( this );
			break;

		case NOTIFICATION_UNPARENTED:
			BoidServer::unlink( this );
			break;

	}

}

void BoidSystem::_bind_methods() {

//	ADD_SIGNAL(MethodInfo("boid_event",PropertyInfo(Variant::INT,"boid"),PropertyInfo(Variant::INT,"event")));
	ADD_SIGNAL(MethodInfo("boid_control_updated"));
	ADD_SIGNAL(MethodInfo("boid_updated"));

	// scripting only
	ClassDB::bind_method(D_METHOD("get_internal_process_time"), &BoidSystem::get_internal_process_time);
	ClassDB::bind_method(D_METHOD("get_collision_count"), &BoidSystem::get_collision_count);
	ClassDB::bind_method(D_METHOD("get_boid"), &BoidSystem::get_boid);
	ClassDB::bind_method(D_METHOD("get_boid_attribute"), &BoidSystem::get_boid_attribute);
	ClassDB::bind_method(D_METHOD("set_boid_attribute"), &BoidSystem::set_boid_attribute);
	ClassDB::bind_method(D_METHOD("apply_action"), &BoidSystem::apply_action);

	// domain related
	ClassDB::bind_method(D_METHOD("set_active", "active"),
			&BoidSystem::set_active);
	ClassDB::bind_method(D_METHOD("is_active"),
			&BoidSystem::is_active);
	ClassDB::bind_method(D_METHOD("set_speed", "speed"),
			&BoidSystem::set_speed);
	ClassDB::bind_method(D_METHOD("get_speed"),
			&BoidSystem::get_speed);

	ClassDB::bind_method(D_METHOD("set_domain_visible", "domain_visible"),
			&BoidSystem::set_domain_visible);
	ClassDB::bind_method(D_METHOD("is_domain_visible"),
			&BoidSystem::is_domain_visible);
	ClassDB::bind_method(D_METHOD("set_cell_visible", "cell_visible"),
			&BoidSystem::set_cell_visible);
	ClassDB::bind_method(D_METHOD("is_cell_visible"),
			&BoidSystem::is_cell_visible);
	ClassDB::bind_method(
			D_METHOD("set_modifier_boids_visible", "modifier_boids_visible"),
			&BoidSystem::set_modifier_boids_visible);
	ClassDB::bind_method(D_METHOD("is_modifier_boids_visible"),
			&BoidSystem::is_modifier_boids_visible);
	ClassDB::bind_method(D_METHOD("set_domain_border", "domain_border"),
			&BoidSystem::set_domain_border);
	ClassDB::bind_method(D_METHOD("get_domain_border"),
			&BoidSystem::get_domain_border);
	ClassDB::bind_method(D_METHOD("set_domain_plane", "domain_plane"),
			&BoidSystem::set_domain_plane);
	ClassDB::bind_method(D_METHOD("get_domain_plane"),
			&BoidSystem::get_domain_plane);
	ClassDB::bind_method(
			D_METHOD("set_domain_border_radius", "domain_border_radius"),
			&BoidSystem::set_domain_border_radius);
	ClassDB::bind_method(D_METHOD("get_domain_border_radius"),
			&BoidSystem::get_domain_border_radius);
	ClassDB::bind_method(D_METHOD("set_domain_x", "domain_x"),
			&BoidSystem::set_domain_x);
	ClassDB::bind_method(D_METHOD("get_domain_x"), &BoidSystem::get_domain_x);
	ClassDB::bind_method(D_METHOD("set_domain_y", "domain_y"),
			&BoidSystem::set_domain_y);
	ClassDB::bind_method(D_METHOD("get_domain_y"), &BoidSystem::get_domain_y);
	ClassDB::bind_method(D_METHOD("set_domain_z", "domain_z"),
			&BoidSystem::set_domain_z);
	ClassDB::bind_method(D_METHOD("get_domain_z"), &BoidSystem::get_domain_z);
	ClassDB::bind_method(D_METHOD("set_cell_size", "cell_size"),
			&BoidSystem::set_cell_size);
	ClassDB::bind_method(D_METHOD("get_cell_size"), &BoidSystem::get_cell_size);
	ClassDB::bind_method(D_METHOD("set_gravity", "gravity"),
			&BoidSystem::set_gravity);
	ClassDB::bind_method(D_METHOD("get_gravity"), &BoidSystem::get_gravity);

	ADD_GROUP("Domain", "");
	ADD_PROPERTY(PropertyInfo(Variant::VECTOR3, "gravity"), "set_gravity",
			"get_gravity");
	ADD_PROPERTY(
			PropertyInfo(Variant::REAL, "cell_size", PROPERTY_HINT_RANGE,
					"0.001,200,0.01,or_greater"), "set_cell_size",
			"get_cell_size");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "domain_x", PROPERTY_HINT_RANGE,
					"1,2000,1,or_greater"), "set_domain_x", "get_domain_x");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "domain_y", PROPERTY_HINT_RANGE,
					"1,2000,1,or_greater"), "set_domain_y", "get_domain_y");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "domain_z", PROPERTY_HINT_RANGE,
					"1,2000,1,or_greater"), "set_domain_z", "get_domain_z");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "domain_plane", PROPERTY_HINT_ENUM,
					"all (3d), floor (xz)"), "set_domain_plane",
			"get_domain_plane");
	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "domain_border", PROPERTY_HINT_ENUM,
					"none,bounce,push,attract,respawn,teleport,deactivate"),
			"set_domain_border", "get_domain_border");
	ADD_PROPERTY(
			PropertyInfo(Variant::REAL, "domain_border_radius",
					PROPERTY_HINT_RANGE, "0.1,100,0.01,or_greater"),
			"set_domain_border_radius", "get_domain_border_radius");

	ADD_GROUP("System", "");
	ADD_PROPERTY(PropertyInfo(Variant::REAL, "speed",
			PROPERTY_HINT_RANGE, "0.01,2,0.001,or_greater"),
			"set_speed", "get_speed");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "active"),
			"set_active", "is_active");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "domain_visible"),
			"set_domain_visible", "is_domain_visible");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "cell_visible"),
			"set_cell_visible", "is_cell_visible");
	ADD_PROPERTY(PropertyInfo(Variant::BOOL, "modifier_boids_visible"),
			"set_modifier_boids_visible", "is_modifier_boids_visible");

}

BoidSystem::BoidSystem() :
		boids(NULL), boid_amount(0), boid_active_amount(0),
		neighbors(NULL), neighbor_count(0),
		domain(NULL), domain_cells(NULL),
		cell_num(0), cell_size(10), cell_size_pow2(100),
		domain_x(20), domain_y(10), domain_z(20), domain_dim(NULL),
		domain_plane( Boid::DomainPlanes::DOMAIN_PLANES_ALL ),
		domain_border( Boid::DomainBorder::BORDER_PUSH ),
		domain_border_radius(20),
		domain_debug(NULL), cell_debug(NULL), modifier_boid_debug(NULL),
		static_boids(NULL), static_boids_amount(0),
		update_request(0), internal_process_time(0), collision_count(0),
		bool_false(false), int_zero(0), float_zero(0),
		v3_zero(0,0,0), v3_one(1,1,1), color_white(1,1,1,1),
		gravity(0,0,0), active(true), speed(1)
		{

	transform_identity = Transform(
			Basis(Vector3(1, 0, 0), Vector3(0, 1, 0), Vector3(0, 0, 1)),
			Vector3(0, 0, 0));

	init_control_groups();

	init_domain(); // must be done before anything else!

	set_process_internal(true);

}

BoidSystem::~BoidSystem() {

	clear_boids();
	clear_static_boids();
	clear_domain();

}

