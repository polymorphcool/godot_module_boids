/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_system.h
 *
 *  Created on: Mar 9, 2021
 *      Author: frankiezafe
 */

#ifndef MODULES_BOIDS_BOID_SYSTEM_H_
#define MODULES_BOIDS_BOID_SYSTEM_H_

#include <map>
#include <iostream>
#include <algorithm>

#include "core/os/os.h"
#include "core/hash_map.h"
#include "scene/main/viewport.h"
#include "scene/3d/spatial.h"
#include "scene/3d/immediate_geometry.h"

#include "boid_common.h"
#include "boid.h"
#include "boid_action.h"
#include "boid_behaviour.h"

class BoidServer;

class BoidSystem : public Spatial {

private:
	GDCLASS(BoidSystem, Spatial); // @suppress("Symbol is not resolved")
	OBJ_CATEGORY("Boid");

private:

	// boids related
	Boid::boid** boids;
	int boid_amount;			// actual number of boids
	int boid_active_amount;
	Boid::boid** neighbors;
	int neighbor_count;
	Vector< Boid::boid_set* > boid_sets;
	Map< void*, Boid::boid_set* > boid_meshset;
	Boid::boid_group boid_groups[boid_GROUP_COUNT];

	// domain related
	Boid::boid** domain;				// pointer to first boid in each cell, other are accessible with boid->next (!=0)
	int* domain_cells;
	Vector3 domain_size;
	Vector3 domain_offset;
	uint32_t cell_num;			// total number of cells // @suppress("Type cannot be resolved")
	float cell_size;
	float cell_size_pow2;		// used in distance computation
	int domain_x;
	int domain_y;
	int domain_z;
	int* domain_dim;
	Boid::DomainPlanes domain_plane;
	Boid::DomainBorder domain_border;
	float domain_border_radius;	// used only when domain_border = BORDER_PUSH
	ImmediateGeometry* domain_debug;
	ImmediateGeometry* cell_debug;
	ImmediateGeometry* modifier_boid_debug;

	// modifiers management
	Vector< Boid::boid_control_data* > controls;
	Boid::boid_control_group sources;
	Boid::boid_control_group targets;
	Boid::boid_control_group attractors;
	Boid::boid_control_group colliders;
	// main storage of modifier boids > pointers to reduce creation by segments
	Boid::boid** static_boids;
	int static_boids_amount;
	Vector< Boid::boid_control_data* > dynamic_modifiers;

	// controls related
	void init_control_groups();
	void clear_control_group( Boid::boid_control_group& p_group );
	void populate_control_group( Boid::boid_control_group& p_group );
	void sort_controls();
	Boid::boid_control_data* get_control_by_id( const int& uid );
	void clear_target( Boid::boid_control_data* p_data );
	bool unique_control( Boid::boid_control_data* p_data );
	void position_control( Boid::boid_control_data* p_data );
	void refresh_control( Boid::boid_control_data* p_data );
	// full regeneration
	void push_regenerate_request( const Boid::boid_control_data* p_data );
	void regenerate_control_group( const Boid::boid_control_group& group, Vector<Boid::boid*>& );
	void regenerate_control_boids();
	// static modifiers related
	void create_static_boid( Boid::boid_control_data* p_data, Vector<Boid::boid*>&, const Vector3& position, const Vector3& cell );
	void get_static_boids_sphere( Boid::boid_control_data* p_data, Vector<Boid::boid*>& );
	void get_static_boids_box( Boid::boid_control_data* p_data, Vector<Boid::boid*>& );
	void get_static_boids( Boid::boid_control_data* p_data, Vector<Boid::boid*>& );
	// dynamic modifiers related
	void register_dynamic_control( Boid::boid_control_data* p_data );
	void unregister_dynamic_control( Boid::boid_control_data* p_data );

	int update_request; // -1 will prevent any new request to be pushed
	int internal_process_time;
	int collision_count;
	// usefull
	bool bool_false;
	int int_zero;
	float float_zero;
	Vector3 v3_zero;
	Vector3 v3_one;
	Transform transform_identity;
	Color color_white;

	// temporary struct used during sources update to share evenly availale boids
	// used with a std::map<void*, boid_sourcing>, first being the boid owners
	struct boid_sourcing {
		int available;
		int requested;
		std::map<Boid::boid_control_data*, int> source_req;
	};

	Vector3 gravity;
	AABB boids_aabb;

	bool active;
	float speed;

protected:

	// engine links

	static void _bind_methods();
	void _notification(int p_what);
	virtual void _validate_property(PropertyInfo &property) const;

	// utils
	void get_cell_axis( const Vector3& position, Vector3& cell_axis );
	int get_cell_id( const Vector3& cell_axis ) const;
	Vector3 keep_in_cell( const Vector3& v );

	void clear_boids();
	void clear_static_boids();
	void clear_domain();
	void init_boid(Boid::boid* b);
	void reset_boid(Boid::boid* b);
	void spawn_boid(Boid::boid* b);
	void source_boid(const Boid::boid_control_data* src, Boid::boid* b);
	void kill_boid(Boid::boid* b);
	void init_domain();
	Boid::boid* get_inactive_boid( Boid::boid_control_data* ctrl );
	void sync_boid_tests(Boid::boid* b);

	void reset_boid_groups();
	void reset_boid_sets();
	void increment_set( Boid::boid* b );
	void update_boid_groups( Boid::boid* b );
	void render_boid_groups();

	// must be threaded
	void apply_domain_plane( int start=-1, int end=-1 );
	void constrain_boid( Boid::boid* b );
	void update_boids();
	void link_in_cell( Boid::boid* b );
	void link_domain();
	inline void boid_collision( Boid::boid* a, Boid::boid* b, const Vector3& distance );

	// all boid processing methods
	bool boid_inside_modifier( Boid::boid* b, Boid::boid_control_data* ctrl );
	void process_boids_attractor( Boid::boid* b, Boid::boid_control_data* ctrl, const float& delta );
	void push_boids_collider( Boid::boid* b, Boid::boid_control_data* ctrl, const Vector3& normal, const float& push_percent, const float &delta);
	void process_boids_collider( Boid::boid* b, Boid::boid_control_data* ctrl, const float &delta);
	void process_boids_target( Boid::boid* b, Boid::boid_control_data* ctrl );
	void process_boids_modifier( Boid::boid* b, Boid::boid_control_data* ctrl, const float& delta );
	bool behave_attr( Boid::boid* b, Boid::boid_behaviour_test* test );
	bool behave_eval_int( const int& i, Boid::boid_behaviour_test* test );
	bool behave_eval_float( const float& f, Boid::boid_behaviour_test* test );
	bool behave_neighborhoud( const Boid::boid* b, const int& group );
	bool behave_inside( Boid::boid* b, Boid::boid_control_data* ctrl );
	void behave_action( Boid::boid* b, const Boid::boid_behaviour_action* action, const float& delta );
	void behave( Boid::boid* b );

	void update_sources();
	void process_boids();
	void process_system();

	void refresh_domain_debug();
	void refresh_cell_debug();
	void refresh_modifier_debug();

	// called by NOTIFICATION_INTERNAL_PROCESS
	void internal_process();

	void push_request( int p_req );

	void link_to_set( Boid::boid_set& p_set, Boid::boid* b );
	void purge_set( Boid::boid_set& p_set );
	void relink_sets();

public:

	// server connection
	void server_update( int32_t tree_id );
	void server_clear();

	// domain related

	void set_cell_size(float p_csize);
	const float& get_cell_size() const;

	void set_domain_x(int p_dx);
	const int& get_domain_x() const;

	void set_domain_y(int p_dy);
	const int& get_domain_y() const;

	void set_domain_z(int p_dz);
	const int& get_domain_z() const;

	void set_domain_border(Boid::DomainBorder p_border);
	Boid::DomainBorder get_domain_border() const;

	void set_domain_plane(Boid::DomainPlanes p_plane);
	Boid::DomainPlanes get_domain_plane() const;

	void set_domain_border_radius(float p_brad);
	const float& get_domain_border_radius() const;

	const Vector3& get_domain_offset() const;

	// system related

	void set_active(const bool& p_proc);
	bool is_active() const;

	void set_gravity(Vector3 p_gravity);
	const Vector3& get_gravity() const;

	void set_domain_visible(bool p_dv);
	bool is_domain_visible();

	void set_cell_visible(bool p_dv);
	bool is_cell_visible();

	void set_modifier_boids_visible(bool p_dv);
	bool is_modifier_boids_visible();

	void set_speed(float p_speed);
	const float& get_speed();

	// mesh related

	void update_set( Boid::boid_set& p_set );
	void remove_set( Boid::boid_set& p_set );

	// controls related

	void validate_control_uid( Boid::boid_control_data* p_data );
	void update_control( Boid::boid_control_data* p_data );
	void update_behaviour( Boid::boid_control_data* p_data );
	void remove_control( Boid::boid_control_data* p_data );

	// behaviour related

	Vector<Boid::boid_control_info> get_behaviour_info();

	// boid related

	void relocate( Boid::boid* b );

	// scripting only

	const int& get_internal_process_time() const;
	const int& get_collision_count() const;
	Dictionary get_boid( const int& i ) const;
	Variant get_boid_attribute( const int& i, Boid::ActionAttribute attr ) const;
	void set_boid_attribute( const int& i, Boid::ActionAttribute attr, Variant v ) const;
	void apply_action( const int& i, Object* action );

	BoidSystem();
	virtual ~BoidSystem();

};

#endif /* MODULES_BOIDS_BOID_SYSTEM_H_ */
