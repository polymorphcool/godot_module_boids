/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 * boid_target.cpp
 *
 *  Created on: Feb 13, 2021
 *      Author: frankiezafe
 */

#include "boid_target.h"

void BoidTarget::set_strength( float p_strength ) {
	strength = p_strength;
	data.force = strength;
}

const float& BoidTarget::get_strength() const {
	return strength;
}

void BoidTarget::set_on_enter( Boid::ControlOnEnter p_enter ) {
	on_enter = p_enter;
	data.on_enter = p_enter;
}

Boid::ControlOnEnter BoidTarget::get_on_enter() const {
	return on_enter;
}

void BoidTarget::_bind_methods() {

	ClassDB::bind_method(D_METHOD("set_strength", "strength"), &BoidTarget::set_strength);
	ClassDB::bind_method(D_METHOD("get_strength"), &BoidTarget::get_strength);

	ClassDB::bind_method(D_METHOD("set_on_enter", "on_enter"), &BoidTarget::set_on_enter);
	ClassDB::bind_method(D_METHOD("get_on_enter"), &BoidTarget::get_on_enter);

	ADD_PROPERTY(
			PropertyInfo(Variant::REAL, "strength", PROPERTY_HINT_RANGE,
					"0,10,0.001,or_greater"), "set_strength", "get_strength");

	ADD_PROPERTY(
			PropertyInfo(Variant::INT, "on_enter", PROPERTY_HINT_ENUM,
					"nothing, free, stop, deactivate"), "set_on_enter",
			"get_on_enter");

}

BoidTarget::BoidTarget() {
	data.owner = this;
	data.type = Boid::ControlType::BCTRL_TYPE_TARGET;
	set_on_enter( Boid::ControlOnEnter::BCTRL_ONENTER_NOTHING );
	set_strength( 1 );
}

BoidTarget::~BoidTarget() {
}

