/*
 *
 *
 *  _________ ____  .-. _________/ ____ .-. ____
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'
 *
 *
 *  art & game engine
 *
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *
 *
 *  This file is part of boids module for godot engine
 *  For the latest info, see http://polymorph.cool/
 *
 *  Copyright (c) 2021 polymorph.cool
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 *  ___________________________________( ^3^)_____________________________________
 *
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *
 *
 */

#include "core/global_constants.h"
#include "register_types.h"
#include "boid.h"
#include "boid_server.h"
#include "boid_system.h"
#include "boid_material.h"
#include "boid_mesh.h"
#include "boid_control.h"
#include "boid_attractor.h"
#include "boid_collider.h"
#include "boid_source.h"
#include "boid_target.h"
#include "boid_action.h"
#include "boid_behaviour.h"
#include "boid_editor_plugin.h"

#ifdef TOOLS_ENABLED
#include "editor/editor_export.h"
#include "editor/editor_node.h"
static void editor_init_callback() {
	EditorNode::get_singleton()->add_resource_conversion_plugin(memnew(BoidMaterialConversionPlugin()));
}
#endif

void register_boids_types() {

#ifndef _3D_DISABLED

//	_global_constants.push_back(_GlobalConstant(m_custom_name, m_constant));
//	BIND_GLOBAL_ENUM_CONSTANT_CUSTOM(BoidBorderManagement::BORDER_NONE);

    ClassDB::register_class<Boid>();
    ClassDB::register_class<BoidServer>();
    ClassDB::register_class<BoidSystem>();
    ClassDB::register_class<BoidMaterial>();
    ClassDB::register_class<BoidMesh>();
    ClassDB::register_class<BoidControl>();
    ClassDB::register_class<BoidAttractor>();
    ClassDB::register_class<BoidCollider>();
    ClassDB::register_class<BoidSource>();
    ClassDB::register_class<BoidTarget>();
    ClassDB::register_class<BoidAction>();
    ClassDB::register_class<BoidBehaviour>();

    // starting the boid server
    BoidServer::get_singleton();

    #ifdef TOOLS_ENABLED
        EditorPlugins::add_by_type<BoidEditorPlugin>();
        EditorPlugins::add_by_type<BoidBehaviourEditorPlugin>();
    	EditorNode::add_init_callback(editor_init_callback);
    #endif

#endif

}

void unregister_boids_types() {

	// stopping the boid server
	BoidServer::kill();

}
